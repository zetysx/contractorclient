import axios from "axios";

const IMGUR_CLIENT_ID = "51218306b5ef8ae";

const API = axios.create({
    baseURL: "https://api.imgur.com/3/image",
    headers: {
        'Authorization': `Client-ID ${IMGUR_CLIENT_ID}`,
        'Accept': 'application/json',
        // 'Content-Type': 'multipart/form-data',
    }
});

export const apiUploadImg = (file) => {
    const formData = new FormData();
    formData.append("image", file);

    return API.post("", formData);
};

export const apiUploadImgBase64 = (dataURL) => {
    const replaceDataURL = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

    const payload = {
        image: replaceDataURL,
        type: "base64",
    };

    return API.post("", payload);
};