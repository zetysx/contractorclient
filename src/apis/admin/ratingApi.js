import { getAdminAPI } from "./index";

export const apiGetRating = (pageIndex) => {
    const API = getAdminAPI();

    return API.get(`rating?pageIndex=${pageIndex}`);
};
