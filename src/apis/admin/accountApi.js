import { getAdminAPI } from "./index";

export const apiGetListAllUserAccount = (pageIndex) => {
    const API = getAdminAPI();
    
    return API.get(`getallaccount?pageIndex=${pageIndex}`);
};

export const apiDisableAccount = (accountId) => {
    const API = getAdminAPI();

    return API.post(`/account/${accountId}/disable`);
};

export const apiSearchAccount = (searchText, pageIndex) => {
    const API = getAdminAPI();

    const payload = {
        ...searchText
    };

    return API.post(`searchaccount?pageIndex=${pageIndex}`, payload);
};