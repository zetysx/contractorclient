import { getAdminAPI } from "./index";


export const apiAdminGetAllContracts = (criterial) => {
    const API = getAdminAPI();
    const payload = {
        ...criterial
    }
    return API.post("contracts", payload);
    // return API.get(`contracts?pageIndex=${pageIndex}`);
};