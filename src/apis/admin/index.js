import axios from "axios";
import { getAccessToken } from "../../helpers/authHelper";
import { baseURL } from "../index";

export const getAdminAPI = () => {
    const accessToken = getAccessToken();
    return axios.create({
        baseURL: `${baseURL}/api/admin`,
        headers: {
            "Authorization": accessToken,
        }
    });
};