import { getAdminAPI } from "./index";

export const apiGetOverview = (accountId) => {
    const API = getAdminAPI();

    return API.get(`${accountId}/overview`);
};