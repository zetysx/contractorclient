import axios from "axios";
import { baseURL } from "../index";

const BASIC_CLIENT_ID = "Basic Y2Fwc3RvbmVrMTE6Z3JvdXAxNw==";
const OAUTH_API = axios.create({
    baseURL: `${baseURL}/oauth/token`,
    headers: {
        "Authorization": BASIC_CLIENT_ID,
        'Content-Type': 'application/x-www-form-urlencoded',
    }
});

export const apiGetAuthTokenAndLogin = (username, password) => {
    const requestBody = `grant_type=password&username=${username}&password=${password}`;
    return OAUTH_API.post("", requestBody);
};

export const apiRefreshToken = (refreshToken) => {
    const requestBody = `grant_type=refresh_token&refresh_token=${refreshToken}`;
    return OAUTH_API.post("", requestBody);
};