import axios from "axios";

const GOOGLE_MAPS_API_KEY = "AIzaSyBQReR1elwAmt2Ji6hof-XJhBNZwH1ELDE";
const baseURL = "https://maps.googleapis.com/maps/api/geocode/";

export const apiGetGeoCoding = (adress) => {
    return axios.get(`${baseURL}json?key=${GOOGLE_MAPS_API_KEY}&address=${adress}`);
};