import { getPrimeAPI } from "./index";

export const apiGetAllPrimeContract = (primeId) => {
    const API = getPrimeAPI();

    return API.get(`${primeId}/contracts`);
};

export const apiGetContractsPrimeDone = (primeId, pageIndex) => {
    const API = getPrimeAPI();

    return API.get(`/${primeId}/contracts/done?pageIndex=${pageIndex}`);
};

export const apiPrimeFillAllDocket = (dockets) => {
    const API = getPrimeAPI();

    const payload = dockets;

    return API.post("/dockets/fill", payload);
};

export const apiGetDocketsPrimeSent = (contractId) => {
    const API = getPrimeAPI();

    return API.post(`/contracts/${contractId}/dockets`);
};