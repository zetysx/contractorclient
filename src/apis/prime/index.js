import axios from "axios";
import { getAccessToken } from "../../helpers/authHelper";
import { baseURL } from "../index";

export const getPrimeAPI = () => {
    const accessToken = getAccessToken();
    return axios.create({
        baseURL: `${baseURL}/api/primecontractor`,
        headers: {
            "Authorization": accessToken,
        }
    });
};