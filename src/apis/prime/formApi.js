import { getPrimeAPI } from "./index";

export const apiGetAllFormByPrimeId = (primeId) => {
    const API = getPrimeAPI();

    return API.get(`${primeId}/forms`);
};

export const apiGetFormDetail = (formId) => {
    const API = getPrimeAPI();

    return API.get(`forms/${formId}`);
};

export const apiSaveForm = (formId, formName, formControls, authorId) => {
    const API = getPrimeAPI();

    const payload = {
        formId,
        formName,
        formControls,
        authorId,
    };

    return API.post("forms/save", payload);
};

export const apiAddFormToContract = (formId, contractId) => {
    const API = getPrimeAPI();

    return API.post(`contracts/${contractId}/forms/${formId}`);
};
