import { getPrimeAPI } from "./index";

export const apiSendRequest = (value) => {
    const API = getPrimeAPI();
    
    const payload = {
        ...value
    };

    return API.post("requests/create", payload);
};

export const apiGetPrimeRequestSentWaitting = (primeId) => {
    const API = getPrimeAPI();

    return API.get(`${primeId}/requests/waitting`);
};

export const apiPrimeRequestNotConfirmDelete = (primeId, requestId) => {
    const API = getPrimeAPI();
    
    return API.delete(`${primeId}/requests/${requestId}/delete`);
};

