import { getPrimeAPI } from "./index";

export const apiGetOverview = (primeId) => {
    const API = getPrimeAPI();
    
    return API.get(`${primeId}/overview`);
};