import { getPrimeAPI } from "./index";

export const apiGetAllPrimeContract = (primeId) => {
    const API = getPrimeAPI();

    return API.get(`${primeId}/contracts`);
};

export const apiGetContractsPrimeDone = (primeId, searchedCompanyName, pageIndex) => {
    const API = getPrimeAPI();

    return API.get(`/${primeId}/contracts/done?searchedCompanyName=${searchedCompanyName}&pageIndex=${pageIndex}`);
};

export const apiCancelContract = (contractId) => {
    const API = getPrimeAPI();

    return API.post(`contracts/${contractId}/cancel`);
};