import { getPrimeAPI } from "./index";

export const apiSearchSubcontractor = (searchValue, criteria) => {
    const API = getPrimeAPI();

    const payload = {
        ...searchValue,
        ...criteria
    };

    return API.post("search", payload);
};

export const apiAutoSearchSubcontractor = (searchValue, primeId) => {
    const API = getPrimeAPI();

    const payload = {
        ...searchValue,
    };

    return API.post(`${primeId}/autosearch`, payload);
};

export const apiGetAutoSearchTitle = (primeId) => {
    const API = getPrimeAPI();

    return API.get(`${primeId}/autosearch`);
};

export const apiGetAutoSearchContent = (autosearchId) => {
    const API = getPrimeAPI();

    return API.get(`autosearch/${autosearchId}`);
};

export const apiChangeAutoSearchStatus = (autosearchId) => {
    const API = getPrimeAPI();

    const payload = {
        ...autosearchId,
    };

    return API.post(`/autosearch/${autosearchId}`, payload);
};

export const apiDeleteAutoSearch = (autosearchId) => {
    const API = getPrimeAPI();

    return API.delete(`autosearch/${autosearchId}`);
};