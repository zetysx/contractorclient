import { getUserAPI } from "./index";

export const apiRegisterUser = (registerPayload) => {

    const API = getUserAPI();

    const payload = {
        ...registerPayload
    };

    return API.post("register", payload, {
        headers: {
            "Authorization": ""
        }
    });
};


