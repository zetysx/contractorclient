import { getUserAPI } from "./index";

export const apiGetAllDocketFromContractId = (contractId) => {
    const API = getUserAPI();

    return API.get(`contracts/${contractId}/dockets`);
};

export const apiConfirmDocket = (docketId) => {
    const API = getUserAPI();

    return API.post(`dockets/${docketId}/confirm`);
};

export const apiDeclineDocket = (docketId, reason) => {
    const API = getUserAPI();

    return API.post(`dockets/${docketId}/decline?declineReason=${reason}`);
};

export const apiApproveDocket = (docketId) => {
    const API = getUserAPI();

    return API.post(`dockets/${docketId}/approve`);
};

export const apiNeedApproveDocket = (docketId) => {
    const API = getUserAPI();

    return API.post(`dockets/${docketId}/needapprove`);
};

export const apiFormFilledDocket = (docketId) => {
    const API = getUserAPI();

    return API.post(`dockets/${docketId}/formFilled`);
};


export const apiRejectDocket = (docketId, reason) => {
    const API = getUserAPI();
    return API.post(`dockets/${docketId}/reject?rejectReason=${reason}`);
};

export const apiReturnDocketBySub = (docketId, reason) => {
    const API = getUserAPI();
    return API.post(`dockets/${docketId}/working?rejectReason=${reason}`);
};

export const apiGetDocketDetailById = (docketId) => {
    const API = getUserAPI();

    return API.get(`dockets/${docketId}`);
};

export const apiGetDocketDetail = (docketId) => {
    const API = getUserAPI();

    return API.get(`dockets/${docketId}`);
};

export const apiUpdateFormDataByDocketId = (docketId, formData) => {
    const API = getUserAPI();
    
    const payload = {
        data: JSON.stringify(formData)
    };
    return API.post(`dockets/${docketId}/form`, payload);
};