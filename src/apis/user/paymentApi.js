import { getUserAPI } from "./index";

export const apiGetContractPayment = (contractId) => {
    const API = getUserAPI();

    return API.get(`contracts/${contractId}/payment`);
};