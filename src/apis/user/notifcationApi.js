import { getUserAPI } from "./index";

export const apiGetNotifcationSmall = (accountId) => {
    const API = getUserAPI();

    return API.get(`${accountId}/notifications`);
};

export const apiGetAllNotifcationPaging = (accountId, page) => {
    const API = getUserAPI();

    return API.get(`${accountId}/notifications?p=${page}`);
};

export const apiSeenNotification = (notificationId) => {
    const API = getUserAPI();

    return API.post(`notification/${notificationId}`);
};

export const apiGetUnseenAmount = (accountId) => {
    const API = getUserAPI();

    return API.get(`${accountId}/notifications/countUnseen`);
};