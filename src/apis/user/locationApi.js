import { getUserAPI } from "./index";

export const apiGetAllLocation = () => {
    const API = getUserAPI();

    return API.get("locations", {
        headers: {
            "Authorization": ""
        }
    });
};