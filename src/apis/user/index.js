import axios from "axios";
import { getAccessToken } from "../../helpers/authHelper";
import { baseURL } from "../index";

export const getUserAPI = () => {
    const accessToken = getAccessToken();
    return axios.create({
        baseURL: `${baseURL}/api/user`,
        headers: {
            "Authorization": accessToken,
        }
    });
};