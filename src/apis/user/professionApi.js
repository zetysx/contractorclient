import { getUserAPI } from "./index";

export const apiGetAllProfession = () => {
    const API = getUserAPI();

    return API.get("professions", {
        headers: {
            "Authorization": ""
        }
    });
};


export const apiGetAllProfessionOfSub = (subId) => {
    const API = getUserAPI();

    return API.get(`/professions?subcontractorId=${subId}`);
};