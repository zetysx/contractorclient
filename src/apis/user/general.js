import { getUserAPI } from "./index";

export const apiGetUserGeneralInfo = () => {
    const API = getUserAPI();

    return API.get("generalInfo");
};

export const apiRemoveAuthTokenAndLogout = () => {
    const API = getUserAPI();

    return API.post("logout");
};

export const apiGetUserDetailInfo = (userId) => {
    const API = getUserAPI();

    return API.get(`/detail/${userId}`);
};