import { getUserAPI } from "./index";

export const apiRating = (ratingData) => {
    const API = getUserAPI();

    return API.post(`rating`,ratingData);
};

export const apiGetRatingList =(accountId, pageIndex)=>{
    const API = getUserAPI();

    return API.get(`${accountId}/rating?pageIndex=${pageIndex}`);
};
