// import { API } from "./index";
import { getUserAPI } from "./index";

export const apiRegisterUser = (registerPayload) => {
    const API = getUserAPI();
    const payload = {
        ...registerPayload
    };

    const headers = {
        params: {
        }
    };

    return API.post("register", payload, headers);
};


export const apiUpdateProfile = (updatePayload) => {
    const userAPI = getUserAPI();
    const payload = {
        ...updatePayload
    };

    return userAPI.post("updateProfile", payload);
}