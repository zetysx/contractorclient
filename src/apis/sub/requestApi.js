import { getSubAPI } from "./index";

export const apiGetAllRequest = (subId) => {
    const API = getSubAPI();

    return API.get(`${subId}/requests`);
};

export const apiDeclineRequest = (requestId) => {
    const API = getSubAPI();
    
    return API.post(`requests/${requestId}/decline`);
};