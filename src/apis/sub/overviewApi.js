import { getSubAPI } from "./index";

export const apiGetOverview = (subId) => {
    const API = getSubAPI();
    
    return API.get(`${subId}/overview`);
};