import axios from "axios";
import { getAccessToken } from "../../helpers/authHelper";
import { baseURL } from "../index";

export const getSubAPI = () => {
    const accessToken = getAccessToken();
    return axios.create({
        baseURL: `${baseURL}/api/subcontractor`,
        headers: {
            "Authorization": accessToken,
        }
    });
};