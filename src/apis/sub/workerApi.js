import { getSubAPI } from "./index";
import { getUserAPI } from "../user/index";

export const apiGetWorkerToAssign = (subId, contractId) => {
    const API = getSubAPI();

    return API.get(`${subId}/workers?contractId=${contractId}`);
};

export const apiAssignWorker = (assignWorkerPayload) => {
    const API = getSubAPI();

    const payload = {
        ...assignWorkerPayload
    };
    return API.post("workers/assign", payload);
};

export const apiSubSchedule = (input) => {
    const API = getSubAPI();

    const payload = {
        ...input
    };
    return API.post("schedule", payload);
};

export const apiBusyWorker = (input) => {
    const API = getSubAPI();

    const payload = {
        ...input
    };
    return API.post("busy", payload);
};

export const apiGetAllWorkersOfSub = (subId) => {
    const API = getSubAPI();

    return API.get(`${subId}/workers`);
};

export const apiGetAllWorkerForSubManagement = (subId) => {
    const API = getSubAPI();

    return API.get(`${subId}/workers/management`);
};

export const apiDisableWorkerBySub = (workerAccountId) => {
    const API = getSubAPI();

    // const dto = {
    //     workerAccountId: workerAccountId,
    //     subcontractorId: subcontractorId
    // }

    // const payload = {
    //     ...dto
    // };
    const payload = {
        workerAccountId 
    };

    //null payload
    // console.log("payload: ", payload);

    return API.post("disableWorker", payload);  
};

export const apiGetWorkerDetail = (subId, workerId) => {
    const API = getSubAPI();

    return API.get(`${subId}/workers/${workerId}/management`);
};

export const apiUpdateWorkerBySubcontractor = (workerPayload) => {
    const UserAPI = getUserAPI();

    const payload = {
        ...workerPayload
    };
    return UserAPI.post(`updateProfile`, payload);
}


export const apiGetProfessionOnContract = (workerId) => {
    const API = getSubAPI();
    return API.get(`workers/${workerId}/professions/working`);
}
