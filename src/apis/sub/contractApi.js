import { getSubAPI } from "./index";

export const apiGetAllSubContract = (subId) => {
    const API = getSubAPI();
    
    return API.get(`${subId}/contracts`);
};

export const apiGetAllSubContractDone = (subId, searchedCompanyName, pageIndex) => {
    const API = getSubAPI();

    return API.get(`${subId}/contracts/done?searchedCompanyName=${searchedCompanyName}&pageIndex=${pageIndex}`);
};

export const apiCancelContract = (contractId) => {
    const API = getSubAPI();

    return API.post(`contracts/${contractId}/cancel`);
};