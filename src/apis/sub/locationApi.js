import { getSubAPI } from "./index";


export const apiGetAllLocationsForUpdate = () => {
    const API = getSubAPI();
    return API.get("/locations");
}

export const apiUpdateLocationCapability = (locationIds) => {
    const API = getSubAPI();
    console.log("API CONSOLE: ", locationIds);
    const payload = {
        locationIds
    };
    console.log("payload: ", payload);
    return API.post("/locations/update", payload);
}