import { getSubAPI } from "./index";

export const apiGetAllProfessionForUpdate = () => {
    const API = getSubAPI();
    return API.get("/professions");
}


export const apiUpdateProfessionCapability = (subcontractorProfessions) => {
    const API = getSubAPI();
    const payload = {
        subcontractorProfessions
    };

    console.log("payload: ", payload);
    return API.post("/professions/update", payload);
}

