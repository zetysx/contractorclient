import React, { Component } from 'react';

import { apiUploadImgBase64 } from "../apis/imgur";

import SignatureCanvas from 'react-signature-canvas';
import {
    Icon,
    message,
    Button,
    Modal,
    Row
} from 'antd';

const SIGNATURE_WIDTH = 600;
const SIGNATURE_HEIGHT = 400;

export default class SignatureUploader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            imageUrl: props.imageUrl,
            isSignatureModalOpen: false,
        }
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            loading: false,
            imageUrl: nextProps.imageUrl
        });
    }

    renderModalFooterClearButton() {
        return (
            <Button key="clear" onClick={this._onClearSignature}>
                Xóa
            </Button>
        );
    };

    renderModalFooterCaptureButton() {
        return (
            <Button key="capture" type="primary" onClick={this._onCaptureSignature}>
                Xác nhận
            </Button>
        );
    };

    renderSignatureCaptureButton() {
        const styles = {
            img: {
                width: "100%",
            },
            textContainer: {
                textAlign: "center",
                // fontSize: 16,
                color: "#666"
            },
            iconStyle: {
                fontSize: 32,
                color: "#999"
            }
        };

        if (this.state.loading) {
            return (
                <div style={styles.textContainer}>
                    <Icon type='loading' style={styles.iconStyle} />
                    <div>Đang tải...</div>
                </div>
            );
        } else {
            if (!this.state.imageUrl) {
                return (
                    <div style={styles.textContainer}>
                        <Icon type='plus' style={styles.iconStyle} />
                        <div>Ký tên</div>
                    </div>
                );
            } else {
                return (
                    <img src={this.state.imageUrl} alt={this.state.imageUrl} style={styles.img} />
                );
            }

        }
    };

    render() {
        const styles = {
            signatureCapture: {
                width: 128,
                height: 128,
                border: "1px dashed #d9d9d9",
                borderRadius: "4px",
                backgroundColor: "#fafafa",
                cursor: "pointer",
                padding: 5,
            },
        };


        return (
            <div>
                <Row
                    type="flex" justify="center" align="middle"
                    style={styles.signatureCapture}
                    onClick={this._onClickOpenSignatureModal}
                >
                    {this.renderSignatureCaptureButton()}
                </Row>

                <Modal
                    visible={this.state.isSignatureModalOpen}
                    onCancel={this._onCloseSignatureModal}
                    width={SIGNATURE_WIDTH}
                    title="Ký tên"
                    bodyStyle={{ padding: 0 }}
                    footer={[this.renderModalFooterClearButton(), this.renderModalFooterCaptureButton()]}
                >
                    <SignatureCanvas
                        penColor='black'
                        canvasProps={{ width: SIGNATURE_WIDTH, height: SIGNATURE_HEIGHT, className: 'signatureCanvas' }}
                        backgroundColor={"#fff"}
                        ref={(ref) => { this.signatureCanvas = ref }}
                    />
                </Modal>
            </div>
        );
    };


    _onClickOpenSignatureModal = () => {
        this.setState({
            isSignatureModalOpen: true,
        });
    };

    _onCloseSignatureModal = () => {
        this.signatureCanvas.clear();
        this.setState({
            isSignatureModalOpen: false,
        });
    };

    _onCaptureSignature = async () => {
        const dataURL = await this.signatureCanvas.toDataURL("image/png", 1);

        const uploadAfterSetState = async () => {
            try {
                const response = await apiUploadImgBase64(dataURL);
                if (response && response.data) {
                    this.setState({
                        imageUrl: response.data.data.link,
                        loading: false,
                    }, () => {
                        if (this.props.onUploadDone) {
                            this.props.onUploadDone(response.data.data.link);
                        }
                    });
                } else {
                    alert("no respone");
                };
            } catch (error) {
                this.setState({
                    imageUrl: undefined,
                    loading: false,
                }, () => {
                    message.error('Gặp lỗi trong quá trình tải ảnh!');
                    if (this.props.onUploadError) {
                        this.props.onUploadError()
                    }
                });
            };
        };

        this.setState({
            loading: true,
            isSignatureModalOpen: false,
        }, () => uploadAfterSetState());
    };

    _onClearSignature = () => {
        this.signatureCanvas.clear()
    };
};
