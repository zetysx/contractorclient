import React, { Component } from 'react';

import { COLOR } from "../constants/theme";

import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer,
} from 'recharts';

import { Card } from 'antd';

export default class LineChartCustom extends Component {
    renderChartLengend(prop) {
        const { payload } = prop
        return (
            <ul style={styles.legend}>
                {payload.map((item, key) => (
                    <li key={key} style={styles.legendli}>
                        <span
                            style={{
                                ...styles.radiusdot,
                                background: item.color
                            }}
                        />
                        {item.value}
                    </li>
                ))}
            </ul>
        );
    };

    renderChartTooltip(prop) {
        const list = prop.payload.map((item, key) => (
            <li key={key} style={styles.tipitem}>
                <span
                    style={{
                        ...styles.radiusdot,
                        background: item.color
                    }}
                />
                <span style={styles.tiptitle}>{`${item.name} tháng ${item.payload.monthYear}:`}</span>
                {` ${new Intl.NumberFormat('en').format(item.value)}`}
            </li>
        ))
        return (
            <div style={styles.tooltip}>
                {/* <p style={styles.tiptitle}>{prop.label}</p> */}
                <ul style={styles.tooltiplist}>{list}</ul>
            </div>
        );
    };

    render() {
        const { data } = this.props;

        return (
            <div style={styles.sales}>
                <Card
                    bordered={false}
                    style={{ padding: 20 }}
                    bodyStyle={{
                        padding: '24px 36px 24px 0',
                    }}
                >
                    <div style={styles.title}>
                        {this.props.title}
                    </div>
                    <ResponsiveContainer minHeight={360}>
                        <LineChart data={data}>
                            <Legend
                                verticalAlign="top"
                                content={(prop) => this.renderChartLengend(prop)}
                            />
                            <XAxis
                                dataKey={this.props.xDataKey}
                                axisLine={{ stroke: COLOR.borderBase, strokeWidth: 1 }}
                                tickLine={false}
                            />
                            <YAxis axisLine={false} tickLine={false} allowDecimals={false} />
                            <CartesianGrid
                                vertical={false}
                                stroke={COLOR.borderBase}
                                strokeDasharray="3 3"
                            />
                            <Tooltip
                                wrapperStyle={{
                                    border: 'none',
                                    boxShadow: '4px 4px 40px rgba(0, 0, 0, 0.05)',
                                }}
                                content={(prop) => this.renderChartTooltip(prop)}
                            />
                            <Line
                                animationDuration={0} 
                                isAnimationActive={false}
                                type="monotone"
                                name={this.props.legends}
                                dataKey={this.props.yDataKey}
                                stroke={COLOR.purple}
                                strokeWidth={3}
                                dot={{ fill: COLOR.purple }}
                                activeDot={{ r: 5, strokeWidth: 0 }}
                            />
                            />
                    </LineChart>
                    </ResponsiveContainer>
                </Card>
            </div>
        );
    };
};

const styles = {
    sales: {
        overflow: "hidden",
    },
    title: {
        marginLeft: 32,
        fontSize: 16,
    },
    radiusdot: {
        width: 12,
        height: 12,
        marginRight: 8,
        borderRadius: "50%",
        display: "inline-block",
    },
    legend: {
        textAlign: "right",
        color: "#999",
        fontSize: 14,
    },
    legendli: {
        height: 48,
        //   line-height: 48px;
        display: "inline-block",
        marginLeft: 24,
    },
    tooltip: {
        background: "#fff",
        padding: 20,
        fontSize: 14,
    },
    tiptitle: {
        fontWeight: 700,
        fontSize: 14,
        marginBottom: 8,
    },
    tooltiplist: {
        listStyle: "none",
        padding: 0,
    },
    tipitem: {
        height: 32,
        // lineHeight: 32,
    }
}