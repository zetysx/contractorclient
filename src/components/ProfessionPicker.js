import React, { Component } from 'react';
import { connect } from 'react-redux';

import { loadAllProfessions } from "../actions/professionAction";

import { Select } from 'antd';
const Option = Select.Option;

class ProfessionPicker extends Component {
    componentDidMount() {
        this.props.loadAllProfessions();
    }

    renderOption() {
        return this.props.professionReducer.professions.map((profes) => {
            return (
                <Option key={profes.professionId} value={profes.professionId}>
                    {profes.professionName}
                </Option>
            );
        });
    }

    render() {
        return (
            <Select
                style={this.props.style}
                placeholder={this.props.placeholder}
                value={this.props.value}
                onChange={this.props.onChange}
                loading={this.props.professionReducer.isFetching}
            >
                {this.renderOption()}
            </Select>
        );
    }
}

function mapStateToProps(state) {
    return {
        professionReducer: state.professionReducer
    };
}

export default connect(
    mapStateToProps,
    { loadAllProfessions }
)(ProfessionPicker);