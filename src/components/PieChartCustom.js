import React, { Component } from 'react';

import { COLOR } from "../constants/theme";

import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    PieChart,
    Pie,
    Cell,
    Tooltip,
    Legend,
    ResponsiveContainer,
} from 'recharts';

import {
    Row,
    Col,
    Card,
} from 'antd';

export default class PieChartCustom extends Component {
    renderChartLengend(prop) {
        const { payload } = prop
        return (
            <ul style={styles.legend}>
                {payload.map((item, key) => (
                    <li key={key} style={styles.legendli}>
                        <span
                            style={{
                                ...styles.radiusdot,
                                background: item.color
                            }}
                        />
                        {item.value}
                    </li>
                ))}
            </ul>
        );
    };

    renderChartTooltip(prop) {
        const list = prop.payload.map((item, key) => (
            <li key={key} style={styles.tipitem}>
                <span
                    style={{
                        ...styles.radiusdot,
                        background: item.color
                    }}
                />
                {`${item.name}: ${new Intl.NumberFormat('en').format(item.value)}`}
            </li>
        ))
        return (
            <div style={styles.tooltip}>
                <p style={styles.tiptitle}>{prop.label}</p>
                <ul style={styles.tooltiplist}>{list}</ul>
            </div>
        );
    };

    render() {
        const { data } = this.props;

        return (
            <div style={styles.sales}>
                <Card
                    bordered={false}
                    bodyStyle={{
                        padding: '24px 36px 24px 0',
                    }}
                >
                    <div style={styles.title}>
                        {this.props.title}
                    </div>
                    <ResponsiveContainer minHeight={360}>
                        <PieChart>
                            <Tooltip
                                wrapperStyle={{
                                    border: 'none',
                                    boxShadow: '4px 4px 40px rgba(0, 0, 0, 0.05)',
                                }}
                                formatter={(value) => new Intl.NumberFormat('en').format(value)}
                                content={(data) => this.renderChartTooltip(data)}
                            />
                            <Legend
                                verticalAlign="top"
                                content={(data) => this.renderChartLengend(data)}
                            />
                            {/*moneySpent   professionName*/}
                            <Pie
                                animationDuration={0} 
                                isAnimationActive={false}
                                data={this.props.data}
                                dataKey={this.props.value}
                                nameKey={this.props.keyData}
                                label={(data)=> new Intl.NumberFormat('en').format(data.value)}
                            >
                                {
                                    this.props.data.map((entry, index) => (
                                        <Cell key={`cell-${index}`} fill={this.props.colors[index]} />
                                    ))
                                }
                            </Pie>
                        </PieChart>
                    </ResponsiveContainer>

                </Card>
            </div>
        );
    };
};

const styles = {
    sales: {
        overflow: "hidden",
    },
    title: {
        marginLeft: 32,
        fontSize: 16,
    },
    radiusdot: {
        width: 12,
        height: 12,
        marginRight: 8,
        borderRadius: "50%",
        display: "inline-block",
    },
    legend: {
        textAlign: "right",
        color: "#999",
        fontSize: 14,
    },
    legendli: {
        height: 48,
        //   line-height: 48px;
        display: "inline-block",
        marginLeft: 24,
    },
    tooltip: {
        background: "#fff",
        padding: 20,
        fontSize: 14,
    },
    tiptitle: {
        fontWeight: 700,
        fontSize: 16,
        marginBottom: 8,
    },
    tooltiplist: {
        listStyle: "none",
        padding: 0,
    },
    tipitem: {
        height: 32,
        // lineHeight: 32,
    }
}

class CustomizedLabel extends Component {
    render() {
        const { x, y, fill, value } = this.props;
        return <text
            x={x}
            y={y}
            fontSize='12'
            fontFamily='sans-serif'
            fill={fill}
            textAnchor="middle">{value} VNĐ
        </text>
    }
}