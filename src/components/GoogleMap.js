import { GoogleMap, Marker, withGoogleMap } from "react-google-maps";
import React, { Component } from 'react';
import { apiGetGeoCoding } from "../apis/google/index";


export class GoogleMapComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            locationMap: undefined
        };
    };

    componentDidMount = async () => {
        if (this.props.locationDetail) {
            try {
                const response = await apiGetGeoCoding(this.props.locationDetail);
                if (response && response.data) {
                    const results = response.data.results;
                    if (results) {
                        const location = results[0].geometry.location;
                        this.setState({ locationMap: location });
                    }

                }
            } catch (ex) {
                console.log("error in map: ",ex);
            }

        }
    }

    render() {
        if (!this.state.locationMap) {
            return null;
        }

        const MyMap = withGoogleMap(props =>
            <GoogleMap
                defaultZoom={18}
                defaultCenter={{ lat: this.state.locationMap.lat, lng: this.state.locationMap.lng }}
            >
                <Marker position={{ lat: this.state.locationMap.lat, lng: this.state.locationMap.lng }} />
            </GoogleMap>
        );

        return (
            <MyMap
                containerElement={<div style={{ height: `400px` }} />}
                mapElement={<div style={{ height: `100%` }} />}
            />
        );
    }
}
