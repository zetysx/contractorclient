import React, { Component } from 'react';

import CountUp from 'react-countup';
import {
    Icon,
    Card,
    Row,
    Col
} from 'antd';

export default class NumberCard extends Component {
    render() {
        const { icon, color, title, number, countUp } = this.props;
        if (this.props.role === "admin")
            return (
                <Card
                    style={stylesForAdmin.numberCard}
                    bordered={false}
                    bodyStyle={{ padding: 10 }}
                >
                    <Icon
                        style={{
                            ...stylesForAdmin.iconWarp,
                            color
                        }}
                        type={icon}
                    />
                    <div style={stylesForAdmin.content}>
                        <p style={stylesForAdmin.title}>
                            {title || 'No Title'}
                        </p>
                        <p style={stylesForAdmin.number}>
                            <CountUp
                                start={0}
                                end={number}
                                duration={2.75}
                                useEasing
                                useGrouping
                                separator=","
                                {...countUp || {}}
                            />
                        </p>
                    </div>
                </Card>
            );

        return (
            <Card
                style={styles.numberCard}
                bordered={false}
                bodyStyle={{ padding: 10 }}
            >
                <Row type="flex" justify="space-between" align="middle">
                    <Col span={8}>
                        <Icon
                            style={{
                                ...styles.iconWarp,
                                color
                            }}
                            type={icon}
                        />
                    </Col>
                    <Col span={16}>
                        <div style={styles.title}>
                            {title || 'No Title'}
                        </div>
                    </Col>
                </Row>
                <div style={styles.number}>
                    <CountUp
                        start={0}
                        end={number}
                        duration={2.75}
                        useEasing
                        useGrouping
                        separator=","
                        {...countUp || {}}
                    />
                </div>
            </Card>
        );

    };
};

const stylesForAdmin = {
    numberCard: {
        padding: 32,
        marginBottom: 24,
    },
    iconWarp: {
        fontSize: 54,
        float: "left"
    },
    content: {
        width: "100%",
        paddingLeft: 78,
    },
    title: {
        // lineHeight: 16,
        fontSize: 16,
        marginBottom: 8,
        height: 16,
        // whiteSpace: "nowrap",
        // textOverflow: "ellipsis",
        // overflow: "hidden",
    },
    number: {
        // lineHeight: 32,
        fontSize: 24,
        height: 32,
        marginBottom: 0,
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        overflow: "hidden",
    },
};

const styles = {
    numberCard: {
        padding: 12,
        width: "100%",
        height: "100%",
    },
    iconWarp: {
        fontSize: 54,
        float: "left"
    },
    title: {
        width: "100%",
        fontSize: 16,
        position: "relative",
        left: "10px",
    },
    number: {
        fontSize: "24px",
        overflow: "hidden",
        width: "100%",
        padding: "10px",
        top: "15px",
        position: "relative",
        textAlign: "center"
    },
};