import React, {Component} from 'react';
import {connect} from 'react-redux';
import {loadContractPayment} from "../actions/viewContractPaymentAction";

// ant
import {Row, Col, Icon, Skeleton} from 'antd';
import {Link} from "react-router-dom";

class ViewWorkerList extends Component {


    renderWorkerList = (workerDTOS) => {
        return workerDTOS.map((worker) => {
            return (
                <div
                    key={`div${worker.workerId}`}>
                    <Row
                        key={`row${worker.workerId}`}
                        type="flex" className={"rowWorker"}
                        align="middle" justify="start"
                        style={{marginBottom: "1px", background: "rgb(255, 255, 255)", color: "rgba(0, 0, 0, 0.65)"}}>
                        {((worker.avatar) ?
                            <img src={worker.avatar} alt={"Avatar worker"} width={"50px"} height={"50px"}/> :
                            <Row type="flex" justify="space-around" align="middle"
                                 style={{height: "50px", width: "50px"}}>
                                <Icon style={{
                                    fontSize: "30px",
                                    color: "#d4d4d4",
                                }} type="user"/></Row>)}
                        <Col span={17} style={{paddingLeft: "10px"}}>
                            <Link
                                to={`/profile/${worker.workerAccId}`}>{worker.username}</Link>
                        </Col>
                    </Row>
                </div>
            )

        })
    }

    render() {
        if (this.props.workerDTOS) {
            return (
                <div style={{margin: "-24px"}}>
                    {this.renderWorkerList(this.props.workerDTOS)}
                </div>
            );
        }
        return <Skeleton/>
    }
}

function mapStateToProps(state) {
    return {}
}

const mapDispatchActionToProps = {
    loadContractPayment
}

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(ViewWorkerList);