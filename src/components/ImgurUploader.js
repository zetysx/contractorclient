import React, { Component } from 'react';

import { apiUploadImg } from "../apis/imgur";

import {
    Upload,
    Icon,
    message
} from 'antd';

export default class ImgurUploader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            imageUrl: props.imageUrl
        }
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            loading: false,
            imageUrl: nextProps.imageUrl
        });
    }

    renderUploadButton() {
        const styles = {
            img: {
                width: "100%",
            }
        };

        if (this.state.loading) {
            return (
                <div>
                    <Icon type='loading' />
                    <div>Đang tải...</div>
                </div>
            );
        } else {
            if (!this.state.imageUrl) {
                return (
                    <div>
                        <Icon type='plus' />
                        <div>Chọn hình</div>
                    </div>
                );
            } else {
                return (
                    <img src={this.state.imageUrl} alt={this.state.imageUrl} style={styles.img} />
                );
            }

        }
    }

    render() {
        return (
            <Upload
                listType="picture-card"
                showUploadList={false}
                customRequest={(customRequest) => this._onRequestUploadImg(customRequest)}
                onChange={(info) => this._onChangeUploadImg(info)}
                beforeUpload={(file) => this._onBeforeUploadFile(file)}
            >
                {this.renderUploadButton()}
            </Upload>
        );
    };

    _onRequestUploadImg = async (customRequest) => {
        const { file, onSuccess, onError } = customRequest;

        try {
            const response = await apiUploadImg(file);
            if (response && response.data) {
                onSuccess(response.data, file);
            } else {
                alert("no respone");
            };
        } catch (error) {
            onError(error, file);
        };
    };

    _onChangeUploadImg = (info) => {
        if (info.file.status === 'uploading') {
            this.setState({ loading: true });
            return;
        }
        if (info.file.status === 'done') {
            this.setState({
                imageUrl: info.file.response.data.link,
                loading: false,
            }, () => {
                if (this.props.onUploadDone) {
                    this.props.onUploadDone(info.file.response.data.link);
                }
            });
        }
        if (info.file.status === 'error') {
            this.setState({
                imageUrl: undefined,
                loading: false,
            }, () => {
                message.error('Gặp lỗi trong quá trình tải ảnh!');
                if (this.props.onUploadError) {
                    this.props.onUploadError()
                }
            });
        }
    };

    _onBeforeUploadFile = (file) => {
        const isJPG = file.type === "image/jpeg";
        const isPNG = file.type === "image/png";

        const isIMG = isJPG || isPNG;

        if (!isJPG && !isPNG) {
            message.error('Chỉ nhận tệp PNG và JPG!');
        }

        const isLt6M = file.size / 1024 / 1024 < 6;
        if (!isLt6M) {
            message.error('Tệp phải nhỏ hơn 6MB!');
        }

        return isIMG && isLt6M;
    };
};