import React, { Component } from 'react';

import { COLOR } from "../constants/theme";

import {
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer,
    BarChart,
    Bar
} from 'recharts';

import { Card } from 'antd';

export default class BarChartCustom extends Component {
    renderChartLengend(prop) {
        const { payload } = prop
        return (
            <ul style={styles.legend}>
                {payload.map((item, key) => (
                    <li key={key} style={styles.legendli}>
                        <span
                            style={{
                                ...styles.radiusdot,
                                background: item.color
                            }}
                        />
                        {item.value}
                    </li>
                ))}
            </ul>
        );
    };

    renderChartTooltip(prop) {
        const list = prop.payload.map((item, key) => {
            var name;
            if(item.name[0].indexOf("Doanh thu")!==-1) {
                name = "Doanh thu tháng "+ item.payload.monthYear;
            } else if(item.name[0].indexOf("Chi tiêu")!==-1) {
                name = "Chi tiêu tháng "+ item.payload.monthYear;
            } else name = item.name;
            
            return (
                <li key={key} style={styles.tipitem}>
                    <span
                        style={{
                            ...styles.radiusdot,
                            background: item.color
                        }}
                    />
                    <span style={styles.tiptitle}>{`${name} :`}</span>
                    {` ${new Intl.NumberFormat('en').format(item.value)} VNĐ`}
                </li>
            );
        })
        return (
            <div style={styles.tooltip}>
                {/* <p style={styles.tiptitle}>{prop.label}</p> */}
                <ul style={styles.tooltiplist}>{list}</ul>
            </div>
        );
    };

    render() {
        const { data } = this.props;

        return (
            <div style={styles.sales}>
                <Card
                    bordered={false}
                    style={{ padding: 20 }}
                    bodyStyle={{
                        padding: '24px 36px 24px 0',
                    }}
                >
                    <div style={styles.title}>
                        {this.props.title}
                    </div>
                    <ResponsiveContainer minHeight={360}>
                        <BarChart data={data}>
                            <Legend
                                verticalAlign="top"
                                content={(prop) => this.renderChartLengend(prop)}
                            />
                            <XAxis
                                dataKey={this.props.xDataKey}
                                axisLine={{ stroke: COLOR.borderBase, strokeWidth: 1 }}
                                tickLine={false}
                            />
                            <YAxis
                                tickFormatter={(value) => new Intl.NumberFormat('en').format(value)}
                                axisLine={false}
                                tickLine={false} />
                            <CartesianGrid
                                vertical={false}
                                stroke={COLOR.borderBase}
                                strokeDasharray="3 3"
                            />
                            <Tooltip
                                wrapperStyle={{
                                    border: 'none',
                                    boxShadow: '4px 4px 40px rgba(0, 0, 0, 0.05)',
                                }}
                                // formatter={(value) => {console.log('ssssssssss') return new Intl.NumberFormat('en').format(value)}}
                                content={(prop) => this.renderChartTooltip(prop)}
                            />
                            <Bar
                                animationDuration={0}
                                isAnimationActive={false}
                                type="monotone"
                                name={this.props.legends}
                                dataKey={this.props.yDataKey}
                                label={(data) => (data.value>0)? new Intl.NumberFormat('en').format(data.value):null}
                                fill={COLOR.green}
                            />
                            />
                    </BarChart>
                    </ResponsiveContainer>
                </Card>
            </div>
        );
    };
};

export const styles = {
    sales: {
        overflow: "hidden",
    },
    title: {
        marginLeft: 32,
        fontSize: 16,
    },
    radiusdot: {
        width: 12,
        height: 12,
        marginRight: 8,
        borderRadius: "50%",
        display: "inline-block",
    },
    legend: {
        textAlign: "right",
        color: "#999",
        fontSize: 14,
    },
    legendli: {
        height: 48,
        //   line-height: 48px;
        display: "inline-block",
        marginLeft: 24,
    },
    tooltip: {
        background: "#fff",
        padding: 20,
        fontSize: 14,
    },
    tiptitle: {
        fontWeight: 700,
        fontSize: 14,
        marginBottom: 8,
    },
    tooltiplist: {
        listStyle: "none",
        padding: 0,
    },
    tipitem: {
        height: 32,
        // lineHeight: 32,
    }
}