import React, { Component } from 'react';
import { connect } from 'react-redux';

import { loadAllLocations } from "../actions/locationAction";

import { Select } from 'antd';
const Option = Select.Option;

class LocationPicker extends Component {
    componentDidMount() {
        this.props.loadAllLocations();
    }

    renderOption() {
        return this.props.locationReducer.locations.map((location) => {
            return (
                <Option key={location.locationId} value={location.locationId}>
                    {`${location.district}, ${location.city}`}
                </Option>
            );
        });
    }

    render() {
        return (
            <Select
                style={this.props.style}
                placeholder={this.props.placeholder}
                value={this.props.value}
                onChange={this.props.onChange}
                loading={this.props.locationReducer.isFetching}
            >
                {this.renderOption()}
            </Select>
        );
    }
}

function mapStateToProps(state) {
    return {
        locationReducer: state.locationReducer
    };
}

export default connect(
    mapStateToProps,
    { loadAllLocations }
)(LocationPicker);