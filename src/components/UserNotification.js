import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { Client } from '@stomp/stompjs';


import { Row, Col, Popover, Icon, Badge, Spin, } from "antd";
import { Scrollbars } from 'react-custom-scrollbars';
import { Link } from "react-router-dom";

import { loadUserNotification, seenNotification, getUnseenNotiAmount, setLocalUnseenNoti } from "../actions/notificationAction"
import { getAutoSearchTitle, } from "../actions/primeSearch/autoSearchAction";
import { loadAllDocketByContractId } from "../actions/docketListByContractIdAction";
import { ROLESID } from '../constants/role';

class UserNotification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            updateNotiInterval: null,
            notifications: [],
            unseen: 0,
            initLoading: true
        };
    }

    componentDidMount() {
        this.client = new Client();
        this.client.configure({
            brokerURL: 'ws://35.185.179.150:8090/ws',
            onConnect: () => {
                this.client.subscribe('/data/' + this.props.profile.accountId, message => {
                    const data = JSON.parse(message.body);
                    this.props.setLocalUnseenNoti(data.unseen);
                    // this.setState({ unseen: data.unseen });
                    this.forceUpdate();
                    if (data.contractId) {
                        switch (this.props.roleId) {
                            case ROLESID.PRIME:

                        }
                        this.props.loadAllDocketByContractId(data.contractId);
                    }
                    if (data.autosearchId) {
                        this.props.getAutoSearchTitle(this.props.profile.userId);
                    }
                });
            },

        });

        this.client.activate();
    }

    componentWillMount() {
        if (this.props.profile.unseenNoti) {
            const _onCallback = () => {
                this.props.setLocalUnseenNoti(this.props.notificationReducer.unseen);
            }
            this.props.getUnseenNotiAmount(this.props.profile.accountId, _onCallback);

        }
    }

    componentWillUnmount() {
        this.client.deactivate();
    }

    _onClickGetNotification = async () => {
        const callback = () => {
            const { notifications } = this.props.notificationReducer;
            this.setState({ notifications: notifications });
            this.setState({ initLoading: false });
        }
        //await
        this.props.loadUserNotification(this.props.profile.accountId, callback);
    }

    _onClickSeenNotification = (notificationId, index) => {
        const newNotficationList = this.state.notifications;
        if (newNotficationList[index].seen) {
            return;
        }
        const oldLocalUnseen = this.props.notificationReducer.localUnseen;
        this.props.setLocalUnseenNoti(oldLocalUnseen - 1);

        newNotficationList[index].seen = true;
        this.setState({ notifications: newNotficationList });
        this.props.seenNotification(notificationId);
    }

    renderNoNotification = () => {
        return (
            <div
                style={{
                    width: '350px',
                    height: '150px',
                    textAlign: 'center',
                    fontWeight: 'bolder',
                    fontSize: '25px',
                    //display: this.props.notificationReducer.isFetching? "none" : "block"
                   // paddingTop: '70px'
                }}
            >
                Bạn không có thông báo nào.
            </div>);
    }

    renderNotificationList = () => {
        return (<div>
            <Scrollbars autoHide style={{ width: '350px', height: '400px' }}>
                <Col >
                    {
                        this.props.notificationReducer.notifications.map((notification, key) => {
                            const seen = notification.seen;
                            const now = moment().format('DD/MM/YYYY HH:mm');
                            var ms = moment(now, "DD/MM/YYYY HH:mm").diff(moment(notification.createdTime, "DD/MM/YYYY HH:mm"));
                            var d = moment.duration(ms);
                            var timePassed = null;
                            if (Math.floor(d.asDays()) > 0) timePassed = Math.floor(d.asDays()) + " ngày trước";
                            else timePassed = Math.floor(d.asHours()) + moment.utc(ms).format(":mm") + " giờ trước";
                            const notificationId = notification.notificationId;
                            var link = null;

                            switch (notification.type) {
                                case "view autosearch":
                                    link = "/autosearch";
                                    break;
                                case "view request":
                                    link = "/request";
                                    break;
                                case "view docket":
                                    link = "/docket";
                                    break;
                                case "view contract":
                                    link = "/docket";
                                    break;
                                default:
                                    break;
                            }

                            return (
                                <Link to={link} key={key} style={{ padding: '0' }}>
                                    <Row
                                        key={key}
                                        style={{
                                            boxSizing: "border-box",
                                            padding: "0 !important",
                                            background: seen ?  "#fff" : "#edf2fa",
                                            paddingTop: "10px",
                                            overflow: "hidden",
                                            borderBottom: '2px solid #dddfe2',
                                            width: '350px',
                                            height: '80px'
                                        }}
                                        onClick={() => this._onClickSeenNotification(notificationId, key)}
                                    >
                                        <div style={{ fontSize: '15px', color: 'black', height: '45px', overflow: 'hidden' }}>{notification.messsage}</div>
                                        <span style={{ fontStyle: 'italic', fontSize: '10', alignSelf: 'flex-end', color: '#90949c' }}>{timePassed}</span>
                                    </Row>
                                </Link>
                            );
                        })}
                </Col>
            </Scrollbars>
            <Row style={{
                height: '25px',
                textAlign: 'center',
                fontWeight: 'bolder',
                fontSize: '20px',
                position: 'static'
            }}>
                <Link to="/notification">
                    Xem tất cả
                    </Link>
            </Row>
        </div>);
    }

    renderNotificationPopover() {
        // console.log('loggg',this.props.notificationReducer);
        return (this.props.notificationReducer.notifications.length != 0 && !this.props.notificationReducer.isFetching) ? (
            <Spin
                spinning={this.props.notificationReducer.isFetching}
                indicator={<Icon type="loading" style={{ fontSize: '60px', textAlign: 'center', alignSelf: 'center', margin: '20px' }} />}
                style={{ width: '350px', height: '100px' }} >
                {this.renderNotificationList()}
            </Spin>
        ) : (
                <Spin
                    spinning={this.props.notificationReducer.isFetching}
                    indicator={<Icon type="loading" style={{ fontSize: '60px', textAlign: 'center', alignSelf: 'center', margin: '20px' }} />}
                    style={{ width: '350px', height: '100px' }} >
                    {this.renderNoNotification()}
                </Spin>
            );
    }

    render() {
        if (!this.props.profile.accountId) {
            return null;
        }
        return (
            <Popover
                placement="bottomRight"
                trigger="click"
                content={this.renderNotificationPopover()}
                onClick={() => this._onClickGetNotification()}
                overlayStyle={{ padding: '0', background: 'red' }}
            >
                <Row>
                    <Badge count={this.props.notificationReducer.localUnseen}>
                        <Icon type="bell" theme="filled" style={{ fontSize: '18px' }} />
                    </Badge>
                </Row>
            </Popover>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.authReducer.profile,
        notificationReducer: state.notificationReducer,
        roleId: state.authReducer.profile.roleId
    };
};
// tách mapsAction ra một biến riêng
const mapDispatchActionToProps = {
    loadUserNotification,
    seenNotification,
    getUnseenNotiAmount,
    loadAllDocketByContractId,
    getAutoSearchTitle,
    setLocalUnseenNoti
};


export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(UserNotification);