import React, {Component} from 'react';
import {connect} from 'react-redux';

import {Button} from "antd";

class ButtonUpdateProfile extends Component {


    render() {
        if(this.props._onEditClicked) {
            return (
                <Button
                    type="primary"
                    size="default"
                    style={{ width: 130, textAlign: "center" }}
                    icon="edit"
                    onClick={() => this.props._onEditClicked()}>
                    Cập nhật
                </Button>
            )
        } else {
            return null;
        }
    }
}

function mapStateToProps(state) {
    return {};
}

export default connect(
    mapStateToProps,
    {}
)(ButtonUpdateProfile);