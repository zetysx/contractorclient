import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loadContractPayment } from "../actions/viewContractPaymentAction";

// ant
import { Row, Col, } from 'antd';

export class ViewContractPayment extends Component {


    render() {
        if (!this.props.contractPayment) return null;
        const contract = this.props.contractPayment;
        return (
            <Col>
                <Row type="flex" justify="space-between" align="middle" style={{
                    fontSize: "16px",
                    paddingBottom: "20px",
                    fontWeight: "bold",
                    borderBottom: "1px solid #aaa",
                    color: "#b76500",
                }}>
                    <Col span={3}>Ngày công</Col>
                    <Col span={11}>Chi tiết công việc</Col>
                    <Col span={3} style={{ fontSize: "10px" }}>Lương từng nhân công</Col>
                    <Col span={2} style={{ fontSize: "10px" }}>X    Số nhân công</Col>
                    <Col span={1} style={{ textAlign: "center", fontSize: "10px" }}>=</Col>
                    <Col span={4} style={{ fontSize: "10px" }}>Tổng tiền ngày công</Col>
                </Row>
                <Row>
                    {contract.docketPayments.map((docket, index) => {
                        return (docket.docketPriceToal) ? (
                            <Row
                                key={`${docket.docketId}${index}`}
                                type="flex" justify="space-between" align="middle" style={{ padding: "10px 0", borderBottom: "1px solid #f1f1f1" }}>
                                <Col span={3}>{docket.dateDocket}</Col>
                                <Col span={11} style={{ paddingRight: "25px", color:"#000" }}>{docket.docketDetail}</Col>
                                <Col span={3}>
                                    <div>{`${parseInt(docket.docketPricePerWorker / 1000)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}<span
                                        style={{
                                            fontWeight: "normal",
                                            margin: "0 1px",
                                        }}>,</span><span
                                            style={{
                                                fontWeight: "normal",

                                            }}>000</span>
                                    </div>
                                </Col>
                                <Col span={1} style={{ fontSize: "10px" }}>X</Col>
                                <Col span={1}>{docket.workerNumber}</Col>
                                <Col span={1} style={{ textAlign: "center" }}>=</Col>
                                <Col span={4}>
                                    <div>{`${parseInt(docket.docketPriceToal / 1000)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}<span
                                        style={{
                                            fontWeight: "normal",
                                            margin: "0 1px",
                                        }}>,</span><span
                                            style={{
                                                fontWeight: "normal",

                                            }}>000</span><sup
                                                style={{
                                                    fontWeight: "normal",
                                                    fontSize: "10px",
                                                    right: "-5px",
                                                    color: "#ad8700",
                                                }}>đ</sup></div>

                                </Col>
                            </Row>
                        ) : "";
                    })}
                </Row>
                <Row type="flex" justify="space-between" align="middle" style={{
                    fontSize: "18px",
                    padding: "16px",
                    color: "#b76500",
                }}>
                    <Col offset={14} span={6}>Tổng tiền: </Col>
                    <Col span={4}>
                        <div>{(
                            ((parseInt(contract.totalPrice)) !== 0) ?
                                (<div>{`${parseInt(contract.totalPrice / 1000)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                    <span
                                        style={{
                                            fontWeight: "normal",
                                            margin: "0 1px",
                                        }}>,</span><span
                                            style={{
                                                fontWeight: "normal",

                                            }}>000</span><sup
                                                style={{
                                                    fontWeight: "normal",
                                                    fontSize: "10px",
                                                    right: "-5px",
                                                    color: "#ad8700",
                                                }}>đ</sup></div>
                                )
                                :
                                <div style={{
                                    fontSize: "12px",
                                    color: "black",
                                    fontStyle: "italic",
                                }}>Chưa có công việc nào hoàn thành</div>
                        )}</div>
                    </Col>
                </Row>
            </Col>
        );
    }
}

function mapStateToProps(state) {
    return {
        contractPayment: state.viewContractPaymentReducer.contractPayment,
    };
}

const mapDispatchActionToProps = {
    loadContractPayment
}

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(ViewContractPayment);