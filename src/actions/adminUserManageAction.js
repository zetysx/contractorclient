import {apiDisableAccount, apiGetListAllUserAccount, apiSearchAccount} from "../apis/admin/accountApi";
import { catchRequestCommonError } from "../helpers/actionHelper";

export const ADMINUSERMANAGE_GET_LIST_ALL_USER_ACCOUNT_LOADING = "ADMINUSERMANAGE_GET_LIST_ALL_USER_ACCOUNT_LOADING";
export const ADMINUSERMANAGE_GET_LIST_ALL_USER_ACCOUNT_SUCCESS = "ADMINUSERMANAGE_GET_LIST_ALL_USER_ACCOUNT_SUCCESS";
export const ADMINUSERMANAGE_GET_LIST_ALL_USER_ACCOUNT_FAIL = "ADMINUSERMANAGE_GET_LIST_ALL_USER_ACCOUNT_FAIL";

export const ADMINUSERMANAGE_RATING_LOADING = "ADMINUSERMANAGE_RATING_LOADING";
export const ADMINUSERMANAGE_RATING_SUCCESS = "ADMINUSERMANAGE_RATING_SUCCESS";
export const ADMINUSERMANAGE_RATING_FAIL = "ADMINUSERMANAGE_RATING_FAIL";

export const disableAccount = (accountId, onSuccessCallback) => {
    return async (dispatch, getState) => {
        dispatch({
            type: ADMINUSERMANAGE_RATING_LOADING
        });

        try {
            const response = await apiDisableAccount(accountId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: ADMINUSERMANAGE_RATING_SUCCESS,
                    });
                }
                if (data.code === 200) {
                    if (onSuccessCallback) {
                        onSuccessCallback();
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: ADMINUSERMANAGE_RATING_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};

export const getListAllUserAccount = (pageIndex) => {
    return async (dispatch, getState) => {
        dispatch({
            type: ADMINUSERMANAGE_GET_LIST_ALL_USER_ACCOUNT_LOADING
        });

        try {
            const response = await apiGetListAllUserAccount(pageIndex);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: ADMINUSERMANAGE_GET_LIST_ALL_USER_ACCOUNT_SUCCESS,
                        allUserAccount: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: ADMINUSERMANAGE_GET_LIST_ALL_USER_ACCOUNT_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};

export const searchAccount = (searchText, pageIndex) => {
    return async (dispatch, getState) => {
        dispatch({
            type: ADMINUSERMANAGE_GET_LIST_ALL_USER_ACCOUNT_LOADING
        });

        try {
            const response = await apiSearchAccount(searchText, pageIndex);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: ADMINUSERMANAGE_GET_LIST_ALL_USER_ACCOUNT_SUCCESS,
                        allUserAccount: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: ADMINUSERMANAGE_GET_LIST_ALL_USER_ACCOUNT_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};
