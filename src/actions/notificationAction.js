
import {
    apiGetNotifcationSmall,
    apiSeenNotification,
    apiGetUnseenAmount,
    apiGetAllNotifcationPaging
} from "../apis/user/notifcationApi";
import { catchRequestCommonError } from "../helpers/actionHelper";

export const GET_NOTIFICATION_LOADING = "GET_NOTIFICATION_LOADING";
export const GET_NOTIFICATION_SUCCESS = "GET_NOTIFICATION_SUCCESS";
export const GET_NOTIFICATION_FAIL = "GET_NOTIFICATION_FAIL";

export const SEEN_NOTIFICATION_LOADING = "SEEN_NOTIFICATION_LOADING";
export const SEEN_NOTIFICATION_SUCCESS = "SEEN_NOTIFICATION_SUCCESS";
export const SEEN_NOTIFICATION_FAIL = "SEEN_NOTIFICATION_FAIL";

export const GET_UNSEEN_NOTI_AMOUNT_LOADING = "GET_UNSEEN_NOTI_AMOUNT_LOADING";
export const GET_UNSEEN_NOTI_AMOUNT_SUCCESS = "GET_UNSEEN_NOTI_AMOUNT_SUCCESS";
export const GET_UNSEEN_NOTI_AMOUNT_FAIL = "GET_UNSEEN_NOTI_AMOUNT_FAIL";

export const GET_NOTIFICATION_PAGE_LOADING = "GET_NOTIFICATION_PAGE_LOADING";
export const GET_NOTIFICATION_PAGE_SUCCESS = "GET_NOTIFICATION_PAGE_SUCCESS";
export const GET_NOTIFICATION_PAGE_FAIL = "GET_NOTIFICATION_PAGE_FAIL";

export const SET_LOCAL_UNSEEN_NOTI_SUCCESS = "SET_LOCAL_UNSEEN_NOTI_SUCCESS";

export const loadUserNotification = (accountId, callback) => {
    return async (dispatch) => {
        dispatch({
            type: GET_NOTIFICATION_LOADING
        });
        try {
            const response = await apiGetNotifcationSmall(accountId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: GET_NOTIFICATION_SUCCESS,
                        notifications: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: GET_NOTIFICATION_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }
        if (callback) {
            callback();
        }
    };
};

export const seenNotification = (notificationId) => {
    return async (dispatch) => {
        dispatch({
            type: SEEN_NOTIFICATION_LOADING
        });
        try {
            const response = await apiSeenNotification(notificationId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: SEEN_NOTIFICATION_SUCCESS
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: SEEN_NOTIFICATION_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }
    };
};

export const getUnseenNotiAmount = (accountId, callback) => {
    return async (dispatch) => {
        dispatch({
            type: GET_UNSEEN_NOTI_AMOUNT_LOADING
        });
        try {
            const response = await apiGetUnseenAmount(accountId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: GET_UNSEEN_NOTI_AMOUNT_SUCCESS,
                        unseen: data
                    });
                }
            }
            callback();
        } catch (error) {
            dispatch({
                type: GET_UNSEEN_NOTI_AMOUNT_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }
    };
};

export const loadUserNotificationPaging = (accountId, page) => {
    return async (dispatch, getState) => {
        const notificationsPage = getState().notificationReducer.notificationsPage;
        dispatch({
            type: GET_NOTIFICATION_PAGE_LOADING
        });
        try {
            const response = await apiGetAllNotifcationPaging(accountId, page);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    if (page >= 1) {
                        var newNotificationPage = notificationsPage.concat(data);
                        dispatch({
                            type: GET_NOTIFICATION_PAGE_SUCCESS,
                            notificationsPage: newNotificationPage
                        });
                    } else {
                        dispatch({
                            type: GET_NOTIFICATION_PAGE_SUCCESS,
                            notificationsPage: data
                        });
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: GET_NOTIFICATION_PAGE_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }
    };
};

export const setLocalUnseenNoti = (unseen) => {
    return async (dispatch) => {
        dispatch({
            type: SET_LOCAL_UNSEEN_NOTI_SUCCESS,
            localUnseen: unseen
        });
    };
};
