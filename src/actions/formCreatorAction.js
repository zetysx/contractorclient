import { apiSaveForm } from "../apis/prime/formApi";
import { catchRequestCommonError } from "../helpers/actionHelper";

export const FORM_CREATOR_CHANGE_FORMCONTROL = "FORM_CREATOR_CHANGE_FORMCONTROL";
export const FORM_CREATOR_CHANGE_EDITDRAWER_VISABLE = "FORM_CREATOR_CHANGE_EDITDRAWER_VISABLE";
export const FORM_CREATOR_CHANGE_SAVEDRAWER_VISABLE = "FORM_CREATOR_CHANGE_SAVEDRAWER_VISABLE";
export const FORM_CREATOR_CHANGE_EDITDRAWER_FROMCONTROL = "FORM_CREATOR_CHANGE_EDITDRAWER_FROMCONTROL";
export const FORM_CREATOR_CREATE_NEWFORM = "FORM_CREATOR_CREATE_NEWFORM";

export const FORM_CREATOR_SAVE_FORM_LOADING = "FORM_CREATOR_SAVE_FORM_LOADING";
export const FORM_CREATOR_SAVE_FORM_SUCCESS = "FORM_CREATOR_SAVE_FORM_SUCCESS";
export const FORM_CREATOR_SAVE_FORM_FAIL = "FORM_CREATOR_SAVE_FORM_FAIL";

export const changeFormControl = (formControl) => {
    return (dispatch) => {
        dispatch({
            type: FORM_CREATOR_CHANGE_FORMCONTROL,
            formControl
        });
    };
};

export const changeEditDrawerVisable = (isEditDrawerVisable) => {
    return (dispatch) => {
        dispatch({
            type: FORM_CREATOR_CHANGE_EDITDRAWER_VISABLE,
            isEditDrawerVisable
        });
    };
};

export const changeSaveDrawerVisable = (isSaveDrawerVisable) => {
    return (dispatch) => {
        dispatch({
            type: FORM_CREATOR_CHANGE_SAVEDRAWER_VISABLE,
            isSaveDrawerVisable
        });
    };
};

export const changeEditDrawerFormControl = (editDrawerFormControl, editDrawerFormControlIndex, onSuccessCallback) => {
    return async (dispatch) => {
        await dispatch({
            type: FORM_CREATOR_CHANGE_EDITDRAWER_FROMCONTROL,
            editDrawerFormControlIndex,
            editDrawerFormControl
        });
        if (onSuccessCallback) {
            onSuccessCallback();
        }
    };
};

export const changeDataToCreateNewForm = (formControl, previousFormId, onSuccessCallback) => {
    return async (dispatch) => {
        await dispatch({
            type: FORM_CREATOR_CREATE_NEWFORM,
            formControl,
            previousFormId
        });
        if (onSuccessCallback) {
            onSuccessCallback();
        }
    };
};

export const saveForm = (formId, formName, formControls, authorId, onSuccessCallback) => {
    return async (dispatch, getState) => {
        dispatch({
            type: FORM_CREATOR_SAVE_FORM_LOADING
        });

        try {
            const response = await apiSaveForm(formId, formName, formControls, authorId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    await dispatch({
                        type: FORM_CREATOR_SAVE_FORM_SUCCESS,
                        // formList: data
                    });
                    if (onSuccessCallback) {
                        onSuccessCallback();
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: FORM_CREATOR_SAVE_FORM_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};