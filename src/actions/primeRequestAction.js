import {
    apiGetPrimeRequestSentWaitting,
    apiPrimeRequestNotConfirmDelete
} from '../apis/prime/requestApi';
import { catchRequestCommonError } from "../helpers/actionHelper";

export const PRIMEREQUEST_GET_SENT_REQUESTS_NOT_CONFIRM_LOADING = "PRIMEREQUEST_GET_SENT_REQUESTS_NOT_CONFIRM_LOADING";
export const PRIMEREQUEST_GET_SENT_REQUESTS_NOT_CONFIRM_SUCCESS = "PRIMEREQUEST_GET_SENT_REQUESTS_NOT_CONFIRM_SUCCESS";
export const PRIMEREQUEST_GET_SENT_REQUESTS_NOT_CONFIRM_FAIL = "PRIMEREQUEST_GET_SENT_REQUESTS_NOT_CONFIRM_FAIL";

export const PRIMEREQUEST_DELETE_SENT_REQUEST_NOT_CONFIRM_LOADING = "PRIMEREQUEST_DELETE_SENT_REQUEST_NOT_CONFIRM_LOADING";
export const PRIMEREQUEST_DELETE_SENT_REQUEST_NOT_CONFIRM_SUCCESS = "PRIMEREQUEST_DELETE_SENT_REQUEST_NOT_CONFIRM_SUCCESS";
export const PRIMEREQUEST_DELETE_SENT_REQUEST_NOT_CONFIRM_FAIL = "PRIMEREQUEST_DELETE_SENT_REQUEST_NOT_CONFIRM_FAIL";

export const loadAllRequestsPrimeSentWaitting = (primeId) => {
    return async (dispatch) => {
        dispatch({
            type: PRIMEREQUEST_GET_SENT_REQUESTS_NOT_CONFIRM_LOADING
        });
        try {
            const response = await apiGetPrimeRequestSentWaitting(primeId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: PRIMEREQUEST_GET_SENT_REQUESTS_NOT_CONFIRM_SUCCESS,
                        allRequestSentNotConfirmPrimeAction: data,
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: PRIMEREQUEST_GET_SENT_REQUESTS_NOT_CONFIRM_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        }
    };
}

export const deleteRequestNotConfirmPrime = (primeId, requestId, onSuccedDeleteRequestCallback, onFailledDeleteRequestCallback) => {
    return async (dispatch) => {
        dispatch({
            type: PRIMEREQUEST_DELETE_SENT_REQUEST_NOT_CONFIRM_LOADING
        });

        try {
            const response = await apiPrimeRequestNotConfirmDelete(primeId, requestId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: PRIMEREQUEST_DELETE_SENT_REQUEST_NOT_CONFIRM_SUCCESS,
                        deleteRequestSentNotConfirmStatusAction: data,
                    });
                }
                if (onSuccedDeleteRequestCallback) {
                    onSuccedDeleteRequestCallback();
                }
            }
        } catch (error) {
            dispatch({
                type: PRIMEREQUEST_DELETE_SENT_REQUEST_NOT_CONFIRM_FAIL,
            });
            catchRequestCommonError(error, dispatch);

            onFailledDeleteRequestCallback();
            //Đông tự bắt case 400 vì duplicate user
            // if (error.response) {
            //     if (error.response.status === 400) {
            //         if (onFailledDeleteRequestCallback) {
                        
            //         }
            //     }
                
            // }
        }
    }

}
