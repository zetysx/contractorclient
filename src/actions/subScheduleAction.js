import {
    apiBusyWorker,
    apiSubSchedule
} from "../apis/sub/workerApi";
import { catchRequestCommonError } from "../helpers/actionHelper";

export const SUBSCHEDULE_SET_BUSY_WORKER_LOADING = "SUBSCHEDULE_SET_BUSY_WORKER_LOADING";
export const SUBSCHEDULE_SET_BUSY_WORKER_SUCCESS = "SUBSCHEDULE_SET_BUSY_WORKER_SUCCESS";
export const SUBSCHEDULE_SET_BUSY_WORKER_FAIL = "SUBSCHEDULE_SET_BUSY_WORKER_FAIL";
export const SUBSCHEDULE_LOAD_SCHEDULE_DATA_LOADING = "SUBSCHEDULE_LOAD_SCHEDULE_DATA_LOADING";
export const SUBSCHEDULE_LOAD_SCHEDULE_DATA_SUCCESS = "SUBSCHEDULE_LOAD_SCHEDULE_DATA_SUCCESS";
export const SUBSCHEDULE_LOAD_SCHEDULE_DATA_FAIL = "SUBSCHEDULE_LOAD_SCHEDULE_DATA_FAIL";


export const setBusyWorkerDay = (busyWorkerDay, onSuccessCallback) => {
    return async (dispatch) => {
        dispatch({
            type: SUBSCHEDULE_SET_BUSY_WORKER_LOADING
        });

        try {
            const response = await apiBusyWorker(busyWorkerDay);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: SUBSCHEDULE_SET_BUSY_WORKER_SUCCESS,
                        setBusyStatus: data
                    });
                    if ((data.code === 200)&&(onSuccessCallback)) {
                            onSuccessCallback();
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: SUBSCHEDULE_SET_BUSY_WORKER_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }
    };
};

export const loadSubSchedule = (input) => {
    return async (dispatch) => {

            dispatch({
                type: SUBSCHEDULE_LOAD_SCHEDULE_DATA_LOADING
            });

        try {
            const response = await apiSubSchedule(input);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: SUBSCHEDULE_LOAD_SCHEDULE_DATA_SUCCESS,
                        subSchedule: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: SUBSCHEDULE_LOAD_SCHEDULE_DATA_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }
    };
};

