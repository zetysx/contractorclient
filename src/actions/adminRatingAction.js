import {apiGetRating} from "../apis/admin/ratingApi";
import { catchRequestCommonError } from "../helpers/actionHelper";

export const ADMINVIEWRATING_RATING_LOADING = "ADMINVIEWRATING_RATING_LOADING";
export const ADMINVIEWRATING_RATING_SUCCESS = "ADMINVIEWRATING_RATING_SUCCESS";
export const ADMINVIEWRATING_RATING_FAIL = "ADMINVIEWRATING_RATING_FAIL";

export const getRatingList = (pageIndex) => {
    return async (dispatch, getState) => {
        dispatch({
            type: ADMINVIEWRATING_RATING_LOADING
        });

        try {
            const response = await apiGetRating(pageIndex);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: ADMINVIEWRATING_RATING_SUCCESS,
                        ratingList: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: ADMINVIEWRATING_RATING_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};
