import { apiGetOverview } from "../apis/prime/overviewApi";
import { catchRequestCommonError } from "../helpers/actionHelper";

export const GET_PRIME_OVERVIEW_LOADING = "GET_PRIME_OVERVIEW_LOADING";
export const GET_PRIME_OVERVIEW_SUCCESS = "GET_PRIME_OVERVIEW_SUCCESS";
export const GET_PRIME_OVERVIEW_FAIL = "GET_PRIME_OVERVIEW_FAIL";

export const loadPrimeOverview = (userId) => {
    return async (dispatch, getState) => {
        dispatch({
            type: GET_PRIME_OVERVIEW_LOADING
        });
        try {
            const response = await apiGetOverview(userId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: GET_PRIME_OVERVIEW_SUCCESS,
                        primeOverview: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: GET_PRIME_OVERVIEW_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }

    };
};