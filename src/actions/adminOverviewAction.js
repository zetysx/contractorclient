import { apiGetOverview } from "../apis/admin/overviewApi";
import { catchRequestCommonError } from "../helpers/actionHelper";

export const GET_ADMIN_OVERVIEW_LOADING = "GET_ADMIN_OVERVIEW_LOADING";
export const GET_ADMIN_OVERVIEW_SUCCESS = "GET_ADMIN_OVERVIEW_SUCCESS";
export const GET_ADMIN_OVERVIEW_FAIL = "GET_ADMIN_OVERVIEW_FAIL";

export const loadAdminOverview = (accountId) => {
    return async (dispatch, getState) => {
        dispatch({
            type: GET_ADMIN_OVERVIEW_LOADING
        });
        try {
            const response = await apiGetOverview(accountId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: GET_ADMIN_OVERVIEW_SUCCESS,
                        adminOverview: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: GET_ADMIN_OVERVIEW_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }
    };
};