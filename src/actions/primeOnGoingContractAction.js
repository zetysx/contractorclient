import { apiGetAllPrimeContract } from "../apis/prime/contractApi";
import { apiAddFormToContract } from "../apis/prime/formApi";
import { message } from "antd";
import { catchRequestCommonError } from "../helpers/actionHelper";

export const PRIMEONGOINGCONTRACT_PRIME_LOAD_ALL_CONTRACT_LOADING = "PRIMEONGOINGCONTRACT_PRIME_LOAD_ALL_CONTRACT_LOADING";
export const PRIMEONGOINGCONTRACT_PRIME_LOAD_ALL_CONTRACT_SUCCESS = "PRIMEONGOINGCONTRACT_PRIME_LOAD_ALL_CONTRACT_SUCCESS";
export const PRIMEONGOINGCONTRACT_PRIME_LOAD_ALL_CONTRACT_FAIL = "PRIMEONGOINGCONTRACT_PRIME_LOAD_ALL_CONTRACT_FAIL";
export const PRIMEONGOINGCONTRACT_ADD_FORM_TO_CONTRACT_LOADING = "PRIMEONGOINGCONTRACT_ADD_FORM_TO_CONTRACT_LOADING";
export const PRIMEONGOINGCONTRACT_ADD_FORM_TO_CONTRACT_SUCCESS = "PRIMEONGOINGCONTRACT_ADD_FORM_TO_CONTRACT_SUCCESS";
export const PRIMEONGOINGCONTRACT_ADD_FORM_TO_CONTRACT_FAIL = "PRIMEONGOINGCONTRACT_ADD_FORM_TO_CONTRACT_FAIL";

export const loadAllContractByPrimeId = (primeId) => {
    return async (dispatch) => {
        dispatch({
            type: PRIMEONGOINGCONTRACT_PRIME_LOAD_ALL_CONTRACT_LOADING
        });

        try {
            const response = await apiGetAllPrimeContract(primeId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: PRIMEONGOINGCONTRACT_PRIME_LOAD_ALL_CONTRACT_SUCCESS,
                        contracts: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: PRIMEONGOINGCONTRACT_PRIME_LOAD_ALL_CONTRACT_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }
    };
};

export const addFormToContract = (formId, contractId, onSuccessCallback) => {
    return async (dispatch, getState) => {
        dispatch({
            type: PRIMEONGOINGCONTRACT_ADD_FORM_TO_CONTRACT_LOADING
        });

        try {
            const response = await apiAddFormToContract(formId, contractId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: PRIMEONGOINGCONTRACT_ADD_FORM_TO_CONTRACT_SUCCESS,
                        addFormToContractStatus: data
                    });
                    if (data.code === 200) {
                        message.success('Đã chọn mẫu chấm công thành công');
                        if (onSuccessCallback) {
                            onSuccessCallback();
                        }
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: PRIMEONGOINGCONTRACT_ADD_FORM_TO_CONTRACT_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};