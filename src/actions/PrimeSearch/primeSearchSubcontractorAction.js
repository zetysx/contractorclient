import {
    apiSearchSubcontractor,
    apiAutoSearchSubcontractor,
} from "../../apis/prime/searchApi";
import { message } from "antd";
import { catchRequestCommonError } from "../../helpers/actionHelper";


export const MANUALSEARCH_SEARCH_SUBCONTRACTOR_LOADING = "MANUALSEARCH_SEARCH_SUBCONTRACTOR_LOADING";
export const MANUALSEARCH_SEARCH_SUBCONTRACTOR_FAIL = "MANUALSEARCH_SEARCH_SUBCONTRACTOR_FAIL";
export const MANUALSEARCH_SEARCH_SUBCONTRACTOR_LOAD_SUCCESS = "MANUALSEARCH_SEARCH_SUBCONTRACTOR_LOAD_SUCCESS";

export const MANUALSEARCH_AUTO_SEARCH_SUBCONTRACTOR_SUCCESS = "MANUALSEARCH_AUTO_SEARCH_SUBCONTRACTOR_SUCCESS";
export const MANUALSEARCH_AUTO_SEARCH_SUBCONTRACTOR_LOADING = "MANUALSEARCH_AUTO_SEARCH_SUBCONTRACTOR_LOADING";
export const MANUALSEARCH_AUTO_SEARCH_SUBCONTRACTOR_FAIL = "MANUALSEARCH_AUTO_SEARCH_SUBCONTRACTOR_FAIL";


export const searchSubcontractor = (searchValue, criteria) => {
    return async (dispatch) => {
        dispatch({
            type: MANUALSEARCH_SEARCH_SUBCONTRACTOR_LOADING
        });

        try {
            const response = await apiSearchSubcontractor(searchValue, criteria);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    if (data.status) {
                        dispatch({
                            type: MANUALSEARCH_SEARCH_SUBCONTRACTOR_LOAD_SUCCESS,
                            searchedSubInfo: data
                        });
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: MANUALSEARCH_SEARCH_SUBCONTRACTOR_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }
    };
};

export const autoSearchSubcontractor = (searchValue, primeId, onSuccessCallback) => {
    return async (dispatch) => {
        dispatch({
            type: MANUALSEARCH_AUTO_SEARCH_SUBCONTRACTOR_LOADING
        });

        try {
            const response = await apiAutoSearchSubcontractor(searchValue, primeId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: MANUALSEARCH_AUTO_SEARCH_SUBCONTRACTOR_SUCCESS,
                        autoSearchStatus: data
                    });
                    if (data.code === 200) {
                        message.success('Đã thiết lập tìm kiếm tự động thành công');
                        if (onSuccessCallback) {
                            onSuccessCallback();
                        }
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: MANUALSEARCH_AUTO_SEARCH_SUBCONTRACTOR_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }
    };
};
