import {
    apiChangeAutoSearchStatus, apiDeleteAutoSearch,
    apiGetAutoSearchContent,
    apiGetAutoSearchTitle
} from "../../apis/prime/searchApi";
import { catchRequestCommonError } from "../../helpers/actionHelper";


export const AUTOSEARCH_GET_AUTO_SEARCH_TITLE_LOADING = "AUTOSEARCH_GET_AUTO_SEARCH_TITLE_LOADING";
export const AUTOSEARCH_GET_AUTO_SEARCH_TITLE_SUCCESS = "AUTOSEARCH_GET_AUTO_SEARCH_TITLE_SUCCESS";
export const AUTOSEARCH_GET_AUTO_SEARCH_TITLE_FAIL = "AUTOSEARCH_GET_AUTO_SEARCH_TITLE_FAIL";

export const AUTOSEARCH_GET_AUTO_SEARCH_CONTENT_LOADING = "AUTOSEARCH_GET_AUTO_SEARCH_CONTENT_LOADING";
export const AUTOSEARCH_GET_AUTO_SEARCH_CONTENT_SUCCESS = "AUTOSEARCH_GET_AUTO_SEARCH_CONTENT_SUCCESS";
export const AUTOSEARCH_GET_AUTO_SEARCH_CONTENT_FAIL = "AUTOSEARCH_GET_AUTO_SEARCH_CONTENT_FAIL";

export const AUTOSEARCH_CHANGE_AUTO_SEARCH_STATUS_LOADING = "AUTOSEARCH_CHANGE_AUTO_SEARCH_STATUS_LOADING";
export const AUTOSEARCH_CHANGE_AUTO_SEARCH_STATUS_SUCCESS = "AUTOSEARCH_CHANGE_AUTO_SEARCH_STATUS_SUCCESS";
export const AUTOSEARCH_CHANGE_AUTO_SEARCH_STATUS_FAIL = "AUTOSEARCH_CHANGE_AUTO_SEARCH_STATUS_FAIL";

export const AUTOSEARCH_DELETE_AUTO_SEARCH_LOADING = "AUTOSEARCH_DELETE_AUTO_SEARCH_LOADING";
export const AUTOSEARCH_DELETE_AUTO_SEARCH_SUCCESS = "AUTOSEARCH_DELETE_AUTO_SEARCH_SUCCESS";
export const AUTOSEARCH_DELETE_AUTO_SEARCH_FAIL = "AUTOSEARCH_DELETE_AUTO_SEARCH_FAIL";


export const getAutoSearchTitle = (primeId, onSuccessCallback) => {
    return async (dispatch) => {
        dispatch({
            type: AUTOSEARCH_GET_AUTO_SEARCH_TITLE_LOADING
        });

        try {
            const response = await apiGetAutoSearchTitle(primeId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: AUTOSEARCH_GET_AUTO_SEARCH_TITLE_SUCCESS,
                        autoSearchTitle: data
                    });
                    if (onSuccessCallback) {
                        onSuccessCallback();
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: AUTOSEARCH_GET_AUTO_SEARCH_TITLE_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }

    };
};

export const getAutoSearchContent = (autosearchId) => {
    return async (dispatch) => {
        dispatch({
            type: AUTOSEARCH_GET_AUTO_SEARCH_CONTENT_LOADING
        });

        try {
            const response = await apiGetAutoSearchContent(autosearchId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: AUTOSEARCH_GET_AUTO_SEARCH_CONTENT_SUCCESS,
                        autoSearchContent: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: AUTOSEARCH_GET_AUTO_SEARCH_CONTENT_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }

    };
};

export const changeAutoSearchStatus = (autosearchId, onSuccessCallback) => {
    return async (dispatch) => {
        dispatch({
            type: AUTOSEARCH_CHANGE_AUTO_SEARCH_STATUS_LOADING
        });

        try {
            const response = await apiChangeAutoSearchStatus(autosearchId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: AUTOSEARCH_CHANGE_AUTO_SEARCH_STATUS_SUCCESS,
                        changeAutoSearchStatus: data
                    });
                    if (onSuccessCallback) {
                        onSuccessCallback();
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: AUTOSEARCH_CHANGE_AUTO_SEARCH_STATUS_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }

    };
};

export const deleteAutoSearch = (autosearchId, onSuccessCallback) => {
    return async (dispatch) => {
        dispatch({
            type: AUTOSEARCH_DELETE_AUTO_SEARCH_LOADING
        });
        
        try {
            const response = await apiDeleteAutoSearch(autosearchId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: AUTOSEARCH_DELETE_AUTO_SEARCH_SUCCESS,
                        deleteAutoSearchStatus: data
                    });
                    if (onSuccessCallback) {
                        onSuccessCallback();
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: AUTOSEARCH_DELETE_AUTO_SEARCH_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }

    };
};