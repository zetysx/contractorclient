import { apiSendRequest } from "../../apis/prime/requestApi";
import { message } from "antd";
import { catchRequestCommonError } from "../../helpers/actionHelper";


export const MANUALSEARCH_SUB_SENT_REQUEST_LOADING = "MANUALSEARCH_SUB_SENT_REQUEST_LOADING";
export const MANUALSEARCH_SUB_SENT_REQUEST_SUCCESS = "MANUALSEARCH_SUB_SENT_REQUEST_SUCCESS";
export const MANUALSEARCH_SUB_SENT_REQUEST_FAIL = "MANUALSEARCH_SUB_SENT_REQUEST_FAIL";

//[needask] tên hàm
export const sendRequest = (request, onSuccessCallback) => {
    return async (dispatch) => {
        dispatch({
            type: MANUALSEARCH_SUB_SENT_REQUEST_LOADING
        });

        try {
            const response = await apiSendRequest(request);

            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: MANUALSEARCH_SUB_SENT_REQUEST_SUCCESS,
                        sentRequestStatus: data
                    });
                    if (data.code === 200) {
                        message.success('Gửi yêu cầu hợp tác thành công');
                        if (onSuccessCallback) {
                            onSuccessCallback();
                        }
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: MANUALSEARCH_SUB_SENT_REQUEST_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }

    };
};