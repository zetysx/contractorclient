import { apiGetAllSubContractDone } from "../apis/sub/contractApi";
import { catchRequestCommonError } from "../helpers/actionHelper";
import {apiRating} from "../apis/user/ratingApi";
import {message} from "antd";

export const SUBCONTRACTHISTORY_SUBCONTRACTOR_GET_ALL_CONTRACT_DONE_LOADING = 
        "SUBCONTRACTHISTORY_SUBCONTRACTOR_GET_ALL_CONTRACT_DONE_LOADING";
export const SUBCONTRACTHISTORY_SUBCONTRACTOR_GET_ALL_CONTRACT_DONE_SUCCESS = 
        "SUBCONTRACTHISTORY_SUBCONTRACTOR_GET_ALL_CONTRACT_DONE_SUCCESS";
export const SUBCONTRACTHISTORY_SUBCONTRACTOR_GET_ALL_CONTRACT_DONE_FAIL = 
        "SUBCONTRACTHISTORY_SUBCONTRACTOR_GET_ALL_CONTRACT_DONE_SUCCESS";

export const DOCKETDETAIL_RATING_LOADING = "DOCKETDETAIL_RATING_LOADING";
export const DOCKETDETAIL_RATING_SUCCESS = "DOCKETDETAIL_RATING_SUCCESS";
export const DOCKETDETAIL_RATING_FAIL = "DOCKETDETAIL_RATING_FAIL";

export const ratingUser = (ratingData, onSuccessCallback) => {
    return async (dispatch, getState) => {
        dispatch({
            type: DOCKETDETAIL_RATING_LOADING
        });

        try {
            const response = await apiRating(ratingData);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: DOCKETDETAIL_RATING_SUCCESS,
                    });
                    if (data.code === 200) {
                        message.success("Cảm ơn bạn đã đánh giá!");
                        if (onSuccessCallback) {
                            onSuccessCallback();
                        }
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: DOCKETDETAIL_RATING_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};

export const loadAllSubDoneContract = (subId, searchedCompanyName, pageIndex) => {
    return async (dispatch, getState) => {
        const contractDonePage = getState().subContractHistoryReducer.subContractsDone;
        dispatch({
            type: SUBCONTRACTHISTORY_SUBCONTRACTOR_GET_ALL_CONTRACT_DONE_LOADING
        });

        try {
            const response = await apiGetAllSubContractDone(subId, searchedCompanyName, pageIndex);
            if(response && response.data) {
                const data = response.data;
                if (data) {
                    if(pageIndex>=1){
                        var newContractDonePage = data.concat(contractDonePage);
                        dispatch({
                            type: SUBCONTRACTHISTORY_SUBCONTRACTOR_GET_ALL_CONTRACT_DONE_SUCCESS,
                            subDoneContractDataAction: newContractDonePage
                        });
                    } else {
                        dispatch({
                            type: SUBCONTRACTHISTORY_SUBCONTRACTOR_GET_ALL_CONTRACT_DONE_SUCCESS,
                            subDoneContractDataAction: data
                        });
                    }
                    
                }
            }
        } catch(error) {
            dispatch({
                type: SUBCONTRACTHISTORY_SUBCONTRACTOR_GET_ALL_CONTRACT_DONE_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }
    }
}