import { apiDeclineRequest, apiGetAllRequest } from "../apis/sub/requestApi";
import { apiAssignWorker, apiGetWorkerToAssign } from "../apis/sub/workerApi";
import { message } from "antd";
import { catchRequestCommonError } from "../helpers/actionHelper";

export const SUBREQUEST_DECLINE_REQUEST_LOADING = "SUBREQUEST_DECLINE_REQUEST_LOADING";
export const SUBREQUEST_DECLINE_REQUEST_SUCCESS = "SUBREQUEST_DECLINE_REQUEST_SUCCESS";
export const SUBREQUEST_DECLINE_REQUEST_FAIL = "SUBREQUEST_DECLINE_REQUEST_FAIL";
export const SUBREQUEST_SUB_LOAD_LIST_REQUEST_SUCCESS = "SUBREQUEST_SUB_LOAD_LIST_REQUEST_SUCCESS";
export const SUBREQUEST_SUB_LOAD_LIST_REQUEST_LOADING = "SUBREQUEST_SUB_LOAD_LIST_REQUEST_LOADING";
export const SUBREQUEST_SUB_LOAD_LIST_REQUEST_FAIL = "SUBREQUEST_SUB_LOAD_LIST_REQUEST_FAIL";
export const SUBREQUEST_WORKER_ASSIGN_BY_SUB_LOAD_SUCCESS = "SUBREQUEST_WORKER_ASSIGN_BY_SUB_LOAD_SUCCESS";
export const SUBREQUEST_WORKER_ASSIGN_BY_SUB_LOADING = "SUBREQUEST_WORKER_ASSIGN_BY_SUB_LOADING";
export const SUBREQUEST_WORKER_ASSIGN_BY_SUB_FAIL = "SUBREQUEST_WORKER_ASSIGN_BY_SUB_FAIL";
export const SUBREQUEST_WORKER_ASSIGN_FAIL = "SUBREQUEST_WORKER_ASSIGN_FAIL";
export const SUBREQUEST_WORKER_ASSIGN_LOADING = "SUBREQUEST_WORKER_ASSIGN_LOADING";
export const SUBREQUEST_WORKER_ASSIGN_SUCCESS = "SUBREQUEST_WORKER_ASSIGN_SUCCESS";


export const loadAllSubRequest = (subId) => {
    return async (dispatch, getState) => {
         dispatch({
                type: SUBREQUEST_SUB_LOAD_LIST_REQUEST_LOADING
            });

        try {
            const response = await apiGetAllRequest(subId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: SUBREQUEST_SUB_LOAD_LIST_REQUEST_SUCCESS,
                        requests: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: SUBREQUEST_SUB_LOAD_LIST_REQUEST_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }
    };
};

export const declineRequest = (requestId, onSuccessCallback) => {
    return async (dispatch) => {
        dispatch({
            type: SUBREQUEST_DECLINE_REQUEST_LOADING
        });

        try {
            const response = await apiDeclineRequest(requestId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: SUBREQUEST_DECLINE_REQUEST_SUCCESS,
                        declineStatus: data
                    });
                    if (data.code === 200) {
                        message.success('Đã từ chối hợp tác thành công');
                        if (onSuccessCallback) {
                            onSuccessCallback();
                        }
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: SUBREQUEST_DECLINE_REQUEST_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }
    };
};


export const loadAllWorkerToAssign = (subId, contractId) => {
    return async (dispatch) => {
        dispatch({
            type: SUBREQUEST_WORKER_ASSIGN_BY_SUB_LOADING
        });

        try {
            const response = await apiGetWorkerToAssign(subId, contractId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: SUBREQUEST_WORKER_ASSIGN_BY_SUB_LOAD_SUCCESS,
                        workers: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: SUBREQUEST_WORKER_ASSIGN_BY_SUB_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }
    };
};

export const workerAssign = (assignWorkerPayload, onSuccessCallback) => {
    return async (dispatch, getState) => {
        dispatch({
            type: SUBREQUEST_WORKER_ASSIGN_LOADING
        });

        try {
            const response = await apiAssignWorker(assignWorkerPayload);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: SUBREQUEST_WORKER_ASSIGN_SUCCESS,
                        assignStatus: data
                    });
                    if (data.code === 200) {
                        message.success('Đã đồng ý hợp tác thành công');
                        if (onSuccessCallback) {
                            onSuccessCallback();
                        }
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: SUBREQUEST_WORKER_ASSIGN_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};


