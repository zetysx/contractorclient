import { apiGetAllSubContract } from "../apis/sub/contractApi";
import { catchRequestCommonError } from "../helpers/actionHelper";

export const SUBONGOINGCONTRACT_SUB_LOAD_ALL_CONTRACT_LOADING = "SUBONGOINGCONTRACT_SUB_LOAD_ALL_CONTRACT_LOADING";
export const SUBONGOINGCONTRACT_SUB_LOAD_ALL_CONTRACT_SUCCESS = "SUBONGOINGCONTRACT_SUB_LOAD_ALL_CONTRACT_SUCCESS";
export const SUBONGOINGCONTRACT_SUB_LOAD_ALL_CONTRACT_FAIL = "SUBONGOINGCONTRACT_SUB_LOAD_ALL_CONTRACT_FAIL";


export const loadAllSubContract = (subId) => {
    return async (dispatch) => {
        dispatch({
            type: SUBONGOINGCONTRACT_SUB_LOAD_ALL_CONTRACT_LOADING
        });

        try {
            const response = await apiGetAllSubContract(subId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: SUBONGOINGCONTRACT_SUB_LOAD_ALL_CONTRACT_SUCCESS,
                        subContracts: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: SUBONGOINGCONTRACT_SUB_LOAD_ALL_CONTRACT_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }
    };
};
