import { apiGetAllFormByPrimeId } from "../apis/prime/formApi";
import { catchRequestCommonError } from "../helpers/actionHelper";


export const PRIMEFORMMANAGE_GET_FORMLIST_LOADING = "PRIMEFORMMANAGE_GET_FORMLIST_LOADING";
export const PRIMEFORMMANAGE_GET_FORMLIST_SUCCESS = "PRIMEFORMMANAGE_GET_FORMLIST_SUCCESS";
export const PRIMEFORMMANAGE_GET_FORMLIST_FAIL = "PRIMEFORMMANAGE_GET_FORMLIST_FAIL";


export const getAllFormByPrimeId = (primeId) => {
    return async (dispatch, getState) => {
        dispatch({
            type: PRIMEFORMMANAGE_GET_FORMLIST_LOADING
        });

        try {
            const response = await apiGetAllFormByPrimeId(primeId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: PRIMEFORMMANAGE_GET_FORMLIST_SUCCESS,
                        formList: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: PRIMEFORMMANAGE_GET_FORMLIST_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};