import { apiCancelContract } from "../apis/prime/contractApi";
import { catchRequestCommonError } from "../helpers/actionHelper";

export const CONTRACT_CANCEL_CONTRACT_LOADING = "CONTRACT_CANCEL_CONTRACT_LOADING";
export const CONTRACT_CANCEL_CONTRACT_SUCCESS = "CONTRACT_CANCEL_CONTRACT_SUCCESS";

export const cancelContract = (contractId) => {
    return async (dispatch) => {

        dispatch({
            type: CONTRACT_CANCEL_CONTRACT_LOADING
        });
        try {
            const response = await apiCancelContract(contractId);

            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: CONTRACT_CANCEL_CONTRACT_SUCCESS,
                    });
                }
            }
        } catch (error) {
            //[reviewcode] không có action fail, không lẽ nếu load không được thì hiện loading mãi
            catchRequestCommonError(error, dispatch);
        }
    };
};