import {
    apiGetContractsPrimeDone
} from "../apis/prime/contractApi";

import {
    apiGetFormDetail
} from "../apis/prime/formApi";
import { catchRequestCommonError } from "../helpers/actionHelper";
import {apiRating} from "../apis/user/ratingApi";
import {message} from "antd";

export const PRIMECONTRACTHISTORY_GET_CONTRACTS_DONE_LOADING = "PRIMECONTRACTHISTORY_GET_CONTRACTS_DONE_LOADING";
export const PRIMECONTRACTHISTORY_GET_CONTRACTS_DONE_SUCCESS = "PRIMECONTRACTHISTORY_GET_CONTRACTS_DONE_SUCCESS";
export const PRIMECONTRACTHISTORY_GET_CONTRACTS_DONE_FAIL = "PRIMECONTRACTHISTORY_GET_CONTRACTS_DONE_FAIL";

export const PRIMECONTRACTHISTORY_GET_CONTRACT_FORM_LOADING = "PRIMECONTRACTHISTORY_GET_CONTRACT_FORM_LOADING";
export const PRIMECONTRACTHISTORY_GET_CONTRACT_FORM_SUCCESS = "PRIMECONTRACTHISTORY_GET_CONTRACT_FORM_SUCCESS";
export const PRIMECONTRACTHISTORY_GET_CONTRACT_FORM_FAIL = "PRIMECONTRACTHISTORY_GET_CONTRACT_FORM_FAIL";

export const DOCKETDETAIL_RATING_LOADING = "DOCKETDETAIL_RATING_LOADING";
export const DOCKETDETAIL_RATING_SUCCESS = "DOCKETDETAIL_RATING_SUCCESS";
export const DOCKETDETAIL_RATING_FAIL = "DOCKETDETAIL_RATING_FAIL";

export const ratingUser = (ratingData, onSuccessCallback) => {
    return async (dispatch, getState) => {
        dispatch({
            type: DOCKETDETAIL_RATING_LOADING
        });

        try {
            const response = await apiRating(ratingData);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: DOCKETDETAIL_RATING_SUCCESS,
                    });
                    if (data.code === 200) {
                        message.success("Cảm ơn bạn đã đánh giá!");
                        if (onSuccessCallback) {
                            onSuccessCallback();
                        }
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: DOCKETDETAIL_RATING_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};

export const loadAllContractsPrimeDone = (primeId, searchedCompanyName,pageIndex) => {
    return async (dispatch, getState) => {
        const primeContractPage = getState().primeContractHistoryReducer.primeContractsDone;
        dispatch({
            type: PRIMECONTRACTHISTORY_GET_CONTRACTS_DONE_LOADING
        });

        try {
            const response = await apiGetContractsPrimeDone(primeId, searchedCompanyName, pageIndex);

            if (response && response.data) {
                const data = response.data;
                if (data) {
                    if (pageIndex >= 1) {
                        var newPrimeContractPage = data.concat(primeContractPage);
                        dispatch({
                            type: PRIMECONTRACTHISTORY_GET_CONTRACTS_DONE_SUCCESS,
                            primeContractsDoneAction: newPrimeContractPage
                        });
                    } else {
                        dispatch({
                            type: PRIMECONTRACTHISTORY_GET_CONTRACTS_DONE_SUCCESS,
                            primeContractsDoneAction: data
                        });
                    }

                }
            }
        } catch (error) {
            dispatch({
                type: PRIMECONTRACTHISTORY_GET_CONTRACTS_DONE_FAIL
            })
            catchRequestCommonError(error, dispatch);
        }

    };
};

export const loadContractFormData = (formId, onSucceedLoadContractFormCallback) => {
    return async (dispatch) => {
        dispatch({
            type: PRIMECONTRACTHISTORY_GET_CONTRACT_FORM_LOADING
        });

        try {
            const response = await apiGetFormDetail(formId);

            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: PRIMECONTRACTHISTORY_GET_CONTRACT_FORM_SUCCESS,
                        primeContractFormDataAction: data
                    });
                }
                if (onSucceedLoadContractFormCallback) {
                    onSucceedLoadContractFormCallback();
                }
            }
        } catch (error) {
            dispatch({
                type: PRIMECONTRACTHISTORY_GET_CONTRACT_FORM_FAIL
            })
            catchRequestCommonError(error, dispatch);
        }

    };
};