import { apiAdminGetAllContracts } from "../apis/admin/contractApi";
import { catchRequestCommonError } from "../helpers/actionHelper";

export const ADMINCONTRACTS_GET_ALL_CONTRACTS_LOADING = "ADMINCONTRACTS_GET_ALL_CONTRACTS_LOADING";
export const ADMINCONTRACTS_GET_ALL_CONTRACTS_SUCCESS = "ADMINCONTRACTS_GET_ALL_CONTRACTS_SUCCESS";
export const ADMINCONTRACTS_GET_ALL_CONTRACTS_FAILURE = "ADMINCONTRACTS_GET_ALL_CONTRACTS_FAILURE";


export const loadContractsList = (criterialPayload) => {
    return async (dispatch) => {
        dispatch({
            type: ADMINCONTRACTS_GET_ALL_CONTRACTS_LOADING
        });

        try {
            const response = await apiAdminGetAllContracts(criterialPayload);
            if(response && response.data) {
                const data = response.data;
                if(data) {
                    dispatch({
                        type: ADMINCONTRACTS_GET_ALL_CONTRACTS_SUCCESS,
                        allContracts: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: ADMINCONTRACTS_GET_ALL_CONTRACTS_FAILURE
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};