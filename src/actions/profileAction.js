import { apiUpdateProfile } from "../apis/user/authApi";
import { catchRequestCommonError } from "../helpers/actionHelper";


export const PROFILEUPDATEMODAL_UPDATE_PROFILE_LOADING = "PROFILEUPDATEMODAL_UPDATE_PROFILE_LOADING";
export const PROFILEUPDATEMODAL_UPDATE_PROFILE_SUCCESS = "PROFILEUPDATEMODAL_UPDATE_PROFILE_SUCCESS";
export const PROFILEUPDATEMODAL_UPDATE_PROFILE_FAILURE = "PROFILEUPDATEMODAL_UPDATE_PROFILE_FAILURE";

export const PROFILEUPDATEMODAL_PASS_PROFILE_TO_MODAL = "PROFILEUPDATEMODAL_PASS_PROFILE_TO_MODAL";
export const PROFILEUPDATEMODAL_CHANGE_MODAL_VISIBILITY = "PROFILEUPDATEMODAL_CHANGE_MODAL_VISIBILITY";



export const updateUserProfile = (profile, onUpdateUserSuccessed, onUpdateUserFailure) => {
    return async (dispatch) => {
        dispatch({
            type: PROFILEUPDATEMODAL_UPDATE_PROFILE_LOADING
        });

        try {
            const response = await apiUpdateProfile(profile);
            if(response && response.data) {
                const data = response.data;
                if(data) {
                    dispatch({
                        type: PROFILEUPDATEMODAL_UPDATE_PROFILE_SUCCESS,
                        updateUserResult: data
                    });
                    if(onUpdateUserSuccessed) {
                        onUpdateUserSuccessed();
                    }
                }
            }
        } catch(error) {
            dispatch({
                type: PROFILEUPDATEMODAL_UPDATE_PROFILE_FAILURE
            });
            catchRequestCommonError(error, dispatch);
            if(onUpdateUserFailure) {
                onUpdateUserFailure();
            }
        }
    };
}


export const passUserProfileToModal = (profile) => {
    return async (dispatch) => {
        await dispatch({
            type: PROFILEUPDATEMODAL_PASS_PROFILE_TO_MODAL,
            profileNeedUpdate: profile
        })
    }
}

export const changeUpdateProfileModalVisibility = () => {
    return async (dispatch) => {
        await dispatch({
            type: PROFILEUPDATEMODAL_CHANGE_MODAL_VISIBILITY
        })
    }
}