import { apiRegisterUser } from "../apis/user/registerApi";
import { catchRequestCommonError } from "../helpers/actionHelper";

export const USERREGISTER_REGISTER_NEW_ACCOUNT_LOADING = "USERREGISTER_REGISTER_NEW_ACCOUNT_LOADING";
export const USERREGISTER_REGISTER_NEW_ACCOUNT_SUCCESS = "USERREGISTER_REGISTER_NEW_ACCOUNT_SUCCESS";
export const USERREGISTER_REGISTER_NEW_ACCOUNT_FAILURE = "USERREGISTER_REGISTER_NEW_ACCOUNT_FAILURE";

export const registerNewAccount = (registerData, onSucceedRegisterCallback, onFailledDuplicatedRegisterCallback) => {
    return async (dispatch) => {
        dispatch({
            type: USERREGISTER_REGISTER_NEW_ACCOUNT_LOADING
        });

        try {
            const response = await apiRegisterUser(registerData);

            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: USERREGISTER_REGISTER_NEW_ACCOUNT_SUCCESS,
                        registerStatus: data
                    });
                    if (onSucceedRegisterCallback) {
                        onSucceedRegisterCallback();
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: USERREGISTER_REGISTER_NEW_ACCOUNT_FAILURE
            });
            catchRequestCommonError(error, dispatch);
            if(error.response) {
                if(error.response.status === 400) {
                    console.log("response: ", error.response);
                    console.log("asdsa: ", error.response.data.code);
                    if (onFailledDuplicatedRegisterCallback) {
                        onFailledDuplicatedRegisterCallback();
                    }
                }
            }
        };
    };
};