import { apiGetAllProfession } from "../apis/user/professionApi";
import { catchRequestCommonError } from "../helpers/actionHelper";

export const PROFESSION_LOADING = "PROFESSION_LOADING";
export const PROFESSION_FAIL = "PROFESSION_FAIL";
export const PROFESSION_LOAD_SUCCESS = "PROFESSION_LOAD_SUCCESS";


export const loadAllProfessions = () => {
    return async (dispatch, getState) => {
        const professions = getState().professionReducer.professions;

        if (professions && professions.length === 0) {
            dispatch({
                type: PROFESSION_LOADING
            });

            try {
                const response = await apiGetAllProfession();
                if (response && response.data) {
                    const data = response.data;
                    if (data) {
                        dispatch({
                            type: PROFESSION_LOAD_SUCCESS,
                            professions: data
                        });
                    }
                }
            } catch (error) {
                dispatch({
                    type: PROFESSION_FAIL
                });
                catchRequestCommonError(error, dispatch);
            }

        }
    };
};