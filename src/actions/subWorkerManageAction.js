import {
    apiDisableWorkerBySub,
    apiGetAllWorkerForSubManagement,
    apiGetWorkerDetail,
    apiUpdateWorkerBySubcontractor,
    apiGetProfessionOnContract
} from '../apis/sub/workerApi';

import {
    apiGetAllProfessionOfSub
} from '../apis/user/professionApi';
import { catchRequestCommonError } from "../helpers/actionHelper";

export const SUBWORKERMANAGE_DISABLE_WORKER_BY_SUB_LOADING = "SUBWORKERMANAGE_DISABLE_WORKER_BY_SUB_LOADING";
export const SUBWORKERMANAGE_DISABLE_WORKER_BY_SUB_SUCCESS = "SUBWORKERMANAGE_DISABLE_WORKER_BY_SUB_SUCCESS";
export const SUBWORKERMANAGE_DISABLE_WORKER_BY_SUB_FAIL = "SUBWORKERMANAGE_DISABLE_WORKER_BY_SUB_FAIL";

export const SUBWORKERMANAGE_GET_ALL_SUBCONTRACTOR_PROFESSION_LOADING = "SUBWORKERMANAGE_GET_ALL_SUBCONTRACTOR_PROFESSION_LOADING";
export const SUBWORKERMANAGE_GET_ALL_SUBCONTRACTOR_PROFESSION_SUCCESS = "SUBWORKERMANAGE_GET_ALL_SUBCONTRACTOR_PROFESSION_SUCCESS";
export const SUBWORKERMANAGE_GET_ALL_SUBCONTRACTOR_PROFESSION_FAIL = "SUBWORKERMANAGE_GET_ALL_SUBCONTRACTOR_PROFESSION_FAIL";

export const SUBWORKERMANAGE_GET_ALL_WORKERS_LOADING = "SUBWORKERMANAGE_GET_ALL_WORKERS_LOADING";
export const SUBWORKERMANAGE_GET_ALL_WORKERS_SUCCESS = "SUBWORKERMANAGE_GET_ALL_WORKERS_SUCCESS";
export const SUBWORKERMANAGE_GET_ALL_WORKERS_FAIL = "SUBWORKERMANAGE_GET_ALL_WORKERS_FAIL";

export const SUBWORKERMANAGE_GET_WORKER_DETAILS_FOR_MANAGE_LOADING = "SUBWORKERMANAGE_GET_WORKER_DETAILS_FOR_MANAGE_LOADING";
export const SUBWORKERMANAGE_GET_WORKER_DETAILS_FOR_MANAGE_SUCCESS = "SUBWORKERMANAGE_GET_WORKER_DETAILS_FOR_MANAGE_SUCCESS";
export const SUBWORKERMANAGE_GET_WORKER_DETAILS_FOR_MANAGE_FAIL = "SUBWORKERMANAGE_GET_WORKER_DETAILS_FOR_MANAGE_FAIL"

export const SUBWORKERMANAGE_CHANGE_VISIBLE_UPDATE_MODAL = "SUBWORKERMANAGE_CHANGE_VISIBLE_UPDATE_MODAL";

export const SUBWORKERMANAGE_WORKERLIST_PASS_WORKER_NEED_UPDATE_TO_MODAL = "SUBWORKERMANAGE_WORKERLIST_PASS_WORKER_NEED_UPDATE_TO_MODAL";

export const SUBWORKERMANAGE_WORKERUPDATE_UPDATE_WORKER_INFORMATION_LOADING = "SUBWORKERMANAGE_WORKERUPDATE_UPDATE_WORKER_INFORMATION_LOADING";
export const SUBWORKERMANAGE_WORKERUPDATE_UPDATE_WORKER_INFORMATION_SUCCESS = "SUBWORKERMANAGE_WORKERUPDATE_UPDATE_WORKER_INFORMATION_SUCCESS";
export const SUBWORKERMANAGE_WORKERUPDATE_UPDATE_WORKER_INFORMATION_FAIL = "SUBWORKERMANAGE_WORKERUPDATE_UPDATE_WORKER_INFORMATION_FAIL";


export const SUBWORKERMANAGE_WORKERUPDATE_CHECKBOXPROFESSION_CHANGE_WORKER_PROFESSION = "SUBWORKERMANAGE_WORKERUPDATE_CHECKBOXPROFESSION_CHANGE_WORKER_PROFESSION";

export const SUBWORKERMANAGE_WORKERUPDATE_GET_PROFESSIONS_LIST_WORKER_WORKING_ON_LOADING = "SUBWORKERMANAGE_WORKERUPDATE_GET_PROFESSIONS_LIST_WORKER_WORKING_ON_LOADING";
export const SUBWORKERMANAGE_WORKERUPDATE_GET_PROFESSIONS_LIST_WORKER_WORKING_ON_SUCCESS = "SUBWORKERMANAGE_WORKERUPDATE_GET_PROFESSIONS_LIST_WORKER_WORKING_ON_SUCCESS";
export const SUBWORKERMANAGE_WORKERUPDATE_GET_PROFESSIONS_LIST_WORKER_WORKING_ON_FAIL = "SUBWORKERMANAGE_WORKERUPDATE_GET_PROFESSIONS_LIST_WORKER_WORKING_ON_FAIL";

export const getWorkerProfessionsWorkingOn = (workerId, onGetProfessionsWorkingSuccess) => {

    const getArrayProfessionId = (professions) => {
        let idArray = [];
        for(var posProf in professions) {
            idArray = idArray.concat(professions[posProf].professionId);
        }
        return idArray;
    }

    return async (dispatch) => {
        dispatch({
            type: SUBWORKERMANAGE_WORKERUPDATE_GET_PROFESSIONS_LIST_WORKER_WORKING_ON_LOADING
        });

        try {
            const response = await apiGetProfessionOnContract(workerId);
            if(response && response.data) {
                const data = response.data;
                if(data) {
                    dispatch({
                        type: SUBWORKERMANAGE_WORKERUPDATE_GET_PROFESSIONS_LIST_WORKER_WORKING_ON_SUCCESS,
                        workerProfessionsWorkingOn: getArrayProfessionId(data)
                    });
                    if(onGetProfessionsWorkingSuccess) {
                        onGetProfessionsWorkingSuccess();
                    }
                }
            }
        } catch(error) {
            dispatch({
                type: SUBWORKERMANAGE_WORKERUPDATE_GET_PROFESSIONS_LIST_WORKER_WORKING_ON_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }
    }
}


export const changeWorkerProfessions = (checkedIndex) => {

    const changeProfessionsOfWorker = (subProfessions, worker, checkedIndex) => {
        let workerUpdate = {...worker};
        //Nếu profession của worker đã có profeesion mà user check => xóa đi
        //Nếu không có => có nghĩa là user muốn add thêm profeesion đó
        if(workerUpdate.workerProfessions.includes(subProfessions[checkedIndex].professionId)) {
            workerUpdate.workerProfessions.splice( 
                workerUpdate.workerProfessions.indexOf(subProfessions[checkedIndex].professionId), 1 );
        } else {
            workerUpdate.workerProfessions = workerUpdate.workerProfessions.concat(subProfessions[checkedIndex].professionId);
        }
        return workerUpdate;
    }



    return (dispatch, getState) => {
        dispatch({
            type: SUBWORKERMANAGE_WORKERUPDATE_CHECKBOXPROFESSION_CHANGE_WORKER_PROFESSION,
            newWorkerWithNewProfessions: {...changeProfessionsOfWorker(getState().subWorkerManageReducer.subcontractorProfessions, 
                getState().subWorkerManageReducer.workerUpdate_Object, checkedIndex
            )}
        });
    }
}

export const passWorkerToModal = (worker) => {
    const getProfessionIdFromProfessionList = (profList) => {
        let profIdList = [];
        if(profList && profList.length > 0) {
            for(var pos = 0; pos < profList.length; pos++) {
                profIdList = profIdList.concat(profList[pos].professionId)
            }
        }
        return profIdList;
    }
    
    let workerPassing = {...worker};
    
    let workerProfessionsId = getProfessionIdFromProfessionList(workerPassing.workerProfessions);
    workerPassing.workerProfessions = [...workerProfessionsId];
    console.log("passWorkerToModal: ", workerPassing);
    return async (dispatch) => {
        await dispatch({
            type: SUBWORKERMANAGE_WORKERLIST_PASS_WORKER_NEED_UPDATE_TO_MODAL,
            workerNeedUpdate: workerPassing
        });
    };
};

export const changeUpdateModalVisibility = (isVisible) => {
    return (dispatch) => {
        dispatch({
            type: SUBWORKERMANAGE_CHANGE_VISIBLE_UPDATE_MODAL,
            isUpdateModalVisible: isVisible
        });
    };
};

export const updateWorkerInformation = (workerPayload, onSucceedUpdateWorkerCallback, onFailUpdateWorkerCallback) => {
    return async (dispatch) => {
        dispatch({
            type: SUBWORKERMANAGE_WORKERUPDATE_UPDATE_WORKER_INFORMATION_LOADING
        });

        try {
            const response = await apiUpdateWorkerBySubcontractor(workerPayload);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: SUBWORKERMANAGE_WORKERUPDATE_UPDATE_WORKER_INFORMATION_SUCCESS,
                        workerUpdateStatus: data
                    });
                    if (onSucceedUpdateWorkerCallback()) {
                        onSucceedUpdateWorkerCallback();
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: SUBWORKERMANAGE_WORKERUPDATE_UPDATE_WORKER_INFORMATION_FAIL
            });
            catchRequestCommonError(error, dispatch);
            if (onFailUpdateWorkerCallback()) {
                onFailUpdateWorkerCallback();
            }
        }
    };
};

export const disableWorkerBySubcontractor = (workerAccountId, onSuccessCallback, onFailCallback) => {
    return async (dispatch) => {
        dispatch({
            type: SUBWORKERMANAGE_DISABLE_WORKER_BY_SUB_LOADING
        });

        try {
            const response = await apiDisableWorkerBySub(workerAccountId);
            if (response && response.data) {
                const data = response.data;
                if (data.code === 200) {
                    dispatch({
                        type: SUBWORKERMANAGE_DISABLE_WORKER_BY_SUB_SUCCESS,
                        workerDisabledStatus: data
                    });
                    if (onSuccessCallback) {
                        onSuccessCallback();
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: SUBWORKERMANAGE_DISABLE_WORKER_BY_SUB_FAIL
            });
            catchRequestCommonError(error, dispatch);

            if (error.response) {
                if (error.response.status === 400) {
                    if (onFailCallback) {
                        onFailCallback();
                    }
                }
            }
        };
    };
};

export const loadAllSubcontractorProfession = (subId) => {
    return async (dispatch) => {
        dispatch({
            type: SUBWORKERMANAGE_GET_ALL_SUBCONTRACTOR_PROFESSION_LOADING
        })
        try {
            const response = await apiGetAllProfessionOfSub(subId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: SUBWORKERMANAGE_GET_ALL_SUBCONTRACTOR_PROFESSION_SUCCESS,
                        subcontractorProfessionsForWorker: data,
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: SUBWORKERMANAGE_GET_ALL_SUBCONTRACTOR_PROFESSION_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        }
    };
};

export const loadAllWorkerOfSubcontractor = (subId) => {
    return async (dispatch) => {
        dispatch({
            type: SUBWORKERMANAGE_GET_ALL_WORKERS_LOADING
        });
        try {
            const response = await apiGetAllWorkerForSubManagement(subId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: SUBWORKERMANAGE_GET_ALL_WORKERS_SUCCESS,
                        allSubWorkers: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: SUBWORKERMANAGE_GET_ALL_WORKERS_FAIL
            })
            catchRequestCommonError(error, dispatch);
        }
    };
};

export const loadWorkerDetailForManagement = (subcontratorId, workerId) => {
    return async (dispatch) => {
        dispatch({
            type: SUBWORKERMANAGE_GET_WORKER_DETAILS_FOR_MANAGE_LOADING
        });

        try {
            const response = await apiGetWorkerDetail(subcontratorId, workerId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: SUBWORKERMANAGE_GET_WORKER_DETAILS_FOR_MANAGE_SUCCESS,
                        workerDetailManage: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: SUBWORKERMANAGE_GET_WORKER_DETAILS_FOR_MANAGE_FAIL
            })
            catchRequestCommonError(error, dispatch);
        }

    };
};