import { apiGetUserDetailInfo } from "../apis/user/general";
import { catchRequestCommonError } from "../helpers/actionHelper";
import {apiGetRatingList} from "../apis/user/ratingApi";

export const USERPROFILE_LOAD_USER_PROFILE_LOADING = "USERPROFILE_LOAD_USER_PROFILE_LOADING";
export const USERPROFILE_LOAD_USER_PROFILE_SUCCESS = "USERPROFILE_LOAD_USER_PROFILE_SUCCESS";
export const USERPROFILE_LOAD_USER_PROFILE_FAIL = "USERPROFILE_LOAD_USER_PROFILE_FAIL";

export const USERPROFILE_RATING_LIST_LOADING = "USERPROFILE_RATING_LIST_LOADING";
export const USERPROFILE_RATING_LIST_SUCCESS = "USERPROFILE_RATING_LIST_SUCCESS";
export const USERPROFILE_RATING_LIST_FAIL = "USERPROFILE_RATING_LIST__FAIL";

export const loadUserProfile = (userId) => {
    return async (dispatch) => {
        dispatch({
            type: USERPROFILE_LOAD_USER_PROFILE_LOADING
        });

        try {
            const response = await apiGetUserDetailInfo(userId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: USERPROFILE_LOAD_USER_PROFILE_SUCCESS,
                        userProfile: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: USERPROFILE_LOAD_USER_PROFILE_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }
    };
};

export const loadRatingList = (accountId, pageIndex) => {
    return async (dispatch) => {
        dispatch({
            type: USERPROFILE_RATING_LIST_LOADING
        });

        try {
            const response = await apiGetRatingList(accountId, pageIndex);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: USERPROFILE_RATING_LIST_SUCCESS,
                        ratingList: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: USERPROFILE_RATING_LIST_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }
    };


};