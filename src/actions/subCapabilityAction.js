

import { catchRequestCommonError } from "../helpers/actionHelper";
import { apiGetAllProfessionForUpdate, apiUpdateProfessionCapability } from "../apis/sub/professionApi";
import { apiGetAllLocationsForUpdate, apiUpdateLocationCapability } from "../apis/sub/locationApi";

export const SUBCAPABILITY_GET_ALL_PROFESSIONS_FOR_UPDATING_LOADING = "SUBCAPABILITY_GET_ALL_PROFESSIONS_FOR_UPDATING_LOADING";
export const SUBCAPABILITY_GET_ALL_PROFESSIONS_FOR_UPDATING_SUCCESS = "SUBCAPABILITY_GET_ALL_PROFESSIONS_FOR_UPDATING_SUCCESS";
export const SUBCAPABILITY_GET_ALL_PROFESSIONS_FOR_UPDATING_FAILURE = "SUBCAPABILITY_GET_ALL_PROFESSIONS_FOR_UPDATING_FAILURE";

export const SUBCAPABILITY_GET_ALL_LOCATIONS_FOR_UPDATING_LOADING = "SUBCAPABILITY_GET_ALL_LOCATIONS_FOR_UPDATING_LOADING";
export const SUBCAPABILITY_GET_ALL_LOCATIONS_FOR_UPDATING_SUCCESS = "SUBCAPABILITY_GET_ALL_LOCATIONS_FOR_UPDATING_SUCCESS";
export const SUBCAPABILITY_GET_ALL_LOCATIONS_FOR_UPDATING_FAILURE = "SUBCAPABILITY_GET_ALL_LOCATIONS_FOR_UPDATING_FAILURE";

export const SUBCAPABILITY_UPDATE_SUBCONTRACTOR_PROFESSIONS_LOADING = "SUBCAPABILITY_UPDATE_SUBCONTRACTOR_PROFESSIONS_LOADING";
export const SUBCAPABILITY_UPDATE_SUBCONTRACTOR_PROFESSIONS_SUCCESS = "SUBCAPABILITY_UPDATE_SUBCONTRACTOR_PROFESSIONS_SUCCESS";
export const SUBCAPABILITY_UPDATE_SUBCONTRACTOR_PROFESSIONS_FAILURE = "SUBCAPABILITY_UPDATE_SUBCONTRACTOR_PROFESSIONS_FAILURE";

export const SUBCAPABILITY_UPDATE_SUBCONTRACTOR_LOCATIONS_LOADING = "SUBCAPABILITY_UPDATE_SUBCONTRACTOR_LOCATIONS_LOADING";
export const SUBCAPABILITY_UPDATE_SUBCONTRACTOR_LOCATIONS_SUCCESS = "SUBCAPABILITY_UPDATE_SUBCONTRACTOR_LOCATIONS_SUCCESS";
export const SUBCAPABILITY_UPDATE_SUBCONTRACTOR_LOCATIONS_FAILURE = "SUBCAPABILITY_UPDATE_SUBCONTRACTOR_LOCATIONS_FAILURE";

export const SUBCAPABILITY_PROFESSION_CHANGE_PRICE = "SUBCAPABILITY_PROFESSION_CHANGE_PRICE";
export const SUBCAPABILITY_PROFESSION_CHANGE_CHECKBOX_PROFESSION = "SUBCAPABILITY_PROFESSION_CHANGE_PROFESSION";
export const SUBCAPABILITY_LOCATION_CHANGE_LOCATION_CHECKBOX = "SUBCAPABILITY_LOCATION_CHANGE_LOCATION_CHECKBOX";





export const changeCheckboxLocation = (locations) => {

    const removeParentNodeInList = (locations) => {
        return locations.filter(location => {return (!location.includes("keycity"))})
    }

    return (dispatch) => {
        const newLocations = removeParentNodeInList(locations);

        dispatch({
            type: SUBCAPABILITY_LOCATION_CHANGE_LOCATION_CHECKBOX,
            newLocations: newLocations
        });
    }
}

export const changePriceProfessions = (professionId, price) => {

    const updateNewPriceProfession = (professions, professionId, price) => {
        return professions.map(profession => {
            if(profession.professionId === professionId) {
                profession.price = price;
            }
            return profession;
        })
    }

    return (dispatch, getState) => {
        const professions = getState().subCapabilityReducer.subcontractorProfessionsForUpdate;
        const newProfessions = updateNewPriceProfession(professions, professionId, price);
        dispatch({
            type: SUBCAPABILITY_PROFESSION_CHANGE_PRICE,
            newProfessions: newProfessions
        });
    }
}

export const changeCheckboxProfessions = (professionId) => {

    const updateStatusOfCheckbox = (professions, professionId) => {
        return professions.map(profession => {
            if(profession.professionId === professionId) {
                profession.subcontractorHas = !profession.subcontractorHas;
            }
            return profession;
        })
    }

    return (dispatch, getState) => {
        const professions = getState().subCapabilityReducer.subcontractorProfessionsForUpdate;
        const newProfessions = updateStatusOfCheckbox(professions, professionId);
        dispatch({
            type: SUBCAPABILITY_PROFESSION_CHANGE_CHECKBOX_PROFESSION,
            newProfessions: newProfessions
        });
    }

}

export const loadSubcontractorProfessionsForUpdate = () => {
    return async (dispatch) => {
        dispatch({
            type: SUBCAPABILITY_GET_ALL_PROFESSIONS_FOR_UPDATING_LOADING
        })
        try {
            const response = await apiGetAllProfessionForUpdate();
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: SUBCAPABILITY_GET_ALL_PROFESSIONS_FOR_UPDATING_SUCCESS,
                        subcontractorProfessionsForUpdate: data,
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: SUBCAPABILITY_GET_ALL_PROFESSIONS_FOR_UPDATING_FAILURE,
            });
            catchRequestCommonError(error, dispatch);
        }
    };
};


export const loadSubcontractorLocationsForUpdate = () => {

    const extractLocation = (locations) => {
        let locationIds = []
        if(locations && locations.length > 0) {
            locations.map(location => {
                if(location.subcontractorHas) 
                    locationIds = locationIds.concat(location.locationId);
            })
        }
        return locationIds;
    }

    return async (dispatch) => {
        dispatch({
            type: SUBCAPABILITY_GET_ALL_LOCATIONS_FOR_UPDATING_LOADING
        })
        try {
            const response = await apiGetAllLocationsForUpdate();
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    const subcontractorLocation = extractLocation(data);
                    dispatch({
                        type: SUBCAPABILITY_GET_ALL_LOCATIONS_FOR_UPDATING_SUCCESS,
                        subcontractorLocationsForUpdate: preProcessLocationsData(data),
                        subcontractorLocation: subcontractorLocation
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: SUBCAPABILITY_GET_ALL_LOCATIONS_FOR_UPDATING_FAILURE,
            });
            catchRequestCommonError(error, dispatch);
        }
    };
}

const preProcessLocationsData = (locations) => {
    let locationTreeData = [];
    let isFoundParent = false;
    if(locations) {
        for(var posLocations in locations) {
            var splitArray = locations[posLocations].locationName.split(" - ");
            if(locationTreeData.length > 0) {
                isFoundParent = false;
                for(var posTree in locationTreeData) {
                    if(locationTreeData[posTree].key.split(" - ")[1] === splitArray[1].trim()) {
                        console.log("locationTreeData[posTree].key.split(" - ")[1]: ", locationTreeData[posTree].key.split(" - ")[1]);
                        locationTreeData[posTree].children = locationTreeData[posTree].children.concat({
                            title: splitArray[0], 
                            key: locations[posLocations].locationId,
                            subcontractorHas: locations[posLocations].subcontractorHas 
                        })
                        isFoundParent = true; 
                    } 
                }
                if(!isFoundParent) {
                    locationTreeData[posLocations] = {
                        title: splitArray[1].trim(),
                        // title: splitArray[1].trim(),
                        key: "keycity - " + splitArray[1].trim(),
                        children: [{
                            title: splitArray[0],
                            key: locations[posLocations].locationId,
                            subcontractorHas: locations[posLocations].subcontractorHas
                        }]
                    }
                }
            } else {
                locationTreeData[posLocations] = {
                    title: splitArray[1].trim(),
                    // title: splitArray[1].trim(),
                    key: "keycity - " + splitArray[1].trim(),
                    children: [{
                        title: splitArray[0],
                        key: locations[posLocations].locationId,
                        subcontractorHas: locations[posLocations].subcontractorHas
                    }]
                }
            }
    
        }
    }
    // console.log("locationTreeData: ", locationTreeData);
    return locationTreeData;
}



export const updateSubcontractorProfessions = (professions, onUpdateProfessionSuccessed, onUpdateProfessionFailled) => {
    return async (dispatch) => {
        dispatch({
            type: SUBCAPABILITY_UPDATE_SUBCONTRACTOR_PROFESSIONS_LOADING
        });

        try {
            const subcontractorProfessions = preProcessProfessionsDataForUpdating(professions);
            console.log("profession update: ", subcontractorProfessions);
            const response = await apiUpdateProfessionCapability(subcontractorProfessions);
            if (response && response.data) {
                const data = response.data;
                if(data) {
                    dispatch({
                        type: SUBCAPABILITY_UPDATE_SUBCONTRACTOR_PROFESSIONS_SUCCESS,
                        subcontractorProfessionCapabilityUpdateData: data,
                    });
                    if(onUpdateProfessionSuccessed) {
                        onUpdateProfessionSuccessed();
                    }
                }
            }
        } catch(error) {
            dispatch({
                type: SUBCAPABILITY_UPDATE_SUBCONTRACTOR_PROFESSIONS_FAILURE,
            });
            catchRequestCommonError(error, dispatch);
            if(onUpdateProfessionFailled) {
                onUpdateProfessionFailled();
            }
        }
    }
}


const preProcessProfessionsDataForUpdating = (professions) => {
    let professionUpdate = [];
    if(professions && professions.length > 0) {
        professionUpdate = professions.filter((profession) => {
            return profession.subcontractorHas;
        })
    }
    return professionUpdate;
}


// const preProcessLocationsDataForUpdating = (locations) => {
//     let locationIds = [];
//     if(locations && locations.length > 0) {
//         for(var posLocation in locations) {
//             if(locations[posLocation].children && locations[posLocation].children.length > 0) {
//                 let children =  locations[posLocation].children;
//                 for(var posChildren in children) {
//                     if(children[posChildren].subcontractorHas) {
//                         locationIds = locationIds.concat(children[posChildren].locationId)
//                     }
//                 }
//             }
//         }
//     }
// }

export const updateSubcontractorLocations = (locations, onUpdateLocationSuccessed, onUpdateLocationFailled) => {
    return async (dispatch) => {
        dispatch({
            type: SUBCAPABILITY_UPDATE_SUBCONTRACTOR_LOCATIONS_LOADING
        });

        try {
            const response = await apiUpdateLocationCapability(locations);
            if (response && response.data) {
                const data = response.data;
                if(data) {
                    dispatch({
                        type: SUBCAPABILITY_UPDATE_SUBCONTRACTOR_LOCATIONS_SUCCESS,
                        subcontractorLocationCapabilityUpdateData: data,
                    });
                    if(onUpdateLocationSuccessed) {
                        onUpdateLocationSuccessed();
                    }
                }
            }
        } catch(error) {
            dispatch({
                type: SUBCAPABILITY_UPDATE_SUBCONTRACTOR_LOCATIONS_FAILURE,
            });
            catchRequestCommonError(error, dispatch);
            if(onUpdateLocationFailled) {
                onUpdateLocationFailled();
            }
        }
    }
}