import { apiGetOverview } from "../apis/sub/overviewApi";
import { catchRequestCommonError } from "../helpers/actionHelper";

export const GET_SUB_OVERVIEW_LOADING = "GET_SUB_OVERVIEW_LOADING";
export const GET_SUB_OVERVIEW_SUCCESS = "GET_SUB_OVERVIEW_SUCCESS";
export const GET_SUB_OVERVIEW_FAIL = "GET_SUB_OVERVIEW_FAIL";

export const loadSubOverview = (userId) => {
    return async (dispatch, getState) => {
        dispatch({
            type: GET_SUB_OVERVIEW_LOADING
        });
        try {
            const response = await apiGetOverview(userId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: GET_SUB_OVERVIEW_SUCCESS,
                        subOverview: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: GET_SUB_OVERVIEW_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }

    };
};