import {
    apiApproveDocket,
    apiConfirmDocket,
    apiDeclineDocket,
    apiGetDocketDetailById,
    apiRejectDocket,
    apiNeedApproveDocket,
    apiFormFilledDocket,
    apiUpdateFormDataByDocketId,
    apiReturnDocketBySub
} from "../apis/user/docketApi";
import { validFormControlAndData } from "../helpers/formHelper";
import { message } from "antd";
import { catchRequestCommonError } from "../helpers/actionHelper";

export const DOCKETDETAIL_APPROVE_DOCKET_LOADING = "DOCKETDETAIL_APPROVE_DOCKET_LOADING";
export const DOCKETDETAIL_APPROVE_DOCKET_SUCCESS = "DOCKETDETAIL_APPROVE_DOCKET_SUCCESS";
export const DOCKETDETAIL_APPROVE_DOCKET_FAIL = "DOCKETDETAIL_APPROVE_DOCKET_FAIL";

export const DOCKETDETAIL_CONFIRM_DOCKET_LOADING = "DOCKETDETAIL_CONFIRM_DOCKET_LOADING";
export const DOCKETDETAIL_CONFIRM_DOCKET_SUCCESS = "DOCKETDETAIL_CONFIRM_DOCKET_SUCCESS";
export const DOCKETDETAIL_CONFIRM_DOCKET_FAIL = "DOCKETDETAIL_CONFIRM_DOCKET_FAIL";

export const DOCKETDETAIL_DECLINE_DOCKET_LOADING = "DOCKETDETAIL_DECLINE_DOCKET_LOADING";
export const DOCKETDETAIL_DECLINE_DOCKET_SUCCESS = "DOCKETDETAIL_DECLINE_DOCKET_SUCCESS";
export const DOCKETDETAIL_DECLINE_DOCKET_FAIL = "DOCKETDETAIL_DECLINE_DOCKET_FAIL";

export const DOCKETDETAIL_REJECT_DOCKET_LOADING = "DOCKETDETAIL_REJECT_DOCKET_LOADING";
export const DOCKETDETAIL_REJECT_DOCKET_SUCCESS = "DOCKETDETAIL_REJECT_DOCKET_SUCCESS";
export const DOCKETDETAIL_REJECT_DOCKET_FAIL = "DOCKETDETAIL_REJECT_DOCKET_FAIL";

export const DOCKETDETAIL_RETURN_DOCKET_LOADING = "DOCKETDETAIL_RETURN_DOCKET_LOADING";
export const DOCKETDETAIL_RETURN_DOCKET_SUCCESS = "DOCKETDETAIL_RETURN_DOCKET_SUCCESS";
export const DOCKETDETAIL_RETURN_DOCKET_FAIL = "DOCKETDETAIL_RETURN_DOCKET_FAIL";

export const DOCKETDETAIL_GET_DOCKETDETAIL_BY_DOCKETID_LOADING = "DOCKETDETAIL_GET_DOCKETDETAIL_BY_DOCKETID_LOADING";
export const DOCKETDETAIL_GET_DOCKETDETAIL_BY_DOCKETID_SUCCESS = "DOCKETDETAIL_GET_DOCKETDETAIL_BY_DOCKETID_SUCCESS";
export const DOCKETDETAIL_GET_DOCKETDETAIL_BY_DOCKETID_FAIL = "DOCKETDETAIL_GET_DOCKETDETAIL_BY_DOCKETID_FAIL";

export const DOCKETDETAIL_CHANGE_INPUTDRAWER_VISABLE = "DOCKETDETAIL_CHANGE_INPUTDRAWER_VISABLE";
export const DOCKETDETAIL_CHANGE_INPUTDRAWER_FROMCONTROL = "DOCKETDETAIL_CHANGE_INPUTDRAWER_FROMCONTROL";
export const DOCKETDETAIL_CHANGE_FORMDATA = "DOCKETDETAIL_CHANGE_FORMDATA";

export const DOCKETDETAIL_UPDATE_FORMDATA_BY_DOCKETID_LOADING = "DOCKETDETAIL_UPDATE_FORMDATA_BY_DOCKETID_LOADING";
export const DOCKETDETAIL_UPDATE_FORMDATA_BY_DOCKETID_SUCCESS = "DOCKETDETAIL_UPDATE_FORMDATA_BY_DOCKETID_SUCCESS";
export const DOCKETDETAIL_UPDATE_FORMDATA_BY_DOCKETID_FAIL = "DOCKETDETAIL_UPDATE_FORMDATA_BY_DOCKETID_FAIL";

export const DOCKETDETAIL_NEEDAPPROVE_DOCKET_LOADING = "DOCKETDETAIL_NEEDAPPROVE_DOCKET_LOADING";
export const DOCKETDETAIL_NEEDAPPROVE_DOCKET_SUCCESS = "DOCKETDETAIL_NEEDAPPROVE_DOCKET_SUCCESS";
export const DOCKETDETAIL_NEEDAPPROVE_DOCKET_FAIL = "DOCKETDETAIL_NEEDAPPROVE_DOCKET_FAIL";

export const DOCKETDETAIL_FORMFILLED_DOCKET_LOADING = "DOCKETDETAIL_FORMFILLED_DOCKET_LOADING";
export const DOCKETDETAIL_FORMFILLED_DOCKET_SUCCESS = "DOCKETDETAIL_FORMFILLED_DOCKET_SUCCESS";
export const DOCKETDETAIL_FORMFILLED_DOCKET_FAIL = "DOCKETDETAIL_FORMFILLED_DOCKET_FAIL";

export const changeInputDrawerVisable = (isControlInputDrawerVisable) => {
    return async (dispatch, getState) => {
        dispatch({
            type: DOCKETDETAIL_CHANGE_INPUTDRAWER_VISABLE,
            isControlInputDrawerVisable
        });
    };
};

export const changeInputDrawerFormControl = (inputControlData, inputControlIndex, isControlInputDrawerVisable, onSuccessCallback) => {
    return async (dispatch) => {
        await dispatch({
            type: DOCKETDETAIL_CHANGE_INPUTDRAWER_FROMCONTROL,
            inputControlData,
            inputControlIndex,
            isControlInputDrawerVisable
        });
        if (onSuccessCallback) {
            onSuccessCallback();
        }
    };
};

export const changeFormData = (formData, isControlInputDrawerVisable, onSuccessCallback) => {
    return async (dispatch) => {
        await dispatch({
            type: DOCKETDETAIL_CHANGE_FORMDATA,
            formData,
            isControlInputDrawerVisable
        });
        if (onSuccessCallback) {
            onSuccessCallback();
        }
    };
};

export const getDocketDetailById = (docketId) => {
    return async (dispatch, getState) => {
        dispatch({
            type: DOCKETDETAIL_GET_DOCKETDETAIL_BY_DOCKETID_LOADING
        });

        try {
            const response = await apiGetDocketDetailById(docketId);
            if (response && response.data) {
                const data = response.data;

                const { formControl, formData } = validFormControlAndData(data.formControl, data.formData, data.predefinedField);

                if (data) {
                    dispatch({
                        type: DOCKETDETAIL_GET_DOCKETDETAIL_BY_DOCKETID_SUCCESS,
                        docketDetail: data,
                        formControl,
                        formData,
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: DOCKETDETAIL_GET_DOCKETDETAIL_BY_DOCKETID_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};

export const updateFormDataByDocketId = (docketId, formData, onSuccessCallback) => {
    return async (dispatch, getState) => {
        dispatch({
            type: DOCKETDETAIL_UPDATE_FORMDATA_BY_DOCKETID_LOADING
        });

        try {
            const response = await apiUpdateFormDataByDocketId(docketId, formData);
            if (response && response.data) {
                const data = response.data;

                if (data.code === 200 && data.message === "OK") {
                    await dispatch({
                        type: DOCKETDETAIL_UPDATE_FORMDATA_BY_DOCKETID_SUCCESS,
                    });
                    if (onSuccessCallback) {
                        onSuccessCallback();
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: DOCKETDETAIL_UPDATE_FORMDATA_BY_DOCKETID_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};

export const approveDocket = (docketId, onSuccessCallback) => {
    return async (dispatch, getState) => {
        dispatch({
            type: DOCKETDETAIL_APPROVE_DOCKET_LOADING
        });

        try {
            const response = await apiApproveDocket(docketId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: DOCKETDETAIL_APPROVE_DOCKET_SUCCESS,
                        approveStatus: data
                    });
                    if (data.code === 200) {
                        message.success("Đã duyệt công việc thành công");
                        if (onSuccessCallback) {
                            onSuccessCallback();
                        }
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: DOCKETDETAIL_APPROVE_DOCKET_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};

export const confirmDocket = (docketId, onSuccessCallback) => {
    return async (dispatch, getState) => {
        dispatch({
            type: DOCKETDETAIL_CONFIRM_DOCKET_LOADING
        });

        try {
            const response = await apiConfirmDocket(docketId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: DOCKETDETAIL_CONFIRM_DOCKET_SUCCESS,
                        confirmStatus: data
                    });
                    if (data.code === 200) {
                        message.success("Đã xác nhận công việc hoàn thành");
                        if (onSuccessCallback) {
                            onSuccessCallback();
                        }
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: DOCKETDETAIL_CONFIRM_DOCKET_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};

export const declineDocket = (docketId, reason, onSuccessCallback) => {
    return async (dispatch, getState) => {
        dispatch({
            type: DOCKETDETAIL_DECLINE_DOCKET_LOADING
        });

        try {
            const response = await apiDeclineDocket(docketId, reason);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: DOCKETDETAIL_DECLINE_DOCKET_SUCCESS,
                        declineStatus: data
                    });
                    if (data.code === 200) {
                        message.success("Công việc đã được trả về cho Thầu chính");
                        if (onSuccessCallback) {
                            onSuccessCallback();
                        }
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: DOCKETDETAIL_DECLINE_DOCKET_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};

export const rejectDocket = (docketId, reason, onSuccessCallback) => {
    return async (dispatch, getState) => {
        dispatch({
            type: DOCKETDETAIL_REJECT_DOCKET_LOADING
        });

        try {
            const response = await apiRejectDocket(docketId, reason);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: DOCKETDETAIL_REJECT_DOCKET_SUCCESS,
                        rejectStatus: data
                    });
                    if (data.code === 200) {
                        message.success("Công việc đã được trả về cho Công nhân làm lại");
                        if (onSuccessCallback) {
                            onSuccessCallback();
                        }
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: DOCKETDETAIL_REJECT_DOCKET_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};

export const returnDocketBySub = (docketId, reason, onSuccessCallback) => {
    return async (dispatch, getState) => {
        dispatch({
            type: DOCKETDETAIL_RETURN_DOCKET_LOADING
        });

        try {
            const response = await apiReturnDocketBySub(docketId, reason);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: DOCKETDETAIL_RETURN_DOCKET_SUCCESS,
                        returnStatus: data
                    });
                    if (data.code === 200) {
                        message.success("Công việc đã được trả về cho Công nhân làm lại");
                        if (onSuccessCallback) {
                            onSuccessCallback();
                        }
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: DOCKETDETAIL_REJECT_DOCKET_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};


export const changeToNeedApproveDocket = (docketId, onSuccessCallback) => {
    return async (dispatch, getState) => {
        dispatch({
            type: DOCKETDETAIL_NEEDAPPROVE_DOCKET_LOADING
        });

        try {
            const response = await apiNeedApproveDocket(docketId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: DOCKETDETAIL_NEEDAPPROVE_DOCKET_SUCCESS,
                        needApproveStatus: data
                    });
                    if (data.code === 200) {
                        message.success("Đã xác nhận công việc thành công");
                        if (onSuccessCallback) {
                            onSuccessCallback();
                        }
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: DOCKETDETAIL_NEEDAPPROVE_DOCKET_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};

export const changeToFormFilledDocket = (docketId, onSuccessCallback) => {
    return async (dispatch, getState) => {
        dispatch({
            type: DOCKETDETAIL_FORMFILLED_DOCKET_LOADING
        });

        try {
            const response = await apiFormFilledDocket(docketId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: DOCKETDETAIL_FORMFILLED_DOCKET_SUCCESS,
                        formFilledStatus: data
                    });
                    if (data.code === 200) {
                        message.success("Đã lưu và gửi qua Thầu phụ");
                        if (onSuccessCallback) {
                            onSuccessCallback();
                        }
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: DOCKETDETAIL_FORMFILLED_DOCKET_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        };
    };
};