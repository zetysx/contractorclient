import { apiGetAllLocation } from "../apis/user/locationApi";
import { catchRequestCommonError } from "../helpers/actionHelper";

export const LOCATION_LOADING = "LOCATION_LOADING";
export const LOCATION_LOAD_SUCCESS = "LOCATION_LOAD_SUCCESS";
export const LOCATION_FAIL = "LOCATION_FAIL";


export const loadAllLocations = () => {
    return async (dispatch, getState) => {
        const locations = getState().locationReducer.locations;
        if (locations && locations.length === 0) {
            dispatch({
                type: LOCATION_LOADING
            });
            try {
                const response = await apiGetAllLocation();
                if (response && response.data) {
                    const data = response.data;
                    if (data) {
                        dispatch({
                            type: LOCATION_LOAD_SUCCESS,
                            locations: data
                        });
                    }
                }
            } catch (error) {
                dispatch({
                    type: LOCATION_FAIL
                });
                catchRequestCommonError(error, dispatch);
            }
        }
    };
};