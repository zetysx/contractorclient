import {
    apiGetAllDocketFromContractId,
} from "../apis/user/docketApi";

import {
    apiPrimeFillAllDocket,
} from "../apis/prime/docketApi";
import { message } from "antd";
import { catchRequestCommonError } from "../helpers/actionHelper";


export const DOCKETLISTBYCONTRACTID_ALL_DOCKET_BY_CONTRACT_ID_LOADING = "DOCKETLISTBYCONTRACTID_ALL_DOCKET_BY_CONTRACT_ID_LOADING";
export const DOCKETLISTBYCONTRACTID_ALL_DOCKET_BY_CONTRACT_ID_SUCCESS = "DOCKETLISTBYCONTRACTID_ALL_DOCKET_BY_CONTRACT_ID_SUCCESS";
export const DOCKETLISTBYCONTRACTID_ALL_DOCKET_BY_CONTRACT_ID_FAIL = "DOCKETLISTBYCONTRACTID_ALL_DOCKET_BY_CONTRACT_ID_FAIL";

export const DOCKETLISTBYCONTRACTID_PRIME_ADD_DOCKET_SUBMIT_LOADING = "DOCKETLISTBYCONTRACTID_PRIME_ADD_DOCKET_SUBMIT_LOADING";
export const DOCKETLISTBYCONTRACTID_PRIME_ADD_DOCKET_SUBMIT_FAIL = "DOCKETLISTBYCONTRACTID_PRIME_ADD_DOCKET_SUBMIT_FAIL";
export const DOCKETLISTBYCONTRACTID_PRIME_ADD_DOCKET_SUBMIT_LOAD_SUCCESS = "DOCKETLISTBYCONTRACTID_PRIME_ADD_DOCKET_SUBMIT_LOAD_SUCCESS";


export const loadAllDocketByContractId = (contractId) => {
    return async (dispatch) => {
        dispatch({
            type: DOCKETLISTBYCONTRACTID_ALL_DOCKET_BY_CONTRACT_ID_LOADING
        });

        try {
            const response = await apiGetAllDocketFromContractId(contractId);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: DOCKETLISTBYCONTRACTID_ALL_DOCKET_BY_CONTRACT_ID_SUCCESS,
                        dockets: data
                    });
                }
            }
        } catch (error) {
            dispatch({
                type: DOCKETLISTBYCONTRACTID_ALL_DOCKET_BY_CONTRACT_ID_FAIL
            });
            catchRequestCommonError(error, dispatch);
        }
    };
};

export const fillAllDocket = (dockets, onSuccessCallback) => {
    return async (dispatch) => {
        dispatch({
            type: DOCKETLISTBYCONTRACTID_PRIME_ADD_DOCKET_SUBMIT_LOADING
        });

        try {
            const response = await apiPrimeFillAllDocket(dockets);
            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: DOCKETLISTBYCONTRACTID_PRIME_ADD_DOCKET_SUBMIT_LOAD_SUCCESS,
                        message: data
                    });
                    if (data.code === 200) {
                        message.success('Đã chọn công việc thành công');
                        if (onSuccessCallback) {
                            onSuccessCallback();
                        }
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: DOCKETLISTBYCONTRACTID_PRIME_ADD_DOCKET_SUBMIT_FAIL,
            });
            catchRequestCommonError(error, dispatch);
        }

    };
};
