import { apiGetContractPayment } from "../apis/user/paymentApi";
import { catchRequestCommonError } from "../helpers/actionHelper";

export const CONTRACT_GET_CONTRACT_PAYMENT_LOADING = "CONTRACT_GET_CONTRACT_PAYMENT_LOADING";
export const CONTRACT_GET_CONTRACT_PAYMENT_SUCCESS = "CONTRACT_GET_CONTRACT_PAYMENT_SUCCESS";

export const loadContractPayment = (contractId) => {
    return async (dispatch) => {

        dispatch({
            type: CONTRACT_GET_CONTRACT_PAYMENT_LOADING
        });
        try {
            const response = await apiGetContractPayment(contractId);

            if (response && response.data) {
                const data = response.data;
                if (data) {
                    dispatch({
                        type: CONTRACT_GET_CONTRACT_PAYMENT_SUCCESS,
                        contractPayment: data
                    });
                }
            }
        } catch (error) {
            //[reviewcode] không có action fail, không lẽ nếu load không được thì hiện loading mãi
            catchRequestCommonError(error, dispatch);
        }
    };
};