import { apiGetAuthTokenAndLogin } from "../apis/auth";
import { apiGetUserGeneralInfo, apiRemoveAuthTokenAndLogout } from "../apis/user/general";
import { setAccessToken, setRefreshToken, setTokenExpires, removeToken, checkAccessToken } from "../helpers/authHelper";
import { catchRequestCommonError } from "../helpers/actionHelper";

export const GET_AUTH_TOKEN_AND_LOGIN_LOADING = "GET_AUTH_TOKEN_AND_LOGIN_LOADING";
export const GET_AUTH_TOKEN_AND_LOGIN_SUCCESS = "GET_AUTH_TOKEN_AND_LOGIN_SUCCESS";
export const GET_AUTH_TOKEN_AND_LOGIN_FAILURE = "GET_AUTH_TOKEN_AND_LOGIN_FAILURE";
export const REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS = "REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS";

export const getAuthTokenAndLogin = (username, password, onLoginSuccessCallback, onWrongUsernamePasswordCallback) => {
    return async (dispatch) => {
        dispatch({
            type: GET_AUTH_TOKEN_AND_LOGIN_LOADING
        });

        try {
            //get token
            const response = await apiGetAuthTokenAndLogin(username, password);

            if (response && response.data) {
                const data = response.data;
                await setAccessToken(data["access_token"]);
                await setRefreshToken(data["refresh_token"]);
                await setTokenExpires(data["expires_in"]);

                //gọi API lấy userInfo dựa trên token
                const generalInfoRespone = await apiGetUserGeneralInfo();
                if (generalInfoRespone && generalInfoRespone.data) {
                    await dispatch({
                        type: GET_AUTH_TOKEN_AND_LOGIN_SUCCESS,
                        profile: generalInfoRespone.data
                    });
                    if (onLoginSuccessCallback) {
                        onLoginSuccessCallback();
                    }
                }
            }
        } catch (error) {
            dispatch({
                type: GET_AUTH_TOKEN_AND_LOGIN_FAILURE
            });

            catchRequestCommonError(error, dispatch);

            if (error.response) {
                if (error.response.status === 400) {
                    // đây là case sai username hoặc password -> server trả về 400 với data bad-cendenti
                    if (onWrongUsernamePasswordCallback) {
                        onWrongUsernamePasswordCallback();
                    }
                }
            }
        };
    };
};

export const checkTokenAndGetUserGeneralInfo = (onCheckSuccessCallback, onCheckFailCallback) => {
    return async (dispatch) => {
        if (!checkAccessToken()) {
            if (onCheckFailCallback) {
                onCheckFailCallback();
            }
            return;
        }

        try {
            dispatch({
                type: GET_AUTH_TOKEN_AND_LOGIN_LOADING
            });

            //gọi API lấy userInfo dựa trên token
            const generalInfoRespone = await apiGetUserGeneralInfo();
            if (generalInfoRespone && generalInfoRespone.data) {
                await dispatch({
                    type: GET_AUTH_TOKEN_AND_LOGIN_SUCCESS,
                    profile: generalInfoRespone.data
                });

                if (onCheckSuccessCallback) {
                    onCheckSuccessCallback();
                }
            }
        } catch (error) {
            dispatch({
                type: GET_AUTH_TOKEN_AND_LOGIN_FAILURE
            });
            catchRequestCommonError(error, dispatch);

            if (onCheckFailCallback) {
                onCheckFailCallback();
            }
        };
    }
};

export const removeAuthTokenAndLogout = () => {
    return async (dispatch) => {
        try {
            //gọi API lấy userInfo dựa trên token
            const response = await apiRemoveAuthTokenAndLogout();
            if (response && response.data) {
                removeToken();
                dispatch({
                    type: REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS,
                });
            }
        } catch (error) {
            removeToken();
            dispatch({
                type: REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS,
            });
        };
    }
};