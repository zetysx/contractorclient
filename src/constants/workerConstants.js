
export const ICON_ANTD_CONSTANTS = {
    USER_DELETE_ICON: "user-delete",
    UNLOCK_ICON: "unlock",
    LOCK_ICON: "lock",
    EDIT_ICON: "edit",
    USER_ADD: "user-add",
    USER: "user",
    PHONE: "phone",
    ENVIRONMENT: "environment",
    MAIL: "mail",
    BANK: "bank",
    PROFILE: "profile",
    CONTACT: "contacts",
    BUSINESS_LICENSE_CERTIFICATED: "safety-certificate",
    AUDIT: "audit",
    PROJECT: "project"

}