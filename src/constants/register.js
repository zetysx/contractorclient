export const CONST_INPUT_FIELDS_NAME = {
    USERNAME: "username",
    PASSWORD: "password",
    PHONE: "phone",
    EMAIL: "email",
    ADDRESS: "address",
    FIRSTNAME: "firstName",
    LASTNAME: "lastName",
    COMPANY_NAME: "companyName",
    BUSINESS_LICENSE: "businessLicense",
    FAX_NUMBER: "faxNumber",
    SUB_DESCRIPTION: "description",
    AVATAR: "avatar"
};

export const CONST_INPUT_PLACEHOLDER = {
    USERNAME: "Tên tài khoản *",
    PASSWORD: "Mật khẩu *",
    PHONE: "Số điện thoại *",
    EMAIL: "Email *",
    ADDRESS: "Địa chỉ *",
    FIRST_NAME: "Họ *",
    LAST_NAME: "Tên *",
    COMPANY_NAME: "Tên doanh nghiệp",
    BUSINESS_LICENSE: "Giấy phép kinh doanh",
    FAX_NUMBER: "Số fax",
    SUB_DESCRIPTION: "Lời giới thiệu về doanh nghiệp"
};

export const APP_STRING = {
    PRIME: "Thầu chính",
    SUB: "Thầu phụ",
    WOKRER: "Công nhân"
}

