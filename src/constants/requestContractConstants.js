
export const TABLE_LIST_CONFIG = {
    RATING: {
        DATAINDEX_KEY: "rate",
        HEADER: "Đánh giá"
    },
    CONTRACT_CODE: {
        DATAINDEX_KEY: "id",
        HEADER: "Mã"
    },
    CONTRACT: {
        DATAINDEX_KEY: "id",
        HEADER: "Hợp đồng"
    },
    SUBCONTRACTOR: {
        DATAINDEX_KEY: "subCompanyName",
        HEADER: "Thầu phụ"
    },
    PRIMECONTRACTOR: {
        DATAINDEX_KEY: "primeCompanyName",
        HEADER: "Thầu chính"
    },
    PROFESSION: {
        DATAINDEX_KEY: "profession",
        HEADER: "Lĩnh vực"
    },
    PRICE_PER_DOCKET: {
        DATAINDEX_KEY: "pricePerDocket",
        HEADER: "Đơn giá"
    },
    START_DATE: {
        DATAINDEX_KEY: "startDate",
        HEADER: "Bắt đầu"
    },
    END_DATE: {
        DATAINDEX_KEY: "endDate",
        HEADER: "Kết thúc"
    },
    CONFIRMED_DATE: {
        DATAINDEX_KEY: "confirmDate",
        HEADER: "Ngày nhận"
    },
    REQUESTED_DATE: {
        DATAINDEX_KEY: "requestDate",
        HEADER: "Ngày gửi"
    },
    WORKER_NUMBER: {
        DATAINDEX_KEY: "workerNumber",
        HEADER: "SL Nhân công"
    },
    LOCATION: {
        DATAINDEX_KEY: "location",
        HEADER: "Địa điểm"
    },
    CONTRACT_STATUS: {
        DATAINDEX_KEY: "status",
        HEADER: "Trạng thái"
    },
    TOTAL_DOCKET: {
        DATAINDEX_KEY: "totalDocket",
        HEADER: "SL công việc"
    },
    TOTAL_MONEY: {
        DATAINDEX_KEY: "totalMoney",
        HEADER: "Tổng tiền"
    },
    ACTION: {
        PAYMENT: {
            DATAINDEX_KEY: "payment",
            HEADER: "Thanh toan"
        }
    },
    COMPLETE_DATE: {
        DATAINDEX_KEY: "completeDate",
        HEADER: "Hoàn thành"
    },
    STARTDATE_ENDDATE: {
        DATAINDEX_KEY: "time",
        HEADER: "Thời gian"
    },
    FORM: {
        DATAINDEX_KEY: "form",
        HEADER: "Hợp đồng"
    },
    ACCOUNT: {
        ID: {
            DATAINDEX_KEY: "id",
            HEADER: "Mã"
        },
        USERNAME: {
            DATAINDEX_KEY: "username",
            HEADER: "Tên đăng nhập"
        },
        FIRST_NAME: {
            DATAINDEX_KEY: "firstName",
            HEADER: "Họ"
        },
        LAST_NAME: {
            DATAINDEX_KEY: "lastName",
            HEADER: "Tên"
        },
        PHONE: {
            DATAINDEX_KEY: "phone",
            HEADER: "Số điện thoại"
        },
        EMAIL: {
            DATAINDEX_KEY: "email",
            HEADER: "Email"
        },
        ADDRESS: {
            DATAINDEX_KEY: "address",
            HEADER: "Địa chỉ"
        },
        PASSWORD: {
            DATAINDEX_KEY: "password",
            HEADER: "Mật khẩu"
        },
        AVATAR: {
            DATAINDEX_KEY: "avatar",
            HEADER: "Ảnh đại diện"
        },
        STATUS: {
            DATAINDEX_KEY: "status",
            HEADER: "Trạng thái"
        },
    },
    PROGRESS: {
        DATAINDEX_KEY: "progress",
        HEADER: "Tiến trình"
    },
    ONGOING_CONTRACT: {
        DATAINDEX_KEY: "onGoingContract",
        HEADER: "Dự án đang tham gia"
    },
    ONGOING_CONTRACT_NUMBER: {
        DATAINDEX_KEY: "onGoingContractNumber",
        HEADER: "SL dự án đang tham gia"
    },
    WORKER_PROFESSION: {
        DATAINDEX_KEY: "workerProfessions",
        HEADER: "Lĩnh vực của nhân công"
    }, 
    WORKER: {
        DATAINDEX_KEY: "worker",
        HEADER: "Nhân công"
    }
}

export const REQUEST_CONTRACT_STATUS = {
    NOT_CONFIRM: 0,
    DECLINE: 1,
    AUTO_SEARCHING: 2,
    AUTO_SEARCH_DONE: 3,
    WORKER_ASSIGNED: 4,
    DOCKETS_DETAIL_FILLED: 5,
    COMPLETE: 6
};

