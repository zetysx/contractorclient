export const REQEXP_CHECK_NATURAL_NUMBER = "/^+$/";
export const REQEXP_CHECK_DECIMAL_NUMBER = "/^[0-9.]+$/";
export const REQEXP_CHECK_EMAIL = "^.+@[^\.].*\.[a-z]{2,}$";
{/*
    Check if: String has 2 more characters: 
        - ^ => matches the begging of a string
        - $ => matches the end of a string
        - \w => matches any words
        - + => need 1 more character
        - \S => matches any character that is not white space or tab or line break 
*/}
export const REQEXP_CHECK_EMPTY_STRING = "^(\w+\S+)$";

// export const REQEXP_CHECK_EMAIL = 