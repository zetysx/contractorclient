export const DOCKETSTATUS = {
    INIT : 0,
    WILL_WORK: 1,
    FILLED : 2,
    CONFIRMED : 3,
    DECLINED : 4,
    WORKING : 5,
    SUB_CHECK: 6,
    NEED_APPROVE : 7,
    APPROVED : 8,
    REJECTED : 9,
    NOT_WORK : 10,
};