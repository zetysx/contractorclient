export const REGISTER_ERROR_MESSAGE = {
    DUPLICATED_USERNAME         : "Tên đăng nhập này đã tồn tại, vui lòng chọn tên đăng nhập khác",
    BLANK_REQUIRED_FIELDS       : "Vui lòng không để trống những trường *",
    PROFESSION_PRICE            : "Giá tiền cho từng lĩnh vực phải lớn hơn 1.000 VNĐ",
    EMAIL_FORMAT                : "Email phải đúng format abc@abc.abc",
    LENGTH                      : '%s giới hạn %d tới %d kí tự',
    PHONE                       : "Số điện thoại giới hạn 8 -> 12 kí tự và không chứa các kí tự khác số"
}