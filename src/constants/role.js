export const ROLESID = {
    PRIME: 1,
    SUB: 2,
    WORKER: 3,
    ADMIN: 4
};

export const ROLESNAME = {
    PRIME: "Thầu chính",
    SUB: "Thầu phụ",
    WORKER: "Công nhân",
    ADMIN: "Admin"
};