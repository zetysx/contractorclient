export const FORM_CONTROL_TYPE = {
    STATIC: "static",
    INPUT: "input",
    PREDEFINED: "predefined",
};

export const FORM_STATIC_CONTROL_TYPE = {
    TEXT: "text",
    IMAGE: "image",
};

export const FORM_INPUT_CONTROL_TYPE = {
    TEXT: "text",
    IMAGE: "image",
    SIGNATURE: "signature",
    DATE: "date",
    NUMBER: "number",
    CHECKBOX: "checkbox",
};

export const FORM_INPUT_CONTROL_ROLE = {
    PRIME: 1,
    SUB: 2,
    WORKER: 3,
};

export const FORM_PREDEFINED_CONTROL_TYPE = {
    PRIMECONTC_COMPANYNAME: "primeContcCompanyname",
    PRIMECONTC_FULLNAME: "primeContcFullname",
    PRIMECONTC_PHONE: "primeContcPhone",
    PRIMECONTC_ADDRESS: "primeContcAddress",

    SUBCONTC_COMPANYNAME: "subContcCompanyname",
    SUBCONTC_FULLNAME: "subContcFullname",
    SUBCONTC_PHONE: "subContcPhone",
    SUBCONTC_ADDRESS: "subContcAddress",

    CONTRACT_PROFESSIONNAME: "contractProfessionname",
    CONTRACT_PRICEPERDOCKET: "contractPricePerDocket",
    CONTRACT_WORKERNUMBER: "contractWorkerNumber",

    DOCKET_DATE: "docketDate",
    DOCKET_LIST_WORKER_FULLNAME: "docketListWorkerFullname",
    DOCKET_LIST_WORKER_PHONE: "docketListWorkerPhone",
};

export const FORM_PREDEFINED_CONTROL_LABEL = {
    PRIMECONTC_COMPANYNAME: "Tên công ty nhà thầu chính",
    PRIMECONTC_FULLNAME: "Tên nhà thầu chính",
    PRIMECONTC_PHONE: "Số điện thoại nhà thầu chính",
    PRIMECONTC_ADDRESS: "Địa chỉ nhà thầu chính",

    SUBCONTC_COMPANYNAME: "Tên công ty nhà thầu phụ",
    SUBCONTC_FULLNAME: "Tên nhà thầu phụ",
    SUBCONTC_PHONE: "Số điện thoại nhà thầu phụ",
    SUBCONTC_ADDRESS: "Địa chỉ nhà thầu phụ",

    CONTRACT_PROFESSIONNAME: "Chuyên ngành làm việc",
    CONTRACT_PRICEPERDOCKET: "Giá tiền mỗi nhân công",
    CONTRACT_WORKERNUMBER: "Số lượng nhân công",

    DOCKET_DATE: "Ngày làm việc",
    DOCKET_LIST_WORKER_FULLNAME: "Danh sách công nhân",
    DOCKET_LIST_WORKER_PHONE: "Danh sách số điện thoại công nhân",
};

export const FORM_CONTROL_DEFAULT_FORMAT = {
    fontSize: 14,
    bold: false,
    italic: false,
    align: "left",
    borderSize: 2,
    borderTop: false,
    borderBottom: false,
    borderLeft: false,
    borderRight: false,
};

export const FORM_ADD_TEMPLATE = {
    staticControl: {
        panelHeader: "Cố định",
        controlData: [
            {
                panelLabel: "Văn bản",
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 120, height: 40 },
                    config: { type: FORM_CONTROL_TYPE.STATIC, statictype: FORM_STATIC_CONTROL_TYPE.TEXT, value: "Văn bản" },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
            {
                panelLabel: "Hình ảnh",
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 400, height: 225 },
                    config: { type: FORM_CONTROL_TYPE.STATIC, statictype: FORM_STATIC_CONTROL_TYPE.IMAGE, value: "Hình ảnh" },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            }
        ]
    },
    inputControl: {
        panelHeader: "Nhập liệu",
        controlData: [
            {
                panelLabel: "Văn bản",
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 120, height: 40 },
                    config: { type: FORM_CONTROL_TYPE.INPUT, inputtype: FORM_INPUT_CONTROL_TYPE.TEXT, role: FORM_INPUT_CONTROL_ROLE.PRIME, isrequire: false, label: "Nhập văn bản" },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
            {
                panelLabel: "Hình ảnh",
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 255, height: 400 },
                    config: { type: FORM_CONTROL_TYPE.INPUT, inputtype: FORM_INPUT_CONTROL_TYPE.IMAGE, role: FORM_INPUT_CONTROL_ROLE.PRIME, isrequire: false, label: "Chụp hình ảnh" },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
            {
                panelLabel: "Chữ ký",
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 300, height: 300 },
                    config: { type: FORM_CONTROL_TYPE.INPUT, inputtype: FORM_INPUT_CONTROL_TYPE.SIGNATURE, role: FORM_INPUT_CONTROL_ROLE.PRIME, isrequire: false, label: "Ký tên" },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
            {
                panelLabel: "Ngày tháng",
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 80, height: 40 },
                    config: { type: FORM_CONTROL_TYPE.INPUT, inputtype: FORM_INPUT_CONTROL_TYPE.DATE, role: FORM_INPUT_CONTROL_ROLE.PRIME, isrequire: false, label: "Nhập ngày" },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
            {
                panelLabel: "Số lượng",
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 60, height: 40 },
                    config: { type: FORM_CONTROL_TYPE.INPUT, inputtype: FORM_INPUT_CONTROL_TYPE.NUMBER, role: FORM_INPUT_CONTROL_ROLE.PRIME, isrequire: false, label: "Nhập số lượng" },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
            // {
            //     panelLabel: "Hộp kiểm",
            //     controlTemp: {
            //         pos: { ox: 0, oy: 0, width: 40, height: 40 },
            //         config: { type: FORM_CONTROL_TYPE.INPUT, inputtype: FORM_INPUT_CONTROL_TYPE.CHECKBOX, role: FORM_INPUT_CONTROL_ROLE.PRIME, isrequire: false, label: "Đánh dấu" },
            //         format: FORM_CONTROL_DEFAULT_FORMAT,
            //     },
            // }
        ]
    },
    predefinedControl: {
        panelHeader: "Định sẵn",
        controlData: [
            {
                panelLabel: FORM_PREDEFINED_CONTROL_LABEL.PRIMECONTC_COMPANYNAME,
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 160, height: 40 },
                    config: { type: FORM_CONTROL_TYPE.PREDEFINED, predefinedtype: FORM_PREDEFINED_CONTROL_TYPE.PRIMECONTC_COMPANYNAME, label: FORM_PREDEFINED_CONTROL_LABEL.PRIMECONTC_COMPANYNAME },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
            {
                panelLabel: FORM_PREDEFINED_CONTROL_LABEL.PRIMECONTC_FULLNAME,
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 160, height: 40 },
                    config: { type: FORM_CONTROL_TYPE.PREDEFINED, predefinedtype: FORM_PREDEFINED_CONTROL_TYPE.PRIMECONTC_FULLNAME, label: FORM_PREDEFINED_CONTROL_LABEL.PRIMECONTC_FULLNAME },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
            {
                panelLabel: FORM_PREDEFINED_CONTROL_LABEL.PRIMECONTC_PHONE,
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 160, height: 40 },
                    config: { type: FORM_CONTROL_TYPE.PREDEFINED, predefinedtype: FORM_PREDEFINED_CONTROL_TYPE.PRIMECONTC_PHONE, label: FORM_PREDEFINED_CONTROL_LABEL.PRIMECONTC_PHONE },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
            {
                panelLabel: FORM_PREDEFINED_CONTROL_LABEL.PRIMECONTC_ADDRESS,
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 240, height: 40 },
                    config: { type: FORM_CONTROL_TYPE.PREDEFINED, predefinedtype: FORM_PREDEFINED_CONTROL_TYPE.PRIMECONTC_ADDRESS, label: FORM_PREDEFINED_CONTROL_LABEL.PRIMECONTC_ADDRESS },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
            {
                panelLabel: FORM_PREDEFINED_CONTROL_LABEL.SUBCONTC_COMPANYNAME,
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 160, height: 40 },
                    config: { type: FORM_CONTROL_TYPE.PREDEFINED, predefinedtype: FORM_PREDEFINED_CONTROL_TYPE.SUBCONTC_COMPANYNAME, label: FORM_PREDEFINED_CONTROL_LABEL.SUBCONTC_COMPANYNAME },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
            {
                panelLabel: FORM_PREDEFINED_CONTROL_LABEL.SUBCONTC_FULLNAME,
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 160, height: 40 },
                    config: { type: FORM_CONTROL_TYPE.PREDEFINED, predefinedtype: FORM_PREDEFINED_CONTROL_TYPE.SUBCONTC_FULLNAME, label: FORM_PREDEFINED_CONTROL_LABEL.SUBCONTC_FULLNAME },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
            {
                panelLabel: FORM_PREDEFINED_CONTROL_LABEL.SUBCONTC_PHONE,
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 160, height: 40 },
                    config: { type: FORM_CONTROL_TYPE.PREDEFINED, predefinedtype: FORM_PREDEFINED_CONTROL_TYPE.SUBCONTC_PHONE, label: FORM_PREDEFINED_CONTROL_LABEL.SUBCONTC_PHONE },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
            {
                panelLabel: FORM_PREDEFINED_CONTROL_LABEL.SUBCONTC_ADDRESS,
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 240, height: 40 },
                    config: { type: FORM_CONTROL_TYPE.PREDEFINED, predefinedtype: FORM_PREDEFINED_CONTROL_TYPE.SUBCONTC_ADDRESS, label: FORM_PREDEFINED_CONTROL_LABEL.SUBCONTC_ADDRESS },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
            {
                panelLabel: FORM_PREDEFINED_CONTROL_LABEL.CONTRACT_PROFESSIONNAME,
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 160, height: 40 },
                    config: { type: FORM_CONTROL_TYPE.PREDEFINED, predefinedtype: FORM_PREDEFINED_CONTROL_TYPE.CONTRACT_PROFESSIONNAME, label: FORM_PREDEFINED_CONTROL_LABEL.CONTRACT_PROFESSIONNAME },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
            {
                panelLabel: FORM_PREDEFINED_CONTROL_LABEL.CONTRACT_PRICEPERDOCKET,
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 80, height: 40 },
                    config: { type: FORM_CONTROL_TYPE.PREDEFINED, predefinedtype: FORM_PREDEFINED_CONTROL_TYPE.CONTRACT_PRICEPERDOCKET, label: FORM_PREDEFINED_CONTROL_LABEL.CONTRACT_PRICEPERDOCKET },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
            {
                panelLabel: FORM_PREDEFINED_CONTROL_LABEL.CONTRACT_WORKERNUMBER,
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 80, height: 40 },
                    config: { type: FORM_CONTROL_TYPE.PREDEFINED, predefinedtype: FORM_PREDEFINED_CONTROL_TYPE.CONTRACT_WORKERNUMBER, label: FORM_PREDEFINED_CONTROL_LABEL.CONTRACT_WORKERNUMBER },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
            {
                panelLabel: FORM_PREDEFINED_CONTROL_LABEL.DOCKET_DATE,
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 160, height: 40 },
                    config: { type: FORM_CONTROL_TYPE.PREDEFINED, predefinedtype: FORM_PREDEFINED_CONTROL_TYPE.DOCKET_DATE, label: FORM_PREDEFINED_CONTROL_LABEL.DOCKET_DATE },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
            {
                panelLabel: FORM_PREDEFINED_CONTROL_LABEL.DOCKET_LIST_WORKER_FULLNAME,
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 200, height: 40 },
                    config: { type: FORM_CONTROL_TYPE.PREDEFINED, predefinedtype: FORM_PREDEFINED_CONTROL_TYPE.DOCKET_LIST_WORKER_FULLNAME, label: FORM_PREDEFINED_CONTROL_LABEL.DOCKET_LIST_WORKER_FULLNAME },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
            {
                panelLabel: FORM_PREDEFINED_CONTROL_LABEL.DOCKET_LIST_WORKER_PHONE,
                controlTemp: {
                    pos: { ox: 0, oy: 0, width: 200, height: 40 },
                    config: { type: FORM_CONTROL_TYPE.PREDEFINED, predefinedtype: FORM_PREDEFINED_CONTROL_TYPE.DOCKET_LIST_WORKER_PHONE, label: FORM_PREDEFINED_CONTROL_LABEL.DOCKET_LIST_WORKER_PHONE },
                    format: FORM_CONTROL_DEFAULT_FORMAT,
                },
            },
        ]
    }
};

export const A4WIDTH = 1240;
export const A4HEIGHT = 1754;
export const DEFAULT_SCALE_PDF = 0.48;
export const FORMCONTROL_MIN_WIDTH = 40;
export const FORMCONTROL_MIN_HEIGHT = 40;
export const DRAG_RESIZE_GRID_NUMBER = 20;
