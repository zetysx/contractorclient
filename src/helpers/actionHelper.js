import { message } from 'antd';
import { removeToken } from "../helpers/authHelper";
import { REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS } from "../actions/authAction";


export const catchRequestCommonError = (error, dispatch, onNetworkProblemCallback) => {
    if (error.response) {
        // có response từ server nhưng status của respone khác 2xx

        // console.log(error.response.data);
        // console.log(error.response.status);
        // console.log(error.response.headers);
        if (error.response.status === 401 || error.response.status === 403) {
            message.error("Bạn phải đăng nhập để tiếp tục!");
            removeToken();
            dispatch({
                type: REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS,
            });
            return;
        }

        if (error.response.status === 404) {
            message.error("Send request bị 404 rồi. Xem lại cái URL đúng chưa!");
            return;
        }

    } else if (error.request) {
        //có request nhưng không có response từ server
        message.error("Kết nối mạng gặp vấn đề. Vui lòng thử lại!");
        if (onNetworkProblemCallback) {
            onNetworkProblemCallback();
        }
    } else {
        // lỗi không phải của axios
        message.error("Lỗi không liên quan đến gọi API - xem trong console: ", error.message);
        console.log(error);
    }
};