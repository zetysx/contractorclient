
import { 
    REQEXP_CHECK_NATURAL_NUMBER,
    REQEXP_CHECK_DECIMAL_NUMBER,
    REQEXP_CHECK_EMAIL,
    REQEXP_CHECK_EMPTY_STRING
} from "../constants/reqexp";
import { REGISTER_ERROR_MESSAGE } from "../constants/message";


const checkEmptyNull = (value) => {
    return value.match(REQEXP_CHECK_EMPTY_STRING);
};

const checkFormatEmail = (email) => {
    return email.match(REQEXP_CHECK_EMAIL);
};

//Not correct
const checkNaturalNumber = (phone) => {
    return phone.match(REQEXP_CHECK_NATURAL_NUMBER);
};

const checkDecimalNumber = (price) => {
    return price.match(REQEXP_CHECK_DECIMAL_NUMBER);
};

export const validateLength = (value, minLength, maxLength) => {
    if(value && minLength >=0 && maxLength >= 0) {
        return value.length >= minLength && value.length <= maxLength;
    }
    return;
};

export const validatePriceProfessionsList = (profList) => {
    if(profList && profList.length > 0) {
        for(var pos in profList)
            if(profList[pos].price <= 1000) 
                return false;
        return true;
    }
    return null;    
}

export const validateEmail = (value, ref) => {
    let noError = true;
    if(!value)  noError = false;
    if(!checkFormatEmail(value)) {
        noError = false;
    } else if(!validateLength(value, 6, 100)) {
        noError = false;
    }
    if(!noError) {
        if(ref) ref.focus();
    }
    return noError;
};

export const validateFieldNullOfObject = (object) => {
    for(var pos in object) {
        if(!object[pos] || object[pos].length === 0) {
            return false;
        }
    }
    return true;
};


export const validatePhone = (value, ref) => {
    if(!validateLength(value, 8, 12)) {
        if(ref) ref.focus();
        return false;
    } 
    // else if(!checkNaturalNumber(value)) { /// Sai
    //     message.error("Số điện thoại không chứa các kí tự khác số");
    //     ref.focus();
    //     return false;
    // }
    return true;
};


export const validateRequiredData = (dataObj) => {
    if(dataObj) {
        if(!validateFieldNullOfObject(dataObj)) {
            return REGISTER_ERROR_MESSAGE.BLANK_REQUIRED_FIELDS;
        }
        if(dataObj.hasOwnProperty("email") && dataObj.email) {
            if(!validateEmail(dataObj.email)) {
                return REGISTER_ERROR_MESSAGE.EMAIL_FORMAT;
            }
        }
        if(dataObj.hasOwnProperty("phone") && dataObj.phone) {
            if(!validatePhone(dataObj.phone)) {
                return REGISTER_ERROR_MESSAGE.PHONE;
            }
        }
        if(dataObj.hasOwnProperty("username") && dataObj.username) {
            if(!validateLength(dataObj.username, 5, 50)) {
                return "Tên đăng nhập giới hạn từ 5 -> 50 kí tự";
            }
        }
        if(dataObj.hasOwnProperty("password") && dataObj.password) {
            if(!validateLength(dataObj.password, 5, 120)) {
                return "Mật khẩu giới hạn từ 5 -> 120 kí tự";
            }
        }
        if(dataObj.hasOwnProperty("address") && dataObj.address) {
            if(!validateLength(dataObj.address, 8, 120)) {
                return "Địa chỉ giới hạn từ 8 -> 120 kí tự";
            }
        }
        if(dataObj.hasOwnProperty("firstName") && dataObj.firstName) {
            if(!validateLength(dataObj.firstName, 1, 60)) {
                return "Họ giới hạn từ 1 -> 60 kí tự";
            }
        }
        if(dataObj.hasOwnProperty("lastName") && dataObj.lastName) {
            if(!validateLength(dataObj.lastName, 1, 60)) {
                return "Tên giới hạn từ 1 -> 60 kí tự";
            }
        }
        if(dataObj.hasOwnProperty("subcontractorProfessions") && dataObj.subcontractorProfessions) {
            if(!validatePriceProfessionsList(dataObj.subcontractorProfessions)) {
                return REGISTER_ERROR_MESSAGE.PROFESSION_PRICE;
            }
        }
        if(dataObj.hasOwnProperty("locationId") && dataObj.locationId) {
            if(dataObj.locationId.length <= 0) {
                return "Vui lòng chọn ít nhất một khu vực làm việc";
            }
        }
    }
    return null;
}


export const validateGeneralDataBounaries = (dataObj) => {
    if(dataObj) {
        if(dataObj.hasOwnProperty("avatar") && dataObj.avatar) {
            if(!validateLength(dataObj.avatar, 0, 120)) {
                return "Đường dẫn ảnh đại diện giới hạn 120 kí tự";
            }
        }
        if(dataObj.hasOwnProperty("companyName") && dataObj.companyName) {
            if(!validateLength(dataObj.companyName, 0, 60)) {
                return "Tên doanh nghiệp giới hạn 60 kí tự";
            }
        }
        if(dataObj.hasOwnProperty("faxNumber") && dataObj.faxNumber) {
            if(!validateLength(dataObj.faxNumber, 0, 20)) {
                return "Số fax giới hạn 20 kí tự";
            }
        }
        if(dataObj.hasOwnProperty("description") && dataObj.description) {
            if(!validateLength(dataObj.description, 0, 120)) {
                return "Lời giới thiệu giới hạn 120 kí tự";
            }
        }
        if(dataObj.hasOwnProperty("businessLicense") && dataObj.businessLicense) {
            if(!validateLength(dataObj.businessLicense, 0, 50)) {
                return "Lời giới thiệu giới hạn 50 kí tự";
            }
        }
    }
    return null;
}


