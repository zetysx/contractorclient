import {
    FORM_CONTROL_TYPE,
    FORM_STATIC_CONTROL_TYPE,
    FORM_INPUT_CONTROL_TYPE,
    FORM_INPUT_CONTROL_ROLE,
    FORM_PREDEFINED_CONTROL_TYPE,
    FORM_PREDEFINED_CONTROL_LABEL
} from '../constants/docketForm';
import {
    DOCKETSTATUS
} from "../constants/docketStatus";
import {
    ROLESID
} from "../constants/role";

export const validFormControlAndData = (formControlString, formDataString, predefinedField) => {
    try {
        const formControl = JSON.parse(formControlString);
        let formData = JSON.parse(formDataString);


        if (!formData || formData.length !== formControl.length) {
            formData = formControl.map((control) => {
                const { type, value, predefinedtype, label } = control.config;
                switch (type) {
                    case FORM_CONTROL_TYPE.STATIC: {
                        return value;
                    }
                    case FORM_CONTROL_TYPE.PREDEFINED: {
                        return predefinedField[predefinedtype];
                    }
                    case FORM_CONTROL_TYPE.INPUT: {
                        return null;
                    }
                    default:
                        return null;
                }
            });
        }

        return {
            formControl,
            formData
        }
    } catch (error) {
        console.log(error);
        alert("validFormControlAndDataError");
        return {
            formControl: [],
            formData: [],
        }
    }
};

export const getStyleFromFormat = (format) => {
    if (!format) {
        return {
            width: '100%',
            height: '100%',
        };
    }

    const { fontSize, bold, italic, align, borderSize, borderTop, borderBottom, borderLeft, borderRight } = format;

    return {
        width: '100%',
        height: '100%',
        fontSize: fontSize ? fontSize : undefined,
        fontWeight: bold ? "bold" : "normal",
        fontStyle: italic ? "italic" : "normal",
        textAlign: align ? align : undefined,
        borderTop: borderTop ? `${borderSize}px solid #333` : undefined,
        borderBottom: borderBottom ? `${borderSize}px solid #333` : undefined,
        borderLeft: borderLeft ? `${borderSize}px solid #333` : undefined,
        borderRight: borderRight ? `${borderSize}px solid #333` : undefined,
    };
};

export const getStyleFromFormatFormCreator = (format) => {
    if (!format) {
        return {
            width: "100%",
            height: "100%",
            border: "2px dashed #bbb",
        };
    }

    const { fontSize, bold, italic, align, borderSize, borderTop, borderBottom, borderLeft, borderRight } = format;

    return {
        width: '100%',
        height: '100%',
        fontSize: fontSize ? fontSize : undefined,
        fontWeight: bold ? "bold" : "normal",
        fontStyle: italic ? "italic" : "normal",
        textAlign: align ? align : undefined,
        borderTop: borderTop ? `${borderSize}px solid #333` : "2px dashed #bbb",
        borderBottom: borderBottom ? `${borderSize}px solid #333` : "2px dashed #bbb",
        borderLeft: borderLeft ? `${borderSize}px solid #333` : "2px dashed #bbb",
        borderRight: borderRight ? `${borderSize}px solid #333` : "2px dashed #bbb",
    };
};

export const getViewStyleFromFormatForPDF = (format) => {
    if (!format) {
        return {
            width: '100%',
            height: '100%',
        };
    }

    const { align, borderSize, borderTop, borderBottom, borderLeft, borderRight } = format;

    return {
        width: '100%',
        height: '100%',
        textAlign: align ? align : undefined,
        borderStyle: "solid",
        borderColor: "#000",
        borderTopWidth: borderTop ? borderSize : 0,
        borderBottomWidth: borderBottom ? borderSize : 0,
        borderLeftWidth: borderLeft ? borderSize : 0,
        borderRightWidth: borderRight ? borderSize : 0,
        fontFamily: "pdfFont",
    };
};

export const getTextStyleFromFormatForPDF = (format) => {
    if (!format) {
        return {
           // width: '100%',
            //height: '100%',
            fontFamily: "pdfFont",
        };
    }
    const { fontSize, bold, italic, align } = format;

    return {
        //width: '100%',
        //height: '100%',
        fontFamily: `pdfFont${bold ? "-bold" : ""}${italic ? "-italic" : ""}`,
        fontSize: fontSize ? fontSize : undefined,
        // fontWeight: bold ? "bold" : "normal",
        // fontStyle: italic ? "italic" : "normal",
        textAlign: align ? align : undefined,
    };
};

export const checkIsFormEditable = (docketStatus, role) => {
    switch (role) {
        case ROLESID.PRIME: {
            return docketStatus === DOCKETSTATUS.WILL_WORK
                || docketStatus === DOCKETSTATUS.DECLINED
                || docketStatus === DOCKETSTATUS.NEED_APPROVE
        }
        case ROLESID.SUB: {
            return docketStatus === DOCKETSTATUS.FILLED
                || docketStatus === DOCKETSTATUS.SUB_CHECK
        }
        default:
            return false;
    }
};

export const checkIsControlEditable = (docketStatus, role, control) => {
    return (checkIsFormEditable(docketStatus, role) && control.config.role === role);
};