import { LOCAL_STORAGE_KEY } from "../constants/localStorage";
import { hasStringValue } from "./dataHelper";

export const setAccessToken = (accessToken) => {
    localStorage.setItem(LOCAL_STORAGE_KEY.ACCESS_TOKEN, accessToken);
};

export const setRefreshToken = (refreshToken) => {
    localStorage.setItem(LOCAL_STORAGE_KEY.REFRESH_TOKEN, refreshToken);
};

export const setTokenExpires = (expiresIn) => {
    localStorage.setItem(LOCAL_STORAGE_KEY.TOKEN_EXPIRES_IN, Date.now() + expiresIn * 1000);
};

export const removeToken = () => {
    localStorage.removeItem(LOCAL_STORAGE_KEY.ACCESS_TOKEN);
    localStorage.removeItem(LOCAL_STORAGE_KEY.REFRESH_TOKEN);
    localStorage.removeItem(LOCAL_STORAGE_KEY.TOKEN_EXPIRES_IN);
};

export const checkAccessToken = () => {
    const access = localStorage.getItem(LOCAL_STORAGE_KEY.ACCESS_TOKEN);
    const refresh = localStorage.getItem(LOCAL_STORAGE_KEY.REFRESH_TOKEN);
    const expires = localStorage.getItem(LOCAL_STORAGE_KEY.TOKEN_EXPIRES_IN);

    return hasStringValue(access) && hasStringValue(refresh) && hasStringValue(expires) && expires > Date.now();
};

export const getAccessToken = () => {
    const accessToken = localStorage.getItem(LOCAL_STORAGE_KEY.ACCESS_TOKEN);
    return `Bearer ${accessToken}`;
};