import moment from 'moment'

export const hasStringValue = (string) => {
    if (string && string.toString().trim() !== "") {
        return true;
    }
    return false;
};

export const stringToBoolean = (string) => {
    if (string) {
        if (string.trim() === "true") {
            return true;
        } else if (string.trim() === "false") {
            return false;
        }
    }
    return undefined;
};


export const hasValue = (value) => {
    if (value !== null && value !== undefined) {
        return true;
    }
    return false;
};

export const formatDate = (date) => {
    return moment(date).format("DD/MM/YYYY");
};