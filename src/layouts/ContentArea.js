import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from "react-router-dom";

import { ROLESID } from '../constants/role';

import { Layout } from "antd";

import PrimeDashboard from "../containers/Dashboard/PrimeDashboard";
import PrimeDocket from '../containers/OnGoingContract/PrimeOnGoingContract/PrimeOnGoingContract';
import ManualSearch from '../containers/PrimeSearch/ManualSearch';
import AutoSearch from '../containers/PrimeSearch/AutoSearch';
import PrimeContractHistory from '../containers/PrimeContractHistory';
import PrimeRequest from '../containers/PrimeRequest';
import FormCreator from '../containers/FormCreator/FormCreator';
import PrimeFormManage from '../containers/PrimeFormManage/PrimeFormManage';

import SubDashboard from "../containers/Dashboard/SubDashboard";
import SubRequest from '../containers/SubRequest/SubRequest';
import SubSchedule from '../containers/SubSchedule/SubSchedule';
import SubDocket from '../containers/OnGoingContract/SubOnGoingContract';
import SubContractHistory from '../containers/SubContractHistory';
import SubWorkerManage from '../containers/SubWorkerManage';
import SubWorkerManageAdd from '../containers/SubWorkerManageAdd';

import AdminDashboard from '../containers/Dashboard/AdminDashboard';
import AdminUserManage from '../containers/AdminUserManage/AdminUserManage';
import AdminRating from '../containers/AdminRating/AdminRating';

import UserAuthLoading from "../containers/UserAuthLoading";
import UserLogin from "../containers/UserLogin";
import UserRegister from '../containers/UserRegister';
import Profile from "../containers/Profiles/Profile";
import ViewContractPayment from "../components/ViewContractPayment";
import DocketDetail from "../containers/DocketDetail/DocketDetail";
import NotificationPage from '../containers/NotificationPage';

import AdminContracts from '../containers/AdminContracts';
import SubCapability from '../containers/SubCapability';

class ContentArea extends Component {
    renderRoutes() {
        const routes = {
            [`${ROLESID.PRIME}`]: [
                {
                    path: "/notification",
                    component: NotificationPage
                },
                {
                    path: "/dashboard",
                    component: PrimeDashboard
                },
                {
                    path: "/payment",
                    component: ViewContractPayment
                },
                {
                    path: "/manualsearch",
                    component: ManualSearch
                },
                {
                    path: "/autosearch",
                    component: AutoSearch
                },
                {
                    path: "/history",
                    component: PrimeContractHistory
                },
                {
                    path: "/request",
                    component: PrimeRequest
                },
                {
                    path: "/docket",
                    exact: true,
                    component: PrimeDocket
                },
                {
                    path: "/formCreator",
                    component: FormCreator
                },
                {
                    path: "/formManage",
                    component: PrimeFormManage
                },
                {
                    path: "/docketDetail/:docketId",
                    component: DocketDetail,
                },
                {
                    path: "/profile/:id",
                    component: Profile
                },
                {
                    path: "/",
                    component: PrimeDashboard
                }
            ],
            [`${ROLESID.SUB}`]: [
                {
                    path: "/notification",
                    component: NotificationPage
                },
                {
                    path: "/dashboard",
                    component: SubDashboard
                },
                {
                    path: "/schedule",
                    component: SubSchedule
                },
                {
                    path: "/capability",
                    component: SubCapability
                },
                {
                    path: "/history",
                    component: SubContractHistory
                },
                {
                    path: "/request",
                    component: SubRequest
                },
                {
                    path: "/docket",
                    exact: true,
                    component: SubDocket
                },
                {
                    path: "/worker",
                    exact: true,
                    component: SubWorkerManage
                },
                {
                    path: "/worker/add",
                    component: SubWorkerManageAdd
                },
                {
                    path: "/profile/:id",
                    component: Profile
                },
                {
                    path: "/docketDetail/:docketId",
                    component: DocketDetail,
                },
                {
                    path: "/",
                    component: SubDashboard
                }
            ],
            [`${ROLESID.ADMIN}`]: [
                {
                    path: "/dashboard",
                    component: AdminDashboard,
                },
                {
                    path: "/userManagement",
                    component: AdminUserManage,
                },
                {
                    path: "/contracts",
                    component: AdminContracts,
                },
                {
                    path: "/rating",
                    component: AdminRating,
                },
                {
                    path: "/profile/:id",
                    component: Profile
                },
                {
                    component: AdminDashboard,
                },
            ],
            DEFAULT: [
                {
                    path: "/login",
                    component: UserLogin,
                },
                {
                    path: "/register",
                    component: UserRegister,
                },
                {
                    component: UserAuthLoading
                }
            ]
        };

        const { roleId } = this.props;

        let routeByRole = routes.DEFAULT;
        if (roleId) {
            routeByRole = routes[`${roleId}`];
        }

        return routeByRole.map((route, index) => {
            return (
                <Route key={index} path={route.path} exact={route.exact} component={route.component} />
            );
        });
    };

    render() {
        const style = {};
        if (this.props.isLogged) {
            style.marginLeft = 200;
        };

        return (
            <Layout style={style}>
                <Layout.Content style={{
                    margin: 0,
                    minHeight: "90vh",
                }}>
                    <Switch>
                        {this.renderRoutes()}
                    </Switch>
                </Layout.Content>
            </Layout>
        );
    };
}

function mapStateToProps(state) {
    return {
        isLogged: state.authReducer.isLogged,
        roleId: state.authReducer.profile.roleId
    };
}

export default connect(
    mapStateToProps,
    {},
    null, { pure: false }
)(ContentArea);