import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Layout } from "antd";

import PrimeMenu from '../containers/SideBarMenu/PrimeMenu';
import SubMenu from '../containers/SideBarMenu/SubMenu';
import AdminMenu from '../containers/SideBarMenu/AdminMenu';

import { ROLESID } from '../constants/role';

class SideBar extends Component {
    renderMenu() {
        const { roleId } = this.props;
        switch (roleId) {
            case ROLESID.PRIME:
                return <PrimeMenu />
            case ROLESID.SUB:
                return <SubMenu />
            case ROLESID.ADMIN:
                return <AdminMenu />
            default:
                return null;
        }
    }

    render() {
        const { isLogged } = this.props;
        if (!isLogged) return null;

        const { Sider } = Layout;
        return (
            <Sider
                width={200}
                style={{
                    overflow: 'auto',
                    height: '100vh',
                    position: 'fixed',
                    left: 0,
                }}
            >
                {this.renderMenu()}
            </Sider>
        );
    }

}

function mapStateToProps(state) {
    return {
        isLogged: state.authReducer.isLogged,
        roleId: state.authReducer.profile.roleId
    };
}

export default connect(
    mapStateToProps,
)(SideBar);