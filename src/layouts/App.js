import React, { Component } from "react";
import { connect } from 'react-redux';
import { BrowserRouter } from "react-router-dom";

import { Layout } from "antd";
import HeaderBar from "./HeaderBar";
import SideBar from "./SideBar";
import ContentArea from "./ContentArea";

// eslint-disable-next-line
import "antd/dist/antd.css";

class App extends Component {
    renderHeaderBar() {
        if (this.props.isLogged) {
            return (<HeaderBar />);
        }
        return null;
    };

    renderSideBarAndContent() {
        const style = {};
        if (this.props.isLogged) {
            style.marginTop = 64;
        };

        return (
            <Layout style={style}>
                <SideBar />
                <ContentArea />
            </Layout>
        );
    };

    render() {
        return (
            <BrowserRouter>
                <Layout style={{minHeight: "100vh"}}>
                    {this.renderHeaderBar()}
                    {this.renderSideBarAndContent()}
                </Layout>
            </BrowserRouter>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        isLogged: state.authReducer.isLogged,
    };
};

export default connect(
    mapStateToProps,
    {},
    null, { pure: false }
)(App);