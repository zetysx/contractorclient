import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Layout, Row, Col } from "antd";
import UserInfo from '../containers/SideBarMenu/UserInfo';
import UserNotification from '../components/UserNotification';

class Header extends Component {
    render() {
        const { Header } = Layout;

        return (
            <Header
                className="header"
                style={{ position: 'fixed', zIndex: 100, width: '100%', color: "#fff", background: "#343a40" }}
            >
                <Row>
                    <Col span={8}>
                        <span style={{ fontSize: "25px" }}><b>Contractor Portal</b></span>
                    </Col>
                    <Col span={16}>
                        <Row type="flex" justify="end">
                            <Col style={{ marginTop: "3px" }}>
                                <UserNotification />
                            </Col>
                            <Col style={{ marginLeft: "30px" }}>
                                <UserInfo />
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Header>
        );
    }
}

function mapStateToProps(state) {
    return {
    };
}

export default connect(
    mapStateToProps,
)(Header);