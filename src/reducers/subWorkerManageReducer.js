import { 
    SUBWORKERMANAGE_DISABLE_WORKER_BY_SUB_LOADING,
    SUBWORKERMANAGE_DISABLE_WORKER_BY_SUB_SUCCESS,
    SUBWORKERMANAGE_DISABLE_WORKER_BY_SUB_FAIL,
    SUBWORKERMANAGE_GET_ALL_SUBCONTRACTOR_PROFESSION_FAIL,
    SUBWORKERMANAGE_GET_ALL_SUBCONTRACTOR_PROFESSION_LOADING,
    SUBWORKERMANAGE_GET_ALL_SUBCONTRACTOR_PROFESSION_SUCCESS,
    SUBWORKERMANAGE_GET_ALL_WORKERS_LOADING,
    SUBWORKERMANAGE_GET_ALL_WORKERS_SUCCESS,
    SUBWORKERMANAGE_GET_ALL_WORKERS_FAIL,
    // SUBWORKERMANAGE_GET_WORKER_DETAILS_FOR_MANAGE_LOADING,
    // SUBWORKERMANAGE_GET_WORKER_DETAILS_FOR_MANAGE_SUCCESS,
    // SUBWORKERMANAGE_GET_WORKER_DETAILS_FOR_MANAGE_FAIL,

    SUBWORKERMANAGE_CHANGE_VISIBLE_UPDATE_MODAL,
    SUBWORKERMANAGE_WORKERLIST_PASS_WORKER_NEED_UPDATE_TO_MODAL,

    SUBWORKERMANAGE_WORKERUPDATE_CHECKBOXPROFESSION_CHANGE_WORKER_PROFESSION,

    SUBWORKERMANAGE_WORKERUPDATE_GET_PROFESSIONS_LIST_WORKER_WORKING_ON_LOADING,
    SUBWORKERMANAGE_WORKERUPDATE_GET_PROFESSIONS_LIST_WORKER_WORKING_ON_SUCCESS,
    SUBWORKERMANAGE_WORKERUPDATE_GET_PROFESSIONS_LIST_WORKER_WORKING_ON_FAIL,
} from '../actions/subWorkerManageAction';

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";

const INIT_STATE = {
    isFetching: false,
    workerDisabledStatus: {},
    subcontractorProfessions: [],
    workerList: [],
    // workerDetailsForManagement: {},

    workerUpdatedStatus: {},
    workerUpdate_modalVisible: false,
    workerUpdate_Object: {},
    workerProfessionsWorkingOn: []
};


export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case SUBWORKERMANAGE_DISABLE_WORKER_BY_SUB_LOADING:
            return {
                ...state,
                isFetching: true
            };
        case SUBWORKERMANAGE_DISABLE_WORKER_BY_SUB_SUCCESS:
            return {
                ...state,
                isFetching: false,
                workerDisabledStatus: action.workerDisabledStatus
            };
        case SUBWORKERMANAGE_DISABLE_WORKER_BY_SUB_FAIL:
            return {
                ...state,
                isFetching: false,
                workerDisabledStatus: {}
            };
        case SUBWORKERMANAGE_GET_ALL_SUBCONTRACTOR_PROFESSION_LOADING:
            return {
                ...state,
                isFetching: true,
                subcontractorProfessions: []
            };
        case SUBWORKERMANAGE_GET_ALL_SUBCONTRACTOR_PROFESSION_SUCCESS:
            return {
                ...state,
                isFetching: false,
                subcontractorProfessions: action.subcontractorProfessionsForWorker
            };
        case SUBWORKERMANAGE_GET_ALL_SUBCONTRACTOR_PROFESSION_FAIL:
            return {
                ...state,
                isFetching: false,
                subcontractorProfessions: []
            };
        case SUBWORKERMANAGE_GET_ALL_WORKERS_LOADING:
            return {
                ...state,
                isFetching: true,
                workerList: []
            };
        case SUBWORKERMANAGE_GET_ALL_WORKERS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                workerList: action.allSubWorkers
            };
        case SUBWORKERMANAGE_GET_ALL_WORKERS_FAIL:
            return {
                ...state,
                isFetching: false,
                workerList: []
            }
        // case SUBWORKERMANAGE_GET_WORKER_DETAILS_FOR_MANAGE_LOADING:
        //     return {
        //         ...state,
        //         isFetching: true,
        //         workerDetailsForManagement: {}
        //     };
        // case SUBWORKERMANAGE_GET_WORKER_DETAILS_FOR_MANAGE_SUCCESS:
        //     return {
        //         ...state,
        //         isFetching: false,
        //         workerDetailsForManagement: action.workerDetailManage
        //     };
        // case SUBWORKERMANAGE_GET_WORKER_DETAILS_FOR_MANAGE_FAIL: 
        //     return {
        //         ...state,
        //         isFetching: false,
        //         workerDetailsForManagement: {}
        //     };
 
        case SUBWORKERMANAGE_WORKERUPDATE_CHECKBOXPROFESSION_CHANGE_WORKER_PROFESSION:
            return {
                ...state,
                isFetching: false,
                workerUpdate_Object: action.newWorkerWithNewProfessions
            };
        case SUBWORKERMANAGE_CHANGE_VISIBLE_UPDATE_MODAL:
            return {
                ...state,
                isFetching: false,
                workerUpdate_modalVisible: action.isUpdateModalVisible
            };
        case SUBWORKERMANAGE_WORKERLIST_PASS_WORKER_NEED_UPDATE_TO_MODAL:
            return {
                ...state,
                isFetching: false,
                workerUpdate_Object: action.workerNeedUpdate,
            };
        case SUBWORKERMANAGE_WORKERUPDATE_GET_PROFESSIONS_LIST_WORKER_WORKING_ON_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case SUBWORKERMANAGE_WORKERUPDATE_GET_PROFESSIONS_LIST_WORKER_WORKING_ON_SUCCESS:
            return {
                ...state,
                isFetching: false,
                workerProfessionsWorkingOn: action.workerProfessionsWorkingOn
            };
        case SUBWORKERMANAGE_WORKERUPDATE_GET_PROFESSIONS_LIST_WORKER_WORKING_ON_FAIL:
            return {
                ...state,
                isFetching: false,
                workerProfessionsWorkingOn: []
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };

        default: 
            return state;
    }
};


