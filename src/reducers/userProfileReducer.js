import {
    USERPROFILE_LOAD_USER_PROFILE_LOADING,
    USERPROFILE_LOAD_USER_PROFILE_SUCCESS,
    USERPROFILE_LOAD_USER_PROFILE_FAIL,
    USERPROFILE_RATING_LIST_LOADING,
    USERPROFILE_RATING_LIST_SUCCESS,
    USERPROFILE_RATING_LIST_FAIL
} from "../actions/userProfileAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";

const INIT_STATE = {
    isFetching: false,
    userProfile: [],
    ratingList:[]
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case USERPROFILE_LOAD_USER_PROFILE_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case USERPROFILE_LOAD_USER_PROFILE_SUCCESS:
            return {
                ...state,
                isFetching: false,
                userProfile: action.userProfile
            };
        case USERPROFILE_LOAD_USER_PROFILE_FAIL:
            return {
                ...state,
                isFetching: false,
            };
        case USERPROFILE_RATING_LIST_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case USERPROFILE_RATING_LIST_SUCCESS:
            return {
                ...state,
                isFetching: false,
                ratingList: action.ratingList
            };
        case USERPROFILE_RATING_LIST_FAIL:
            return {
                ...state,
                isFetching: false,
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};