import {
    FORM_CREATOR_CHANGE_FORMCONTROL,
    FORM_CREATOR_CHANGE_EDITDRAWER_VISABLE,
    FORM_CREATOR_CHANGE_EDITDRAWER_FROMCONTROL,
    FORM_CREATOR_CHANGE_SAVEDRAWER_VISABLE,
    FORM_CREATOR_CREATE_NEWFORM,
    FORM_CREATOR_SAVE_FORM_LOADING,
    FORM_CREATOR_SAVE_FORM_SUCCESS,
    FORM_CREATOR_SAVE_FORM_FAIL,
} from "../actions/formCreatorAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";


const INIT_STATE = {
    isFetching: false,
    editDrawerFormControl: undefined,
    editDrawerFormControlIndex: undefined,

    isSaveDrawerVisable: false,
    isEditDrawerVisable: false,
    
    previousFormId: undefined,
    formControl: [],
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case FORM_CREATOR_CHANGE_FORMCONTROL:
            return {
                ...state,
                formControl: action.formControl
            };
        case FORM_CREATOR_CHANGE_EDITDRAWER_VISABLE:
            return {
                ...state,
                isEditDrawerVisable: action.isEditDrawerVisable
            };
        case FORM_CREATOR_CHANGE_SAVEDRAWER_VISABLE:
            return {
                ...state,
                isSaveDrawerVisable: action.isSaveDrawerVisable
            };
        case FORM_CREATOR_CHANGE_EDITDRAWER_FROMCONTROL:
            return {
                ...state,
                editDrawerFormControlIndex: action.editDrawerFormControlIndex,
                editDrawerFormControl: action.editDrawerFormControl
            };
        case FORM_CREATOR_CREATE_NEWFORM:
            return {
                ...INIT_STATE,
                formControl: action.formControl,
                previousFormId: action.previousFormId,
            };
        case FORM_CREATOR_SAVE_FORM_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case FORM_CREATOR_SAVE_FORM_SUCCESS:
            return {
                ...state,
                isFetching: false
            };
        case FORM_CREATOR_SAVE_FORM_FAIL:
            return {
                ...state,
                isFetching: false
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};