import {
    ADMINUSERMANAGE_GET_LIST_ALL_USER_ACCOUNT_LOADING,
    ADMINUSERMANAGE_GET_LIST_ALL_USER_ACCOUNT_SUCCESS,
    ADMINUSERMANAGE_GET_LIST_ALL_USER_ACCOUNT_FAIL,
    ADMINUSERMANAGE_RATING_FAIL,
    ADMINUSERMANAGE_RATING_LOADING,
    ADMINUSERMANAGE_RATING_SUCCESS
} from "../actions/adminUserManageAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";


const INIT_STATE = {
    isFetching: false,
    allUserAccount: [],
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case ADMINUSERMANAGE_GET_LIST_ALL_USER_ACCOUNT_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case ADMINUSERMANAGE_GET_LIST_ALL_USER_ACCOUNT_SUCCESS:
            return {
                ...state,
                isFetching: false,
                allUserAccount: action.allUserAccount
            };
        case ADMINUSERMANAGE_GET_LIST_ALL_USER_ACCOUNT_FAIL:
            return {
                ...state,
                isFetching: false,
            };
        case ADMINUSERMANAGE_RATING_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case ADMINUSERMANAGE_RATING_SUCCESS:
            return {
                ...state,
                isFetching: false,
            };
        case ADMINUSERMANAGE_RATING_FAIL:
            return {
                ...state,
                isFetching: false,
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};