import {
    GET_PRIME_OVERVIEW_LOADING,
    GET_PRIME_OVERVIEW_SUCCESS,
    GET_PRIME_OVERVIEW_FAIL
} from "../actions/primeOverviewAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";

const INIT_STATE = {
    isFetching: false,
    primeOverview: null,
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_PRIME_OVERVIEW_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case GET_PRIME_OVERVIEW_SUCCESS:
            return {
                ...state,
                isFetching: false,
                primeOverview: action.primeOverview
            };
        case GET_PRIME_OVERVIEW_FAIL:
            return {
                ...state,
                isFetching: false,
                primeOverview: null,
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};