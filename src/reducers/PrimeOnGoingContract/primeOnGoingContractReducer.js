import {
    PRIMEONGOINGCONTRACT_PRIME_LOAD_ALL_CONTRACT_LOADING,
    PRIMEONGOINGCONTRACT_PRIME_LOAD_ALL_CONTRACT_SUCCESS,
    PRIMEONGOINGCONTRACT_PRIME_LOAD_ALL_CONTRACT_FAIL,
} from "../../actions/primeOnGoingContractAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../../actions/authAction";


const INIT_STATE = {
    isFetching: false,
    contracts: [],
    primeContractsDone: [],
    contractPayment: null,
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case PRIMEONGOINGCONTRACT_PRIME_LOAD_ALL_CONTRACT_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case PRIMEONGOINGCONTRACT_PRIME_LOAD_ALL_CONTRACT_SUCCESS:
            return {
                ...state,
                isFetching: false,
                contracts: action.contracts
            };
        case PRIMEONGOINGCONTRACT_PRIME_LOAD_ALL_CONTRACT_FAIL:
            return {
                ...state,
                isFetching: true,
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};