import {
    PRIMEONGOINGCONTRACT_ADD_FORM_TO_CONTRACT_LOADING,
    PRIMEONGOINGCONTRACT_ADD_FORM_TO_CONTRACT_SUCCESS,
    PRIMEONGOINGCONTRACT_ADD_FORM_TO_CONTRACT_FAIL
} from "../../actions/primeOnGoingContractAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../../actions/authAction";

const INIT_STATE = {
    isFetching: false,
    addFormToContractStatus: [],
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case PRIMEONGOINGCONTRACT_ADD_FORM_TO_CONTRACT_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case PRIMEONGOINGCONTRACT_ADD_FORM_TO_CONTRACT_SUCCESS:
            return {
                ...state,
                isFetching: false,
                addFormToContractStatus: action.addFormToContractStatus
            };
        case PRIMEONGOINGCONTRACT_ADD_FORM_TO_CONTRACT_FAIL:
            return {
                ...state,
                isFetching: false,
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};