import { combineReducers } from "redux";

import authReducer from "./authReducer";
import locationReducer from "./locationReducer";
import professionReducer from './professionReducer';
import docketListByContractIdReducer from './docketListByContractIdReducer';
import subScheduleReducer from './subScheduleReducer';
import sentRequestReducer from './primeSearch/sentRequestReducer';
import formCreatorReducer from './formCreatorReducer';
import primeFormManageReducer from './primeFormManageReducer';
import userProfileReducer from './userProfileReducer';
import notificationReducer from './notificationReducer';
import registerReducer from './registerReducer';
import docketDetailReducer from "./docketDetailReducer";
import primeAddFormToContractReducer from './primeOnGoingContract/primeAddFormToContractReducer';
import subWorkerManageReducer from './subWorkerManageReducer';
import primeRequestReducer from './primeRequestReducer';
import primeContractHistoryReducer from './primeContractHistoryReducer';
import subContractHistoryReducer from './subContractHistoryReducer';
import primeOverviewReducer from './primeOverviewReducer';
import subOverviewReducer from './subOverviewReducer';
import adminOverviewReducer from './adminOverviewReducer';
import adminUserManageReducer from './adminUserManageReducer';
import autoSearchReducer from './primeSearch/autoSearchReducer';
import manualSearchReducer from './primeSearch/manualSearchReducer';
import primeOnGoingContractReducer from './primeOnGoingContract/primeOnGoingContractReducer';
import subOnGoingContractReducer from './subOnGoingContractReducer';
import viewContractPaymentReducer from './viewContractPaymentReducer';
import subRequestReducer from './subRequestReducer';
import adminContractsReducer from './adminContractsReducer';
import profileReducer from './profileReducer';
import subCapabilityReducer from './subCapabilityReducer';
import adminRatingReducer from './adminRatingReducer';



export default combineReducers({
    authReducer,
    locationReducer,
    professionReducer,
    docketListByContractIdReducer,
    subScheduleReducer,
    sentRequestReducer,
    formCreatorReducer,
    primeFormManageReducer,
    userProfileReducer,
    notificationReducer,
    registerReducer,
    docketDetailReducer,
    primeAddFormToContractReducer,
    subWorkerManageReducer,
    primeRequestReducer,
    primeContractHistoryReducer,
    subContractHistoryReducer,
    primeOverviewReducer,
    subOverviewReducer,
    adminOverviewReducer,
    adminUserManageReducer,
    autoSearchReducer,
    manualSearchReducer,
    primeOnGoingContractReducer,
    subOnGoingContractReducer,
    viewContractPaymentReducer,
    subRequestReducer,
    adminContractsReducer,
    profileReducer,
    subCapabilityReducer,
    adminRatingReducer
});