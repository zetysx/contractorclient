import {
    ADMINCONTRACTS_GET_ALL_CONTRACTS_LOADING,
    ADMINCONTRACTS_GET_ALL_CONTRACTS_SUCCESS,
    ADMINCONTRACTS_GET_ALL_CONTRACTS_FAILURE
} from "../actions/adminContractsAction";

const INIT_STATE = {
    isFetching: false,
    allContracts: [],
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case ADMINCONTRACTS_GET_ALL_CONTRACTS_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case ADMINCONTRACTS_GET_ALL_CONTRACTS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                allContracts: action.allContracts
            };
        case ADMINCONTRACTS_GET_ALL_CONTRACTS_FAILURE:
            return {
                ...state,
                isFetching: false,
            };
        default:
            return state;
    }
};