import {
    DOCKETDETAIL_GET_DOCKETDETAIL_BY_DOCKETID_LOADING,
    DOCKETDETAIL_GET_DOCKETDETAIL_BY_DOCKETID_SUCCESS,
    DOCKETDETAIL_GET_DOCKETDETAIL_BY_DOCKETID_FAIL,
    DOCKETDETAIL_CHANGE_INPUTDRAWER_VISABLE,
    DOCKETDETAIL_CHANGE_INPUTDRAWER_FROMCONTROL,
    DOCKETDETAIL_CHANGE_FORMDATA,
    DOCKETDETAIL_UPDATE_FORMDATA_BY_DOCKETID_LOADING,
    DOCKETDETAIL_UPDATE_FORMDATA_BY_DOCKETID_SUCCESS,
    DOCKETDETAIL_UPDATE_FORMDATA_BY_DOCKETID_FAIL
} from "../actions/docketDetailAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";


const INIT_STATE = {
    isFetching: false,
    isUpdateFormFetching: false,

    docketDetail: undefined,
    formControl: undefined,
    formData: undefined,

    isControlInputDrawerVisable: false,
    inputControlIndex: undefined,
    inputControlData: undefined
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case DOCKETDETAIL_GET_DOCKETDETAIL_BY_DOCKETID_LOADING:
            return {
                ...state,
                isFetching: true
            };
        case DOCKETDETAIL_GET_DOCKETDETAIL_BY_DOCKETID_SUCCESS:
            return {
                ...state,
                isFetching: false,
                docketDetail: action.docketDetail,
                formControl: action.formControl,
                formData: action.formData,
            };
        case DOCKETDETAIL_GET_DOCKETDETAIL_BY_DOCKETID_FAIL:
            return {
                ...state,
                isFetching: false,
                docketDetail: undefined,
            };
        case DOCKETDETAIL_CHANGE_INPUTDRAWER_VISABLE:
            return {
                ...state,
                isControlInputDrawerVisable: action.isControlInputDrawerVisable
            };
        case DOCKETDETAIL_CHANGE_INPUTDRAWER_FROMCONTROL:
            return {
                ...state,
                inputControlIndex: action.inputControlIndex,
                inputControlData: action.inputControlData,
                isControlInputDrawerVisable: action.isControlInputDrawerVisable
            };
        case DOCKETDETAIL_CHANGE_FORMDATA:
            return {
                ...state,
                formData: action.formData,
                isControlInputDrawerVisable: action.isControlInputDrawerVisable,
            };
        case DOCKETDETAIL_UPDATE_FORMDATA_BY_DOCKETID_LOADING:
            return {
                ...state,
                isUpdateFormFetching: true
            };
        case DOCKETDETAIL_UPDATE_FORMDATA_BY_DOCKETID_SUCCESS:
            return {
                ...state,
                isUpdateFormFetching: false,
            };
        case DOCKETDETAIL_UPDATE_FORMDATA_BY_DOCKETID_FAIL:
            return {
                ...state,
                isUpdateFormFetching: false,
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};