import { 
    SUBCONTRACTHISTORY_SUBCONTRACTOR_GET_ALL_CONTRACT_DONE_LOADING,
    SUBCONTRACTHISTORY_SUBCONTRACTOR_GET_ALL_CONTRACT_DONE_SUCCESS,
    SUBCONTRACTHISTORY_SUBCONTRACTOR_GET_ALL_CONTRACT_DONE_FAIL
} from '../actions/subContractHistoryAction';

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";

const INIT_STATE = {
    isFetching: false,
    subContractsDone: []
};


export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case SUBCONTRACTHISTORY_SUBCONTRACTOR_GET_ALL_CONTRACT_DONE_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case SUBCONTRACTHISTORY_SUBCONTRACTOR_GET_ALL_CONTRACT_DONE_SUCCESS:
            return {
                isFetching: false,
                subContractsDone: action.subDoneContractDataAction
            };
        case SUBCONTRACTHISTORY_SUBCONTRACTOR_GET_ALL_CONTRACT_DONE_FAIL:
            return {
                isFetching: false,
                subContractsDone: []
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };

        default: 
            return state;
    }
};