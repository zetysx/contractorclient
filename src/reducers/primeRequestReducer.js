import { 
    PRIMEREQUEST_GET_SENT_REQUESTS_NOT_CONFIRM_LOADING,
    PRIMEREQUEST_GET_SENT_REQUESTS_NOT_CONFIRM_SUCCESS,
    PRIMEREQUEST_GET_SENT_REQUESTS_NOT_CONFIRM_FAIL,
    PRIMEREQUEST_DELETE_SENT_REQUEST_NOT_CONFIRM_LOADING,
    PRIMEREQUEST_DELETE_SENT_REQUEST_NOT_CONFIRM_SUCCESS,
    PRIMEREQUEST_DELETE_SENT_REQUEST_NOT_CONFIRM_FAIL
} from '../actions/primeRequestAction';

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";

const INIT_STATE = {
    isFetching: false,
    requestDeletedStatus: [],
    requestsPrimeSentWaitting: []
};


export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case PRIMEREQUEST_GET_SENT_REQUESTS_NOT_CONFIRM_LOADING:
            return {
                ...state,
                isFetching: true
            };
        case PRIMEREQUEST_GET_SENT_REQUESTS_NOT_CONFIRM_SUCCESS:
            return {
                ...state,
                isFetching: false,
                requestsPrimeSentWaitting: action.allRequestSentNotConfirmPrimeAction
            };
        case PRIMEREQUEST_GET_SENT_REQUESTS_NOT_CONFIRM_FAIL:
            return {
                ...state,
                isFetching: false,
                requestsPrimeSentWaitting: []
            };
        case PRIMEREQUEST_DELETE_SENT_REQUEST_NOT_CONFIRM_LOADING:
            return {
                ...state,
                isFetching: true,
                requestDeletedStatus: []
            };
        case PRIMEREQUEST_DELETE_SENT_REQUEST_NOT_CONFIRM_SUCCESS:
            return {
                ...state,
                isFetching: false,
                requestDeletedStatus: action.deleteRequestSentNotConfirmStatusAction
            };
        case PRIMEREQUEST_DELETE_SENT_REQUEST_NOT_CONFIRM_FAIL:
            return {
                ...state,
                isFetching: false,
                requestDeletedStatus: []
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default: 
            return state;
    }
};