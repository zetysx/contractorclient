import {
    PROFILEUPDATEMODAL_UPDATE_PROFILE_LOADING,
    PROFILEUPDATEMODAL_UPDATE_PROFILE_SUCCESS,
    PROFILEUPDATEMODAL_UPDATE_PROFILE_FAILURE,
    PROFILEUPDATEMODAL_PASS_PROFILE_TO_MODAL,
    PROFILEUPDATEMODAL_CHANGE_MODAL_VISIBILITY
} from "../actions/profileAction";


const INIT_STATE = {
    isFetching: false,
    updateUserResult: {},
    profileUpdate: {},
    updateModalVisible: false
};

export default (state = INIT_STATE, action) => {
    switch(action.type) {
        case PROFILEUPDATEMODAL_UPDATE_PROFILE_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case PROFILEUPDATEMODAL_UPDATE_PROFILE_SUCCESS:
            return {
                ...state,
                isFetching: false,
                updateUserResult: action.updateUserResult
            };
        case PROFILEUPDATEMODAL_UPDATE_PROFILE_FAILURE:
            return {
                ...state,
                isFetching: false,
                updateUserResult: {}
            };
        case PROFILEUPDATEMODAL_PASS_PROFILE_TO_MODAL:
            return {
                ...state,
                isFetching: false,
                profileUpdate: action.profileNeedUpdate
            };
        case PROFILEUPDATEMODAL_CHANGE_MODAL_VISIBILITY:
            return {
                ...state,
                isFetching: false,
                updateModalVisible: !state.updateModalVisible
            };
        default:
            return state;
    }
}