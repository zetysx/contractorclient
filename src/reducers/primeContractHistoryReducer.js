import {
    PRIMECONTRACTHISTORY_GET_CONTRACTS_DONE_FAIL,
    PRIMECONTRACTHISTORY_GET_CONTRACTS_DONE_LOADING,
    PRIMECONTRACTHISTORY_GET_CONTRACTS_DONE_SUCCESS,

    PRIMECONTRACTHISTORY_GET_CONTRACT_FORM_LOADING,
    PRIMECONTRACTHISTORY_GET_CONTRACT_FORM_SUCCESS,
    PRIMECONTRACTHISTORY_GET_CONTRACT_FORM_FAIL
} from '../actions/primeContractHistoryAction';

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";


const INIT_STATE = {
    isFetching: false,
    primeContractsDone: [],
    primeContractFormData: {}
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case PRIMECONTRACTHISTORY_GET_CONTRACTS_DONE_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case PRIMECONTRACTHISTORY_GET_CONTRACTS_DONE_SUCCESS:
            return {
                ...state,
                isFetching: false,
                primeContractsDone: action.primeContractsDoneAction
            };
        case PRIMECONTRACTHISTORY_GET_CONTRACTS_DONE_FAIL:
            return {
                ...state,
                isFetching: false,
                primeContractsDone: []
            };

        case PRIMECONTRACTHISTORY_GET_CONTRACT_FORM_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case PRIMECONTRACTHISTORY_GET_CONTRACT_FORM_SUCCESS:
            return {
                ...state,
                isFetching: false,
                primeContractFormData: action.primeContractFormDataAction
            };
        case PRIMECONTRACTHISTORY_GET_CONTRACT_FORM_FAIL:
            return {
                ...state,
                isFetching: false,
                primeContractFormData: {}
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};