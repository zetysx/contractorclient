import { PROFESSION_LOADING, PROFESSION_LOAD_SUCCESS } from "../actions/professionAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";

const INIT_STATE = {
    isFetching: false,
    professions: []
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case PROFESSION_LOADING:
            return {
                isFetching: true,
                professions: []
            };
        case PROFESSION_LOAD_SUCCESS:
            return {
                isFetching: false,
                professions: action.professions
            };
        // case GET_AUTH_TOKEN_AND_LOGIN_FAILURE:
        //     return {
        //         isFetching: false,
        //         isLogged: false,
        //         profile: {}
        //     };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};