import {
    DOCKETLISTBYCONTRACTID_ALL_DOCKET_BY_CONTRACT_ID_LOADING,
    DOCKETLISTBYCONTRACTID_ALL_DOCKET_BY_CONTRACT_ID_SUCCESS,
} from "../actions/docketListByContractIdAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";


const INIT_STATE = {
    isFetchingAllDocketFromContractId: false,
    allDocketWaitFill: [],
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case DOCKETLISTBYCONTRACTID_ALL_DOCKET_BY_CONTRACT_ID_LOADING:
            return {
                ...state,
                isFetchingAllDocketFromContractId: true,
            };
        case DOCKETLISTBYCONTRACTID_ALL_DOCKET_BY_CONTRACT_ID_SUCCESS:
            return {
                ...state,
                isFetchingAllDocketFromContractId: false,
                allDocketWaitFill: action.dockets
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};