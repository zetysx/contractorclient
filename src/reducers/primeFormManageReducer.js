import {
    PRIMEFORMMANAGE_GET_FORMLIST_LOADING,
    PRIMEFORMMANAGE_GET_FORMLIST_SUCCESS,
    PRIMEFORMMANAGE_GET_FORMLIST_FAIL
} from "../actions/primeFormManageAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";

const INIT_STATE = {
    isFetching: false,
    formList: [],
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case PRIMEFORMMANAGE_GET_FORMLIST_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case PRIMEFORMMANAGE_GET_FORMLIST_SUCCESS:
            return {
                isFetching: false,
                formList: action.formList
            };
        case PRIMEFORMMANAGE_GET_FORMLIST_FAIL:
            return {
                isFetching: false,
                formList: [],
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};