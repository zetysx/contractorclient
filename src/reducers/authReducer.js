import {
    GET_AUTH_TOKEN_AND_LOGIN_LOADING,
    GET_AUTH_TOKEN_AND_LOGIN_SUCCESS,
    GET_AUTH_TOKEN_AND_LOGIN_FAILURE,
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";

const INIT_STATE = {
    isFetching: false,
    isLogged: false,
    profile: {
    }
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_AUTH_TOKEN_AND_LOGIN_LOADING:
            return {
                isFetching: true,
                isLogged: false,
                profile: {}
            };
        case GET_AUTH_TOKEN_AND_LOGIN_SUCCESS:
            action.profile.roleId = action.profile.role;
            return {
                isFetching: false,
                isLogged: true,
                profile: action.profile
            };
        case GET_AUTH_TOKEN_AND_LOGIN_FAILURE:
            return {
                isFetching: false,
                isLogged: false,
                profile: {}
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};