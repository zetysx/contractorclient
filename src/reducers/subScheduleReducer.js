import {
    SUBSCHEDULE_LOAD_SCHEDULE_DATA_LOADING,
    SUBSCHEDULE_LOAD_SCHEDULE_DATA_SUCCESS,
    SUBSCHEDULE_LOAD_SCHEDULE_DATA_FAIL,
    SUBSCHEDULE_SET_BUSY_WORKER_LOADING,
    SUBSCHEDULE_SET_BUSY_WORKER_SUCCESS,
    SUBSCHEDULE_SET_BUSY_WORKER_FAIL,
} from "../actions/subScheduleAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";

const INIT_STATE = {
    isFetching: false,
    subSchedule: [],
    setBusyStatus: "",
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case SUBSCHEDULE_LOAD_SCHEDULE_DATA_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case SUBSCHEDULE_LOAD_SCHEDULE_DATA_SUCCESS:
            return {
                ...state,
                isFetching: false,
                subSchedule: action.subSchedule
            };
        case SUBSCHEDULE_LOAD_SCHEDULE_DATA_FAIL:
            return {
                ...state,
                isFetching: false,
            };

        case SUBSCHEDULE_SET_BUSY_WORKER_LOADING:
            return {
                ...state,
                isFetching: true,
                setBusyStatus: ""
            };
        case SUBSCHEDULE_SET_BUSY_WORKER_SUCCESS:
            return {
                ...state,
                isFetching: false,
                setBusyStatus: action.setBusyStatus
            };
        case SUBSCHEDULE_SET_BUSY_WORKER_FAIL:
            return {
                ...state,
                isFetching: false,
                setBusyStatus: ""
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};