import {
    GET_NOTIFICATION_LOADING,
    GET_NOTIFICATION_SUCCESS,
    GET_UNSEEN_NOTI_AMOUNT_LOADING,
    GET_UNSEEN_NOTI_AMOUNT_SUCCESS,
    GET_NOTIFICATION_PAGE_LOADING,
    GET_NOTIFICATION_PAGE_SUCCESS,
    SET_LOCAL_UNSEEN_NOTI_SUCCESS
} from "../actions/notificationAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";


const INIT_STATE = {
    isFetching: false,
    notifications: [],
    unseen: 0,
    localUnseen :0
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_NOTIFICATION_LOADING:
            return {
                isFetching: true,
                ...state,
            };
        case GET_NOTIFICATION_SUCCESS:
            return {
                isFetching: false,
                ...state,
                notifications: action.notifications
            };
        case GET_UNSEEN_NOTI_AMOUNT_LOADING:
            return {
                isFetching: true,
                ...state,
                unseen: 0
            };
        case GET_UNSEEN_NOTI_AMOUNT_SUCCESS:
            return {
                isFetching: false,
                ...state,
                unseen: action.unseen
            };
        case GET_NOTIFICATION_PAGE_LOADING:
            return {
                isFetching: true,
                ...state,
            };
        case GET_NOTIFICATION_PAGE_SUCCESS:
            return {
                isFetching: false,
                ...state,
                notificationsPage: action.notificationsPage
            };
        case SET_LOCAL_UNSEEN_NOTI_SUCCESS:
            return {
                ...state,
                localUnseen: action.localUnseen
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};