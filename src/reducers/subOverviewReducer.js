import {
    GET_SUB_OVERVIEW_LOADING,
    GET_SUB_OVERVIEW_SUCCESS,
    GET_SUB_OVERVIEW_FAIL
} from "../actions/subOverviewAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";

const INIT_STATE = {
    isFetching: false,
    subOverview: null,
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_SUB_OVERVIEW_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case GET_SUB_OVERVIEW_SUCCESS:
            return {
                ...state,
                isFetching: false,
                subOverview: action.subOverview
            };
        case GET_SUB_OVERVIEW_FAIL:
            return {
                ...state,
                isFetching: false,
                subOverview: null,
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};