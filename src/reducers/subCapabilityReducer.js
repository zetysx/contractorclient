import {
    SUBCAPABILITY_GET_ALL_PROFESSIONS_FOR_UPDATING_LOADING,
    SUBCAPABILITY_GET_ALL_PROFESSIONS_FOR_UPDATING_SUCCESS,
    SUBCAPABILITY_GET_ALL_PROFESSIONS_FOR_UPDATING_FAILURE,

    SUBCAPABILITY_GET_ALL_LOCATIONS_FOR_UPDATING_LOADING,
    SUBCAPABILITY_GET_ALL_LOCATIONS_FOR_UPDATING_SUCCESS,
    SUBCAPABILITY_GET_ALL_LOCATIONS_FOR_UPDATING_FAILURE,

    SUBCAPABILITY_UPDATE_SUBCONTRACTOR_PROFESSIONS_LOADING,
    SUBCAPABILITY_UPDATE_SUBCONTRACTOR_PROFESSIONS_SUCCESS,
    SUBCAPABILITY_UPDATE_SUBCONTRACTOR_PROFESSIONS_FAILURE,

    // SUBCAPABILITY_UPDATE_SUBCONTRACTOR_LOCATIONS_LOADING,
    // SUBCAPABILITY_UPDATE_SUBCONTRACTOR_LOCATIONS_SUCCESS,
    // SUBCAPABILITY_UPDATE_SUBCONTRACTOR_LOCATIONS_FAILURE,

    SUBCAPABILITY_PROFESSION_CHANGE_PRICE,
    SUBCAPABILITY_PROFESSION_CHANGE_CHECKBOX_PROFESSION,
    SUBCAPABILITY_LOCATION_CHANGE_LOCATION_CHECKBOX,
} from '../actions/subCapabilityAction';


const INIT_STATE = {
    isFetching: false,
    subcontractorProfessionsForUpdate: [],
    subcontractorLocationsForUpdate: [],
    subListLocationChecked: [],
    updateSubcontractorProfessionStatus: {},
    updateSubcontractorLocationStatus: {},
};


export default (state = INIT_STATE, action) => {
    switch(action.type) {
        case SUBCAPABILITY_GET_ALL_PROFESSIONS_FOR_UPDATING_LOADING:
            return {
                ...state,
                isFetching: true
            };
        case SUBCAPABILITY_GET_ALL_PROFESSIONS_FOR_UPDATING_SUCCESS:
            return {
                ...state,
                isFetching: false,
                subcontractorProfessionsForUpdate: action.subcontractorProfessionsForUpdate
            };
        case SUBCAPABILITY_GET_ALL_PROFESSIONS_FOR_UPDATING_FAILURE:
            return {
                ...state,
                isFetching: false,
                subcontractorProfessionsForUpdate: []
            }
        case SUBCAPABILITY_GET_ALL_LOCATIONS_FOR_UPDATING_LOADING:
            return {
                ...state,
                isFetching: true
            };
        case SUBCAPABILITY_GET_ALL_LOCATIONS_FOR_UPDATING_SUCCESS:
            // console.log("REDUCER: ", action.subcontractorLocationsForUpdate);
            return {
                ...state,
                isFetching: false,
                subcontractorLocationsForUpdate: action.subcontractorLocationsForUpdate,
                subListLocationChecked: action.subcontractorLocation
            };
        case SUBCAPABILITY_GET_ALL_LOCATIONS_FOR_UPDATING_FAILURE:
            return {
                ...state,
                isFetching: false,
                subcontractorLocationsForUpdate: []
            };
        case SUBCAPABILITY_UPDATE_SUBCONTRACTOR_PROFESSIONS_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case SUBCAPABILITY_UPDATE_SUBCONTRACTOR_PROFESSIONS_SUCCESS: 
            return {
                ...state,
                isFetching: false,
                updateSubcontractorProfessionStatus: action.subcontractorProfessionCapabilityUpdateData
            };
        case SUBCAPABILITY_UPDATE_SUBCONTRACTOR_PROFESSIONS_FAILURE:
            return {
                ...state,
                isFetching: false,
                updateSubcontractorProfessionStatus: {}
            };
        // case SUBCAPABILITY_UPDATE_SUBCONTRACTOR_LOCATIONS_LOADING:
        //     return {
        //         ...state,
        //         isFetching: true
        //     };
        // case SUBCAPABILITY_UPDATE_SUBCONTRACTOR_LOCATIONS_SUCCESS:
        //     return {
        //         ...state,
        //         isFetching: false,
        //         updateSubcontractorLocationStatus: action.
        //     }
        case SUBCAPABILITY_PROFESSION_CHANGE_PRICE:
            return {
                ...state,
                isFetching: false,
                subcontractorProfessionsForUpdate: action.newProfessions
            };
        case SUBCAPABILITY_PROFESSION_CHANGE_CHECKBOX_PROFESSION:
            return {
                ...state,
                isFetching: false,
                subcontractorProfessionsForUpdate: action.newProfessions
            };
        case SUBCAPABILITY_LOCATION_CHANGE_LOCATION_CHECKBOX:
            return {
                ...state,
                isFetching: false,
                subListLocationChecked: action.newLocations
            };
        
        default: 
            return state;
    }
}