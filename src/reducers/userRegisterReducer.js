import { 
    USERREGISTER_REGISTER_NEW_ACCOUNT_LOADING,
    USERREGISTER_REGISTER_NEW_ACCOUNT_SUCCESS,
    USERREGISTER_REGISTER_NEW_ACCOUNT_FAILURE 
} from "../actions/authAction";

const INIT_STATE = {
    isFetching: false,
    registerStatus: []
};

export default (state = INIT_STATE, action) => {
    switch(action.type) {
        case USERREGISTER_REGISTER_NEW_ACCOUNT_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case USERREGISTER_REGISTER_NEW_ACCOUNT_SUCCESS:
            return {
                isFetching: false,
                registerStatus: action.registerStatus
            };
        case USERREGISTER_REGISTER_NEW_ACCOUNT_FAILURE:
            return {
                isFetching: false,
                registerStatus: null
            };
        default:
            return state;
    }
}