import {
    AUTOSEARCH_GET_AUTO_SEARCH_TITLE_LOADING,
    AUTOSEARCH_GET_AUTO_SEARCH_TITLE_SUCCESS,
    AUTOSEARCH_GET_AUTO_SEARCH_TITLE_FAIL,
    AUTOSEARCH_GET_AUTO_SEARCH_CONTENT_LOADING,
    AUTOSEARCH_GET_AUTO_SEARCH_CONTENT_SUCCESS,
    AUTOSEARCH_GET_AUTO_SEARCH_CONTENT_FAIL,
    AUTOSEARCH_CHANGE_AUTO_SEARCH_STATUS_LOADING,
    AUTOSEARCH_CHANGE_AUTO_SEARCH_STATUS_SUCCESS,
    AUTOSEARCH_CHANGE_AUTO_SEARCH_STATUS_FAIL,
    AUTOSEARCH_DELETE_AUTO_SEARCH_LOADING,
    AUTOSEARCH_DELETE_AUTO_SEARCH_SUCCESS,
    AUTOSEARCH_DELETE_AUTO_SEARCH_FAIL,
} from "../../actions/primeSearch/autoSearchAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../../actions/authAction";

const INIT_STATE = {
    isFetching: false,
    autoSearchTitle:[],
    autoSearchContent:[],
    changeAutoSearchStatus:[],
    deleteAutoSearchStatus:null,
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case AUTOSEARCH_GET_AUTO_SEARCH_TITLE_LOADING:
            return {
                ...state,
                isFetching: true,
                autoSearchTitle: []
            };
        case AUTOSEARCH_GET_AUTO_SEARCH_TITLE_SUCCESS:
            return {
                ...state,
                isFetching: false,
                autoSearchTitle: action.autoSearchTitle
            };
        case AUTOSEARCH_GET_AUTO_SEARCH_TITLE_FAIL:
            return {
                ...state,
                isFetching: false,
            };

        case AUTOSEARCH_GET_AUTO_SEARCH_CONTENT_LOADING:
            return {
                ...state,
                isFetching: true,
                autoSearchContent: []
            };
        case AUTOSEARCH_GET_AUTO_SEARCH_CONTENT_SUCCESS:
            return {
                ...state,
                isFetching: false,
                autoSearchContent: action.autoSearchContent
            };
        case AUTOSEARCH_GET_AUTO_SEARCH_CONTENT_FAIL:
            return {
                ...state,
                isFetching: false,
            };

        case AUTOSEARCH_CHANGE_AUTO_SEARCH_STATUS_LOADING:
            return {
                ...state,
                isFetching: true,
                changeAutoSearchStatus: []
            };
        case AUTOSEARCH_CHANGE_AUTO_SEARCH_STATUS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                changeAutoSearchStatus: action.changeAutoSearchStatus
            };
        case AUTOSEARCH_CHANGE_AUTO_SEARCH_STATUS_FAIL:
            return {
                ...state,
                isFetching: false,
            };

        case AUTOSEARCH_DELETE_AUTO_SEARCH_LOADING:
            return {
                ...state,
                isFetching: true,
                deleteAutoSearchStatus: []
            };
        case AUTOSEARCH_DELETE_AUTO_SEARCH_SUCCESS:
            return {
                ...state,
                isFetching: false,
                deleteAutoSearchStatus: action.deleteAutoSearchStatus
            };
        case AUTOSEARCH_DELETE_AUTO_SEARCH_FAIL:
            return {
                ...state,
                isFetching: false,
            };

        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};