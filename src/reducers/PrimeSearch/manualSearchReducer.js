import {
    MANUALSEARCH_SEARCH_SUBCONTRACTOR_LOADING,
    MANUALSEARCH_SEARCH_SUBCONTRACTOR_FAIL,
    MANUALSEARCH_SEARCH_SUBCONTRACTOR_LOAD_SUCCESS,
    MANUALSEARCH_AUTO_SEARCH_SUBCONTRACTOR_LOADING,
    MANUALSEARCH_AUTO_SEARCH_SUBCONTRACTOR_SUCCESS,
    MANUALSEARCH_AUTO_SEARCH_SUBCONTRACTOR_FAIL,

} from "../../actions/primeSearch/primeSearchSubcontractorAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../../actions/authAction";


const INIT_STATE = {
    isFetching: false,
    autoSearchStatus:"",
    searchedSubInfo: [],
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case MANUALSEARCH_SEARCH_SUBCONTRACTOR_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case MANUALSEARCH_SEARCH_SUBCONTRACTOR_LOAD_SUCCESS:
            return {
                ...state,
                isFetching: false,
                searchedSubInfo: action.searchedSubInfo
            };
        case MANUALSEARCH_SEARCH_SUBCONTRACTOR_FAIL:
            return {
                ...state,
                isFetching: false,
            };

        case MANUALSEARCH_AUTO_SEARCH_SUBCONTRACTOR_LOADING:
            return {
                ...state,
                isFetching: true,
                autoSearchStatus: ""
            };
        case MANUALSEARCH_AUTO_SEARCH_SUBCONTRACTOR_SUCCESS:
            return {
                ...state,
                isFetching: false,
                autoSearchStatus: action.autoSearchStatus
            };
        case MANUALSEARCH_AUTO_SEARCH_SUBCONTRACTOR_FAIL:
            return {
                ...state,
                isFetching: false,
            };

        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};