import {
    MANUALSEARCH_SUB_SENT_REQUEST_LOADING,
    MANUALSEARCH_SUB_SENT_REQUEST_SUCCESS,
    MANUALSEARCH_SUB_SENT_REQUEST_FAIL,
} from "../../actions/primeSearch/sentRequestAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../../actions/authAction";


const INIT_STATE = {
    isFetching: false,
    sentRequestStatus: [],
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case MANUALSEARCH_SUB_SENT_REQUEST_LOADING:
            return {
                ...state,
                isFetching: true,
                sentRequestStatus: []
            };
        case MANUALSEARCH_SUB_SENT_REQUEST_SUCCESS:
            return {
                ...state,
                isFetching: false,
                sentRequestStatus: action.sentRequestStatus
            };
        case MANUALSEARCH_SUB_SENT_REQUEST_FAIL:
            return {
                ...state,
                isFetching: false,
            };

        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};