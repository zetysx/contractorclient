import {
    SUBREQUEST_SUB_LOAD_LIST_REQUEST_LOADING,
    SUBREQUEST_SUB_LOAD_LIST_REQUEST_SUCCESS,
    SUBREQUEST_WORKER_ASSIGN_LOADING,
    SUBREQUEST_WORKER_ASSIGN_SUCCESS,
    SUBREQUEST_WORKER_ASSIGN_FAIL,
    SUBREQUEST_DECLINE_REQUEST_LOADING,
    SUBREQUEST_DECLINE_REQUEST_SUCCESS,
    SUBREQUEST_WORKER_ASSIGN_BY_SUB_LOADING, SUBREQUEST_WORKER_ASSIGN_BY_SUB_LOAD_SUCCESS
} from "../actions/subRequestAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";

const INIT_STATE = {
    isFetching: false,
    isFetchingWorker:false,
    workers: [],
    requests: [],
    assignStatus:{},
    declineStatus:[]
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case SUBREQUEST_WORKER_ASSIGN_BY_SUB_LOADING:
            return {
                ...state,
                isFetchingWorker: true,
            };
        case SUBREQUEST_WORKER_ASSIGN_BY_SUB_LOAD_SUCCESS:
            return {
                ...state,
                isFetchingWorker: false,
                workers: action.workers
            };

        case SUBREQUEST_SUB_LOAD_LIST_REQUEST_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case SUBREQUEST_SUB_LOAD_LIST_REQUEST_SUCCESS:
            return {
                ...state,
                isFetching: false,
                requests: action.requests
            };
        case SUBREQUEST_WORKER_ASSIGN_LOADING:
            return {
                ...state,
                isFetching: true,
                assignStatus: ""
            };
        case SUBREQUEST_WORKER_ASSIGN_SUCCESS:
            return {
                ...state,
                isFetching: false,
                assignStatus: action.assignStatus
            };
        case SUBREQUEST_WORKER_ASSIGN_FAIL:
            return {
                ...state,
                isFetching: false,
                assignStatus: action.assignStatus
            };
        case SUBREQUEST_DECLINE_REQUEST_LOADING:
            return {
                ...state,
                isFetching: true,
                declineStatus: ""
            };
        case SUBREQUEST_DECLINE_REQUEST_SUCCESS:
            return {
                ...state,
                isFetching: false,
                declineStatus: action.declineStatus
            };

        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};