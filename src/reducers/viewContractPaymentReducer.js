import {
    CONTRACT_GET_CONTRACT_PAYMENT_LOADING,
    CONTRACT_GET_CONTRACT_PAYMENT_SUCCESS
} from "../actions/viewContractPaymentAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";

const INIT_STATE = {
    isFetching: false,
    contractPayment: null,
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case CONTRACT_GET_CONTRACT_PAYMENT_LOADING:
            return {
                isFetching: true,
                ...state,
                contractPayment: null
            };
        case CONTRACT_GET_CONTRACT_PAYMENT_SUCCESS:
            return {
                isFetching: false,
                ...state,
                contractPayment: action.contractPayment
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};