import {
    ADMINVIEWRATING_RATING_LOADING,
    ADMINVIEWRATING_RATING_SUCCESS,
    ADMINVIEWRATING_RATING_FAIL,
} from "../actions/adminRatingAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";


const INIT_STATE = {
    isFetching: false,
    ratingList: [],
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case ADMINVIEWRATING_RATING_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case ADMINVIEWRATING_RATING_SUCCESS:
            return {
                ...state,
                isFetching: false,
                ratingList: action.ratingList
            };
        case ADMINVIEWRATING_RATING_FAIL:
            return {
                ...state,
                isFetching: false,
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};