import {
    SUBONGOINGCONTRACT_SUB_LOAD_ALL_CONTRACT_LOADING,
    SUBONGOINGCONTRACT_SUB_LOAD_ALL_CONTRACT_SUCCESS,
} from "../actions/subOnGoingContractAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";

const INIT_STATE = {
    isFetching: false,
    subContracts: [],
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case SUBONGOINGCONTRACT_SUB_LOAD_ALL_CONTRACT_LOADING:
            return {
                ...state,
                isFetching: true,
                subContracts: []
            };
        case SUBONGOINGCONTRACT_SUB_LOAD_ALL_CONTRACT_SUCCESS:
            return {
                ...state,
                isFetching: false,
                subContracts: action.subContracts
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};