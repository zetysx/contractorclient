import { LOCATION_LOADING, LOCATION_LOAD_SUCCESS } from "../actions/locationAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";


const INIT_STATE = {
    isFetching: false,
    locations: []
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case LOCATION_LOADING:
            return {
                isFetching: true,
                locations: []
            };
        case LOCATION_LOAD_SUCCESS:
            return {
                isFetching: false,
                locations: action.locations
            };
        // case GET_AUTH_TOKEN_AND_LOGIN_FAILURE:
        //     return {
        //         isFetching: false,
        //         isLogged: false,
        //         profile: {}
        //     };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};