import {
    GET_ADMIN_OVERVIEW_LOADING,
    GET_ADMIN_OVERVIEW_SUCCESS,
    GET_ADMIN_OVERVIEW_FAIL,
} from "../actions/adminOverviewAction";

import {
    REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS
} from "../actions/authAction";


const INIT_STATE = {
    isFetching: false,
    adminOverview: null,
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_ADMIN_OVERVIEW_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case GET_ADMIN_OVERVIEW_SUCCESS:
            return {
                ...state,
                isFetching: false,
                adminOverview: action.adminOverview
            };
        case GET_ADMIN_OVERVIEW_FAIL:
            return {
                ...state,
                isFetching: false,
                adminOverview: null,
            };
        case REMOVE_AUTH_TOKEN_AND_LOGOUT_SUCCESS:
            return {
                ...INIT_STATE
            };
        default:
            return state;
    }
};