import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
    Spin,
    Row,
    Col
} from 'antd';

import { checkTokenAndGetUserGeneralInfo } from "../actions/authAction";

class UserAuthLoading extends Component {
    componentDidMount() {
        const onFailCallback = () => {
            this.props.history.push({
                pathname: '/login',
            });
        };

        this.props.checkTokenAndGetUserGeneralInfo(undefined, onFailCallback);
    };

    render() {
        return (
            <Row
                type="flex" justify="center" align="middle"
                style={{
                    witdh: "100vh",
                    height: "100vh",
                    backgroundColor: "#fafafa",
                }}
            >
                <Col
                    style={{
                        paddingBottom: 100,
                        textAlign: "center"
                    }}
                >
                    <Spin spinning={true} delay={0} size="large">
                    </Spin>
                    <div
                        style={{
                            fontSize: 14,
                            fontWeight: 600,
                            color: "#8fc9fb"
                        }}
                    >
                        loading
                    </div>
                </Col>
            </Row>
        );
    }
};

const mapStateToProps = (state) => {
    return {
    };
};

const mapDispatchActionToProps = {
    checkTokenAndGetUserGeneralInfo
};

export default connect(
    mapStateToProps,
    mapDispatchActionToProps,
)(UserAuthLoading);