import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";

import {ROLESNAME} from "../../constants/role";

import {getListAllUserAccount, disableAccount, searchAccount} from '../../actions/adminUserManageAction';

import {Button, Icon, Tooltip, Tag, Spin, Pagination, Drawer, Input, Radio} from "antd";
import ViewWorkerList from "../../components/ViewWorkerList";

const Search = Input.Search;

class AdminUserManage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pageIndex: 0,
            visible: false,
            role: null,
            isEnable: null,
            searchText: ""
        }
    }

    componentDidMount() {
        this.props.getListAllUserAccount(0);
    };

    renderRoleTag = (role, workers) => {
        if (role === 1) {
            return <Tag color="#f50">{ROLESNAME.PRIME}</Tag>
        } else if (role === 2) {
            return <div>
                <Tag color="#108ee9">{ROLESNAME.SUB}</Tag>
                {(
                    (workers)
                        ?
                        (<Button
                            style={{
                                fontSize: "12px",
                                padding: "0 8px",
                                height: "25px",
                                marginTop: "5px"
                            }}
                            onClick={() => {
                                this.workerForDrawer = workers;
                                this.setState({visible: true});
                            }}>
                            <span>{workers.length} nhân công</span>
                        </Button>)
                        :
                        ("")
                )}
            </div>
        }
        return "";
    };

    renderDisableButton = (enable, contractOngoing, accountId) => {
        const onDisableSuccess = () => {
            this.props.getListAllUserAccount(this.state.pageIndex);
        };

        let disabledButton = false;
        if (contractOngoing > 0) {
            disabledButton = true;
        }

        if (enable === true) {
            return (
                <Tooltip
                    title={(contractOngoing === 0) ? null : `Bạn không thể vô hiệu hoá vì Nhà thầu này đang có ${contractOngoing} dự án đang làm`
                    }
                >
                    <Button
                        type={"danger"}
                        style={{
                            padding: "0 10px",
                            fontSize: "12px",
                            width: "85px"
                        }}
                        disabled={disabledButton}
                        onClick={() => {
                            this.props.disableAccount(accountId, onDisableSuccess);
                        }}
                    >Vô hiệu hoá</Button>
                </Tooltip>
            )
        } else {
            return (
                <Button
                    type={"primary"}
                    style={{
                        padding: "0 10px",
                        fontSize: "12px",
                        width: "85px"
                    }}
                    onClick={() => {
                        this.props.disableAccount(accountId, onDisableSuccess);
                    }}
                >Kích hoạt</Button>
            )
        }
    };

    renderAccountListRow = (accountList) => {
        if (accountList) {
            if (accountList.length === 0) {
                return (
                    <tr>
                        <td colSpan={8} style={{
                            padding: "40px 0",
                            textAlign: "center",
                            fontSize: "16px",
                        }}>
                            Không có kết quả
                        </td>
                    </tr>
                )
            } else {
                const styleTd = {
                    padding: "25px 10px",
                    fontSize: "12px"
                };
                return accountList.map((account) => {
                    return (
                        <tr key={account.accountId}
                            style={
                                ((account.enable === true) ? {
                                        borderBottom: "1px solid #f1f1f1",
                                        padding: "20px 0"
                                    } :
                                    {
                                        borderBottom: "1px solid #f1f1f1",
                                        padding: "20px 0",
                                        background: "rgb(251, 237, 237)",
                                    })
                            }>
                            <td style={{
                                paddingLeft: "20px",
                                textAlign: "center"
                            }}>
                                {this.renderRoleTag(account.role, account.workersOfSub)}
                            </td>
                            <td style={styleTd}>
                                <span style={{
                                    padding: "2px 6px",
                                    background: "rgb(162, 162, 162)",
                                    textAlign: "center",
                                    color: "#fff",
                                    borderRadius: " 5px",
                                    fontWeight: "bold",
                                    fontSize: "12px",
                                }}>{account.accountId}</span>
                            </td>
                            <td style={styleTd}>
                                <Link
                                    to={`/profile/${account.accountId}`}>{account.username}</Link>
                            </td>
                            <td style={styleTd}>
                                {account.companyName}
                            </td>
                            <td style={styleTd}>
                                {account.firstName} {account.lastName}
                            </td>
                            <td style={styleTd}>
                                {account.phone}
                            </td>
                            <td style={styleTd}>
                                {account.address}
                            </td>
                            <td style={styleTd}>
                                {account.email}
                            </td>
                            <td style={{
                                paddingRight: "20px",
                            }}>
                                {this.renderDisableButton(account.enable, account.contractOngoing, account.accountId)}
                            </td>
                        </tr>
                    )
                });
            }
        }
    };

    render() {
        const styleTd = {
            padding: "25px 5px",
        };
        return (
            <div>
                <Drawer
                    title={"Danh sách nhân công"}
                    placement="right"
                    closable={false}
                    onClose={this._onClickCloseModal}
                    visible={this.state.visible}
                >
                    <ViewWorkerList workerDTOS={this.workerForDrawer}/>
                </Drawer>


                <div style={{padding: "50px 50px"}}>

                    <div
                        style={{
                            marginBottom: "40px",
                        }}
                    >
                        <Search
                            placeholder="Tìm kiếm Nhà thầu"
                            onChange={event => {
                                this.setState({searchText: event.target.value})
                            }}
                            value={this.state.searchText}
                            onSearch={event => this._onClickSearchContractor()}
                            style={{
                                width: "300px",
                            }}
                        />
                        <Radio.Group defaultValue={null} buttonStyle="solid"
                                     style={{
                                         display: "inline",
                                         marginLeft: "40px"
                                     }}
                                     onChange={event => {
                                         this.setState({role: event.target.value},
                                             () => {
                                                 this._onClickSearchContractor()
                                             })
                                     }}
                        >
                            <Radio.Button value="1">Thầu chính</Radio.Button>
                            <Radio.Button value="2">Thầu phụ</Radio.Button>
                            <Radio.Button value={null}>Cả 2</Radio.Button>
                        </Radio.Group>

                        <Radio.Group defaultValue={null} buttonStyle="solid"
                                     style={{
                                         display: "inline",
                                         marginLeft: "40px"
                                     }}
                                     onChange={event => {
                                         this.setState({isEnable: event.target.value},
                                             () => {
                                                 this._onClickSearchContractor()
                                             })
                                     }}
                        >
                            <Radio.Button value="false">Vô hiệu</Radio.Button>
                            <Radio.Button value="true">Kích hoạt</Radio.Button>
                            <Radio.Button value={null}>Cả 2</Radio.Button>
                        </Radio.Group>


                    </div>

                    <Spin spinning={this.props.isFetching}>
                        <table
                            border="0"
                            style={{
                                background: "#fff",
                                width: "100%"
                            }}>
                            <thead>
                            <tr style={{
                                fontSize: "14px",
                                background: "#c9e4ff"
                            }}>
                                <td></td>
                                <td style={styleTd}><Tooltip
                                    title="Mã tài khoản. Mỗi tài khoản người dùng sẽ có một mã số duy nhất.">
                                    Mã <Icon type="question-circle"/>
                                </Tooltip></td>
                                <td style={styleTd}>Tài khoản</td>
                                <td style={styleTd}>Tên công ty</td>
                                <td style={styleTd}>Họ và tên</td>
                                <td style={styleTd}>SĐT</td>
                                <td style={styleTd}>Địa chỉ</td>
                                <td style={styleTd}>Email</td>
                                <td style={styleTd}></td>
                            </tr>
                            </thead>
                            <tbody>
                            {this.renderAccountListRow(this.props.allUserAccount.accountList)}
                            </tbody>
                        </table>
                    </Spin>

                    <div
                        style={{
                            textAlign: "center",
                            background: "rgb(255, 255, 255)",
                            marginTop: "25px",
                            padding: "20px 0px",
                            borderRadius: "10px",
                            width: "40%",
                            marginLeft: "30%",
                        }}>
                        <Pagination
                            onChange={this._onChangePageIndex}
                            defaultCurrent={this.state.pageIndex}
                            pageSize={20}
                            total={this.props.allUserAccount.pageTotal}
                        />
                    </div>
                </div>
            </div>
        );
    }

    _onClickCloseModal = () => {
        this.setState({
            visible: false,
        });
    };

    _onChangePageIndex = (page) => {
        this.setState({pageIndex: page - 1}, () => {
            const searchData = {
                searchText: this.state.searchText,
                isEnable: this.state.isEnable,
                role: this.state.role,
            };
            this.props.searchAccount(searchData, this.state.pageIndex);
        });
    };

    _onClickSearchContractor = () => {
        const searchData = {
            searchText: this.state.searchText,
            isEnable: this.state.isEnable,
            role: this.state.role,
        };
        this.props.searchAccount(searchData, 0);
    };
};

const
    mapStateToProps = (state) => {
        return {
            allUserAccount: state.adminUserManageReducer.allUserAccount,
            isFetching: state.adminUserManageReducer.isFetching
        }
    };

const
    mapDispatchActionToProps = {
        getListAllUserAccount,
        disableAccount,
        searchAccount
    };

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(AdminUserManage)
;