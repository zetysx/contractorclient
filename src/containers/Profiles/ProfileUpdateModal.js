import React, { Component } from 'react';
import { TABLE_LIST_CONFIG } from "../../constants/requestContractConstants";
import { Input, Modal, Icon, message, Spin } from "antd";
import { ICON_ANTD_CONSTANTS } from "../../constants/workerConstants";
import { connect } from 'react-redux';

import ImgurUploader from "../../components/ImgurUploader";
import { validateRequiredData, validateGeneralDataBounaries } from "../../helpers/validateHelper";
import { changeUpdateProfileModalVisibility, updateUserProfile } from "../../actions/profileAction";
import { ROLESID } from "../../constants/role";
import {loadUserProfile} from "../../actions/userProfileAction";

class ProfileUpdateModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            profile: null
        }
    }

    componentDidMount() {
        this.setState({
            ...this.state,
            profile: {...this.props.profileReducer.profileUpdate}
        });
    }

    componentWillReceiveProps(nextProps, state) {
        // if(nextProps.profileReducer.profileUpdate !== state.profile) {
            this.setState({
                ...this.state,
                profile: {...nextProps.profileReducer.profileUpdate}
            });
        // }
    }

    _onHandleProfileFieldChange = (input, value) => {
        let state = {...this.state};
        state.profile[input] = value;
        this.setState({
            ...state
        })
    }


    getRequiredFieldsBaseOnRole = (profile) => {
        if(profile) {
            const generalRequiredFields = {
                email       : profile.email,
                phone       : profile.phone,
                address     : profile.address,
                firstName   : profile.firstName,
                lastName    : profile.lastName,
            }
    
            switch(this.props.authReducer.profile.role) {
                case ROLESID.PRIME:
                    return generalRequiredFields;
                case ROLESID.SUB:
                    return generalRequiredFields;
                default:
                    return null;
            }
        }
    }


    _onUpdateClick = () => {
        
        const onHandleSuccessCallbackUpdateProfile = () => {
            message.success("Cập nhật trang cá nhân thành công");
            this._onCloseModal();
            this.props.loadUserProfile(this.props.authReducer.profile.accountId);
        }
    
        const onHandleFailCallbackUpdateProfile = () => {
            message.warning("Cập nhật trang cá nhân thất bại");
            this._onCloseModal();
        }

        const profileData = {...this.state.profile};
        
        if(profileData) {
            let listRequiredFields = this.getRequiredFieldsBaseOnRole(profileData);

            let errorMessageRequired = validateRequiredData(listRequiredFields);
            if(errorMessageRequired != null) {
                message.warn(errorMessageRequired);
                return;
            }

            let errorMessageGeneral = validateGeneralDataBounaries(profileData);
            if(errorMessageGeneral != null) {
                message.warn(errorMessageGeneral);
                return;
            }
        
            this.props.updateUserProfile(profileData, 
                    onHandleSuccessCallbackUpdateProfile, 
                    onHandleFailCallbackUpdateProfile);
        }
    }

    _onFinishedUploadingAvatar = (link) => {
        link.trim() === "" ? this._onHandleProfileFieldChange("avatar", null) : this._onHandleProfileFieldChange("avatar", link);
    }


    renderGeneralDetailFields = (profile) => {
        return (
            <div>
                <div>
                    <h3 style={{fontStyle: "italic", textAlign: "center"}}>{TABLE_LIST_CONFIG.ACCOUNT.AVATAR.HEADER}</h3>
                    <div style={{textAlign: "center"}}>
                        <div style={{display: "inline-block"}}>
                            <ImgurUploader 
                                imageUrl={profile.avatar}
                                onUploadDone={(link) => { this._onFinishedUploadingAvatar(link) }}
                            />
                        </div>
                    </div>
                </div>
                <div>
                    <h3 style={{fontStyle: "italic"}}>
                        <Icon type={ICON_ANTD_CONSTANTS.PROFILE}/> {TABLE_LIST_CONFIG.ACCOUNT.FIRST_NAME.HEADER} *
                    </h3>
                    <Input
                        name={TABLE_LIST_CONFIG.ACCOUNT.FIRST_NAME.DATAINDEX_KEY}
                        value={profile.firstName}
                        onChange={e => this._onHandleProfileFieldChange(e.target.name, e.target.value)}
                    />
                </div>
                <div>
                    <h3 style={{fontStyle: "italic"}}>
                        <Icon type={ICON_ANTD_CONSTANTS.PROFILE}/> {TABLE_LIST_CONFIG.ACCOUNT.LAST_NAME.HEADER} *
                    </h3>
                    <Input
                        name={TABLE_LIST_CONFIG.ACCOUNT.LAST_NAME.DATAINDEX_KEY}
                        value={profile.lastName}
                        onChange={e => this._onHandleProfileFieldChange(e.target.name, e.target.value)}
                    />
                </div>
                <div>
                    <h3 style={{fontStyle: "italic"}}>
                    <Icon type={ICON_ANTD_CONSTANTS.PHONE}/> {TABLE_LIST_CONFIG.ACCOUNT.PHONE.HEADER} *
                    </h3>
                    <Input
                        name={TABLE_LIST_CONFIG.ACCOUNT.PHONE.DATAINDEX_KEY}
                        value={profile.phone}
                        onChange={e => this._onHandleProfileFieldChange(e.target.name, e.target.value)}
                    />
                </div>
                <div>
                    <h3 style={{fontStyle: "italic"}}>
                        <Icon type={ICON_ANTD_CONSTANTS.ENVIRONMENT}/> {TABLE_LIST_CONFIG.ACCOUNT.ADDRESS.HEADER} *
                    </h3>
                    <Input
                        name={TABLE_LIST_CONFIG.ACCOUNT.ADDRESS.DATAINDEX_KEY}
                        value={profile.address}
                        onChange={e => this._onHandleProfileFieldChange(e.target.name, e.target.value)}
                    />
                </div>
                <div>
                    <h3 style={{fontStyle: "italic"}}>
                        <Icon type={ICON_ANTD_CONSTANTS.MAIL}/> {TABLE_LIST_CONFIG.ACCOUNT.EMAIL.HEADER} *
                    </h3>
                    <Input
                        name={TABLE_LIST_CONFIG.ACCOUNT.EMAIL.DATAINDEX_KEY}
                        value={profile.email}
                        onChange={e => this._onHandleProfileFieldChange(e.target.name, e.target.value)}
                    />
                </div>
            </div>
        )
    }

    renderRoleDetailFields = (profile) => {
        switch(this.props.authReducer.profile.role) {
            case ROLESID.SUB:
                return (
                    <div>
                        {/*
                            <div>
                            <h3 style={{fontStyle: "italic"}}>
                                <Icon type={ICON_ANTD_CONSTANTS.PROFILE}/> {TABLE_LIST_CONFIG.PROFESSION.HEADER} *
                            </h3>
                            
                        </div>
                        <div>
                            <h3 style={{fontStyle: "italic"}}>
                                <Icon type={ICON_ANTD_CONSTANTS.ENVIRONMENT}/> Khu vực làm việc *
                            </h3>
                        </div>
                        */}
                        <div>
                            <h3 style={{fontStyle: "italic"}}>
                                <Icon type={ICON_ANTD_CONSTANTS.CONTACT}/> Số fax
                            </h3>
                            <Input
                                name="faxNumber"
                                value={profile.faxNumber}
                                onChange={e => this._onHandleProfileFieldChange(e.target.name, e.target.value)}
                            />
                        </div>
                        <div>
                            <h3 style={{fontStyle: "italic"}}>
                                <Icon type={ICON_ANTD_CONSTANTS.BANK}/> Tên doanh nghiệp
                            </h3>
                            <Input
                                name="companyName"
                                value={profile.companyName}
                                onChange={e => this._onHandleProfileFieldChange(e.target.name, e.target.value)}
                            />
                        </div>
                        <div>
                            <h3 style={{fontStyle: "italic"}}>
                                <Icon type={ICON_ANTD_CONSTANTS.PROFILE}/> Lời giới thiệu
                            </h3>
                            <Input.TextArea
                                name="description"
                                value={profile.description}
                                onChange={e => this._onHandleProfileFieldChange(e.target.name, e.target.value)}
                            />
                        </div>
                    </div>
                )
            case ROLESID.PRIME:
                return (
                    <div>
                        <div>
                            <h3 style={{fontStyle: "italic"}}>
                                <Icon type={ICON_ANTD_CONSTANTS.BANK}/> Tên doanh nghiệp
                            </h3>
                            <Input
                                name="companyName"
                                value={profile.companyName}
                                onChange={e => this._onHandleProfileFieldChange(e.target.name, e.target.value)}
                            />
                        </div>
                        <div>
                            <h3 style={{fontStyle: "italic"}}>
                                <Icon type={ICON_ANTD_CONSTANTS.CONTACT}/> Số fax
                            </h3>
                            <Input
                                name="faxNumber"
                                value={profile.faxNumber}
                                onChange={e => this._onHandleProfileFieldChange(e.target.name, e.target.value)}
                            />
                        </div>
                        <div>
                            <h3 style={{fontStyle: "italic"}}>
                                <Icon type={ICON_ANTD_CONSTANTS.BUSINESS_LICENSE_CERTIFICATED}/> Giấy phép kinh doanh
                            </h3>
                            <Input
                                name="businessLicense"
                                value={profile.businessLicense}
                                onChange={e => this._onHandleProfileFieldChange(e.target.name, e.target.value)}
                            />
                        </div>                        
                    </div>
                )
            default:
                return null;
        }
    }

    renderProfileDetail = (profile) => {
        if (!profile) {
            return null;
        }
        return (
            <div>
                {this.renderGeneralDetailFields(profile)}
                {this.renderRoleDetailFields(profile)}
            </div>
        )
    }

    _onHandleCancel = () => {
        this._onCloseModal();
    }


    _onCloseModal = () => {

        const resetState = () => {
            this.setState(() => {
                return {profile: {}}
            })
        }

        this.props.changeUpdateProfileModalVisibility();
        resetState();
    }

    

    render() {
        return (
            <Spin spinning={this.props.profileReducer.isFetching} size="large">
                <Modal
                    title="Thông tin cá nhân"
                    okText="Lưu"
                    cancelText="Hủy"
                    onCancel={() => this._onHandleCancel()}
                    onOk={() => this._onUpdateClick()}
                    visible={this.props.profileReducer.updateModalVisible}
                >
                    {this.renderProfileDetail(this.state.profile)}
                </Modal>
            </Spin>
            
        )
    }
}


function mapStateToProps(state) {
    return {
        authReducer: state.authReducer,
        profileReducer: state.profileReducer
    };
}

const mapDispatchActionToProps = {
    changeUpdateProfileModalVisibility,
    updateUserProfile,
    loadUserProfile
}

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(ProfileUpdateModal);

