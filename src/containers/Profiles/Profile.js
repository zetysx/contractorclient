import React, {Component} from 'react';
import {connect} from 'react-redux';
import {loadUserProfile, loadRatingList} from "../../actions/userProfileAction";
import {ROLESID} from "../../constants/role";
import PrimeProfile from "./PrimeProfile";
import SubProfile from "./SubProfile";
import WorkerProfile from "./WorkerProfile";

class Profile extends Component {

    componentDidMount() {
        this.props.loadUserProfile(this.props.match.params.id);
        this.currentId = this.props.match.params.id;
    }

    componentWillReceiveProps(nextProps, nextContext) { // force update when change accountId in link
        if (this.currentId !== nextProps.match.params.id) {
            this.props.loadUserProfile(this.props.match.params.id);
            this.currentId = this.props.match.params.id;
        }
    }

    render() {
        if (this.props.match.params) {
            if (this.props.match.params.id) {
                if (this.props.userProfile.role === ROLESID.PRIME) return <PrimeProfile history={this.props.history}
                                                                                        userProfile={this.props.userProfile}/>;
                else if (this.props.userProfile.role === ROLESID.SUB) return <SubProfile history={this.props.history}
                                                                                         userProfile={this.props.userProfile}/>;
                else if (this.props.userProfile.role === ROLESID.WORKER) return <WorkerProfile
                    history={this.props.history}
                    userProfile={this.props.userProfile}/>;
                return <div>Profile này không tồn tại!</div>
            }
        }
        return <div>Đang tải...</div>
    }
}

function mapStateToProps(state) {
    return {
        accountId: state.authReducer.profile.accountId,
        userProfile: state.userProfileReducer.userProfile
    };
}

export default connect(
    mapStateToProps,
    {loadUserProfile, loadRatingList}
)(Profile);