import React, {Component} from 'react';
import {connect} from 'react-redux';
// import {loadUserProfile} from "../../actions/userProfileAction";
// import {Row, Col, Icon, Button} from "antd";
import { ROLESID } from "../../constants/role";
import { passUserProfileToModal, changeUpdateProfileModalVisibility } from "../../actions/profileAction";
import ProfileUpdateModal from "../Profiles/ProfileUpdateModal";

import {loadUserProfile, loadRatingList} from "../../actions/userProfileAction";
import {Row, Col, Icon, Rate, Card, Pagination, Button} from "antd";
import {Link} from "react-router-dom";
const COVERPROFILE = require("../../assets/coverProfile.jpg");

class SubProfile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pageIndex: 0
        }
    }

    componentDidMount() {
        this.props.loadRatingList(this.props.userProfile.accountId, this.state.pageIndex);
    }

    _onUpdateProfileClick = (profile) => {
        if(profile) {
            this.props.changeUpdateProfileModalVisibility();
            this.props.passUserProfileToModal(profile);
        }
    }

    renderSubProList = (subProList) => {
        if (subProList)
            return subProList.map((item) => {
                return <Row
                    key={`${item.professionName}profession`}
                >
                    <Col span={13}>{item.professionName}</Col>
                    <Col span={1}>:</Col>
                    <Col span={10} style={{
                        textAlign: "center",
                        background: "rgb(255, 212, 0)",
                        color: "#000",
                        borderRadius: "100px",
                        marginBottom: "10px",
                    }}>{item.price} <sup>đ</sup></Col>
                </Row>
            })
    };

    renderSubLocationList = (subLocationList) => {
        if (subLocationList)
            return subLocationList.map((item) => {
                return <li key={`${item}location`} style={{margin:"5px 0"}}><Icon type="environment" theme="twoTone" /> {item}</li>
            })
    };

    renderRatingList = () => {
        if (!this.props.ratingList.ratingListItemDTOList) return null;
        return this.props.ratingList.ratingListItemDTOList.map((ratingItem, index) => {
                return (
                    <Row style={{
                        padding: "20px 20px",
                        borderBottom: "solid 1px #efefef",
                    }} key={index}>
                        <Row style={{
                            marginBottom: "10px"
                        }}>
                            <Col span={12} style={{
                                fontSize: "18px",
                            }}>
                                <Link
                                    to={`/profile/${ratingItem.raterAccountId}`}>{ratingItem.raterCompanyname}</Link>
                            </Col>
                            <Col span={9} style={{
                                textAlign: "right",
                                paddingRight: "10px",
                                fontStyle: "italic",
                            }}>
                                <span style={{
                                    fontSize: "12px",
                                    paddingRight: "5px",
                                }}>trong Dự án</span><span>{ratingItem.profession}</span>
                            </Col>
                            <Col span={3} style={{textAlign: "right"}}>
                                <span><Icon type="environment"/> {ratingItem.location}</span>
                            </Col>
                        </Row>
                        <Row style={{
                            padding: "10px 10px 10px 20px",
                            background: "#f7f7f7",
                            borderRadius: "20px",
                            margin: "0 10px"
                        }}>
                            <Rate disabled defaultValue={ratingItem.rating} style={{
                                fontSize: "23px",
                                display: "block",
                                marginBottom: "10px",
                            }}/>
                            {ratingItem.review}
                        </Row>
                    </Row>
                )
            }
        )
    };

    render() {
        if (!this.props.userProfile) {
            return <div/>
        }

        const userProfile = this.props.userProfile;
        return (
            <div
                style={{
                    width: "100%",
                    padding: "50px",
                }}
            >
            {this.props.authReducer.profile.role === ROLESID.SUB ?
                (
                    <Row>
                        <Button
                            type="primary"
                            size="default"
                            style={{ textAlign: "center" }}
                            icon="edit"
                            onClick={() => this._onUpdateProfileClick(userProfile)}>
                            Cập nhật thông tin cá nhân
                        </Button>
                    </Row>
                ) : (
                    <p></p>
                )
            }
                <Row
                    type="flex" justify="space-between" align="bottom"
                    style={{
                        background: `url(${COVERPROFILE})`,
                        backgroundSize: "cover",
                        backgroundRepeat: "repeat",
                        backgroundAttachment: "fixed",
                        backgroundPosition:"left top",
                        height: "300px",
                        zIndex: "1",
                        position: "relative"
                    }}>
                    <Col span={7} style={{
                        textAlign: "right"
                    }}>
                        <img alt={"avatar"} style={{
                            borderRadius: "100%",
                            width: "200px",
                            height: "200px",
                            position: "relative",
                            bottom: "-49px",
                            border: "5px #fff solid",
                            boxShadow: "0 0 2px #989898",
                        }}
                             src={userProfile.avatar}/>
                    </Col>
                    <Col span={16} style={{
                        position: "relative",
                        top: "20px",
                        left:"-7px"
                    }}>
                         <span style={{
                             fontSize: "20px",
                             letterSpacing: " 2px",
                             borderRadius: "100px",
                             color: "#fff",
                             background: "rgb(60, 139, 206)",
                             textAlign: "center",
                             padding: "5px 10px",
                         }}>Nhà thầu phụ
                    </span>
                        <p style={{
                            fontSize: "25px",
                            fontWeight: "bold",
                            color: "#fff",
                            textShadow:"1px 1px 2px #000",
                            marginTop:"5px",
                            marginLeft:"7px"
                        }}>{userProfile.companyName}</p>
                    </Col>
                </Row>
                <Row
                    type="flex" justify="space-between" align="middle"
                    style={{
                        marginBottom:"40px",
                        background: "#fff",
                        boxShadow: "0 0 1px rgb(226, 226, 226)",
                        borderTop: "0",
                        borderRadius: "0 0 6px 6px,",
                        zIndex: "0",
                        position: "relative"

                    }}
                >
                    <Col span={7}/>
                    <Col span={16}>
                        <p style={{
                            color: "rgb(156, 156, 156)",
                            fontStyle: "italic",
                            fontSize: "18px",
                            marginTop: "17px",
                        }}>Số nhân công: {userProfile.workerNumber}</p>
                    </Col>
                </Row>
                <Row type="flex" justify="space-between" align="top">
                    <Col span={7}>
                        <Card title="" bordered={false} style={{
                            marginBottom: "20px",
                            fontSize:"18px",
                            paddingLeft: "15px",
                            borderLeft: "5px solid #1890ff",
                        }}>
                            <Row style={{padding:"10px 0"}}>
                                  <span style={{paddingRight: "10px"}}>
                                            <Icon type="setting" spin/>
                                        </span>
                                <span style={{paddingRight: "10px"}}>
                                            Dự án đang làm:
                                        </span>
                                <span>
                                            {userProfile.contractOngoing}
                                        </span>
                            </Row>
                            <Row style={{padding:"10px 0"}}>
                                        <span style={{paddingRight: "10px"}}>
                                            <Icon type="file-done"/>
                                        </span>
                                <span style={{paddingRight: "10px"}}>
                                            Dự án đã làm:
                                        </span>
                                <span>
                                          {userProfile.contractComplete}
                                        </span>

                            </Row>
                        </Card>
                        <Card title="Lĩnh vực - giá" bordered={false} style={{
                            marginBottom: "20px",
                        }}>
                            {this.renderSubProList(userProfile.subcontractorProfessions)}
                        </Card>
                        <Card title="Nơi hoạt động" bordered={false} style={{
                            marginBottom: "20px",
                            fontSize:"15px",
                            paddingLeft: "15px",
                        }}>
                            {this.renderSubLocationList(userProfile.locationNames)}
                        </Card>
                        <Card title="Liên hệ" bordered={false} style={{marginBottom: "20px"}}>
                            <Row style={{
                                paddingBottom: "10px",
                            }}>
                                <Col span={3} style={{
                                    fontSize: "15px",
                                }}><Icon type="idcard" theme="twoTone"/></Col><Col style={{}}
                                                                                   span={16}>
                                <div style={{color:"#1890ff"}}>Người đại điện:</div>
                                {userProfile.firstName} {userProfile.lastName}
                            </Col>
                            </Row>
                            <Row style={{
                                paddingBottom: "10px",
                            }}>
                                <Col span={3} style={{
                                    fontSize: "15px",
                                }}><Icon type="phone" theme="twoTone"/></Col><Col style={{}}
                                                                                  span={16}>{userProfile.phone}</Col>
                            </Row>
                            <Row style={{
                                paddingBottom: "10px",
                            }}>
                                <Col span={3} style={{
                                    fontSize: "15px",
                                }}><Icon type="home" theme="twoTone"/></Col><Col style={{}}
                                                                                 span={16}>{userProfile.address}</Col>
                            </Row>
                            <Row style={{
                                paddingBottom: "10px",
                            }}>
                                <Col span={3} style={{
                                    fontSize: "15px",
                                }}><Icon type="mail" theme="twoTone"/></Col><Col style={{}}
                                                                                 span={16}>{userProfile.email}</Col>
                            </Row>
                        </Card>
                    </Col>
                    <Col span={16}>
                        <Card title="Giới thiệu" bordered={false} style={{marginBottom: "20px"}}>
                            {userProfile.description}
                        </Card>

                        <Card title="Xếp hạng" bordered={false} style={{marginBottom: "20px"}}>
                            <Row style={{
                                padding: "0 20px",
                                marginBottom: "20px",
                                fontSize: "15px",
                            }}>
                                <Col span={3} style={{
                                    textAlign:"right",
                                    paddingRight:"6px"
                                }}>
                                <span style={{
                                    position: "relative",
                                    top:"-10px",
                                    fontSize:"57px",
                                    color:"#0084ff"
                                }}>
                                    <Icon type="thunderbolt"/>
                                </span>
                                </Col>
                                <Col span={21}>
                                <span style={{paddingRight: "10px"}}>
                                    Điểm tích luỹ từ công việc đã làm:
                                </span>
                                    <div style={{fontSize:"33px", color:"#0084ff"}}>
                                        {userProfile.experience}
                                    </div>
                                </Col>
                            </Row>
                            <Row style={{
                                padding: "0 20px",
                                fontSize: "15px",
                            }}>
                                <Col span={3} style={{
                                    textAlign:"right",
                                    paddingRight:"6px"
                                }}>
                                <span style={{
                                    position: "relative",
                                    top:"-10px",
                                    fontSize:"57px",
                                    color:"rgb(204, 188, 89)"
                                }}>
                                    <Icon type="team" />
                                </span>
                                </Col>
                                <Col span={21}>
                                <span style={{paddingRight: "10px"}}>
                                    Đánh giá trung bình bởi các Thầu chính đã hợp tác:
                                </span>
                                    <div>
                                        <Rate
                                            style={{
                                                fontSize:"30px"
                                            }}
                                            disabled allowHalf defaultValue={userProfile.avgRating}/>
                                        <span style={{
                                            fontSize: "11px",
                                            color: "#aaa",
                                        }}>(Đánh giá bởi {userProfile.numberRating} nhà thầu chính)</span>

                                    </div>
                                </Col>
                            </Row>
                        </Card>

                        <Card title="Đánh giá từ Nhà thầu" bordered={false} style={{marginBottom: "20px"}}>
                            {this.renderRatingList()}
                            <div
                                style={{
                                    textAlign: "center",
                                    marginTop: "25px",
                                    padding: "20px 0px",
                                }}>
                                <Pagination onChange={this._onChangePageIndex} defaultCurrent={this.state.pageIndex}
                                            pageSize={10}
                                            total={this.props.ratingList.elementNumber}/>
                            </div>
                        </Card>
                    </Col>
                </Row>
                <ProfileUpdateModal/>
            </div>
        );
    }

    _onChangePageIndex = (page) => {
        this.setState({pageIndex: page - 1}, () => {
            this.props.loadRatingList(this.props.userProfile.accountId, this.state.pageIndex);
        });
    };

}

function mapStateToProps(state) {
    return {
        authReducer: state.authReducer,
        profileReducer: state.profileReducer,
        accountId: state.authReducer.profile.accountId,
        ratingList: state.userProfileReducer.ratingList,
    };
}

const mapDispatchActionToProps = {
    loadUserProfile,
    passUserProfileToModal,
    changeUpdateProfileModalVisibility,
    loadRatingList
    };

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(SubProfile);