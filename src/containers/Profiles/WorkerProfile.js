import React, {Component} from 'react';
import {connect} from 'react-redux';
import {loadUserProfile} from "../../actions/userProfileAction";

import {Row, Col, Icon, Button} from "antd";

class WorkerProfile extends Component {


    render() {

        const userProfile = this.props.userProfile;
        if (userProfile) {
            return <div style={{padding: "50px", background: "#fff"}}>
                <Row
                    key={"bigRow"}
                    gutter={16} type="flex" justify="space-around" align="middle" style={{
                    padding: "0 0 30px 0",
                    textAlign: "center",
                }}>
                    <Col span={24}><img alt={"worker avatar"}
                                        style={{borderRadius: "100%", width: "250px", height: "250px"}}
                                        src={userProfile.avatar}/></Col>
                    <div style={{
                        fontSize: "14px",
                        letterSpacing: " 2px",
                        borderRadius: "100px",
                        color: "#fff",
                        width: "130px",
                        background: "rgb(60, 139, 206)",
                        marginBottom: "-10px",
                        marginTop: "20px",
                        lineHeight: "28px",
                    }}>Công nhân
                    </div>

                    <Col span={24}>
                        <p style={{
                            fontSize: "40px",
                            fontWeight: "bold",
                            color: "#3c8bce",
                            marginBottom: "-10px",
                            lineHeight: "90px",
                        }}>{userProfile.username}</p>
                        <Button onClick={() => {
                            this.props.history.push(`/profile/${userProfile.subcontractorAccId}`);
                        }}>
                            Xem thông tin Thầu phụ của công nhân này
                        </Button>
                    </Col>
                </Row>

                <Row gutter={16}>
                    <Col span={3}/>
                    <Col style={{}} span={8}><p style={{
                        fontSize: "20px",
                        fontWeight: "bold",
                    }}>Liên hệ:</p></Col>
                    <Col style={{
                        fontSize: "20px",
                        color: "#e66a00",

                    }} span={10}><p>Lĩnh vực làm việc:</p></Col>
                </Row>

                <Row gutter={16}>
                    <Col span={3}/>
                    <Col span={8}>
                        <Row style={{
                            paddingBottom: "10px",
                        }}>
                            <Col span={3} style={{
                                fontSize: "15px",
                            }}><Icon type="phone" theme="twoTone"/></Col><Col style={{}}
                                                                              span={16}>{userProfile.phone}</Col>
                        </Row>
                        <Row style={{
                            paddingBottom: "10px",
                        }}>
                            <Col span={3} style={{
                                fontSize: "15px",
                            }}><Icon type="home" theme="twoTone"/></Col><Col style={{}}
                                                                             span={16}>{userProfile.address}</Col>
                        </Row>
                        <Row style={{
                            paddingBottom: "10px",
                        }}>
                            <Col span={3} style={{
                                fontSize: "15px",
                            }}><Icon type="mail" theme="twoTone"/></Col><Col style={{}}
                                                                             span={16}>{userProfile.email}</Col>
                        </Row>
                    </Col>
                    <Col
                        style={{
                            top: "-10px",
                            fontSize: "15px",
                        }}
                        span={10}>{userProfile.workerProfessionNames.map((item) => {
                        return <div key={`${item}profession`}>- {item}</div>
                    })}</Col>
                </Row>

            </div>
        } else return <div>Loading...</div>
    }
}

function mapStateToProps(state) {
    return {};
}

export default connect(
    mapStateToProps,
    {loadUserProfile}
)(WorkerProfile);