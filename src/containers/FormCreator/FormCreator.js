import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
    changeFormControl,
    changeEditDrawerVisable,
    changeEditDrawerFormControl,
    changeSaveDrawerVisable
} from '../../actions/formCreatorAction';

import {
    getStyleFromFormatFormCreator
} from "../../helpers/formHelper";

import {
    FORM_CONTROL_TYPE,
    FORM_STATIC_CONTROL_TYPE,
    FORM_ADD_TEMPLATE,
    A4WIDTH,
    A4HEIGHT,
    FORMCONTROL_MIN_WIDTH,
    FORMCONTROL_MIN_HEIGHT,
    DRAG_RESIZE_GRID_NUMBER
} from '../../constants/docketForm';

import FormControlEditDrawer from './FormControlEditDrawer';
import FormSaveDrawer from './FormSaveDrawer'

import { Rnd } from 'react-rnd';
import {
    Icon,
    Row,
    Col,
    Collapse,
    Button
} from 'antd';
const Panel = Collapse.Panel;

export class FormCreator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            a4PageFormScale: 1, //styles to scale A4 Page
        }
    };

    //đọc width từ collumn để scale A4 page cho đúng với tỷ lệ màn hình
    componentDidMount() {
        this.setState({
            a4PageFormScale: this.a4PageFormCol.getBoundingClientRect().width / A4WIDTH,
            A4PageFormContainerWidth: this.a4PageFormCol.getBoundingClientRect().width,
            A4PageFormContainerHeight: A4HEIGHT * this.a4PageFormCol.getBoundingClientRect().width / A4WIDTH,
            ////styles to scale A4 Page
        }, () => {
            this.forceUpdate();
        });
    };

    //render FormControl có type là static
    renderStaticControlContent(control, index) {
        const { statictype, value } = control.config;
        const style = getStyleFromFormatFormCreator(control.format);

        switch (statictype) {
            case FORM_STATIC_CONTROL_TYPE.TEXT: {
                return (
                    <div
                        key={`staticControlContent${index}`}
                        style={style}
                    >
                        {value}
                    </div>
                );
            }
            case FORM_STATIC_CONTROL_TYPE.IMAGE: {
                return (
                    <div
                        key={`staticControlContent${index}`}
                        style={style}
                    >
                        <img
                            src={value}
                            alt={value}
                            style={{ width: '100%', height: '100%' }}
                        />
                    </div>
                );
            }
            default: {
                return null;
            }
        }
    };

    //render FormControl có type là input
    renderInputControlContent(control, index) {
        const { label } = control.config;
        const style = getStyleFromFormatFormCreator(control.format);

        return (
            <div
                key={`inputControlContent${index}`}
                style={style}
            >
                {label}
            </div>
        );
    };

    //render FormControl có type là predefined
    renderPredefinedControlContent(control, index) {
        const { label } = control.config;
        const style = getStyleFromFormatFormCreator(control.format);

        return (
            <div
                key={`predefinedControlContent${index}`}
                style={style}
            >
                {label}
            </div>
        );
    };

    //render nội dung của cái form control
    renderControlContent(control, index) {
        const { type } = control.config;
        switch (type) {
            case FORM_CONTROL_TYPE.STATIC: {
                return this.renderStaticControlContent(control, index);
            }
            case FORM_CONTROL_TYPE.INPUT: {
                return this.renderInputControlContent(control, index);
            }
            case FORM_CONTROL_TYPE.PREDEFINED: {
                return this.renderPredefinedControlContent(control, index);
            }
            default:
                return null;
        }
    };

    //render RND -> bao nội dung Control cho phép kéo thả resize
    renderControlRnd() {
        const { formControl } = this.props;

        const styles = {
            dragContainer: {
                boxSizing: "border-box",
                position: "absolute",
                right: 5,
                bottom: 5,
            },
            formControlContainer: {
                boxSizing: "border-box",
                overflow: "hidden",
                width: '100%',
                height: '100%',
                MozUserSelect: "none",
                WebkitUserSelect: "none",
                msUserSelect: "none",
                userSelect: "none",
            },
            formControlContentContainer: {
                width: '100%',
                height: '100%',
            }
        };

        return formControl.map((control, index) => {
            const { pos } = control;

            return (
                <Rnd
                    key={index}
                    size={{ width: pos.width, height: pos.height }}
                    position={{ x: pos.ox, y: pos.oy }}
                    onDragStop={(event, pos) => { this._onDragStopRnd(event, pos, index) }}
                    onResizeStop={(event, direction, ref, delta, position) => {
                        this._onResizeStopRnd(event, direction, ref, delta, position, index);
                    }}
                    scale={this.state.a4PageFormScale}//scale for every control in A4Paper
                    minWidth={FORMCONTROL_MIN_WIDTH}
                    minHeight={FORMCONTROL_MIN_HEIGHT}
                    // resizeGrid={[DRAG_RESIZE_GRID_NUMBER, DRAG_RESIZE_GRID_NUMBER]}
                    // dragGrid={[DRAG_RESIZE_GRID_NUMBER, DRAG_RESIZE_GRID_NUMBER]}
                    bounds="parent"
                    dragHandleClassName="draghandler"
                >
                    <div style={styles.formControlContainer}>
                        <Row
                            // type="flex" justify="center" align="middle"
                            style={styles.formControlContentContainer}
                            onClick={() => this._onClickControl(control, index)}
                        >
                            {this.renderControlContent(control, index)}
                        </Row>
                        <div className="draghandler" style={styles.dragContainer}>
                            <Icon type="drag" />
                        </div>
                    </div>
                </Rnd>
            );
        });
    };

    //render trang A4 bao gồm nhiều cái control
    renderA4Page() {
        const styles = {
            a4PageForm: {
                boxSizing: "border-box",
                position: "relative", //must relative for draggable work
                width: A4WIDTH,
                height: A4HEIGHT,
                backgroundColor: "#fff",
                transform: `scale(${this.state.a4PageFormScale})`,//styles to scale A4 Paper
                transformOrigin: "left top"
            },
        };

        return (
            <div style={styles.a4PageForm} className={"a4PageForm"}>
                {this.renderControlRnd()}
            </div>
        );
    };

    //render cái menu bênh cạnh để add control
    renderControlAddingPanel() {
        const styles = {
            controlAddContainer: {
                width: '100%',
                height: 40,
            },
            addIconContainer: {
                width: 30,
                height: 30,
            },
            controlPanelLabelContainer: {
                height: 30,
                marginLeft: 30,
                paddingLeft: 10,
            }
        };

        const addControlPanel = FORM_ADD_TEMPLATE;

        const renderControlPanel = (controlData) => {
            return controlData.map((control, index) => {
                return (
                    <div style={styles.controlAddContainer} key={`${control.controlTemp.config.type}${index}`}>
                        <Rnd
                            position={{ x: 0, y: 0 }}
                            size={{ width: 30, height: 30 }}
                            enableResizing={false}
                            onDragStop={(event, data) => { this._onDragStopControlAdding(event, data, control.controlTemp) }}
                            bounds=".formCreatorContainer"
                        >
                            <Row
                                type="flex" justify="center" align="middle"
                                style={styles.addIconContainer}
                            >
                                <Icon type="drag" />
                            </Row>
                        </Rnd>
                        <Row
                            type="flex" justify="start" align="middle"
                            style={styles.controlPanelLabelContainer}
                        >
                            {control.panelLabel}
                        </Row>
                    </div>
                );
            });
        };

        return (
            <Collapse defaultActiveKey={['1', '2', '3']} bordered={false} destroyInactivePanel>
                <Panel header={addControlPanel.staticControl.panelHeader} key="1" disabled>
                    {renderControlPanel(addControlPanel.staticControl.controlData)}
                </Panel>
                <Panel header={addControlPanel.inputControl.panelHeader} key="2" disabled>
                    {renderControlPanel(addControlPanel.inputControl.controlData)}
                </Panel>
                <Panel header={addControlPanel.predefinedControl.panelHeader} key="3" disabled>
                    {renderControlPanel(addControlPanel.predefinedControl.controlData)}
                </Panel>
            </Collapse>
        );
    };

    renderSaveButton() {
        const styles = {
            saveButtonContainer: {
                width: "100%",
                textAlign: "center",
                backgroundColor: "#fff",
                // padding: 20,
            },
        };
        return (
            <div
                style={styles.saveButtonContainer}
                onClick={() => this._onClickSaveFormButton()}
            >
                <Button block type="primary" icon="save" loading={this.state.iconLoading} onClick={this.enterIconLoading}>
                    Lưu mẫu chấm công
                </Button>
            </div>
        );
    };

    render() {
        const styles = {
            formCreatorContainer: {
                boxSizing: "border-box",
                position: "relative", // for drag and drop
            },
            a4PageFormContainer: {
                boxSizing: "border-box",
                border: "2px solid #aaa",
                overflowY: "hidden",
                overflowX: "hidden",
                //for scale A4Page
                width: this.state.A4PageFormContainerWidth ? this.state.A4PageFormContainerWidth : "100%",
                height: this.state.A4PageFormContainerHeight ? this.state.A4PageFormContainerHeight : "100%",
            },
            controlAddPanelContainer: {
                boxSizing: "border-box",
                marginLeft: 10,
                border: "2px solid #aaa",
            },
            saveButtonContainer: {
                boxSizing: "border-box",
                marginLeft: 10,
                marginBottom: 10,
                // border: "2px solid #aaa",
            },
            staticRowCol: {
                position: "static" // for drag and drop
            },
        };

        return (
            <div style={{ padding: "50px" }}>
                <div
                    style={styles.formCreatorContainer}
                    className={"formCreatorContainer"}
                >
                    <Row style={styles.staticRowCol}>
                        <Col span={19} style={styles.staticRowCol}>
                            <div
                                style={styles.a4PageFormContainer}
                                ref={(ref) => { this.a4PageFormCol = ref }}
                            >
                                {this.renderA4Page()}
                            </div>
                        </Col>

                        <Col span={5} style={styles.staticRowCol}>
                            <div style={styles.saveButtonContainer}>
                                {this.renderSaveButton()}
                            </div>
                            <div style={styles.controlAddPanelContainer}>
                                {this.renderControlAddingPanel()}
                            </div>
                        </Col>
                    </Row>
                    <FormControlEditDrawer />
                    <FormSaveDrawer history={this.props.history} />
                    {/* truyền history xuống để khi save đổi màn hình */}
                </div>
            </div>
        );
    };

    //click lên save button thì mở drawer để save
    _onClickSaveFormButton = () => {
        this.props.changeSaveDrawerVisable(true);
    };

    //click lên formControl thì thay đổi current trên reducer và mở drawer để edit
    _onClickControl = (control, index) => {
        const onSuccessCallback = () => {
            this.props.changeEditDrawerVisable(true);
        };

        const { pos, config, format } = control;
        const newPos = {
            ...pos
        };
        const newConfig = {
            ...config
        };
        const newFormat = {
            ...format
        }

        const newControl = {
            pos: newPos,
            config: newConfig,
            format: newFormat,
        };

        this.props.changeEditDrawerFormControl(newControl, index, onSuccessCallback);
    };

    //Rnd drag với form -> thay đổi x y
    _onDragStopRnd = (event, pos, index) => {
        const { formControl } = this.props;

        formControl[index].pos.ox = Math.round(pos.x / DRAG_RESIZE_GRID_NUMBER) * DRAG_RESIZE_GRID_NUMBER;
        formControl[index].pos.oy = Math.round(pos.y / DRAG_RESIZE_GRID_NUMBER) * DRAG_RESIZE_GRID_NUMBER;

        this.props.changeFormControl(formControl);
    };

    //Rnd resize với form -> thay đổi width height
    _onResizeStopRnd = (event, direction, ref, delta, position, index) => {
        const { formControl } = this.props;
        formControl[index].pos.width = Math.round(parseInt(ref.style.width) / DRAG_RESIZE_GRID_NUMBER) * DRAG_RESIZE_GRID_NUMBER;
        formControl[index].pos.height = Math.round(parseInt(ref.style.height) / DRAG_RESIZE_GRID_NUMBER) * DRAG_RESIZE_GRID_NUMBER;
        this.props.changeFormControl(formControl);
    };

    //Khi stop drag thì xét vị trí drag để adding new control vào form
    _onDragStopControlAdding = (event, data, controlTemp) => {
        const { lastX, lastY } = data;
        const x = Math.round((lastX / this.state.a4PageFormScale) / DRAG_RESIZE_GRID_NUMBER) * DRAG_RESIZE_GRID_NUMBER;
        const y = Math.round((lastY / this.state.a4PageFormScale) / DRAG_RESIZE_GRID_NUMBER) * DRAG_RESIZE_GRID_NUMBER;

        if (x <= A4WIDTH - FORMCONTROL_MIN_WIDTH && y <= A4HEIGHT - FORMCONTROL_MIN_HEIGHT) {

            const newPos = {
                ...controlTemp.pos,
                ox: x,
                oy: y,
            };
            const newConfig = {
                ...controlTemp.config
            };
            const newFormat = {
                ...controlTemp.format
            };
            const newControl = {
                pos: newPos,
                config: newConfig,
                format: newFormat,
            };

            const { formControl } = this.props;

            const newFormControl = [
                ...formControl,
                newControl
            ];

            this.props.changeFormControl(newFormControl);
        };
    };
}

const mapStateToProps = (state) => {
    return {
        formControl: state.formCreatorReducer.formControl,
    };
};

const mapDispatchActionToProps = {
    changeFormControl,
    changeEditDrawerVisable,
    changeSaveDrawerVisable,
    changeEditDrawerFormControl,
};

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(FormCreator)