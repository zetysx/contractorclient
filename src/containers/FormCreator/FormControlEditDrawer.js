import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
    changeFormControl,
    changeEditDrawerVisable,
    changeEditDrawerFormControl,
} from '../../actions/formCreatorAction';

import {
    FORM_CONTROL_TYPE,
    FORM_STATIC_CONTROL_TYPE,
    FORM_INPUT_CONTROL_TYPE,
    FORM_INPUT_CONTROL_ROLE,
    FORM_CONTROL_DEFAULT_FORMAT,
} from '../../constants/docketForm';

import ImgurUploader from '../../components/ImgurUploader';

import {
    Row,
    Col,
    Drawer,
    Button,
    Form,
    Input,
    Select,
    Checkbox,
} from 'antd';
const { Option } = Select;

export class FormControlEditDrawer extends Component {
    static getDerivedStateFromProps(props, state) {
        if (props.isEditDrawerVisable !== state.isEditDrawerVisable) {
            return {
                isEditDrawerVisable: props.isEditDrawerVisable,
                editDrawerFormControl: props.editDrawerFormControl,
                editDrawerFormControlIndex: props.editDrawerFormControlIndex,
            }
        }
        return null;
    };

    constructor(props) {
        super(props);
        this.state = {
            editDrawerFormControl: undefined,
            editDrawerFormControlIndex: undefined,
        }
    };

    //render button trong drawer để edit form
    renderControlEditorButton() {
        return (
            <div
                style={{
                    position: 'absolute',
                    left: 0,
                    bottom: 0,
                    width: '100%',
                    borderTop: '1px solid #e9e9e9',
                    padding: '10px 16px',
                    background: '#fff',
                    textAlign: 'right',
                }}
            >
                <Row gutter={16}>
                    <Col span={12}>
                        <Button
                            block
                            type="danger"
                            onClick={() => this._onClickDeleteButton()}
                        >
                            Xóa
                        </Button>
                    </Col>
                    <Col span={12}>
                        <Button block onClick={() => this._onClickDoneButton()} type="primary">
                            Hoàn tất
                        </Button>
                    </Col>
                </Row>
            </div>
        )
    };

    renderStaticControlEditor() {
        const styles = {
            controlNameHeader: {
                fontSize: 18,
                fontWeight: "bold",
            }
        };

        const { editDrawerFormControl, editDrawerFormControlIndex } = this.state;

        if (!editDrawerFormControl || editDrawerFormControlIndex === undefined) {
            return null;
        }

        const { statictype, value } = editDrawerFormControl.config;

        switch (statictype) {
            case FORM_STATIC_CONTROL_TYPE.TEXT: {
                return (
                    <div>
                        <Row>
                            <Col span={24} style={styles.controlNameHeader}>
                                Trường văn bản
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>
                                <Form.Item label="Nội dung">
                                    <Input.TextArea
                                        value={value}
                                        placeholder="Nhập nội dung"
                                        autosize={{ minRows: 3, maxRows: 15 }}
                                        onChange={(event) => this._onChangeStaticTypeText(event)}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </div>
                );
            }
            case FORM_STATIC_CONTROL_TYPE.IMAGE: {
                return (
                    <div>
                        <Row>
                            <Col span={24} style={styles.controlNameHeader}>
                                Trường hình ảnh
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>
                                <Form.Item label="Tải hình mới">
                                    <ImgurUploader
                                        imageUrl={value}
                                        onUploadDone={(link) => { this._onUploadDoneImgurImage(link) }}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </div>
                );
            }
            default:
                return null;
        }
    };

    renderInputControlEditor() {
        const styles = {
            controlNameHeader: {
                fontSize: 18,
                fontWeight: "bold",
            }
        };

        const { editDrawerFormControl, editDrawerFormControlIndex } = this.state;

        if (!editDrawerFormControl || editDrawerFormControlIndex === undefined) {
            return null;
        }

        const { inputtype, role, isrequire, label } = editDrawerFormControl.config;

        const renderControlName = () => {
            switch (inputtype) {
                case FORM_INPUT_CONTROL_TYPE.TEXT: {
                    return "Trường nhập văn bản";
                }
                case FORM_INPUT_CONTROL_TYPE.IMAGE: {
                    return "Trường chụp hình ảnh";
                }
                case FORM_INPUT_CONTROL_TYPE.SIGNATURE: {
                    return "Trường ký tên";
                }
                case FORM_INPUT_CONTROL_TYPE.DATE: {
                    return "Trường chọn ngày";
                }
                case FORM_INPUT_CONTROL_TYPE.NUMBER: {
                    return "Trường nhập số";
                }
                case FORM_INPUT_CONTROL_TYPE.CHECKBOX: {
                    return "Trường hộp kiểm";
                }
                default:
                    return null;
            }
        }

        return (
            <div>
                <Row>
                    <Col span={24} style={styles.controlNameHeader}>
                        {renderControlName()}
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                        <Form.Item label="Nhãn trường nhập:">
                            <Input
                                placeholder="Nhãn trường nhập"
                                value={label}
                                onChange={(event) => this._onChangeInputTypeLabel(event)}
                            />
                        </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                        <Form.Item label="Chọn người nhập:">
                            <Select
                                placeholder="Chọn người nhập"
                                style={{ width: '100%' }}
                                value={role}
                                onChange={(value) => { this._onChangeInputTypeRole(value) }}
                            >
                                <Option value={FORM_INPUT_CONTROL_ROLE.PRIME}>Nhà thầu chính</Option>
                                <Option value={FORM_INPUT_CONTROL_ROLE.SUB}>Nhà thầu phụ</Option>
                                <Option value={FORM_INPUT_CONTROL_ROLE.WORKER}>Công nhân</Option>
                            </Select>
                        </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                        <Form.Item label="Bắt buộc:">
                            <Checkbox
                                checked={isrequire}
                                onChange={(event) => this._onChangeInputTypeIsRequire(event)}
                            >
                                Bắt buộc điền
                            </Checkbox>
                        </Form.Item>
                    </Col>
                </Row>
            </div>
        );
    };

    renderPredefinedControlEditor() {
        const styles = {
            controlNameHeader: {
                fontSize: 18,
                fontWeight: "bold",
            }
        };

        const { editDrawerFormControl, editDrawerFormControlIndex } = this.state;

        if (!editDrawerFormControl || editDrawerFormControlIndex === undefined) {
            return null;
        }

        const { label } = editDrawerFormControl.config;

        return (
            <div>
                <Row>
                    <Col span={24} style={styles.controlNameHeader}>
                        Trường định sẵn
                    </Col>
                </Row>
                <Row>
                    <Col span={24}>
                        {label}
                    </Col>
                </Row>
            </div>
        );
    };

    //render cái nội dung của drawer edit form
    renderControlEditor() {
        const { editDrawerFormControl, editDrawerFormControlIndex } = this.state;

        if (!editDrawerFormControl || editDrawerFormControlIndex === undefined) {
            return null;
        }

        const { type } = editDrawerFormControl.config;

        switch (type) {
            case FORM_CONTROL_TYPE.STATIC: {
                return this.renderStaticControlEditor();
            }
            case FORM_CONTROL_TYPE.INPUT: {
                return this.renderInputControlEditor();
            }
            case FORM_CONTROL_TYPE.PREDEFINED: {
                return this.renderPredefinedControlEditor();
            }
            default:
                return null;
        }
    };

    renderFormatEditor() {
        const { editDrawerFormControl, editDrawerFormControlIndex } = this.state;

        if (!editDrawerFormControl || editDrawerFormControlIndex === undefined) {
            return null;
        }

        const styles = {
            controlNameHeader: {
                fontSize: 18,
                fontWeight: "bold",
            }
        };
        let { format } = editDrawerFormControl;
        if (!format) {
            format = {
                ...FORM_CONTROL_DEFAULT_FORMAT
            };
        }

        const { fontSize, bold, italic, align, borderSize, borderTop, borderBottom, borderLeft, borderRight } = format;
        const _onChangeField = (fieldName, value) => {
            format[fieldName] = value;
            this._onChangeControlFormat(format);
        };

        const renderTextFormat = () => {
            const { statictype, inputtype } = editDrawerFormControl.config;

            if (statictype === FORM_STATIC_CONTROL_TYPE.IMAGE
                || inputtype === FORM_INPUT_CONTROL_TYPE.IMAGE
                || inputtype === FORM_INPUT_CONTROL_TYPE.SIGNATURE
                || inputtype === FORM_INPUT_CONTROL_TYPE.CHECKBOX) {
                return null;
            }

            return (
                <div>
                    <Row>
                        <Col span={24} style={styles.controlNameHeader}>
                            Định dạng văn bản
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24}>
                            <Form.Item label="Cỡ chữ">
                                <Select
                                    placeholder="Cỡ chữ"
                                    style={{ width: '100%' }}
                                    value={fontSize}
                                    onChange={(value) => { _onChangeField("fontSize", value) }}
                                >
                                    <Option value={12}>12</Option>
                                    <Option value={14}>14</Option>
                                    <Option value={16}>16</Option>
                                    <Option value={20}>20</Option>
                                    <Option value={24}>24</Option>
                                    <Option value={36}>36</Option>
                                    <Option value={48}>48</Option>
                                    <Option value={72}>72</Option>
                                </Select>
                            </Form.Item>
                            <Form.Item label="Định dạng">
                                <Button
                                    icon="bold"
                                    type={bold ? "primary" : "default"}
                                    onClick={() => { _onChangeField("bold", !bold) }}
                                />
                                <Button
                                    style={{ marginLeft: 5 }}
                                    icon="italic"
                                    type={italic ? "primary" : "default"}
                                    onClick={() => { _onChangeField("italic", !italic) }}
                                />
                                <Button
                                    style={{ marginLeft: 10 }}
                                    icon="align-left"
                                    type={align === "left" ? "primary" : "default"}
                                    onClick={() => { _onChangeField("align", "left") }}
                                />
                                <Button
                                    style={{ marginLeft: 5 }}
                                    icon="align-center"
                                    type={align === "center" ? "primary" : "default"}
                                    onClick={() => { _onChangeField("align", "center") }}
                                />
                                <Button
                                    style={{ marginLeft: 5 }}
                                    icon="align-right"
                                    type={align === "right" ? "primary" : "default"}
                                    onClick={() => { _onChangeField("align", "right") }}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                </div>
            );
        };

        const renderBorderFormat = () => {
            return (
                <div>
                    <Row>
                        <Col span={24} style={styles.controlNameHeader}>
                            Đường viền
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24}>
                            <Form.Item label="Độ dày">
                                <Select
                                    placeholder="Độ dày"
                                    style={{ width: '100%' }}
                                    value={borderSize}
                                    onChange={(value) => { _onChangeField("borderSize", value) }}
                                >
                                    <Option value={2}>2</Option>
                                    <Option value={3}>3</Option>
                                    <Option value={4}>4</Option>
                                    <Option value={6}>6</Option>
                                    <Option value={8}>8</Option>
                                </Select>
                            </Form.Item>
                            <Form.Item label="Đường viền">
                                <Button
                                    icon="border-top"
                                    type={borderTop ? "primary" : "default"}
                                    onClick={() => { _onChangeField("borderTop", !borderTop) }}
                                />
                                <Button
                                    style={{ marginLeft: 5 }}
                                    icon="border-bottom"
                                    type={borderBottom ? "primary" : "default"}
                                    onClick={() => { _onChangeField("borderBottom", !borderBottom) }}
                                />
                                <Button
                                    style={{ marginLeft: 5 }}
                                    icon="border-left"
                                    type={borderLeft ? "primary" : "default"}
                                    onClick={() => { _onChangeField("borderLeft", !borderLeft) }}
                                />
                                <Button
                                    style={{ marginLeft: 5 }}
                                    icon="border-right"
                                    type={borderRight ? "primary" : "default"}
                                    onClick={() => { _onChangeField("borderRight", !borderRight) }}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                </div>
            );
        };

        return (
            <div>
                {renderTextFormat()}
                {renderBorderFormat()}
            </div>
        );
    };

    //render cái Edit Drawer -> khi click vào form control nó sẽ hiện ra để edit form control
    render() {
        const { isEditDrawerVisable } = this.props;
        return (
            <Drawer
                title="Chỉnh sửa trường dữ liệu"
                placement="right"
                closable={false}
                onClose={this._onCloseFormControlEditDrawer}
                visible={isEditDrawerVisable}
            >
                {this.renderControlEditor()}
                {this.renderFormatEditor()}
                {this.renderControlEditorButton()}
            </Drawer>
        );
    };

    //tắt drawer edit
    _onCloseFormControlEditDrawer = () => {
        this.props.changeEditDrawerVisable(false);
    };

    //delete control
    _onClickDeleteButton = () => {
        const { formControl } = this.props;
        const { editDrawerFormControlIndex } = this.state;

        const newFormControl = formControl.filter((control, index) => index !== editDrawerFormControlIndex);
        this.props.changeFormControl(newFormControl);
        this.props.changeEditDrawerVisable(false);
    };

    //click done button để lưu xuống formControl
    _onClickDoneButton = () => {
        const { formControl } = this.props;
        const { editDrawerFormControl, editDrawerFormControlIndex } = this.state;
        const newFormControl = [
            ...formControl
        ];

        newFormControl[editDrawerFormControlIndex] = editDrawerFormControl;
        this.props.changeFormControl(newFormControl);
        this.props.changeEditDrawerVisable(false);
    };

    //các hàm liên quan đến thay đổi data trong control
    _onChangeStaticTypeText = (event) => {
        // console.log(event.target.value);
        const { editDrawerFormControl } = this.state;
        editDrawerFormControl.config.value = event.target.value;

        this.setState({
            editDrawerFormControl
        });
        // this.props.changeEditDrawerFormControl(editDrawerFormControl, editDrawerFormControlIndex);
    };

    _onChangeInputTypeLabel = (event) => {
        // console.log(event.target.value);
        const { editDrawerFormControl } = this.state;
        editDrawerFormControl.config.label = event.target.value;

        this.setState({
            editDrawerFormControl
        });
        // this.props.changeEditDrawerFormControl(editDrawerFormControl, editDrawerFormControlIndex);
    };

    _onChangeInputTypeRole = (value) => {
        // console.log(value);
        const { editDrawerFormControl } = this.state;
        editDrawerFormControl.config.role = value;

        this.setState({
            editDrawerFormControl
        });
        // this.props.changeEditDrawerFormControl(editDrawerFormControl, editDrawerFormControlIndex);
    };

    _onChangeInputTypeIsRequire = (event) => {
        // console.log(event.target.checked);
        const { editDrawerFormControl } = this.state;
        editDrawerFormControl.config.isrequire = event.target.checked;

        this.setState({
            editDrawerFormControl
        });
        // this.props.changeEditDrawerFormControl(editDrawerFormControl, editDrawerFormControlIndex);
    };

    _onUploadDoneImgurImage = (link) => {
        const { editDrawerFormControl } = this.state;
        editDrawerFormControl.config.value = link;

        this.setState({
            editDrawerFormControl
        });
        // this.props.changeEditDrawerFormControl(editDrawerFormControl, editDrawerFormControlIndex);
    };

    _onChangeControlFormat = (format) => {
        const { editDrawerFormControl } = this.state;
        editDrawerFormControl.format = format;

        this.setState({
            editDrawerFormControl
        });
    };
}

const mapStateToProps = (state) => {
    return {
        formControl: state.formCreatorReducer.formControl,
        isEditDrawerVisable: state.formCreatorReducer.isEditDrawerVisable,
        editDrawerFormControlIndex: state.formCreatorReducer.editDrawerFormControlIndex,
        editDrawerFormControl: state.formCreatorReducer.editDrawerFormControl,
    };
};

const mapDispatchActionToProps = {
    changeFormControl,
    changeEditDrawerVisable,
    changeEditDrawerFormControl,
};

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(FormControlEditDrawer)