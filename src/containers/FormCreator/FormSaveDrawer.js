import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
    changeSaveDrawerVisable,
    saveForm
} from '../../actions/formCreatorAction';

import {
    Row,
    Col,
    Drawer,
    Button,
    Form,
    Input,
} from 'antd';

export class FormSaveDrawer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formName: "",
        }
    };

    //render cái Edit Drawer -> khi click vào form control nó sẽ hiện ra để edit form control
    render() {
        const { isSaveDrawerVisable } = this.props;
        return (
            <Drawer
                title="Lưu mẫu chấm công"
                placement="right"
                closable={false}
                onClose={this._onCloseFormSaveDrawer}
                visible={isSaveDrawerVisable}
            >
                <Row>
                    <Col span={24}>
                        <Form.Item label="Tên mẫu chấm công:">
                            <Input
                                placeholder="Tên mẫu chấm công"
                                value={this.state.formName}
                                onChange={(event) => this.setState({
                                    formName: event.target.value
                                })}
                            />
                        </Form.Item>
                    </Col>
                </Row>
                <div
                    style={{
                        position: 'absolute',
                        left: 0,
                        bottom: 0,
                        width: '100%',
                        borderTop: '1px solid #e9e9e9',
                        padding: '10px 16px',
                        background: '#fff',
                        textAlign: 'right',
                    }}
                >
                    <Row gutter={16}>
                        <Col span={24}>
                            <Button
                                block
                                type="primary"
                                icon="save"
                                onClick={() => this._onClickDoneButton()}
                            >
                                Hoàn tất
                        </Button>
                        </Col>
                    </Row>
                </div >
            </Drawer>
        );
    };

    //tắt drawer edit
    _onCloseFormSaveDrawer = () => {
        this.setState({
            formName: "",
        });
        this.props.changeSaveDrawerVisable(false);
    };

    //khi click button này sẽ validate và save form
    _onClickDoneButton = () => {
        if (this.state.formName !== null && this.state.formName !== undefined && this.state.formName !== "") {
            const formId = this.props.previousFormId;
            const formName = this.state.formName;
            const formControls = JSON.stringify(this.props.formControl);
            const authorId = this.props.profile.accountId;

            const onSuccessCallback = () => {
                this.setState({
                    formName: "",
                });
                this.props.changeSaveDrawerVisable(false);
                this.props.history.push('/formManage');
            }
            this.props.saveForm(formId, formName, formControls, authorId, onSuccessCallback);
        };
    };
}

const mapStateToProps = (state) => {
    return {
        profile: state.authReducer.profile,
        previousFormId: state.formCreatorReducer.previousFormId,
        formControl: state.formCreatorReducer.formControl,
        isSaveDrawerVisable: state.formCreatorReducer.isSaveDrawerVisable,
    };
};

const mapDispatchActionToProps = {
    changeSaveDrawerVisable,
    saveForm
};

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(FormSaveDrawer)
