import React, {Component} from 'react';
import {connect} from 'react-redux';
import moment from 'moment';

import {loadSubSchedule, setBusyWorkerDay} from "../../actions/subScheduleAction";


import {DatePicker, Popconfirm, message, Icon, Col, Row, Button} from 'antd';

const {WeekPicker} = DatePicker;


class SubSchedule extends Component {
    constructor(props) {
        super(props);

        this.state = {
            monday: moment().startOf('isoweek').format("DD/MM/YYYY"),
            sunday: moment().startOf('isoweek').add(6, 'days').format("DD/MM/YYYY"),
            today: moment().format("DD/MM/YYYY"),
            docketContractInfo: ""
        };
    }

    componentDidMount() {
        this.setCalendarFollowDatepicker();
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.workerReducer) {
            this.styleByClass();
        }
    }

    setCalendarFollowDatepicker = () => {
        const input = {
            "subcontractorId": this.props.authReducer.profile.userId,
            "startDate": this.state.monday,
            "endDate": this.state.sunday
        };
        this.props.loadSubSchedule(input);
    }

    renderTableHeader = (monday, sunday) => {
        const mondayConst = moment(monday, "DD/MM/YYYY");
        const sundayConst = moment(sunday, 'DD/MM/YYYY');
        const arr = [0, 1, 2, 3, 4, 5, 6];

        return arr.map((i, index) => {
            let mondayTemp = null;
            mondayTemp = mondayConst.clone().add(i, 'days');

            if (mondayTemp.format("DD/MM/YYYY") === this.state.today) {
                return (
                    <td key={`${index}${i}today`}
                        style={{
                            width: "11.57%",
                            /*
                                                    border: "1px solid rgb(181, 181, 181)",
                            */

                            textAlign: "center",
                            background: "rgb(245, 227, 101)",
                            color: "#000",
                            verticalAlign: "middle",
                        }}>HÔM NAY<br/>{mondayTemp.format("DD/MM")}</td>
                )
            } else if (mondayTemp.format("DD/MM/YYYY") === sundayConst.format("DD/MM/YYYY")) {
                return (
                    <td key={`${index}${i}${mondayTemp}`}
                        style={{
                            width: "11.57%",
                            textAlign: "center",
                            verticalAlign: "middle",
                        }}>Chủ nhật<br/>{mondayTemp.format("DD/MM")}</td>
                )
            } else return (
                <td key={`${index}${i}${mondayTemp}`}
                    style={{
                        width: "11.57%",
                        textAlign: "center",
                        verticalAlign: "middle",
                    }}>Thứ {i + 2}<br/>{mondayTemp.format("DD/MM")}</td>
            )
        })
    };

    /*render từng ngày theo hàng workerID*/
    renderTableContentEachDay = (eachDayList, workerId) => {
        return eachDayList.map((eachDay, index) => {
            /*Kiểm tra xem mỗi ngày là available, busy hay working*/


            if (eachDay.workerStatus === "Available") return ( /*nếu là available hoặc busy thì thêm xoá ngày bận*/
                <Popconfirm
                    key={`${index}${eachDay.date}Available`}
                    title={`Bạn có muốn thêm ${moment(eachDay.date, "YYYY-MM-DD").format("DD/MM")} nghỉ?`}
                    onConfirm={() => this._onClickSetBusy(eachDay.date, workerId)} okText="Thêm" cancelText="Huỷ">
                    <td
                        key={`${index}td`}
                        style={{
                            border: "1px solid rgb(206, 206, 206)",
                            textAlign: "center",
                            verticalAlign: "middle",
                            fontSize: "0"
                        }}
                        onMouseOver={(event) => {
                            event.target.style.background = "rgb(255, 218, 218)";
                            event.target.style.transition = "0.2s";
                            event.target.style.fontSize = "10px";
                        }}
                        onMouseOut={(event) => {
                            event.target.style.background = "rgb(255, 255, 255)";
                            event.target.style.transition = "0.2s";
                            event.target.style.fontSize = "0";
                        }}
                    >
                        Thêm ngày nghỉ
                    </td>
                </Popconfirm>
            );

            else if (eachDay.workerStatus === "Busy") return ( /*nếu là available hoặc busy thì thêm xoá ngày bận*/
                <Popconfirm
                    key={`${index}${eachDay.date}Busy`}
                    title={`Bạn có muốn bỏ ngày nghỉ?`} onConfirm={() => this._onClickRemoveBusy(eachDay.date, workerId)}
                    okText="Bỏ" cancelText="Không bỏ">
                    <td
                        key={`${index}${eachDay.date}`}
                        style={{
                            border: "1px solid rgb(206, 206, 206)",
                            textAlign: "center",
                            verticalAlign: "middle",
                            background: "#e4504f",
                            fontSize: "15px"
                        }}
                        onMouseOver={(event) => {
                            event.target.style.background = "rgb(232, 153, 152)";
                            event.target.style.transition = "0.2s";
                            event.target.style.fontSize = "10px";
                        }}
                        onMouseOut={(event) => {
                            event.target.style.background = "#e4504f";
                            event.target.style.transition = "0.2s";
                            event.target.style.fontSize = "15px";
                        }}
                    >
                        <Icon type="poweroff"/>
                    </td>
                </Popconfirm>
            );

            /*khi status là working*/
            /*kiểm tra xem thứ 2 có trùng với ngày bắt đầu contract hay ko*/
            else if (moment(eachDay.date, "YYYY-MM-DD").format("DD/MM/YYYY") === this.state.monday) {

                /*ngay ngày thứ 2, nếu nó bằng với contractStartDate thì là double border left*/
                if (moment(eachDay.date, "YYYY-MM-DD").format("DD/MM/YYYY") === moment(eachDay.contractStartDate, "YYYY-MM-DD").format("DD/MM/YYYY")) {

                    /*ngay ngày thứ 2, nếu nó bằng với contractEnđDate thì là double border right*/
                    if (moment(eachDay.date, "YYYY-MM-DD").format("DD/MM/YYYY") === moment(eachDay.contractEnđDate, "YYYY-MM-DD").format("DD/MM/YYYY")) {
                        return (
                            <td
                                key={`${index}${eachDay.date}`}
                                className={`docketCell docketDate${eachDay.date} docketStatus${eachDay.docketStatus} docketId${eachDay.docketId} contractId${eachDay.contractId}`}
                                style={{
                                    borderBottom: "solid 2px rgb(175, 175, 175)",
                                    borderTop: "solid 2px rgb(175, 175, 175)",
                                    borderLeft: "#000 double 5px",
                                    borderRight: "#000 double 5px"
                                }}
                                dangerouslySetInnerHTML={{__html: this.renderDocketStatusContent(eachDay.docketStatus, eachDay.docketId)}}
                                onMouseOver={() => this._onmouseoverCellToShowDetail(eachDay.docketId, eachDay.contractId, eachDay.docketDetail, eachDay.docketStatus, eachDay.pricePerDocket, eachDay.profession, eachDay.primeCompanyName, eachDay.contractStartDate, eachDay.contractEnđDate)}
                                onMouseOut={() => this._onmouseoutCellToShowDetail(eachDay.docketId, eachDay.contractId)}
                            >
                            </td>
                        )
                    } else {
                        return (
                            <td
                                key={`${index}${eachDay.date}`}

                                className={`docketCell docketDate${eachDay.date} docketStatus${eachDay.docketStatus} docketId${eachDay.docketId} contractId${eachDay.contractId}`}
                                style={{
                                    borderBottom: "solid 2px rgb(175, 175, 175)",
                                    borderTop: "solid 2px rgb(175, 175, 175)",
                                    borderLeft: "#000 double 5px"
                                }}
                                dangerouslySetInnerHTML={{__html: this.renderDocketStatusContent(eachDay.docketStatus, eachDay.docketId)}}
                                onMouseOver={() => this._onmouseoverCellToShowDetail(eachDay.docketId, eachDay.contractId, eachDay.docketDetail, eachDay.docketStatus, eachDay.pricePerDocket, eachDay.profession, eachDay.primeCompanyName, eachDay.contractStartDate, eachDay.contractEnđDate)}
                                onMouseOut={() => this._onmouseoutCellToShowDetail(eachDay.docketId, eachDay.contractId)}
                            >

                            </td>
                        )
                    }
                } else {

                    return (
                        <td
                            key={`${index}${eachDay.date}`}
                            className={`docketCell docketDate${eachDay.date} docketStatus${eachDay.docketStatus} docketId${eachDay.docketId} contractId${eachDay.contractId}`}
                            style={{
                                borderBottom: "solid 2px rgb(175, 175, 175)",
                                borderTop: "solid 2px rgb(175, 175, 175)",
                                borderLeft: "#000 dashed 3px"
                            }}
                            dangerouslySetInnerHTML={{__html: this.renderDocketStatusContent(eachDay.docketStatus, eachDay.docketId)}}
                            onMouseOver={() => this._onmouseoverCellToShowDetail(eachDay.docketId, eachDay.contractId, eachDay.docketDetail, eachDay.docketStatus, eachDay.pricePerDocket, eachDay.profession, eachDay.primeCompanyName, eachDay.contractStartDate, eachDay.contractEnđDate)}
                            onMouseOut={() => this._onmouseoutCellToShowDetail(eachDay.docketId, eachDay.contractId)}
                        >

                        </td>
                    )
                }
            }

            /*kiểm tra xem cn có trùng với ngày kết thúc contract hay ko*/
            else if (moment(eachDay.date, "YYYY-MM-DD").format("DD/MM/YYYY") === this.state.sunday) {

                /*ngay ngày cn, nếu nó bằng với contractEnđDate thì là ||, không thì là gạch nối*/

                if (moment(eachDay.date, "YYYY-MM-DD").format("DD/MM/YYYY") === moment(eachDay.contractEnđDate, "YYYY-MM-DD").format("DD/MM/YYYY")) {
                    return (
                        <td
                            key={`${index}${eachDay.date}`}
                            className={`docketCell docketDate${eachDay.date} docketStatus${eachDay.docketStatus} docketId${eachDay.docketId} contractId${eachDay.contractId}`}
                            style={{
                                borderBottom: "solid 2px rgb(175, 175, 175)",
                                borderTop: "solid 2px rgb(175, 175, 175)",
                                borderRight: "#000 double 5px"
                            }}
                            dangerouslySetInnerHTML={{__html: this.renderDocketStatusContent(eachDay.docketStatus, eachDay.docketId)}}
                            onMouseOver={() => this._onmouseoverCellToShowDetail(eachDay.docketId, eachDay.contractId, eachDay.docketDetail, eachDay.docketStatus, eachDay.pricePerDocket, eachDay.profession, eachDay.primeCompanyName, eachDay.contractStartDate, eachDay.contractEnđDate)}
                            onMouseOut={() => this._onmouseoutCellToShowDetail(eachDay.docketId, eachDay.contractId)}
                        >

                        </td>
                    )
                } else {
                    return (
                        <td
                            key={`${index}${eachDay.date}`}
                            className={`docketCell docketDate${eachDay.date} docketStatus${eachDay.docketStatus} docketId${eachDay.docketId} contractId${eachDay.contractId}`}
                            style={{
                                borderBottom: "solid 2px rgb(175, 175, 175)",
                                borderTop: "solid 2px rgb(175, 175, 175)",
                                borderRight: "#000 dashed 3px"
                            }}
                            dangerouslySetInnerHTML={{__html: this.renderDocketStatusContent(eachDay.docketStatus, eachDay.docketId)}}
                            onMouseOver={() => this._onmouseoverCellToShowDetail(eachDay.docketId, eachDay.contractId, eachDay.docketDetail, eachDay.docketStatus, eachDay.pricePerDocket, eachDay.profession, eachDay.primeCompanyName, eachDay.contractStartDate, eachDay.contractEnđDate)}
                            onMouseOut={() => this._onmouseoutCellToShowDetail(eachDay.docketId, eachDay.contractId)}
                        >

                        </td>
                    )
                }
            }

            /*nếu ngày bắt đầu contract bằng ngày kết thúc (contract chỉ có 1 ngày làm)*/
            else if (moment(eachDay.contractStartDate, "YYYY-MM-DD").format("DD/MM/YYYY") === moment(eachDay.contractEnđDate, "YYYY-MM-DD").format("DD/MM/YYYY")) return (
                <td
                    key={`${index}${eachDay.date}`}
                    className={`docketCell docketDate${eachDay.date} docketStatus${eachDay.docketStatus} docketId${eachDay.docketId} contractId${eachDay.contractId}`}
                    style={{
                        borderBottom: "solid 2px rgb(175, 175, 175)",
                        borderTop: "solid 2px rgb(175, 175, 175)",
                        borderLeft: "#000 double 5px",
                        borderRight: "#000 double 5px"
                    }}
                    dangerouslySetInnerHTML={{__html: this.renderDocketStatusContent(eachDay.docketStatus, eachDay.docketId)}}
                    onMouseOver={() => this._onmouseoverCellToShowDetail(eachDay.docketId, eachDay.contractId, eachDay.docketDetail, eachDay.docketStatus, eachDay.pricePerDocket, eachDay.profession, eachDay.primeCompanyName, eachDay.contractStartDate, eachDay.contractEnđDate)}
                    onMouseOut={() => this._onmouseoutCellToShowDetail(eachDay.docketId, eachDay.contractId)}
                >

                </td>
            );

            /*kiểm tra nếu docket này là docket đầu của contract*/
            else if (moment(eachDay.date, "YYYY-MM-DD").format("DD/MM/YYYY") === moment(eachDay.contractStartDate, "YYYY-MM-DD").format("DD/MM/YYYY")) return (
                <td
                    key={`${index}${eachDay.date}`}
                    className={`docketCell docketDate${eachDay.date} docketStatus${eachDay.docketStatus} docketId${eachDay.docketId} contractId${eachDay.contractId}`}
                    style={{
                        borderBottom: "solid 2px rgb(175, 175, 175)",
                        borderTop: "solid 2px rgb(175, 175, 175)",
                        borderLeft: "#000 double 5px"
                    }}
                    dangerouslySetInnerHTML={{__html: this.renderDocketStatusContent(eachDay.docketStatus, eachDay.docketId)}}
                    onMouseOver={() => this._onmouseoverCellToShowDetail(eachDay.docketId, eachDay.contractId, eachDay.docketDetail, eachDay.docketStatus, eachDay.pricePerDocket, eachDay.profession, eachDay.primeCompanyName, eachDay.contractStartDate, eachDay.contractEnđDate)}
                    onMouseOut={() => this._onmouseoutCellToShowDetail(eachDay.docketId, eachDay.contractId)}
                >

                </td>
            );

            /*kiểm tra nếu docket này là docket cuối của contract*/
            else if (moment(eachDay.date, "YYYY-MM-DD").format("DD/MM/YYYY") === moment(eachDay.contractEnđDate, "YYYY-MM-DD").format("DD/MM/YYYY")) return (
                <td
                    key={`${index}${eachDay.date}`}
                    className={`docketCell docketDate${eachDay.date} docketStatus${eachDay.docketStatus} docketId${eachDay.docketId} contractId${eachDay.contractId}`}
                    style={{
                        borderBottom: "solid 2px rgb(175, 175, 175)",
                        borderTop: "solid 2px rgb(175, 175, 175)",
                        borderRight: "#000 double 5px"
                    }}
                    dangerouslySetInnerHTML={{__html: this.renderDocketStatusContent(eachDay.docketStatus, eachDay.docketId)}}
                    onMouseOver={() => this._onmouseoverCellToShowDetail(eachDay.docketId, eachDay.contractId, eachDay.docketDetail, eachDay.docketStatus, eachDay.pricePerDocket, eachDay.profession, eachDay.primeCompanyName, eachDay.contractStartDate, eachDay.contractEnđDate)}
                    onMouseOut={() => this._onmouseoutCellToShowDetail(eachDay.docketId, eachDay.contractId)}
                >

                </td>
            );

            else return (
                    <td
                        key={`${index}${eachDay.date}`}
                        className={`docketCell docketDate${eachDay.date} docketStatus${eachDay.docketStatus} docketId${eachDay.docketId} contractId${eachDay.contractId}`}
                        style={{
                            borderBottom: "solid 2px rgb(175, 175, 175)",
                            borderTop: "solid 2px rgb(175, 175, 175)"
                        }}
                        dangerouslySetInnerHTML={{__html: this.renderDocketStatusContent(eachDay.docketStatus, eachDay.docketId)}}
                        onMouseOver={() => this._onmouseoverCellToShowDetail(eachDay.docketId, eachDay.contractId, eachDay.docketDetail, eachDay.docketStatus, eachDay.pricePerDocket, eachDay.profession, eachDay.primeCompanyName, eachDay.contractStartDate, eachDay.contractEnđDate)}
                        onMouseOut={() => this._onmouseoutCellToShowDetail(eachDay.docketId, eachDay.contractId)}
                    >

                    </td>
                )
        })

    };

    renderTableContent = (workerReducer) => {
        if (workerReducer)
            return (

                /*render từng row*/
                workerReducer.map((eachRow) => {
                    return (
                        <tr
                            key={`${eachRow.workerId}`}
                            style={{height: "80px"}}>
                            <td
                                style={{
                                    padding: "0 0 0 5px",
                                    border: "1px solid rgb(218, 218, 218)",
                                    borderRight: "0",
                                    borderLeft: "0",
                                }}>
                                <img alt={"avatar"} style={{borderRadius: "100%"}} src={eachRow.avatar}
                                     width={"60px"}
                                     height={"60px"}/>
                            </td>
                            <td
                                style={{
                                    paddingLeft: "10px",
                                    border: "1px solid rgb(218, 218, 218)",
                                    textAlign: "left",
                                    verticalAlign: "middle",
                                    borderRight: "0",
                                    borderLeft: "0",
                                }}>{eachRow.username}</td>
                            {this.renderTableContentEachDay(eachRow.eachDayScheduleDTOS, eachRow.workerId)}
                            <td></td>
                        </tr>
                    )
                }))
    };

    renderDocketStatusContent = (docketStatus, docketId) => {
        if (docketStatus === 0) {
            return (
                `<i class="fas fa-user-edit"></i><p>Chờ điền</p>`
            );
        } else if (docketStatus === 1) {
            return (
                `<i class="fas fa-clipboard-list"></i><p>Chờ duyệt</p>`
            );

        } else if (docketStatus === 2) {
            return (
                `<i class="far fa-calendar-check"></i><p>Đã duyệt</p>`
            );

        } else if (docketStatus === 3) {
            return (
                `<i class="fas fa-user-edit"></i><p>Chờ điền lại</p>`
            );

        } else if (docketStatus === 4) {
            return (
                `<i class="fas fa-tools"></i><p>Đang làm</p>`
            );

        } else if (docketStatus === 5) {
            return (
                `<i class="far fa-eye"></i><p>Chờ xác nhận<br/>hoàn thành</p>`
            );

        } else if (docketStatus === 6) {
            return (
                `<i class="far fa-check-circle"></i><p>Xong!</p>`
            );

        }
    };

    render() {
        return (
            <div>
                <table border="0" style={{width: "100%", background: "#fff", border: "0"}}>

                    <thead>
                    <tr style={{height: "80px"}}>

                        {/*ô chọn tuần*/}
                        <td colSpan={"2"} style={{
                            textAlign: "center",
                            verticalAlign: "middle",
                            width: "15%",
                            border: "1px solid rgb(208, 208, 208)",
                            borderRight: "0",
                            borderTop: "0",
                        }}>
                            <Row type="flex" justify="space-between" align="middle">
                                <Col span={18}>
                                    <WeekPicker
                                        placeholder="Chọn tuần"
                                        onChange={this._onWeekChange}/>
                                </Col>
                                <Col span={6} style={{textAlign: "right",}}>
                                    <Button style={{height: "80px"}} type="primary"
                                            onClick={() => this._onChangeNextPreviousWeek("previous")}><Icon
                                        type="double-left"/></Button></Col></Row>
                        </td>

                        {/*các cột ngày từ t2 đến cn, có today*/}
                        {this.renderTableHeader(this.state.monday, this.state.sunday)}

                        {/*cột next week*/}
                        <td style={{width: "2%",}}>
                            <Button style={{height: "80px"}} type="primary"
                                    onClick={() => this._onChangeNextPreviousWeek("next")}><Icon
                                type="double-right"/></Button>
                        </td>
                    </tr>

                    </thead>
                    <tbody>
                    {this.renderTableContent(this.props.workerReducer)}
                    </tbody>
                </table>

                {/*hiển thị thông tin docket*/}
                <div id={"docketContractInfo"} style={{
                    width: "auto",
                    padding: "5px",
                    height: "auto",
                    maxWidth: "40%",
                    bottom: "0px",
                    right: "0px",
                    background: "rgb(0, 0, 0)",
                    position: "fixed",
                    boxShadow: "2px 2px 8px 1px #000",
                    opacity: "0.8",
                    color: "rgb(255, 255, 255)",
                    fontSize: "14px",
                    borderRadius: "10px 0px 0px",
                }}
                     dangerouslySetInnerHTML={{__html: this.state.docketContractInfo}}
                />
            </div>
        );
    }


    _onClickSetBusy = (date, workerId) => {
        const input = {
            "workerId": workerId,
            "dateOff": moment(date, "YYYY-MM-DD").format("DD/MM/YYYY")
        };
        this.props.setBusyWorkerDay(input, () => {
            message.success('Đã thêm ngày bận');
            this.setCalendarFollowDatepicker();
        });
    };

    _onClickRemoveBusy = (date, workerId) => {
        const input = {
            "workerId": workerId,
            "dateOff": moment(date, "YYYY-MM-DD").format("DD/MM/YYYY")
        };
        this.props.setBusyWorkerDay(input, () => {
            message.success('Đã huỷ ngày bận');
            this.setCalendarFollowDatepicker();

        });
    };

    _onmouseoverCellToShowDetail = (docketId, contractId, docketDetail, docketStatus, pricePerDocket, profession, primeCompanyName, contractStartDate, contractEnđDate) => {
        if ((docketId) && (contractId)) {
            let allCellContract = document.getElementsByClassName(`contractId${contractId}`);

            if (allCellContract) {
                for (let i = 0; i < allCellContract.length; i++) {
                    allCellContract[i].style.borderLeftColor = "red";
                    allCellContract[i].style.borderRightColor = "red";
                    allCellContract[i].style.zIndex = "9";
                    allCellContract[i].style.boxShadow = "inset 0 0 100px 100px rgba(000, 000, 000, 0.1)";
                    allCellContract[i].style.transition = "0.5s";
                }
            }

            let allCellDocket = document.getElementsByClassName(`docketId${docketId}`);
            if (allCellDocket) {
                for (let i = 0; i < allCellDocket.length; i++) {
                    allCellDocket[i].style.boxShadow = "inset 0 0 100px 100px rgba(000, 000, 000, 0.4)";
                    allCellDocket[i].style.color = "#fff";
                    allCellDocket[i].style.transition = "0.5s";
                }
            }

            const afterSetState = () => {
                document.getElementById("docketContractInfo").style.opacity = "0.8";
                document.getElementById("docketContractInfo").style.transition = "0.3s";
            };

            if ((docketStatus === 0) || (docketStatus === 3)) {
                this.setState({
                    docketContractInfo: `<table border="0">
                    <tr>Chưa điền nội dung CV</tr>
                     <tr>
                        <td>Mã dự án</td>
                        <td>:</td>
                        <td>${contractId}</td>
                        </tr>
                    <tr>
                        <td>Tên thầu chính</td>
                        <td>:</td>
                        <td>${primeCompanyName}</td>
                        </tr>                    
                </table>`
                }, () => {
                    afterSetState();
                });
            } else {
                this.setState({
                    docketContractInfo: `<table border="0">
                    <tr>
                        <td>Nội dung CV</td>
                       <td>:</td>
                        <td>${docketDetail}</td>
                    </tr>
                     <tr>
                        <td>Mã dự án</td>
                        <td>:</td>
                        <td>${contractId}</td>
                        </tr>
                    <tr>
                        <td>Tên thầu chính</td>
                        <td>:</td>
                        <td>${primeCompanyName}</td>
                        </tr>                    
                </table>`
                }, () => {
                    afterSetState();
                });
            }
        }
    };

    _onmouseoutCellToShowDetail = (docketId, contractId) => {
        if ((docketId) && (contractId)) {
            let allCellContract = document.getElementsByClassName(`contractId${contractId}`);
            if (allCellContract) {
                for (let i = 0; i < allCellContract.length; i++) {
                    allCellContract[i].style.borderLeftColor = "#000";
                    allCellContract[i].style.borderRightColor = "#000";
                    allCellContract[i].style.boxShadow = "inset 0 0 100px 100px rgba(000, 000, 000, 0)";
                    allCellContract[i].style.transition = "0.5s";
                }
            }

            let allCellDocket = document.getElementsByClassName(`docketId${docketId}`);
            if (allCellDocket) {
                for (let i = 0; i < allCellDocket.length; i++) {
                    allCellDocket[i].style.boxShadow = "inset 0 0 100px 100px rgba(0, 0, 0, 0)";
                    allCellDocket[i].style.color = "rgba(0, 0, 0, 0.65)";
                    allCellDocket[i].style.transition = "0.5s";
                }
            }

            document.getElementById("docketContractInfo").style.opacity = "0";
            document.getElementById("docketContractInfo").style.transition = "0.3s";
        }
    };

    _onWeekChange = (date) => {
        this.setState({
            monday: moment().day("Monday").year(date.format("YYYY")).week(date.format("WW")).format("DD/MM/YYYY"),
            sunday: moment().day("Monday").year(date.format("YYYY")).week(date.format("WW")).add(6, 'days').format("DD/MM/YYYY")
        }, () => {
            const input = {
                "subcontractorId": this.props.authReducer.profile.userId,
                "startDate": this.state.monday,
                "endDate": this.state.sunday
            };
            this.props.loadSubSchedule(input);
        });
    };

    _onChangeNextPreviousWeek = (status) => {
        if (status === "next") {
            const newMonday = moment(this.state.sunday, "DD/MM/YYYY").add(1, "days").format("DD/MM/YYYY");
            const newSunday = moment(this.state.sunday, "DD/MM/YYYY").add(7, "days").format("DD/MM/YYYY");

            this.setState({
                monday: newMonday,
                sunday: newSunday
            }, () => {
                const input = {
                    "subcontractorId": this.props.authReducer.profile.userId,
                    "startDate": this.state.monday,
                    "endDate": this.state.sunday
                };
                this.props.loadSubSchedule(input);
            });
        } else if (status === "previous") {
            const newSunday = moment(this.state.monday, "DD/MM/YYYY").add(-1, "days").format("DD/MM/YYYY");
            const newMonday = moment(this.state.monday, "DD/MM/YYYY").add(-7, "days").format("DD/MM/YYYY");

            this.setState({
                monday: newMonday,
                sunday: newSunday
            }, () => {
                const input = {
                    "subcontractorId": this.props.authReducer.profile.userId,
                    "startDate": this.state.monday,
                    "endDate": this.state.sunday
                };
                this.props.loadSubSchedule(input);
            });
        }
    };

    styleByClass = () => {
        let docketCell = document.getElementsByClassName("docketCell");
        if (docketCell) {
            for (let i = 0; i < docketCell.length; i++) {
                docketCell[i].style.textAlign = "center";
                docketCell[i].style.verticalAlign = "middle";
            }

        }

        let pInDocketCell = document.querySelectorAll(".docketCell p");
        if (pInDocketCell) {
            for (let i = 0; i < pInDocketCell.length; i++) {
                pInDocketCell[i].style.fontSize = "12px";
            }
        }

        let docketStatus0 = document.getElementsByClassName("docketStatus0");
        if (docketStatus0) {
            for (let i = 0; i < docketStatus0.length; i++) {
                docketStatus0[i].style.background = "rgb(226, 242, 255)";
            }
        }

        let docketStatus1 = document.getElementsByClassName("docketStatus1");
        if (docketStatus1) {
            for (let i = 0; i < docketStatus1.length; i++) {
                docketStatus1[i].style.background = "rgb(252, 226, 255)";
            }
        }

        let docketStatus2 = document.getElementsByClassName("docketStatus2");
        if (docketStatus2) {
            for (let i = 0; i < docketStatus2.length; i++) {
                docketStatus2[i].style.background = "rgb(176, 255, 136)";
            }
        }

        let docketStatus3 = document.getElementsByClassName("docketStatus3");
        if (docketStatus3) {
            for (let i = 0; i < docketStatus3.length; i++) {
                docketStatus3[i].style.background = "rgb(226, 242, 255)";
            }
        }

        let docketStatus4 = document.getElementsByClassName("docketStatus4");
        if (docketStatus4) {
            for (let i = 0; i < docketStatus4.length; i++) {
                docketStatus4[i].style.background = "rgb(245, 227, 101)";
            }
        }

        let docketStatus5 = document.getElementsByClassName("docketStatus5");
        if (docketStatus5) {
            for (let i = 0; i < docketStatus5.length; i++) {
                docketStatus5[i].style.background = "rgb(252, 226, 255)";
            }
        }

        let docketStatus6 = document.getElementsByClassName("docketStatus6");
        if (docketStatus6) {
            for (let i = 0; i < docketStatus6.length; i++) {
                docketStatus6[i].style.background = "rgb(87, 255, 123)";
                docketStatus6[i].style.verticalAlign = "middle";

            }
        }

    };
}

function mapStateToProps(state) {
    return {
        workerReducer: state.subScheduleReducer.subSchedule,
        authReducer: state.authReducer,
    };
}

export default connect(
    mapStateToProps,
    {loadSubSchedule, setBusyWorkerDay}
)(SubSchedule);