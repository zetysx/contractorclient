import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Select, Spin } from "antd";

class LocationSelecter extends Component {

    constructor(props) {
        super(props);
        this.selecterConfig = {
            mode: "multiple",
            placeholder: "Bấm để chọn khu vực làm việc",
            locationSelecterStyle: {
                width: '100%',
                marginTop: 15
            }
        }
    }

    render() {
        const Option = Select.Option;
        
        const locationsChildren = this.props.locationDBData.map((location) => {
            return (
                <Option key={location.locationId}>{location.city} Quận {location.district}</Option>
            )
        })

        return (
            <Spin spinning={this.props.locationDBData.length === 0}>
                <Select
                    mode={this.selecterConfig.mode}
                    // style={{ width: '100%', marginTop: 15 }}
                    style={this.selecterConfig.locationSelecterStyle}
                    placeholder={this.selecterConfig.placeholder}
                    onChange={(key) => this.props._onLocationSelect(key)}
                >
                    {locationsChildren}
                </Select>
            </Spin>
        )
    }

}


function mapStateToProps(){
    return {
        
    }
}

export default connect(
    mapStateToProps
)(LocationSelecter);