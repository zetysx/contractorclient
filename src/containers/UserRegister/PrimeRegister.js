
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Icon, Button, Collapse, message } from 'antd';
import GeneralInputField from "./GeneralInputField";
import { CONST_INPUT_FIELDS_NAME } from "../../constants/register";
import { registerNewAccount } from "../../actions/userRegisterAction";
import { 
    validateFieldNullOfObject, 
    validatePhone, 
    validateEmail,
    validateLength 
} from "../../helpers/validateHelper";
import { REGISTER_ERROR_MESSAGE } from "../../constants/message";

class PrimeRegister extends Component {

    
    constructor(props) {
        super(props);
        this.state = {
            registerData: {
                username        : "",
                password        : "",
                phone           : "",  
                address         : "",
                firstName       : null,
                lastName        : null,
                avatar          : null,
                email           : "",
                role            : 1,
                companyName     : "",
                faxNumber       : null,
                businessLicense : null,
            },
        };

    }

    _onHandleFieldChange = (input, value) => {
        const state = {...this.state};
        const registerDataTmp = {...state.registerData};
        registerDataTmp[input] = value;
        state.registerData = registerDataTmp;
        this.setState({
            ...state
        })
    }

    
    validateRequiredRegisterData = (dataObj) => {
        if(dataObj) {
            if(!validateFieldNullOfObject(dataObj)) {
                message.warn(REGISTER_ERROR_MESSAGE.BLANK_REQUIRED_FIELDS);
                return false;
            }
            if(dataObj.hasOwnProperty("email") && dataObj.email) {
                if(!validateEmail(dataObj.email)) {
                    message.warn(REGISTER_ERROR_MESSAGE.EMAIL_FORMAT);
                    return false;
                }
            }
            if(dataObj.hasOwnProperty("phone") && dataObj.phone) {
                if(!validatePhone(dataObj.phone)) {
                    message.warn(REGISTER_ERROR_MESSAGE.PHONE);
                    return false;
                }
            }
            if(dataObj.hasOwnProperty("username") && dataObj.username) {
                if(!validateLength(dataObj.username, 5, 50)) {
                    message.warn("Tên đăng nhập giới hạn từ 5 -> 50 kí tự");
                    return false;
                }
            }
            if(dataObj.hasOwnProperty("password") && dataObj.password) {
                if(!validateLength(dataObj.password, 5, 120)) {
                    message.warn("Mật khẩu giới hạn từ 5 -> 120 kí tự");
                    return false;
                }
            }
            if(dataObj.hasOwnProperty("address") && dataObj.address) {
                if(!validateLength(dataObj.address, 8, 250)) {
                    message.warn("Địa chỉ giới hạn từ 8 -> 250 kí tự");
                    return false;
                }
            }
            if(dataObj.hasOwnProperty("firstName") && dataObj.firstName) {
                if(!validateLength(dataObj.firstName, 1, 60)) {
                    message.warn("Họ giới hạn từ 1 -> 60 kí tự");
                    return false;
                }
            }
            if(dataObj.hasOwnProperty("lastName") && dataObj.lastName) {
                if(!validateLength(dataObj.lastName, 1, 60)) {
                    message.warn("Tên giới hạn từ 1 -> 60 kí tự");
                    return false;
                }
            }
        }
        return true;
    }

    validateGeneralDataBounaries = (data) => {
        if(data) {
            if(data.hasOwnProperty("avatar") && data.avatar) {
                if(!validateLength(data.avatar, 0, 120)) {
                    message.warn("Đường dẫn ảnh đại diện giới hạn 120 kí tự");
                    return false;
                }
            }
            if(data.hasOwnProperty("companyName") && data.companyName) {
                if(!validateLength(data.companyName, 0, 60)) {
                    message.warn("Tên doanh nghiệp giới hạn 60 kí tự");
                    return false;
                }
            }
            if(data.hasOwnProperty("faxNumber") && data.faxNumber) {
                if(!validateLength(data.faxNumber, 0, 20)) {
                    message.warn("Số fax giới hạn 20 kí tự");
                    return false;
                }
            }
            if(data.hasOwnProperty("businessLicense") && data.businessLicense) {
                if(!validateLength(data.businessLicense, 0, 50)) {
                    message.warn("Chứng chỉ kinh doanh giới hạn 50 kí tự");
                    return false;
                }
            }
        }
        return true;
    }


    _onRegisterClick = () => {

        const onSuccessCallbackRegister = () => {
            message.success("Đăng kí thành công");
            this.props.userRegisterHistory.push("/login");
        }
    
        const onFailDuplicatedCallbackRegister = () => {
            message.warn(REGISTER_ERROR_MESSAGE.DUPLICATED_USERNAME);
            return;
        }

        let listRequiredFields = {
            username                    : this.state.registerData.username,
            password                    : this.state.registerData.password,
            email                       : this.state.registerData.email,
            phone                       : this.state.registerData.phone,
            address                     : this.state.registerData.address,
            firstName                   : this.state.registerData.firstName,
            listName                    : this.state.registerData.lastName
        }

        if(!this.validateRequiredRegisterData(listRequiredFields)) {
            return;
        }
        if(!this.validateGeneralDataBounaries(this.state.registerData)) {
            return;
        }
        
        
        const registerData = this.state.registerData; 
        this.props.registerNewAccount(registerData, onSuccessCallbackRegister, onFailDuplicatedCallbackRegister);
    }
    

    renderRegisterButton = () => {

        const registerButtonStyle = {marginTop: 15, width: "40%", display: "inline-block", height: 40, float: "left"}

        return (
            <div>
                <Button
                    type="primary"
                    onClick={() => { this._onRegisterClick() }}
                    style={registerButtonStyle}
                >
                    Đăng ký
                </Button>
                <Button
                    type="primary"
                    onClick={() => { this._onRegisterClick() }}
                    style={{...registerButtonStyle, marginLeft: 10}}
                >
                    Đăng nhập
                </Button>
            </div>
        )
    }


    renderGeneralInfo = () => {

        const generalFields = [
            CONST_INPUT_FIELDS_NAME.USERNAME,
            CONST_INPUT_FIELDS_NAME.PASSWORD,
            CONST_INPUT_FIELDS_NAME.PHONE,
            CONST_INPUT_FIELDS_NAME.ADDRESS,
            CONST_INPUT_FIELDS_NAME.EMAIL,
            CONST_INPUT_FIELDS_NAME.FIRSTNAME,
            CONST_INPUT_FIELDS_NAME.LASTNAME,
        ]

        return (
            <GeneralInputField 
                _onHandleFieldChange={this._onHandleFieldChange}
                listFields={generalFields}
                fieldData={null}
            />
        )
        
    }

    renderAdditionalInfo = () => {


        // const renderOptionHeader = () => {
        //     return (
        //         <div style={{fontSize: "20px"}}>
        //             <Tag color="#2db7f5">Thông tin bổ sung</Tag>
        //         </div>
        //     )
        // }


        const customPanelStyle = {
            background: '#f7f7f7',
            borderRadius: 4,
            marginBottom: 24,
            border: 0,
            overflow: 'hidden',
        };

        const OptionalInfoPanel = Collapse.Panel;

        const addtionalFields = [
            CONST_INPUT_FIELDS_NAME.COMPANY_NAME,
            CONST_INPUT_FIELDS_NAME.FAX_NUMBER,
            CONST_INPUT_FIELDS_NAME.BUSINESS_LICENSE
        ]

        return (
            <Collapse
                bordered={false}
                // expandIcon={<Icon type="info"/>}
                expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 90 : 0} />}
            >
                <OptionalInfoPanel
                    // header={renderOptionHeader()}
                    header="Thông tin thêm"
                    style={customPanelStyle}
                >

                    <GeneralInputField 
                        _onHandleFieldChange={this._onHandleFieldChange}
                        listFields={addtionalFields}
                        fieldData={null}
                    />
                    <GeneralInputField
                        _onHandleFieldChange={this._onHandleFieldChange}
                        listFields={[CONST_INPUT_FIELDS_NAME.AVATAR]}
                        fieldData={this.state.registerData.avatar}
                    />
                </OptionalInfoPanel>
            </Collapse>
        )
    }


    renderRegisterForm = () => {
        return (
            <div style={{width: 600, padding: "50px 50px", background: "#fff", borderRadius: 15, display: "inline-block"}}>
                <h1 style={{textAlign: "center", marginBottom: 20}}>Mẫu đăng kí thầu chính</h1>
                <div>
                    {this.renderGeneralInfo()}
                </div>
                <div>
                    {this.renderAdditionalInfo()}
                </div>
                <div style={{textAlign: "center"}}>
                    {this.renderRegisterButton()}
                </div>
                <div>
                    <a href="/login" style={{float: "right", fontSize: "15px"}}> Đăng nhập </a>
                </div>
            </div>
        );
    }

    render(){
        return (
            <div style={{padding: 50}}>
                {this.renderRegisterForm()}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        userRegisterReducer: state.userRegisterReducer
    };
}

const mapDispatchActionToProps = {
    registerNewAccount
}

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(PrimeRegister);
