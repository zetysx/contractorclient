import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Checkbox, Row, Col, InputNumber, Spin } from "antd";

const CheckboxGroup = Checkbox.Group;
class ProfessionAndPriceCheckBox extends Component {


    constructor(props) {
        super(props);
        this.state = {
            profData: props.professionDBData,
            _onProfessionCheckBoxCheck: props._onProfessionCheckBoxCheck,
        };
    }

    componentWillReceiveProps(props) {
        this.setState({
            profData: props.professionDBData,
            _onProfessionCheckBoxCheck: props._onProfessionCheckBoxCheck,
        });

        this.forceUpdate();
    }


    render() {
        return (
        
            <Spin spinning={this.props.professionDBData.length === 0}>
                <div>
                    <CheckboxGroup 
                        onChange={(value) => this.props._onProfessionCheckBoxCheck(value)}
                    >
                        <Row style={{marginTop: 15}}>
                            {this.renderEachProfessionChild(this.props.professionDBData)}
                        </Row>
                        
                    </CheckboxGroup>
                </div>
            </Spin>
        )
    }


    renderEachProfessionChild(professionDataFromDB) {
        return professionDataFromDB.map((profession, index) => 
            <div 
                name={profession.professionName} 
                key={profession.professionId} 
                style={{width: 300, marginTop: 20, backgroundColor: "red"}}
            >
                <Col span={12} style={{marginBottom: 15}}>{this.renderCheckBox(profession)}</Col>
                <Col span={12} style={{marginBottom: 15}}>{this.renderPriceInput(profession)}</Col>
            </div>
        );
    }

    renderCheckBox(professionObj) {
        return (
            <Checkbox 
                key = {professionObj.professionId}  
                value = {professionObj.professionId}
                style={{float: "left"}}
                
            >
                {professionObj.professionName}
            </Checkbox>
        );
    }


    renderPriceInput = (professionObj) => {
        return (
            <div>
                <InputNumber
                    defaultValue={0}
                    formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                    parser={value => value.replace(/\$\s?|(,*)/g, '')}
                    disabled={this.props.professionChecked.includes(professionObj.professionId) ? false : true}
                    min={1000}
                    max={2000000}
                    onChange={(value) => this.props._handleAssignProfessionAndPrice(professionObj.professionId, value)}
                /> VNĐ
            </div>
        );
    }
}


function mapStateToProps(state){
    return {

    }
}

export default connect(
    mapStateToProps
)(ProfessionAndPriceCheckBox);