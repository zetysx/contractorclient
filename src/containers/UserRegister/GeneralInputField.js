import React, { Component } from "react";
import { connect } from 'react-redux';

import { Icon, Input } from "antd";
import { 
    CONST_INPUT_FIELDS_NAME, 
    CONST_INPUT_PLACEHOLDER,
} from "../../constants/register";
import ImgurUploader from "../../components/ImgurUploader";
import {
    ICON_ANTD_CONSTANTS
} from "../../constants/workerConstants";

class GeneralInputField extends Component {


    constructor(props) {
        super();
        this.state = {
            listFields: props.listFields,
            fieldValue: props.fieldData
        }
        this.iconStyle = {
            color: 'rgba(0,0,0,.25)'
        }

        this.inputStyle = {
            marginBottom: 10,
            height: 40
        }
    }
    

    render() {
        
        return (
            <div>
                {this.renderRegisterForm(this.state.listFields, this.state.fieldValue)}
            </div>
        )
    }


    renderRegisterForm = (listFields, data) => {
        if(listFields) {
            return listFields.map((field, index) => {
                return (
                    this.renderEachInput(field, index, data)
                )
            })
        }
    }


    renderEachInput = (inputName, index, data) => {
        switch(inputName) {
            case CONST_INPUT_FIELDS_NAME.USERNAME:
                return (
                    <Input
                        key={index}
                        placeholder={CONST_INPUT_PLACEHOLDER.USERNAME}
                        defaultValue={data ? data.username : null}
                        name={CONST_INPUT_FIELDS_NAME.USERNAME}
                        prefix={
                            <Icon 
                                type={ICON_ANTD_CONSTANTS.USER}
                                style={this.iconStyle} 
                            />
                        }
                        ref={node => this.userNameInput = node}
                        onChange={e => this.props._onHandleFieldChange(e.target.name, e.target.value)}
                        style={this.inputStyle}
                    />
                );
            case CONST_INPUT_FIELDS_NAME.PASSWORD:
                return (
                    <Input.Password
                        key={index}
                        placeholder={CONST_INPUT_PLACEHOLDER.PASSWORD}
                        defaultValue={data ? data.password : null}
                        name={CONST_INPUT_FIELDS_NAME.PASSWORD}
                        prefix={
                            <Icon 
                                type={ICON_ANTD_CONSTANTS.LOCK_ICON} 
                                style={this.iconStyle}
                            />
                        }
                        ref={node => this.passwordInput = node}
                        onChange={e => this.props._onHandleFieldChange(e.target.name, e.target.value)}
                        style={this.inputStyle}
                    />
                );
            case CONST_INPUT_FIELDS_NAME.PHONE:
                return ( 
                    <Input
                        key={index}
                        defaultValue={data ? data.phone : null}
                        placeholder={CONST_INPUT_PLACEHOLDER.PHONE}
                        name={CONST_INPUT_FIELDS_NAME.PHONE}
                        ref={node => this.phoneInput = node}
                        prefix={
                            <Icon 
                                type={ICON_ANTD_CONSTANTS.PHONE} 
                                style={this.iconStyle} 
                            />
                        }
                        onChange={e => this.props._onHandleFieldChange(e.target.name, e.target.value)}
                        style={this.inputStyle}
                    />
                );
            case CONST_INPUT_FIELDS_NAME.ADDRESS:
                return (
                    <Input
                        key={index}
                        defaultValue={data ? data.address : null}
                        placeholder={CONST_INPUT_PLACEHOLDER.ADDRESS}
                        name={CONST_INPUT_FIELDS_NAME.ADDRESS}
                        ref={node => this.addressInput = node}
                        prefix={
                            <Icon 
                                type={ICON_ANTD_CONSTANTS.ENVIRONMENT}
                                style={this.iconStyle} 
                            />
                        }
                        onChange={e => this.props._onHandleFieldChange(e.target.name, e.target.value)}
                        style={this.inputStyle}
                    />
                );
            case CONST_INPUT_FIELDS_NAME.EMAIL:
                return (
                    <Input
                        key={index}
                        defaultValue={data ? data.email : null}
                        placeholder={CONST_INPUT_PLACEHOLDER.EMAIL}
                        name={CONST_INPUT_FIELDS_NAME.EMAIL}
                        ref={node => this.emailInput = node}
                        prefix={
                            <Icon 
                                type={ICON_ANTD_CONSTANTS.MAIL} 
                                style={this.iconStyle} />
                            }
                        onChange={e => this.props._onHandleFieldChange(e.target.name, e.target.value)}
                        style={this.inputStyle}
                    />
                );
            case CONST_INPUT_FIELDS_NAME.COMPANY_NAME:
                return (
                    <Input
                        key={index}
                        defaultValue={data ? data.companyName : null}
                        placeholder={CONST_INPUT_PLACEHOLDER.COMPANY_NAME}
                        name={CONST_INPUT_FIELDS_NAME.COMPANY_NAME}
                        ref={node => this.companyNameInput = node}
                        prefix={
                            <Icon 
                                type={ICON_ANTD_CONSTANTS.BANK} 
                                style={this.iconStyle} />
                            }
                        onChange={e => this.props._onHandleFieldChange(e.target.name, e.target.value)}
                        style={this.inputStyle}
                    />
                );
            case CONST_INPUT_FIELDS_NAME.FIRSTNAME:
                return (
                    <Input
                        key={index}
                        defaultValue={data ? data.firstName : null}
                        placeholder={CONST_INPUT_PLACEHOLDER.FIRST_NAME}
                        name={CONST_INPUT_FIELDS_NAME.FIRSTNAME}
                        ref={node => this.firstNameInput = node}
                        prefix={
                            <Icon 
                                type={ICON_ANTD_CONSTANTS.PROFILE} 
                                style={this.iconStyle} 
                            />
                        }
                        onChange={e => this.props._onHandleFieldChange(e.target.name, e.target.value)}
                        style={this.inputStyle}
                    />
                );
            case CONST_INPUT_FIELDS_NAME.LASTNAME:
                return (
                    <Input
                        key={index}
                        defaultValue={data ? data.lastName : null}
                        placeholder={CONST_INPUT_PLACEHOLDER.LAST_NAME}
                        name={CONST_INPUT_FIELDS_NAME.LASTNAME}
                        ref={node => this.lastNameInput = node}
                        prefix={
                            <Icon 
                                type={ICON_ANTD_CONSTANTS.PROFILE} 
                                style={this.iconStyle} 
                            />
                        }
                        onChange={e => this.props._onHandleFieldChange(e.target.name, e.target.value)}
                        style={this.inputStyle}
                    />
                );
            case CONST_INPUT_FIELDS_NAME.FAX_NUMBER:
                return (
                    <Input
                        key={index}
                        defaultValue={data ? data.faxNumber : null}
                        placeholder={CONST_INPUT_PLACEHOLDER.FAX_NUMBER}
                        name={CONST_INPUT_FIELDS_NAME.FAX_NUMBER}
                        ref={node => this.faxNumberInput = node}
                        prefix={
                            <Icon 
                                type={ICON_ANTD_CONSTANTS.CONTACT} 
                                style={this.iconStyle} 
                            />
                        }
                        onChange={e => this.props._onHandleFieldChange(e.target.name, e.target.value)}
                        style={this.inputStyle}
                    />
                );
            case CONST_INPUT_FIELDS_NAME.BUSINESS_LICENSE:
                return (
                    <Input
                        key={index}
                        defaultValue={data ? data.bussinessLicense : null}
                        placeholder={CONST_INPUT_PLACEHOLDER.BUSINESS_LICENSE}
                        name={CONST_INPUT_FIELDS_NAME.BUSINESS_LICENSE}
                        ref={node => this.businessLicenseInput = node}
                        prefix={
                            <Icon 
                                type={ICON_ANTD_CONSTANTS.BUSINESS_LICENSE_CERTIFICATED} 
                                style={this.iconStyle} 
                            />
                        }
                        onChange={e => this.props._onHandleFieldChange(e.target.name, e.target.value)}
                        style={this.inputStyle}
                    />
                );
            case CONST_INPUT_FIELDS_NAME.SUB_DESCRIPTION:
                return (
                    <Input.TextArea 
                        key={index}
                        defaultValue={data ? data.description : null}
                        ref={node => this.subDescriptionInput = node}
                        style={{marginBottom: 10, height: 100}}
                        placeholder={CONST_INPUT_PLACEHOLDER.SUB_DESCRIPTION}
                        name={CONST_INPUT_FIELDS_NAME.SUB_DESCRIPTION}
                        onChange={e => this.props._onHandleFieldChange(e.target.name, e.target.value)}
                        >

                    </Input.TextArea>
                );
            case CONST_INPUT_FIELDS_NAME.AVATAR:
                return (
                    <ImgurUploader 
                        key={index}
                        imageUrl={this.props.fieldData}
                        // onUploadDone={(link) => { this.props._onHandleFieldChange(link) }}
                        onUploadDone={(link) => this.props._onHandleFieldChange(CONST_INPUT_FIELDS_NAME.AVATAR, link)}
                    />
                );
            default: 
                return null;
        }
    }

}

function mapStateToProps(){
    return {
        
    }
}

export default connect(
    mapStateToProps
)(GeneralInputField);
