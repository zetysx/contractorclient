
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Icon, Button, Collapse, message, Tag } from 'antd';
import { loadAllLocations } from "../../actions/locationAction";
import { loadAllProfessions } from "../../actions/professionAction";
import { CONST_INPUT_FIELDS_NAME } from "../../constants/register";
import GeneralInputField from "./GeneralInputField";
import ProfessionAndPriceCheckBox from "./ProfessionAndPriceCheckBox";
import LocationSelecter from "./LocationSelecter";
import { 
    registerNewAccount
} from "../../actions/userRegisterAction";
import { 
    validateFieldNullOfObject, 
    validatePriceProfessionsList, 
    validatePhone, 
    validateEmail,
    validateLength
} from "../../helpers/validateHelper";
import { REGISTER_ERROR_MESSAGE } from "../../constants/message";

class SubRegister extends Component {

    
    constructor(props) {
        super(props);
        this.state = {
            registerData: {
                username        : "",
                password        : "",
                phone           : "",
                address         : "",
                firstName       : null,
                lastName        : null,
                avatar          : null,
                email           : "",
                role            : 2,
                companyName     : "",
                faxNumber       : null,
                subcontractorProfessions: [],
                locationId      : [],
                description     : null,
            },
        };

        this.listProfChecked = []

    }

    componentDidMount() {
        this.props.loadAllLocations();
        this.props.loadAllProfessions();
    }

    _onHandleFieldChange = (input, value) => {
        console.log("INPUT: ", input);
        console.log("value: ", value);
        const state = {...this.state};
        const registerDataTmp = {...state.registerData};
        registerDataTmp[input] = value;
        state.registerData = registerDataTmp;
        this.setState({
            ...state
        }, () => {
            console.log("AFTER UPLOAD: ", this.state.registerData);
        })
    }

    validateRequiredRegisterData = (dataObj) => {
        if(dataObj) {
            if(!validateFieldNullOfObject(dataObj)) {
                message.warn(REGISTER_ERROR_MESSAGE.BLANK_REQUIRED_FIELDS);
                return false;
            }
            if(dataObj.hasOwnProperty("email") && dataObj.email) {
                if(!validateEmail(dataObj.email)) {
                    message.warn(REGISTER_ERROR_MESSAGE.EMAIL_FORMAT);
                    return false;
                }
            }
            if(dataObj.hasOwnProperty("phone") && dataObj.phone) {
                if(!validatePhone(dataObj.phone)) {
                    message.warn(REGISTER_ERROR_MESSAGE.PHONE);
                    return false;
                }
            }
            if(dataObj.hasOwnProperty("username") && dataObj.username) {
                if(!validateLength(dataObj.username, 5, 50)) {
                    message.warn("Tên đăng nhập giới hạn từ 5 -> 50 kí tự");
                    return false;
                }
            }
            if(dataObj.hasOwnProperty("password") && dataObj.password) {
                if(!validateLength(dataObj.password, 5, 120)) {
                    message.warn("Mật khẩu giới hạn từ 5 -> 120 kí tự");
                    return false;
                }
            }
            if(dataObj.hasOwnProperty("address") && dataObj.address) {
                if(!validateLength(dataObj.address, 8, 250)) {
                    message.warn("Địa chỉ giới hạn từ 8 -> 250 kí tự");
                    return false;
                }
            }
            if(dataObj.hasOwnProperty("firstName") && dataObj.firstName) {
                if(!validateLength(dataObj.firstName, 1, 60)) {
                    message.warn("Họ giới hạn từ 1 -> 60 kí tự");
                    return false;
                }
            }
            if(dataObj.hasOwnProperty("lastName") && dataObj.lastName) {
                if(!validateLength(dataObj.lastName, 1, 60)) {
                    message.warn("Tên giới hạn từ 1 -> 60 kí tự");
                    return false;
                }
            }
            if(dataObj.hasOwnProperty("subcontractorProfessions") && dataObj.subcontractorProfessions) {
                if(!validatePriceProfessionsList(dataObj.subcontractorProfessions)) {
                    message.warn(REGISTER_ERROR_MESSAGE.PROFESSION_PRICE);
                    return false;
                }
            }
        }
        return true;
    }

    validateGeneralDataBounaries = (data) => {
        if(data) {
            if(data.hasOwnProperty("avatar") && data.avatar) {
                if(!validateLength(data.avatar, 0, 120)) {
                    message.warn("Đường dẫn ảnh đại diện giới hạn 120 kí tự");
                    return false;
                }
            }
            if(data.hasOwnProperty("companyName") && data.companyName) {
                if(!validateLength(data.companyName, 0, 60)) {
                    message.warn("Tên doanh nghiệp giới hạn 60 kí tự");
                    return false;
                }
            }
            if(data.hasOwnProperty("faxNumber") && data.faxNumber) {
                if(!validateLength(data.faxNumber, 0, 20)) {
                    message.warn("Số fax giới hạn 20 kí tự");
                    return false;
                }
            }
            if(data.hasOwnProperty("description") && data.description) {
                if(!validateLength(data.description, 0, 120)) {
                    message.warn("Lời giới thiệu giới hạn 120 kí tự");
                    return false;
                }
            }
        }
        return true;
    }


    _onRegisterClick = () => {

        const onSuccessCallbackRegister = () => {
            message.success("Đăng kí thành công");
            this.props.userRegisterHistory.push("/login");
        }
        
        const registerData = this.state.registerData; 
        let listRequiredFields = {
            username                    : this.state.registerData.username,
            password                    : this.state.registerData.password,
            email                       : this.state.registerData.email,
            phone                       : this.state.registerData.phone,
            address                     : this.state.registerData.address,
            subcontractorProfessions    : this.state.registerData.subcontractorProfessions,
            locationId                  : this.state.registerData.locationId,
            firstName                   : this.state.registerData.firstName,
            listName                    : this.state.registerData.lastName
        }
    
        const onFailDuplicatedCallbackRegister = () => {
            message.error(REGISTER_ERROR_MESSAGE.DUPLICATED_USERNAME);
            return;
        }
        
        if(!this.validateRequiredRegisterData(listRequiredFields)) {
            return;
        }
        if(!this.validateGeneralDataBounaries(this.state.registerData)) {
            return;
        }

        this.props.registerNewAccount(registerData, onSuccessCallbackRegister, onFailDuplicatedCallbackRegister);
    }
    

    //CheckedValues alway return as an array
    _onProfessionCheckBoxCheck = (checkedValues) => {
        this.listProfChecked = checkedValues;

        let stateData = this.state.registerData.subcontractorProfessions;
        //If none checked => assign empty list to subProf
        if(checkedValues.length === 0) {
            this._onHandleFieldChange("subcontractorProfessions", checkedValues);
        } else {
            let finalListProfAndPrice = []
            //If stateData empty => create each object as each checkedValue
            if(stateData.length === 0) {
                
                for(var pos in checkedValues) {
                    let objTmp = {
                        professionId: checkedValues[pos],
                        price: 0
                    };
                    finalListProfAndPrice = finalListProfAndPrice.concat(objTmp);
                }
            } else {
                //In case user checked new box or unchecked a box ==> stateData was old array, checkedValues was new,
                // ====> have to update stateData with checkedValues
                for(var pos1 in checkedValues) {
                    let profTmp = {
                        professionId: checkedValues[pos1],
                        price: 0
                    };
                    let finalDupProf = this.checkDuplicatedProfessionInState(profTmp);
                    finalListProfAndPrice = finalListProfAndPrice.concat(finalDupProf);
                }
            }
            this._onHandleFieldChange("subcontractorProfessions", finalListProfAndPrice);  
            this.forceUpdate();
        }
    }

    
    checkDuplicatedProfessionInState = (profObj) => {
        let stateProfList = [...this.state.registerData.subcontractorProfessions];
    
        for (var pos in stateProfList) {
            if(stateProfList[pos].professionId === profObj.professionId) {
                profObj.price = stateProfList[pos].price;
            }
        }
        return profObj;
    }


    _onLocationSelect = (selectedValue) => {
        this._onHandleFieldChange("locationId", selectedValue);
    }

    renderRegisterButton = () => {

        const registerButtonStyle = {marginTop: 15, width: "50%", display: "inline-block", height: 40}

        return (
            <Button
                type="primary"
                onClick={() => { this._onRegisterClick() }}
                style={registerButtonStyle}
            >
                Đăng ký
            </Button>
        )
    }

    renderSubWorkInfo = () => {

        const locationListFetched = this.props.locationReducer.locations;
        const professionListFetched = this.props.professionReducer.professions;

        return (
            <div> 
                <div>
                    <label style={{marginBottom: 15, textAlign: "center", fontStyle: "italic", fontSize: "20px"}}>
                        <Tag style={{marginBottom: 15, textAlign: "center", fontStyle: "italic", fontSize: "20px", height: 25}}>
                            * Thông tin về năng lực
                        </Tag>
                    </label>
                    <br/>
                    <ProfessionAndPriceCheckBox 
                        professionDBData={professionListFetched} 
                        _onProfessionCheckBoxCheck={this._onProfessionCheckBoxCheck}
                        professionChecked={this.listProfChecked}
                        _handleAssignProfessionAndPrice={this._handleAssignProfessionAndPrice}
                    />
                </div>
                <div>
                    <LocationSelecter 
                        locationDBData={locationListFetched} 
                        _onLocationSelect={this._onLocationSelect}
                    />
                </div>
            </div>
        )
    }

    _handleAssignProfessionAndPrice = (professionId, price) => {

        let isExist = false;
        let subProf = {
            professionId: professionId,
            price: price
        };
        let subProfList = [...this.state.registerData.subcontractorProfessions];
        
        if(subProfList.length !== 0) {

            for(var pos in subProfList) {
                if(subProfList[pos] === subProf.professionId || subProfList[pos].professionId === subProf.professionId) {
                    subProfList[pos] = subProf;
                    isExist = true;
                }
            }
            if(!isExist) {
                subProfList.concat(subProf);
            }
        } else subProfList.concat(subProf);
        this._onHandleFieldChange("subcontractorProfessions", subProfList);
    }

    renderGeneralInfo = () => {

        const generalFields = [
            CONST_INPUT_FIELDS_NAME.USERNAME,
            CONST_INPUT_FIELDS_NAME.PASSWORD,
            CONST_INPUT_FIELDS_NAME.PHONE,
            CONST_INPUT_FIELDS_NAME.ADDRESS,
            CONST_INPUT_FIELDS_NAME.EMAIL,
            CONST_INPUT_FIELDS_NAME.FIRSTNAME,
            CONST_INPUT_FIELDS_NAME.LASTNAME,
        ]

        return (
            <GeneralInputField 
                _onHandleFieldChange={this._onHandleFieldChange}
                listFields={generalFields}
                fieldData={null}
            />
        )
        
    }

    renderAdditionalInfo = () => {


        const customPanelStyle = {
            background: '#f7f7f7',
            borderRadius: 4,
            marginBottom: 24,
            border: 0,
            overflow: 'hidden',
        };

        const OptionalInfoPanel = Collapse.Panel;

        const addtionalFields = [
            CONST_INPUT_FIELDS_NAME.COMPANY_NAME,
            CONST_INPUT_FIELDS_NAME.FAX_NUMBER,
            CONST_INPUT_FIELDS_NAME.SUB_DESCRIPTION
        ]

        return (
            <Collapse
                bordered={false}
                expandIcon={<Icon type="info"/>}
            >
                <OptionalInfoPanel
                    header="Thông tin bổ sung"
                    style={customPanelStyle}
                >
                    <GeneralInputField 
                        _onHandleFieldChange={this._onHandleFieldChange}
                        listFields={addtionalFields}
                        fieldData={null}
                    />
                    <span style={{fontStyle: "italic", marginLeft: 10}}>Hình đại diện</span>
                    <GeneralInputField
                        _onHandleFieldChange={this._onHandleFieldChange}
                        listFields={[CONST_INPUT_FIELDS_NAME.AVATAR]}
                        fieldData={this.state.registerData.avatar}
                    />
                </OptionalInfoPanel>
            </Collapse>
        )
    }


    renderRegisterForm = () => {
        return (
            <div style={{width: 600, padding: "50px 50px", background: "#fff", borderRadius: 15, display: "inline-block"}}>    
                <h1 style={{textAlign: "center", marginBottom: 20}}>Mẫu đăng kí thầu phụ</h1>
                <div>
                    {this.renderGeneralInfo()}
                </div>
                <div>
                    {this.renderAdditionalInfo()}
                </div>
                <div>
                    {this.renderSubWorkInfo()}
                </div>
                <div style={{textAlign: "center"}}>
                    {this.renderRegisterButton()}
                </div>
                <div>
                    <a href="/login" style={{float: "right", fontSize: "15px"}}> Đăng nhập </a>
                </div>
            </div>
        );
    }

    render(){
        return (
            <div style={{padding: 50, marginLeft: "25%"}}>
                {this.renderRegisterForm()}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        registerReducer: state.registerReducer,
        locationReducer: state.locationReducer,
        professionReducer: state.professionReducer,
        userRegisterReducer: state.userRegisterReducer
    };
}

const mapDispatchActionToProps = {
    loadAllLocations,
    loadAllProfessions,
    registerNewAccount
}

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(SubRegister);
