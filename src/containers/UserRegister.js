import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Col, Popover} from "antd";
import PrimeRegister from "../containers/UserRegister/PrimeRegister";
import SubRegister from "../containers/UserRegister/SubRegister";
import { ROLESID } from "../constants/role";


class UserRegister extends Component {


    constructor(props) {
        super(props);
        this.state = {
            role: null
        };
    }


    renderRegisterPage = () => {
        return (
            <div style={{marginTop: 150, margin: "auto", display: "table", padding: 100}}>
                <div>
                    <div style={{width: "100%", backgroundColor: "#00BFFF", textAlign: "center", marginBottom: 50}}>
                        <div style={{borderRadius: "10px"}}><h1>ĐĂNG KÍ</h1></div>
                    </div>
                    <div style={{width: "100%"}}>
                        {/*<Col span={24} style={{textAlign: "center", fontSize: 50, fontStyle: "bold"}}><label>Bạn là </label></Col>*/}
                        <div style={{display: "block", textAlign: "center"}}>
                            <div style={{display: "inline-block", textAlign: "center"}} onClick={() => this._onChoosingActorRegisterPage(ROLESID.PRIME)}>
                                <div style={{display: "inline-block"}}>
                                    <Col span={24}>
                                        <label style={{fontSize: 30, fontStyle: "bold"}}>Thầu chính</label>
                                    </Col>
                                </div>
                                <div>
                                    <Col span={24}>
                                        <div style={{height: 300, width: 300, float: "left", borderRadius: 15, cursor: "pointer"}}>
                                            <Popover content={this.renderPrimecontractorDescription()}>
                                                <img 
                                                    alt=""
                                                    src="https://smartbid.co/wp-content/uploads/2016/12/SB-Manage-Subcontractor-Data-Icon-Full.png"
                                                    width="300"
                                                    height="300"
                                                />
                                            </Popover>
                                        </div>
                                    </Col>
                                </div>
                            </div>
                            <div style={{display: "inline-block", textAlign: "center", marginLeft: 50}} onClick={() => this._onChoosingActorRegisterPage(ROLESID.SUB)}>
                                <div style={{display: "inline-block", width: "100%"}}>
                                    <Col span={24}>
                                        <label style={{fontSize: 30, fontStyle: "bold"}}>Thầu phụ</label>
                                    </Col>
                                </div>
                                <div>
                                    <Col span={24}>
                                        <div style={{height: 300, width: 300, float: "left", borderRadius: 15, cursor: "pointer"}}>
                                            <Popover content={this.renderSupcontractorDescription()}>
                                                <img
                                                    alt=""
                                                    src="https://smartbid.co/wp-content/uploads/2016/12/SB-Manage-Subcontractor-Data-Icon-Full.png"
                                                    width="300"
                                                    height="300"
                                                />
                                            </Popover>
                                        </div>
                                    </Col>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <a href="/login" style={{float: "right", fontSize: "15px"}}> Đăng nhập </a>
                </div>
            </div>
        )
    }

    _onChoosingActorRegisterPage = (newRole) => {
        this.setState({
            ...this.state,
            role: newRole
        });
    }

    render(){
        if(!this.state.role) {
            return (
                this.renderRegisterPage()
            );
        } else if(this.state.role === ROLESID.PRIME) {
            return <PrimeRegister userRegisterHistory={this.props.history}/>
        } else if(this.state.role === ROLESID.SUB) {
            return <SubRegister userRegisterHistory={this.props.history}/>
        }
    }

    renderSupcontractorDescription = () => {
        return (
            <div style={{textAlign: "center"}}>
                <h3>Nhà thầu phụ</h3>
                <p>* Người nhận dự án từ nhà thầu chính</p>
            </div>
        )
    }

    renderPrimecontractorDescription = () => {
        return (
            <div style={{textAlign: "center"}}>
                <h3>Nhà thầu chính</h3>
                <p>* Người đi tìm nhà thầu phụ</p>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
    };
}

const mapDispatchActionToProps = {
}

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(UserRegister);
