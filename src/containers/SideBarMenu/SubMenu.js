import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Link } from "react-router-dom";
import { Menu, Icon } from "antd";

class SubMenu extends Component {
    render() {
        return (
            <Menu
                mode="inline"
                defaultSelectedKeys={['1']}
                defaultOpenKeys={['1']}
                style={{ height: '100%' }}
                theme={"light"}
            >
                <Menu.Item key="1">
                    <Link to="/dashboard">
                        <Icon type="area-chart" />
                        <span>Tổng quan</span>
                    </Link>
                </Menu.Item>
                <Menu.Item key="3">
                    <Link to="/request">
                        <Icon type="inbox" />
                        <span>Lời mời</span>
                    </Link>
                </Menu.Item>

                <Menu.Item key="6">
                    <Link to="/docket">
                        <Icon type="setting" spin />
                        <span>Dự án đang làm</span>
                    </Link>
                </Menu.Item>


                <Menu.Item key="5">
                    <Link to="/schedule">
                        <Icon type="calendar" />
                        <span>Lịch làm việc</span>
                    </Link>
                </Menu.Item>

                <Menu.Item key="7">
                    <Link to="/capability">
                        <Icon type="hdd" />
                        <span>Hồ sơ</span>
                    </Link>
                </Menu.Item>


                <Menu.Item key="8">
                    <Link to="/worker">
                        <Icon type="user" />
                        <span>Nhân sự</span>
                    </Link>
                </Menu.Item>
                <Menu.Item key="10">
                    <Link to="/history">
                        <Icon type="book" />
                        <span>Lịch sử</span></Link>
                </Menu.Item>
            </Menu>
        );
    }
}

function mapStateToProps(state) {
    return {

    };
}

export default connect(
    mapStateToProps,
)(SubMenu);