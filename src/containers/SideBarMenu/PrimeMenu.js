import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Link } from "react-router-dom";
import { Menu, Icon } from "antd";

class PrimeMenu extends Component {
    render() {
        return (
            <Menu
                mode="inline"
                defaultSelectedKeys={['1']}
                defaultOpenKeys={['1']}
                style={{ height: '100%' }}
                theme={"light"}
            >
                <Menu.Item key="1">
                    <Link to="/dashboard">
                        <Icon type="pie-chart" />
                        <span>Tổng quan</span>
                    </Link>
                </Menu.Item>

                <Menu.Item key="2">
                    <Link to="/manualsearch">
                        <Icon type="search" />
                        <span>Tìm kiếm</span>
                    </Link>
                </Menu.Item>
                <Menu.Item key="4">
                    <Link to="/request">
                        <Icon type="inbox" />
                        <span>Lời mời</span>
                    </Link>
                </Menu.Item>

                <Menu.Item key="5">
                    <Link to="/docket">
                        <Icon type="setting" spin />
                        <span>Dự án đang làm</span>
                    </Link>
                </Menu.Item>
                <Menu.Item key="7">
                    <Link to="/formManage">
                        <Icon type="form" />
                        <span>Mẫu hợp đồng</span></Link>
                </Menu.Item>

                <Menu.Item key="3">
                    <Link to="/autosearch">
                        <Icon type="robot" />
                        <span>Tìm kiếm tự động</span>
                    </Link>
                </Menu.Item>
                <Menu.Item key="6">
                    <Link to="/history">
                        <Icon type="book" />
                        <span>Lịch sử</span>
                    </Link>
                </Menu.Item>
            </Menu>
        );
    }
}

function mapStateToProps(state) {
    return {

    };
}

export default connect(
    mapStateToProps,
)(PrimeMenu);