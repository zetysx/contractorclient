import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Link } from "react-router-dom";
import {
    Menu,
    Icon
} from "antd";

class AdminMenu extends Component {
    render() {
        return (
            <Menu
                mode="inline"
                style={{ height: '100%' }}
                theme={"light"}
                defaultOpenKeys={['1']}
            >
                <Menu.Item key="1">
                    <Link to="/dashboard">
                        <Icon type="pie-chart" />
                        <span>Tổng quan</span>
                    </Link>
                </Menu.Item>
                <Menu.Item key="3">
                    <Link to="/userManagement">
                        <Icon type="team" />
                        <span>Quản lý tài khoản</span>
                    </Link>
                </Menu.Item>
                <Menu.Item key="4">
                    <Link to="/contracts">
                        <Icon type="project" />
                        <span>Dự án</span>
                        </Link>
                        </Menu.Item>
                <Menu.Item key="6">
                    <Link to="/rating">
                        <Icon type="star" />
                        <span>Đánh giá</span>
                    </Link>
                </Menu.Item>
            </Menu>
        );
    }
}

function mapStateToProps(state) {
    return {

    };
}

export default connect(
    mapStateToProps,
)(AdminMenu);