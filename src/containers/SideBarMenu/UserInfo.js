import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Avatar, Row, Col, Popover, Button } from "antd";
// import { Link } from "react-router-dom";

import { removeAuthTokenAndLogout } from "../../actions/authAction";
import {Link} from "react-router-dom";
import {ROLESID} from "../../constants/role";

class UserInfo extends Component {

    onClickSignout = (e) => {
        this.props.removeAuthTokenAndLogout();
    };

    renderProfileButton=()=>{
      if (this.props.profile.role!==ROLESID.ADMIN){
          return (
              <Row>
                  <Link to={`/profile/${this.props.profile.accountId}`}>
                      <Button type="dashed" block>Profile</Button>
                  </Link>
              </Row>
          )
      }
    };

    renderUserInfoPopover() {
        return (
            <Col>
                {this.renderProfileButton()}
                <Row style={{ marginTop: "5px" }}>
                    <Button type="dashed" block onClick={this.onClickSignout}>Signout</Button>
                </Row>
            </Col>
        );
    }

    render() {
        if (!this.props.profile.accountId) {
            return null;
        }

        const { firstName, avatar } = this.props.profile;
        return (
            <Popover placement="bottomRight" content={this.renderUserInfoPopover()} trigger="click">
                <Row>
                    <span style={{ color: '#eee', marginRight: 4 }}>Hi,</span>
                    <span><b>{firstName}</b></span>
                    <Avatar shape="square" size="large" src={avatar} style={{ marginLeft: 8 }} />
                </Row>
            </Popover>
        );
    }
}

function mapStateToProps(state) {
    return {
        profile: state.authReducer.profile
    };
}

export default connect(
    mapStateToProps,
    { removeAuthTokenAndLogout }
)(UserInfo);