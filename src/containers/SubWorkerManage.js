import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Row, Card, Col, AutoComplete, Avatar, Tooltip } from 'antd'
import {
    loadAllWorkerOfSubcontractor,
} from '../actions/subWorkerManageAction';
// import {Link} from "react-router-dom";
import {
    ICON_ANTD_CONSTANTS
} from '../constants/workerConstants';

import WorkerList from "./SubWorkerManage/WorkerList";


class SubWorkerManage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchedWorkerList: [],
            selectedWorker: undefined,
        }
    }

    componentDidMount() {
        this.props.loadAllWorkerOfSubcontractor(this.props.authReducer.profile.userId);
    }

    _onAddWorkerButtonClick = () => {
        this.props.history.push("/worker/add");
    }

    renderWorkerList = (workerList) => {
        return (
            <WorkerList
                workerListOfSub={workerList}
            />
        );
    }

    renderEachWorkerSearchInput = (worker) => {
        const Option = AutoComplete.Option;
        return (
            <Option 
                key={worker.workerId} 
                text={worker.firstName + ' ' + worker.lastName}
            >
                <Avatar src={worker.avatar}/>
                <span>{worker.firstName + ' ' + worker.lastName}</span>
            </Option>
        )
    }

    renderAutoCompleteSearch = (workers) => {
        return (
            <div style={{ width: 300 }}>
                <AutoComplete
                    dropdownStyle={{width: 300}}
                    size="default"
                    style={{ width: '100%' }}
                    placeholder="Tìm kiếm theo tên"
                    onSelect={(value) => this._onSelect(value)}
                    onSearch={(value) => this._onSearch(value, workers)}
                    // onBlur={(value) => this._onSearch(value, workers)}
                    optionLabelProp="text"
                >
                    {/*{this.state.searchedWorkerList ? this.state.searchedWorkerList : workerListForSearch}*/}

                    {/*
                    {this.state.isSearchingWorker ? 
                        (
                            <span>searching...</span>
                        ) : (
                            this.state.searchedWorkerList && this.state.searchedWorkerList.length > 0 ? this.state.searchedWorkerList.map(this.renderEachWorkerSearchInput) : 
                                workers.map(this.renderEachWorkerSearchInput)
                        )
                    }*/}
                    {this.state.searchedWorkerList && this.state.searchedWorkerList.length > 0 ? this.state.searchedWorkerList.map(this.renderEachWorkerSearchInput) : 
                                                    workers.map(this.renderEachWorkerSearchInput)}
                </AutoComplete>
            </div>
        )
    }

    _onSelect = (value) => {
        this.setState({
            selectedWorker: value
        })
    }

    _onSearch = (value, workersList) => {
        let searchedWorkers = [];
        for(var posWorker in workersList) {
            if(workersList[posWorker].lastName.toLowerCase().includes(value.toLowerCase()) || 
            workersList[posWorker].firstName.toLowerCase().includes(value.toLowerCase())) {
                searchedWorkers = searchedWorkers.concat(workersList[posWorker]);
            }
        }
        this.setState({
            searchedWorkerList: searchedWorkers
        })
    }

    renderHeaderCard = (workers) => {
        return (
            <div style={{ marginTop: 0, marginBottom: 0, width: "100%" }}>
                <div style={{ width: "100%" }}>
                    <Row>
                        <Col span={12}>{this.renderAutoCompleteSearch(workers)}
                        </Col>
                        <Col span={12}>
                            {workers.length >= 7 ? 
                                (
                                    <Tooltip title="Số lượng nhân công được tạo là 7 nhân công">
                                        <Button
                                            type="primary"
                                            icon={ICON_ANTD_CONSTANTS.USER_ADD}
                                            style={{ marginBottom: 0, float: "right", marginRight: 5 }}
                                            onClick={() => this._onAddWorkerButtonClick()}
                                            disabled={true}
                                        >
                                            Thêm nhân công
                                        </Button>        
                                    </Tooltip>
                                ) : (
                                    <Button
                                            type="primary"
                                            icon={ICON_ANTD_CONSTANTS.USER_ADD}
                                            style={{ marginBottom: 0, float: "right", marginRight: 5 }}
                                            onClick={() => this._onAddWorkerButtonClick()}
                                            disabled={false}
                                        >
                                            Thêm nhân công
                                    </Button>        
                                )
                            }
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }

    render() {
        return (
            <div style={{padding: 50}}>
                <div>
                    <Row>
                        <Card
                            style={{ borderRadius: 10 }}
                            title={this.renderHeaderCard(this.props.allWorkersOfASub)}
                        >
                            {/*this.renderWorkerList(this.props.allWorkersOfASub)*/}
                            {this.renderWorkerList(this.state.searchedWorkerList && this.state.searchedWorkerList.length > 0 ? 
                                this.state.searchedWorkerList : this.props.allWorkersOfASub)}
                        </Card>
                    </Row>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        authReducer: state.authReducer,
        allWorkersOfASub: state.subWorkerManageReducer.workerList
    }
}

const mapDispatchActionToProps = {
    loadAllWorkerOfSubcontractor
}

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(SubWorkerManage);