import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import {Link} from "react-router-dom";
import { 
    Button, 
    Table, 
    Col, 
    Row, 
    Tooltip, 
    Icon, 
    Tag, 
    Modal, 
    Card, 
    Pagination, 
    Input,
    Radio,
    Checkbox 
} from 'antd';
import { TABLE_LIST_CONFIG } from '../constants/requestContractConstants';
import { loadContractsList } from '../actions/adminContractsAction';
import {ViewContractPayment} from "../components/ViewContractPayment";
import { loadContractPayment } from "../actions/viewContractPaymentAction";
import {REQUESTCONTRACTSTATUS} from "../constants/requestContractStatus";
import { loadAllProfessions } from "../actions/professionAction";


class AdminContracts extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pageIndex: 0,
            sortCol: "id",
            sortOrder: "desc",
            pageSize: 20,
            locationFilters: null,
            professionFilters: null,
            contractStatusFilters: null,
            startDate: null,
            endDate: null,
            primeCompanyName: null,
            subCompanyName: null,

            visibleModal: false
        }
        
    }


    renderProfessionFilterList = (contracts) => {
        let professionFilterList = [];
        if(contracts && contracts.length > 0) {
            professionFilterList = [...new Set(contracts.map(contract => contract.profession))];
        }
        professionFilterList = professionFilterList.map((profession) => {
            return { text: profession, value: profession }
        })
        return professionFilterList;
    }


    renderLocationFilterList = (contracts) => {
        let locationFilterList = [];
        if(contracts && contracts.length > 0) {
            locationFilterList = [...new Set(contracts.map(contract => contract.location))];
        }
        locationFilterList = locationFilterList.map((location) => {
            return { text: location, value: location }
        })
        return locationFilterList;
    }

    _onClickShowPaymentModal = (contractId) => {
        this.props.loadContractPayment(contractId);
        this.setState({visibleModal:true});
    }

    componentDidMount() {
        this.loadContracts();
        this.props.loadAllProfessions();
    }


    _onHandleSortOrder = () => {
        // let sortOrderTmp = null;
        // this.state.sortOrder === "asc" ? sortOrderTmp = "desc" : sortOrderTmp = "asc";
        // console.log("this.state.sortOrder === :", this.state.sortOrder === "desc");
        // console.log("sortorder: ", sortOrderTmp);
        // this.setState({
        //     sortOrder: sortOrderTmp
        // });
    }

    loadContracts = () => {
        const criterial = {
            pageIndex: this.state.pageIndex === 0 ? this.state.pageIndex : this.state.pageIndex - 1,
            sortCol: this.state.sortCol,
            sortOrder: this.state.sortOrder,
            pageSize: this.state.pageSize,
            professionFilters: this.state.professionFilters,
            startDateFilter: this.state.startDateFilter,
            endDateFilter: this.state.endDateFilter,
            contractStatusFilters: this.state.contractStatusFilters,
            locationFilters: this.state.locationFilters ,
            subCompanyName: this.state.subCompanyName,
            primeCompanyName: this.state.primeCompanyName
        }
        // console.log("FILTER: ", criterial);
        this.props.loadContractsList(criterial);
    }


    renderPaymenModal = () => {
        return(
            <Modal className={'PrimeSearchModal'}
                   title="Bảng giá tiền của dự án hoan thành"
                   visible={this.state.visibleModal}
                   onOk={this._onCloseModal}
                   onCancel={this._onCloseModal}
            >
                <ViewContractPayment contractPayment={this.props.contractPayment} />
            </Modal>
        );
    }


    _onCloseModal = () => {
        this.setState({visibleModal:false});
    }

    render(){
        // console.log("PROPS: ", this.props);
        let tableConfig = [{
            title: 
                <Tooltip title="Mã hợp đồng. Mỗi hợp đồng có một mã duy nhất.">
                    {TABLE_LIST_CONFIG.CONTRACT_CODE.HEADER} <Icon type="info-circle"/>
                </Tooltip>,
            dataIndex: '',
            key: '',
            render: (text, record, index) => {
                return <Tooltip title="Mã dự án. Mỗi dự án sẽ có một mã số duy nhất.">
                    <div style={{
                        padding: "2px 5px",
                        background: "rgb(162, 162, 162)",
                        textAlign: "center",
                        color: "#fff",
                        borderRadius: " 5px",
                        fontWeight: "bold",
                        fontSize: "15px",
                    }}>{record.id}</div>
                </Tooltip>;
            },
            // defaultSortOrder: 'descend',
            sorter: () => this._onHandleSortOrder()
            
        }, {
            title: TABLE_LIST_CONFIG.SUBCONTRACTOR.HEADER,
            dataIndex: '',
            key: '',
            render: (text, record, index) => {
                return (
                    <Link to={`/profile/${record.subAccountId}`}>{record.subCompanyName}</Link>
                )
            }
        }, {
            title: TABLE_LIST_CONFIG.PRIMECONTRACTOR.HEADER,
            dataIndex: '',
            key: '',
            render: (text, record, index) => {
                return (
                    <Link to={`/profile/${record.primeAccountId}`}>{record.primeCompanyName}</Link>
                )
            }
                        
        }, {
            title: TABLE_LIST_CONFIG.STARTDATE_ENDDATE.HEADER,
            dataIndex: '',
            key: '',
            render: (text, record, index) => {
                return <div style={{width: 160}}>
                    <Col span={24} style={{marginTop: 10, textAlign: "center"}}>{moment(record.startDate).format('DD/MM/YYYY')} - {moment(record.endDate).format('DD/MM/YYYY')}</Col>
                </div>;
            },
            // filterIcon: <Icon type="calendar" />
        }, {
            title: TABLE_LIST_CONFIG.CONTRACT_STATUS.HEADER,
            dataIndex: TABLE_LIST_CONFIG.CONTRACT_STATUS.DATAINDEX_KEY,
            key: TABLE_LIST_CONFIG.CONTRACT_STATUS.DATAINDEX_KEY,
            // filterIcon: filtered => <Icon type="flag" theme="filled" style={{ color: filtered ? '#1890ff' : undefined }} />,
            // filters: [
            //     { text: <Tag color="#87d068">Đã hoàn thành</Tag>, value: "DONE" },
            //     { text: <Tag color="#2db7f5">Đang triển khai</Tag>, value: "WORKING" },
            // ],
            // filterMultiple: false,
            // onFilter: (value, record) => {
            //     if(value === "DONE") {
            //         return record.status === 4
            //     } else{
            //         return record.status !== 4
            //     }
            // },
            render: (text, record, index) => {
                if(record.status === 4 && record.completeDate) {
                    return (
                        <div>
                            <Row>
                                <Col><Tag color="#87d068">Đã hoàn thành</Tag></Col>
                                <Col><p style={{marginLeft: 15, fontSize: 12}}>{moment(record.completeDate).format('DD/MM/YYYY')}</p></Col>
                            </Row>
                        </div>
                    )
                } else {
                    return <Tag color="#2db7f5">Đang triển khai</Tag>
                }
                    // return `${moment(record.completeDate).format('DD/MM/YYYY')}`;
            }
        }, {
            title: TABLE_LIST_CONFIG.LOCATION.HEADER,
            dataIndex: TABLE_LIST_CONFIG.LOCATION.DATAINDEX_KEY,
            key: TABLE_LIST_CONFIG.LOCATION.DATAINDEX_KEY,
            // filters: this.renderLocationFilterList(this.props.adminContractsReducer.allContracts),
            // filterIcon: filtered => <Icon type="pushpin" theme="filled" style={{ color: filtered ? '#1890ff' : undefined }} />,
            // onFilter: (value, record) => {
            //     return record.location === value;
            // }
        }, {
            title: TABLE_LIST_CONFIG.PROFESSION.HEADER,
            dataIndex: TABLE_LIST_CONFIG.PROFESSION.DATAINDEX_KEY,
            key: TABLE_LIST_CONFIG.PROFESSION.DATAINDEX_KEY,
            // filters: this.renderProfessionFilterList(this.props.adminContractsReducer.allContracts),
            // filterIcon: filtered => <Icon type="profile" theme="filled" style={{ color: filtered ? '#1890ff' : undefined }} />,
            // onFilter: (value, record) => {
            //     return record.profession === value;
            // }
        }, {
            title: TABLE_LIST_CONFIG.PRICE_PER_DOCKET.HEADER,
            dataIndex: TABLE_LIST_CONFIG.PRICE_PER_DOCKET.DATAINDEX_KEY,
            key: TABLE_LIST_CONFIG.PRICE_PER_DOCKET.DATAINDEX_KEY,
            render: (text, record, index) => { return <div>
                    <span>{`${record.pricePerDocket}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</span>
                    <sup
                        style={{
                            fontWeight: "normal",
                            right: "-5px",
                            color: "#ad8700",
                        }}>đ</sup>
                </div>
            },
            // defaultSortOrder: 'descend',
            // sorter: () => record1.pricePerDocket - record2.pricePerDocket
        }, {
            title: TABLE_LIST_CONFIG.ACTION.PAYMENT.HEADER,
            dataIndex: '',
            key: '',
            render: (text, record, index) => { return <Button 
                        type="primary" 
                        shape="circle-outline" 
                        icon="money-collect"
                        style={{marginLeft: 30}} 
                        size="large" 
                        onClick={()=>this._onClickShowPaymentModal(record.id)}
                    />
            }
        }]
        return (
            <div style={{padding: 50, textAlign: "center"}}>
                {/*<Card title={this.renderHeaderCard(this.state.contracts)} style={{ borderRadius: 10 }}>*/}
                    <div style={{marginBottom: 10}}>
                        {this.renderSearchPanel()}
                    </div>
                    <div>
                        <Table
                            columns={tableConfig}
                            pagination={false}
                            // expandedRowRender="abc"
                            dataSource={this.props.adminContractsReducer.allContracts}
                            rowKey={record => record.id}
                            // title={this.renderHeaderCard(this.state.contracts)}
                            // title="abc"
                            // onChange={this._onHandleTableChange}
                        />
                        {this.renderPaymenModal()}
                    </div>
                    <div style={{display: "inline-box", marginTop: 50}}>
                        <Pagination 
                            defaultCurrent={1} 
                            current={this.state.pageIndex}
                            total={this.props.adminContractsReducer.allContracts && this.props.adminContractsReducer.allContracts.length > 0 ? 
                                this.props.adminContractsReducer.allContracts[0].totalContractRequest : this.state.pageSize} 
                            pageSize={this.state.pageSize}
                            onChange={(value) => this._onPageIndexChange(value)}
                        />
                    </div>
                {/*</Card>*/}
            </div>
        )
    }

    renderSearchPanel = () => {
        return (
            <Card 
                title={<span style={{fontSize: "15px", fontStyle: "italic"}}>Bộ lọc <Icon type="search"/></span>} 
                style={{textAlign: "left", borderRadius: 15}}
                extra={<div>
                            <Button icon="filter" style={{width: 100}} type="primary" onClick={() => this._onFilterClick()}>Lọc</Button>
                        </div>}
                >
                <div>
                    <Row>
                        <Col span={16}>
                            <Input
                                style={{width: 500, marginBottom: 15, marginRight: 15}}
                                name="primeCompanyName"
                                allowClear
                                addonBefore="Tên công ty của thầu chính"
                                onChange={(e) => this._onSearchInputFieldsChange(e.target.name, e.target.value)}
                            />
                            <Input
                                style={{width: 500}}
                                name="subCompanyName"
                                allowClear
                                addonBefore="Tên công ty của thầu phụ"
                                onChange={(e) => this._onSearchInputFieldsChange(e.target.name, e.target.value)}
                            />
                            <div style={{float: "left", marginRight: 30, marginTop: 20}}>
                                <span style={{fontSize: "15px"}}>Trạng thái của dự án</span><br/>
                                <Radio.Group name="contractStatusFilters" defaultValue="ALL" buttonStyle="solid" onChange={(e) => this._onContractStatusChange(e.target.name, e.target.value)}>
                                    <Radio.Button value="DONE" buttonStyle="solid">Đã hoàn thành</Radio.Button>
                                    <Radio.Button value="WORKING" buttonStyle="solid">Đang thực thi</Radio.Button>
                                    <Radio.Button value="ALL" buttonStyle="solid">Tất cả</Radio.Button>
                                </Radio.Group>
                            </div>
                        </Col>
                        <Col span={8}>
                            <div style={{float: "left"}}>
                                <span style={{fontSize: "15px"}}><Icon type="profile"/>Lĩnh vực</span>
                                {/*
                                <Radio.Group name="professionFilters" defaultValue="a" buttonStyle="solid" onChange={(e) => this._onSearchInputFieldsChange(e.target.name, [e.target.value])}>
                                    {this.renderProfessionFilter(this.props.professionReducer.professions)}
                                </Radio.Group>*/}
                                <Checkbox.Group style={{ width: '100%' }} name="professionFilters" onChange={(value) => this._onSearchInputFieldsChange("professionFilters", value)}>
                                    {this.renderProfessionFilter(this.props.professionReducer.professions)}
                                </Checkbox.Group>
                            </div>
                        </Col>
                    </Row>
                </div>
            </Card>
        )
    }

    _onFilterClick = () => {
        this.setState({
            pageIndex: 0
        }, () => {
            this.loadContracts();
        })
    }

    renderProfessionFilter = (professions) => {
        return professions.map(profession => {
            return <Row key={profession.professionId}>
                        <Col span={24}><Checkbox value={profession.professionId}>{profession.professionName}</Checkbox></Col>
                    </Row>
        })
    }

    
    _onContractStatusChange = (name, value) => {
        let requestStatus = [];
        if(value === "DONE") {
            requestStatus = requestStatus.concat(REQUESTCONTRACTSTATUS.COMPLETE);
        } else if(value === "WORKING") {
            requestStatus = requestStatus.concat(REQUESTCONTRACTSTATUS.DOCKETS_DETAIL_FILLED);
            requestStatus = requestStatus.concat(REQUESTCONTRACTSTATUS.WORKER_ASSIGNED);
        } else if(value === "ALL") {
            requestStatus = requestStatus.concat(REQUESTCONTRACTSTATUS.DOCKETS_DETAIL_FILLED);
            requestStatus = requestStatus.concat(REQUESTCONTRACTSTATUS.WORKER_ASSIGNED);
            requestStatus = requestStatus.concat(REQUESTCONTRACTSTATUS.COMPLETE);
        }
        this._onSearchInputFieldsChange(name, requestStatus);
    }

    _onSearchInputFieldsChange = (name, value) => {
        this.setState({
            [name]: value
        })
    }

    _onPageIndexChange = (page) => {
        this.setState({pageIndex: page}, () => {
            this.loadContracts();
        })
        
    }


    renderHeaderCard = (contracts) => {
        return (
            <div>
                <Row>
                    <Col span={24}>
                        <Icon type="project" theme="twoTone"/>  
                        <span style={{fontSize: "20px", color: "#0080ff", fontStyle: "italic"}}>Danh sách dự án</span>
                    </Col>
                </Row>
                <Row>
                    <Col span={6} offset={18}><span style={{fontStyle: "italic"}}>Tổng số dự án: </span>  <span>{contracts.length}</span></Col>
                    <Col span={6} offset={18}>
                        <span style={{fontStyle: "italic"}}>Hoàn thành: </span>  
                        <span>{(contracts.filter(contract => {return contract.status === 4})).length}</span>
                    </Col>
                    <Col span={6} offset={18}>
                        <span style={{fontStyle: "italic"}}>Đang thực thi: </span>  
                        <span>{(contracts.filter(contract => {return contract.status !== 4})).length}</span>
                    </Col>
                </Row>
            </div>
        )
    }

}

function mapStateToProps(state) {
    return {
        adminContractsReducer: state.adminContractsReducer,
        contractPayment: state.viewContractPaymentReducer.contractPayment,
        professionReducer: state.professionReducer,
    };
}

const mapDispatchActionToProps = {
    loadContractsList,
    loadContractPayment,
    loadAllProfessions
}

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(AdminContracts);
