import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
    getStyleFromFormat,
    getStyleFromFormatFormCreator
} from "../../helpers/formHelper";

import {
    FORM_CONTROL_TYPE,
    FORM_STATIC_CONTROL_TYPE,
    A4WIDTH,
    A4HEIGHT
} from '../../constants/docketForm';

import { Rnd } from 'react-rnd';
import {
    Row,
} from 'antd';

class StaticForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            a4PageFormScale: 1, //styles to scale A4 Page
        };
    };

    componentDidMount() {
        this.setState({
            a4PageFormScale: this.a4PageFormCol.getBoundingClientRect().width / A4WIDTH,
            A4PageFormContainerWidth: this.a4PageFormCol.getBoundingClientRect().width,
            A4PageFormContainerHeight: A4HEIGHT * this.a4PageFormCol.getBoundingClientRect().width / A4WIDTH,
            ////styles to scale A4 Page
        }, () => {
            this.forceUpdate();
        });
    };

    renderStaticControlContent(control, index) {
        const { statictype, value } = control.config;
        const style = getStyleFromFormat(control.format);

        switch (statictype) {
            case FORM_STATIC_CONTROL_TYPE.TEXT: {
                return (
                    <div
                        key={`staticControlContent${index}`}
                        style={style}
                    >
                        {value}
                    </div>
                );
            }
            case FORM_STATIC_CONTROL_TYPE.IMAGE: {
                return (
                    <div
                        key={`staticControlContent${index}`}
                        style={style}
                    >
                        <img
                            src={value}
                            alt={value}
                            style={{ width: '100%', height: '100%' }}
                        />
                    </div>
                );
            }
            default: {
                return null;
            }
        }
    };

    //render FormControl có type là input
    renderInputControlContent(control, index) {
        const { label } = control.config;
        const style = getStyleFromFormat(control.format);
        style.backgroundColor = "#eee";

        return (
            <div
                key={`inputControlContent${index}`}
                style={style}
            >
                {label}
            </div>
        );
    };

    //render FormControl có type là predefined
    renderPredefinedControlContent(control, index) {
        const { label } = control.config;
        const style = getStyleFromFormat(control.format);
        style.backgroundColor = "#eee";

        return (
            <div
                key={`predefinedControlContent${index}`}
                style={style}
            >
                {label}
            </div>
        );
    };

    //render nội dung của cái form control
    renderControlContent(control, index) {
        const { type } = control.config;
        switch (type) {
            case FORM_CONTROL_TYPE.STATIC: {
                return this.renderStaticControlContent(control, index);
            }
            case FORM_CONTROL_TYPE.INPUT: {
                return this.renderInputControlContent(control, index);
            }
            case FORM_CONTROL_TYPE.PREDEFINED: {
                return this.renderPredefinedControlContent(control, index);
            }
            default:
                return null;
        }
    };

    renderControlRnd() {
        const { formControl } = this.props;

        const styles = {
            formControlContainer: {
                overflow: "hidden",
                width: '100%',
                height: '100%',
                MozUserSelect: "none",
                WebkitUserSelect: "none",
                msUserSelect: "none",
                userSelect: "none",
            },
            formControlContentContainer: {
                width: '100%',
                height: '100%',
            }
        };

        return formControl.map((control, index) => {
            const { pos } = control;

            return (
                <Rnd
                    key={index}
                    size={{ width: pos.width, height: pos.height }}
                    position={{ x: pos.ox, y: pos.oy }}
                    scale={this.state.a4PageFormScale}//scale for every control in A4Paper
                    bounds="parent"
                    enableResizing={false}
                    disableDragging={true}
                >
                    <div style={styles.formControlContainer}>
                        <Row
                            type="flex" justify="center" align="middle"
                            style={styles.formControlContentContainer}
                        >
                            {this.renderControlContent(control, index)}
                        </Row>
                    </div>
                </Rnd>
            );
        });
    };

    renderA4Page() {
        const styles = {
            a4PageForm: {
                boxSizing: "border-box",
                position: "relative", //must relative for draggable work
                width: A4WIDTH,
                height: A4HEIGHT,
                backgroundColor: "#fff",
                transform: `scale(${this.state.a4PageFormScale})`,//styles to scale A4 Paper
                transformOrigin: "left top"
            },
        };

        return (
            <div style={styles.a4PageForm}>
                {this.renderControlRnd()}
            </div>
        );
    };

    render() {
        const styles = {
            a4PageFormContainer: {
                boxSizing: "border-box",
                borderBottom: "1px solid #eee",
                overflowY: "hidden",
                overflowX: "hidden",
                //for scale A4Page
                width: this.state.A4PageFormContainerWidth ? this.state.A4PageFormContainerWidth : "100%",
                height: this.state.A4PageFormContainerHeight ? this.state.A4PageFormContainerHeight : "100%",
            },
        };
        return (
            <div
                style={styles.a4PageFormContainer}
                ref={(ref) => { this.a4PageFormCol = ref }}
            >
                {this.renderA4Page()}
            </div>
        );
    };
}

const mapStateToProps = (state) => {
    return {
    };
};

const mapDispatchActionToProps = {
};

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(StaticForm);