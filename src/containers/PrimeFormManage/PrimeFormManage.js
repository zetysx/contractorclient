import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getAllFormByPrimeId } from '../../actions/primeFormManageAction';
import { changeDataToCreateNewForm } from '../../actions/formCreatorAction';

import { COLOR } from "../../constants/theme";

import StaticForm from './StaticForm';
import {
    Row,
    Col,
    Card,
    Icon,
    Tooltip
} from 'antd';

class PrimeFormManage extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        this.props.getAllFormByPrimeId(this.props.profile.userId);
    };

    renderFormCardChangeButton(index) {
        return (
            <Tooltip title="Chỉnh sửa mẫu chấm công này">
                <div onClick={() => { this._onClickChangeFormButton(index) }}>
                    <Icon type="edit" />
                </div>
            </Tooltip>
        );
    };

    renderFormCardDeleteButton(index) {
        return (
            <Tooltip title="Xóa mẫu chấm công này">
                <div onClick={() => { this._onClickDeleteFormButton(index) }}>
                    <Icon type="delete" />
                </div>
            </Tooltip>
        );
    };

    renderCardStaticForm(form, index) {
        const formControl = JSON.parse(form.formControls);
        return (
            <StaticForm formControl={formControl} key={`staticFormCard${index}`} />
        );
    }

    renderFormListCard() {
        const styles = {
            formContainer: {
                paddingBottom: 16,
                paddingRight: 16,
            }
        };

        const { formList } = this.props;

        if (!formList) {
            return null;
        };

        return formList.map((form, index) => {
            return (
                <Col span={6} style={styles.formContainer} key={form.formId}>
                    <Card key={form.formId}
                        cover={this.renderCardStaticForm(form, index)}
                        actions={[this.renderFormCardChangeButton(index), this.renderFormCardDeleteButton(index)]}
                    >
                        <Card.Meta
                            title={form.formName}
                            description={(<div><Icon type="calendar"/>{` ${form.createdTime}`}</div>)}
                        />
                    </Card>
                </Col>
            );
        });
    };

    renderAddingCard() {
        const styles = {
            formContainer: {
                paddingBottom: 16,
                paddingRight: 16,
            },
            addingCard: {
                textAlign: "center",
                backgroundColor: "#f8f8f8"
            }
        };

        return (
            <Col span={6} style={styles.formContainer}>
                <Card style={styles.addingCard}>
                    <Tooltip title="Tạo mẫu chấm công mới">
                        <div onClick={() => { this._onClickNewFormButton() }}>
                            <Icon type="plus" style={{ fontSize: 28 }} />
                        </div>
                    </Tooltip>
                </Card>
            </Col>
        );
    };

    render() {
        return (
            <div style={{ padding: "50px 75px" }}>
                <div
                    style={{
                        fontSize: 20,
                        fontWeight: "bold",
                        color: COLOR.primaryBlue
                    }}
                >
                    Mẫu chấm công
                </div>
                <Row
                    style={{
                        paddingTop: 20
                    }}
                >
                    {this.renderFormListCard()}
                    {this.renderAddingCard()}
                </Row>
            </div>

        );
    };

    _onClickNewFormButton() {
        const onSuccessCallback = () => {
            this.props.history.push('/formCreator');
        };
        this.props.changeDataToCreateNewForm([], undefined, onSuccessCallback);
    };

    _onClickChangeFormButton(index) {
        const onSuccessCallback = () => {
            this.props.history.push('/formCreator');
        };

        const { formControls, formId } = this.props.formList[index];
        const formControlJson = JSON.parse(formControls);

        const newFormControl = formControlJson.map((control) => {
            const { pos, config, format } = control;
            const newPos = { ...pos };
            const newConfig = { ...config };
            const newFormat = { ...format };
            return {
                pos: newPos,
                config: newConfig,
                format: newFormat,
            };
        });

        this.props.changeDataToCreateNewForm(newFormControl, formId, onSuccessCallback);
    };

    _onClickDeleteFormButton(index) {
        alert(index);
    };
}

const mapStateToProps = (state) => {
    return {
        profile: state.authReducer.profile,
        formList: state.primeFormManageReducer.formList,
    };
};

const mapDispatchActionToProps = {
    getAllFormByPrimeId,
    changeDataToCreateNewForm
};

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(PrimeFormManage);