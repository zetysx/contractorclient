import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from "react-router-dom";

import {getRatingList} from '../../actions/adminRatingAction';

import {Icon, Tooltip, Spin, Pagination, Rate} from "antd";

class AdminRating extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pageIndex: 0,
            visible: false,
        }
    }

    componentDidMount() {
        this.props.getRatingList(0);
    };

    renderTableRow = (contractRating) => {
        if (contractRating) {
            if (contractRating.length === 0) {
                return (
                    <tr>
                        Hiện tại hệ thống chưa có đánh giá nào
                    </tr>
                )
            } else {
                const styleTd = {
                    padding: "25px 10px",
                    fontSize: "13px"
                };
                return contractRating.map((contract) => {
                    return (
                        <tr key={contract.contractId}
                            style={{
                                borderBottom: "1px solid #f1f1f1",
                                padding: "20px 0",
                                background: "rgb(255, 255, 255)",
                            }}>
                            <td style={styleTd}>
                                <Link
                                    to={`/profile/${contract.primeAccountId}`}>{contract.primeCompanyname}</Link>
                            </td>
                            <td style={styleTd}>
                                {(
                                    (contract.primeRating)
                                        ?
                                        (
                                            <div>
                                                <Rate disabled defaultValue={contract.primeRating} style={{
                                                    fontSize: "23px",
                                                    display: "block",
                                                    marginBottom: "20px",
                                                    textAlign: "center"
                                                }}/>
                                                {contract.primeReview}
                                                <div style={{
                                                    fontSize: "10px",
                                                    color: "#b3b3b3",
                                                    textAlign: "center",
                                                    marginTop: "20px"
                                                }}>
                                                    {contract.primeTimeRated}
                                                </div>
                                            </div>
                                        )
                                        :
                                        (
                                            <div style={{
                                                color: "#b3b3b3",
                                                textAlign: "center",
                                            }}>
                                                Chưa có đánh giá
                                            </div>
                                        )
                                )}
                            </td>
                            <td style={styleTd}>
                                <span style={{
                                    padding: "2px 6px",
                                    background: "rgb(162, 162, 162)",
                                    textAlign: "center",
                                    color: "#fff",
                                    borderRadius: " 5px",
                                    fontWeight: "bold",
                                    fontSize: "14px",
                                }}>{contract.contractId}</span>
                            </td>
                            <td style={styleTd}>
                                {(
                                    (contract.subRating)
                                        ?
                                        (
                                            <div>
                                                <Rate disabled defaultValue={contract.subRating} style={{
                                                    fontSize: "23px",
                                                    display: "block",
                                                    marginBottom: "20px",
                                                    textAlign: "center"
                                                }}/>
                                                {contract.subReview}
                                                <div style={{
                                                    fontSize: "10px",
                                                    color: "#b3b3b3",
                                                    textAlign: "center",
                                                    marginTop: "20px"
                                                }}>
                                                    {contract.subTimeRated}
                                                </div>
                                            </div>
                                        )
                                        :
                                        (
                                            <div style={{
                                                color: "#b3b3b3",
                                                textAlign: "center",
                                            }}>
                                                Chưa có đánh giá
                                            </div>
                                        )
                                )}
                            </td>
                            <td style={styleTd}>
                                <Link
                                    to={`/profile/${contract.subAccountId}`}>{contract.subCompanyname}</Link>
                            </td>
                        </tr>
                    )
                });
            }
        }
    };

    render() {
        const styleTd = {
            padding: "25px 5px",
        };
        return (
            <Spin spinning={this.props.isFetching === true}>

                <div style={{padding: "75px 50px"}}>
                    <Spin spinning={this.props.isFetching}>
                        <table
                            border="0"
                            style={{
                                background: "#fff",
                                width: "100%"
                            }}>
                            <thead>
                            <tr style={{
                                fontSize: "14px",
                                background: "#c9e4ff",
                                textAlign: "center"
                            }}>
                                <td style={styleTd}>Thầu chính</td>
                                <td style={styleTd}>Thầu chính đánh giá</td>
                                <td style={styleTd}>
                                    <Tooltip
                                        title="Mã dự án. Mỗi dự án sẽ có một mã số duy nhất.">
                                        Dự án <Icon type="question-circle"/>
                                    </Tooltip>
                                </td>
                                <td style={styleTd}>Thầu phụ đánh giá</td>
                                <td style={styleTd}>Thầu phụ</td>
                            </tr>
                            </thead>
                            <tbody>
                            {this.renderTableRow(this.props.ratingList.ratingBothSidesDTOList)}
                            </tbody>
                        </table>
                    </Spin>

                    <div
                        style={{
                            textAlign: "center",
                            background: "rgb(255, 255, 255)",
                            marginTop: "25px",
                            padding: "20px 0px",
                            borderRadius: "10px",
                            width: "40%",
                            marginLeft: "30%",
                        }}>
                        <Pagination onChange={this._onChangePageIndex} defaultCurrent={this.state.pageIndex}
                                    pageSize={13}
                                    total={this.props.ratingList.elementNumber}/>
                    </div>
                </div>
            </Spin>
        );
    }

    _onChangePageIndex = (page) => {
        this.setState({pageIndex: page - 1}, () => {
            this.props.getRatingList(this.state.pageIndex);
        });
    };

};

const
    mapStateToProps = (state) => {
        return {
            ratingList: state.adminRatingReducer.ratingList,
            isFetching: state.adminRatingReducer.isFetching

        }
    };

const
    mapDispatchActionToProps = {
        getRatingList
    };

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(AdminRating)
;