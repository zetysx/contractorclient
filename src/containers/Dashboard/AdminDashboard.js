import React, { Component } from 'react';
import { connect } from 'react-redux';

import { COLOR } from "../../constants/theme";

import {loadAdminOverview} from '../../actions/adminOverviewAction';

import NumberCard from '../../components/NumberCard';
import {
    Row,
    Col,
} from 'antd';

class AdminDashboard extends Component {

    componentDidMount() {
        this.props.loadAdminOverview(this.props.authReducer.profile.accountId);
    }

    render() {
        if (!this.props.adminOverviewReducer.adminOverview) return null;
        const numbers = [
            {
                icon: 'team',
                color: COLOR.green,
                title: 'Nhà thầu chính',
                number: this.props.adminOverviewReducer.adminOverview.primecontractorAmount,
            },
            {
                icon: 'tool',
                color: COLOR.blue,
                title: 'Nhà thầu phụ',
                number: this.props.adminOverviewReducer.adminOverview.subcontractorAmount,
            },
            {
                icon: 'contacts',
                color: COLOR.purple,
                title: 'Nhân công',
                number: this.props.adminOverviewReducer.adminOverview.workerAmount,
            },
            {
                icon: 'dollar',
                color: COLOR.red,
                title: 'Dự án',
                number: this.props.adminOverviewReducer.adminOverview.contractAmount,
            },
        ];


        const numberCards = numbers.map((item, key) => (
            <Col key={key} lg={12} md={12}>
                <NumberCard role={"admin"} {...item} />
            </Col>
        ));

        return (
            <Row gutter={24}  style={{padding:"50px"}}>
                {numberCards}

            </Row>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        authReducer: state.authReducer,
        adminOverviewReducer: state.adminOverviewReducer
    };
};

const mapDispatchActionToProps = {
    loadAdminOverview
};


export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(AdminDashboard);