import React, { Component } from 'react';
import { connect } from 'react-redux';

import { COLOR } from "../../constants/theme";

import { loadSubOverview } from '../../actions/subOverviewAction';

import PieChartCustom from '../../components/PieChartCustom';
import LineChartCustom from '../../components/LineChartCustom';
import BarChartCustom from '../../components/BarChartCustom';
import NumberCard from '../../components/NumberCard';
import {
    Row,
    Col,
    Card
} from 'antd';
import {
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer,
    ComposedChart,
    Bar
} from 'recharts';

const colors = [COLOR.purple, COLOR.red, COLOR.green, COLOR.yellow, COLOR.sky]
const styles = {
    sales: {
        overflow: "hidden",
    },
    title: {
        marginLeft: 32,
        fontSize: 16,
    },
    radiusdot: {
        width: 12,
        height: 12,
        marginRight: 8,
        borderRadius: "50%",
        display: "inline-block",
    },
    legend: {
        textAlign: "right",
        color: "#999",
        fontSize: 14,
    },
    legendli: {
        height: 48,
        //   line-height: 48px;
        display: "inline-block",
        marginLeft: 24,
    },
    tooltip: {
        background: "#fff",
        padding: 20,
        fontSize: 14,
    },
    tiptitle: {
        fontWeight: 700,
        fontSize: 14,
        marginBottom: 8,
    },
    tooltiplist: {
        listStyle: "none",
        padding: 0,
    },
    tipitem: {
        height: 32,
        // lineHeight: 32,
    }
}

class SubDashboard extends Component {


    componentDidMount() {
        this.props.loadSubOverview(this.props.authReducer.profile.userId);
    }


    renderChartLengend(prop) {
        const { payload } = prop
        return (
            <ul style={styles.legend}>
                {payload.map((item, key) => (
                    <li key={key} style={styles.legendli}>
                        <span
                            style={{
                                ...styles.radiusdot,
                                background: item.color
                            }}
                        />
                        {item.value}
                    </li>
                ))}
            </ul>
        );
    };

    renderChartTooltip(prop) {
        const list = prop.payload.map((item, key) => (
            <li key={key} style={styles.tipitem}>
                <span
                    style={{
                        ...styles.radiusdot,
                        background: item.color
                    }}
                />
                {`${item.name}: ${new Intl.NumberFormat('en').format(item.value)}`}
            </li>
        ))
        return (
            <div style={styles.tooltip}>
                <p style={styles.tiptitle}>{prop.label}</p>
                <ul style={styles.tooltiplist}>{list}</ul>
            </div>
        );
    };

    render() {
        const numbers = [
            {
                icon: 'team',
                color: COLOR.green,
                title: 'Trung bình doanh thu 1 dự án(VNĐ)',
                number: 12,
            },
            {
                icon: 'tool',
                color: COLOR.blue,
                title: 'Tổng doanh thu(VNĐ)',
                number: 8,
            },
            {
                icon: 'contacts',
                color: COLOR.purple,
                title: 'Dự án đang thi hành',
                number: 15,
            },
            {
                icon: 'dollar',
                color: COLOR.grass,
                title: 'Dự án đã hoàn thành',
                number: 80,
            },
        ];
        if (!this.props.subOverviewReducer.subOverview) return null;
        numbers[0].number = (this.props.subOverviewReducer.subOverview.averageMoneyEarnedPerContract != null) ? this.props.subOverviewReducer.subOverview.averageMoneyEarnedPerContract : 0;
        numbers[1].number = (this.props.subOverviewReducer.subOverview.totalMoneyEarned != null) ? this.props.subOverviewReducer.subOverview.totalMoneyEarned : 0;
        numbers[2].number = this.props.subOverviewReducer.subOverview.contractInProgress;
        numbers[3].number = this.props.subOverviewReducer.subOverview.totalContractComplete;
        console.log(this.props.subOverviewReducer.subOverview);
        return (
            <Row gutter={24} style={{ padding: "50px" }}>
                <Row type="flex" justify="space-between" align="top" gutter={24}>
                    <Col lg={12} md={24} gutter={24}>
                        <Row type="flex" justify="space-between" align="top" gutter={24}>
                            <Col lg={12} md={12} style={{ height: "100%" }}>
                                <NumberCard {...numbers[0]} />
                            </Col>
                            <Col lg={12} md={12}>
                                <NumberCard {...numbers[1]} />
                            </Col>
                        </Row>
                        <Row type="flex" justify="space-between" align="bottom" gutter={24} style={{ position: "relative", top: "16px" }}>
                            <Col lg={12} md={12}>
                                <NumberCard {...numbers[2]} />
                            </Col>
                            <Col lg={12} md={12}>
                                <NumberCard {...numbers[3]} />
                            </Col>
                        </Row>
                    </Col>
                    <Col lg={12} md={24}>
                        <PieChartCustom
                            keyData="professionName"
                            value="moneyEarned"
                            data={this.props.subOverviewReducer.subOverview.totalMoneyPerProfession}
                            colors={colors}
                            title="Tổng doanh thu từng chuyên môn(VNĐ)"
                        />
                    </Col>
                </Row>


                <br />
                <Row>
                    <Col lg={24} md={24}>
                        <LineChartCustom
                            title="Số dự án thành công từng tháng"
                            data={this.props.subOverviewReducer.subOverview.monthlyContractCompletes}
                            xDataKey="monthYear"
                            yDataKey="contractComplete"
                            legends={"Số dự án thành công"}
                        />
                    </Col>
                </Row>

                <br />
                <Row>
                    <Col lg={24} md={24}>
                        <BarChartCustom
                            title="Doanh thu từng tháng (VNĐ)"
                            data={this.props.subOverviewReducer.subOverview.monthlyRevenue}
                            xDataKey="monthYear"
                            yDataKey="expenseOrRevenue"
                            legends={"Doanh thu hàng tháng"}
                        />
                    </Col>
                </Row>
                <br />

                <Row>
                    <Col lg={24} md={24}>
                        <div style={styles.sales}>
                            <Card
                                bordered={false}
                                style={{ padding: 20 }}
                                bodyStyle={{
                                    padding: '24px 36px 24px 0',
                                }}
                            >
                                <div style={styles.title}>
                                    Công việc đang làm và đã hoàn thành của từng công nhân tháng vừa qua
                                </div>
                                <ResponsiveContainer minHeight={360}>
                                    <ComposedChart data={this.props.subOverviewReducer.subOverview.workerDocketStatisticPastMonths}>
                                        <Legend
                                            verticalAlign="top"
                                            content={(prop) => this.renderChartLengend(prop)}
                                        />
                                        <XAxis
                                            dataKey={"workerName"}
                                            axisLine={{ stroke: COLOR.borderBase, strokeWidth: 1 }}
                                            tickLine={false}
                                        />
                                        <YAxis
                                            tickFormatter={(value) => new Intl.NumberFormat('en').format(value)}
                                            axisLine={false}
                                            tickLine={false} />
                                        <CartesianGrid
                                            vertical={false}
                                            stroke={COLOR.borderBase}
                                            strokeDasharray="3 3"
                                        />
                                        <Tooltip
                                            wrapperStyle={{
                                                border: 'none',
                                                boxShadow: '4px 4px 40px rgba(0, 0, 0, 0.05)',
                                            }}
                                            // formatter={(value) => {console.log('ssssssssss') return new Intl.NumberFormat('en').format(value)}}
                                            content={(prop) => this.renderChartTooltip(prop)}
                                        />
                                        <Bar
                                            type="monotone"
                                            name={"Công việc đã hoàn thành"}
                                            dataKey={"docketCompletePastMonth"}
                                            //label={(data) => new Intl.NumberFormat('en').format(data.value)}
                                            fill={COLOR.sky}
                                        />
                                        <Bar
                                            type="monotone"
                                            name={"Công việc đang làm"}
                                            dataKey={"docketStillWorkingPastMonth"}
                                            //label={(data) => new Intl.NumberFormat('en').format(data.value)}
                                            fill={COLOR.yellow}
                                        />
                                        />
                    </ComposedChart >
                                </ResponsiveContainer>
                            </Card>
                        </div>
                    </Col>
                </Row>
                <Card style={{ marginTop: '20px' }}>
                    <Row>{this.props.subOverviewReducer.subOverview.primeJobAdvertise}</Row>
                </Card>

            </Row>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        authReducer: state.authReducer,
        subOverviewReducer: state.subOverviewReducer,
    };
};

const mapDispatchActionToProps = {
    loadSubOverview
};


export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(SubDashboard);