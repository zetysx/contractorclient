import React, { Component } from 'react';
import { connect } from 'react-redux';

import { COLOR } from "../../constants/theme";

import { loadPrimeOverview } from '../../actions/primeOverviewAction';

import NumberCard from '../../components/NumberCard';
import PieChartCustom from '../../components/PieChartCustom';
import LineChartCustom from '../../components/LineChartCustom';
import BarChartCustom from '../../components/BarChartCustom';
import {
    Row,
    Col,
} from 'antd';

const colors = [COLOR.purple, COLOR.red, COLOR.green, COLOR.yellow]

class PrimeDashboard extends Component {

    componentDidMount() {
        this.props.loadPrimeOverview(this.props.authReducer.profile.userId);
    }

    render() {
        var numbers = [
            {
                icon: 'team',
                color: "rgb(24, 144, 255)",
                title: 'Trung bình tiền chi 1 dự án(VNĐ)',
                number: 25800.56,
            },
            {
                icon: 'dollar',
                color: "rgb(24, 144, 255)",
                title: 'Tổng tiền bỏ ra(VNĐ)',
                number: 25800236,
            },
            {
                icon: 'contacts',
                color: "rgb(239, 94, 39)",
                title: 'Dự án đang thi hành',
                number: 15,
            },
            {
                icon: 'contacts',
                color: "rgb(239, 94, 39)",
                title: 'Dự án đã hoàn thành',
                number: 80,
            },
        ];


        if (!this.props.primeOverviewReducer.primeOverview) return null;
        numbers[0].number = this.props.primeOverviewReducer.primeOverview.averageMoneySpentPerContract;
        numbers[1].number = this.props.primeOverviewReducer.primeOverview.totalMoneySpent;
        numbers[2].number = this.props.primeOverviewReducer.primeOverview.contractInProgress;
        numbers[3].number = this.props.primeOverviewReducer.primeOverview.totalContractComplete;

        return (
            <Row gutter={24}  style={{padding:"50px"}}>
                <Row type="flex" justify="space-between" align="top" gutter={24}>
                    <Col lg={12} md={24} gutter={24}>
                        <Row type="flex" justify="space-between" align="top" gutter={24}>
                            <Col lg={12} md={12} style={{ height: "100%" }}>
                                <NumberCard {...numbers[0]} />
                            </Col>
                            <Col lg={12} md={12}>
                                <NumberCard {...numbers[1]} />
                            </Col>
                        </Row>
                        <Row type="flex" justify="space-between" align="bottom" gutter={24} style={{ position: "relative", top: "16px" }}>
                            <Col lg={12} md={12}>
                                <NumberCard {...numbers[2]} />
                            </Col>
                            <Col lg={12} md={12}>
                                <NumberCard {...numbers[3]} />
                            </Col>
                        </Row>
                    </Col>
                    <Col lg={12} md={24}>
                        <PieChartCustom
                            keyData="professionName"
                            value="moneySpent"
                            data={this.props.primeOverviewReducer.primeOverview.totalMoneyPerProfession}
                            colors={colors}
                            title="Tổng chi tiêu từng chuyên môn(VNĐ)"
                        />
                    </Col>
                </Row>


                <br />
                <Row>
                    <Col lg={24} md={24}>
                        <LineChartCustom
                            title="Số dự án thành công từng tháng"
                            data={this.props.primeOverviewReducer.primeOverview.monthlyContractCompletes}
                            xDataKey="monthYear"
                            yDataKey="contractComplete"
                            legends={"Số dự án thành công"}
                        />
                    </Col>
                </Row>

                <br />
                <Row>
                    <Col lg={24} md={24}>
                        <BarChartCustom
                            title="Chi tiêu từng tháng (VNĐ)"
                            data={this.props.primeOverviewReducer.primeOverview.monthlyExpenses}
                            xDataKey="monthYear"
                            yDataKey="expenseOrRevenue"
                            legends={"Chi tiêu hàng tháng"}
                        />
                    </Col>
                </Row>
            </Row>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        authReducer: state.authReducer,
        primeOverviewReducer: state.primeOverviewReducer,
    };
};

const mapDispatchActionToProps = {
    loadPrimeOverview,
};


export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(PrimeDashboard);