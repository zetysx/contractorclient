import React, {Component} from 'react';
import {connect} from 'react-redux';
import moment from 'moment';

import {Progress, Row, Select, Col, Button, Icon, message, Timeline, Steps, Affix, Tooltip, Spin, List} from 'antd';

import {loadAllDocketByContractId} from "../../actions/docketListByContractIdAction";

class ContractDetail extends Component {


    componentDidMount() {
        this.props.loadAllDocketByContractId(this.props.contractId);
    }


    renderDocketDescription = (docket) => {
        return (
            <div>
                <div>
                    {this.renderListHeader()}
                </div>
                <div>
                    {this.renderDocketDetail(docket)}
                </div>
            </div>
        )
    }


    renderDocketDetail = (docket) => {
        return (
            <Row>
                <Col span={3}>{docket.id}</Col>
                <Col span={3}>{docket.docketDetail}</Col>
                <Col span={3}>{docket.date}</Col>
                <Col span={3}>{docket.status}</Col>
            </Row>
        )
    }


    renderListHeader = () => {
        return (
            <Row>
                <Col span={3}>Ngày</Col>
                <Col span={3}>Chi tiết công việc</Col>
                <Col span={3}>Status</Col>
            </Row>
        )
    }


    
    
    render() {
        return (
            <div>
                <List
                    dataSource={this.props.allDocketFromContractId}
                    bordered
                    renderItem={docket => (
                        <List.Item key={docket.id} actions={[<a onClick={this.showDrawer}>View Profile</a>]}>
                        <List.Item.Meta
                            title={<a href="https://ant.design/index-cn">{docket.id}</a>}
                            description={this.renderDocketDescription(docket)}
                        />
                        </List.Item>
                    )}
                />
            </div>
        )
    }

};



function mapStateToProps(state) {

    return {
        isFetchingAllDocketFromContractId: state.docketListByContractIdReducer.isFetchingAllDocketFromContractId,
        isFetching: state.docketListByContractIdReducer.isFetching,
        allDocketFromContractId: state.docketListByContractIdReducer.allDocketWaitFill,
    };
}

export default connect(
    mapStateToProps,
    {}
)(ContractDetail);