import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Button, Modal, Row, Col, List, Icon, Card, Input, Rate, Spin, message, Tooltip} from 'antd';
import {ViewContractPayment} from "../components/ViewContractPayment";
import {loadContractPayment} from "../actions/viewContractPaymentAction";
import {ICON_ANTD_CONSTANTS} from '../constants/workerConstants';
import {
    loadAllContractsPrimeDone,
    loadContractFormData,
    ratingUser
} from "../actions/primeContractHistoryAction";
import {Link} from "react-router-dom";
import {TABLE_LIST_CONFIG} from '../constants/requestContractConstants';
import {ROLESID} from "../constants/role";
import DocketList from '../containers/PrimeContractHistory/DocketList';
import moment from 'moment';

const {TextArea} = Input;

class PrimeContractHistory extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pageIndex: 0,
            searchedCompanyName: "",
            visiblePaymentModal: false,

            // for Rating of ant
            visibleRatingModal: false,
            value: null,
            review: null,
        };
        this.contract = null; // rating modal

        this.buttonLoadMoreStyle = {
            marginTop: 40,
            width: 200
        }
        this.styCol = {
            marginTop: 25
        }
    }


    //START: PAYMENT
    _onClickShowPaymentModal = (contractId) => {
        this.props.loadContractPayment(contractId);
        this.setState({visibleModal: true});

    }

    renderPayment = (record) => {
        const contractId = record.id;
        return (
            <Button
                type="primary"
                shape="circle-outline"
                icon="money-collect"
                size="large"
                onClick={() => this._onClickShowPaymentModal(contractId)}
            />
        );
    }

    renderPaymenModal = () => {
        return (
            <Modal className={'PrimeSearchModal'}
                   title="Bảng giá tiền của dự án hoan thành"
                   visible={this.state.visibleModal}
                   onOk={this._onModalOk}
                   onCancel={this.handleCancel}
            >
                <ViewContractPayment contractPayment={this.props.contractPayment}/>
            </Modal>
        );
    }

    _onModalOk = () => {
        this.setState({visibleModal: false});
    }

    //END: PAYMENT

    componentDidMount() {
        this.props.loadAllContractsPrimeDone(
            this.props.authReducer.profile.userId,
            this.state.searchedCompanyName,
            this.state.pageIndex);
    }

    renderEmptyContractHistory = () => {

        const container = {
            fontSize: "20px",
            textAlign: "center",
            color: "rgb(165, 165, 165)",
        }

        const iconStyle = {fontSize: "80px"}
        const instructionStyle = {
            fontSize: "15px",
            color: "rgb(154, 154, 154)",
            textAlign: "left",
            marginLeft: "31%",
        }

        const resultStyle = {fontWeight: "bold"}

        return (
            <div style={container}>
                <Icon type={ICON_ANTD_CONSTANTS.PROJECT} style={iconStyle}/>
                <br/>
                <div style={resultStyle}>Hiện tại chưa có hợp đồng nào đã hoàn thành</div>
                <br/>
                <div style={instructionStyle}>
                    Hãy tìm kiểm tra mục lời mời của bạn để xem có lời mời nào không, <br/>
                    hoặc kiểm tra các lịch trình của bạn
                </div>

            </div>
        )
    }

    renderHeaderCard = () => {
        const InputSearch = Input.Search;
        return (
            <div>
                <div style={{marginTop: 30, marginBottom: 30}}>
                    <InputSearch
                        onSearch={(value) => this._onSearchSubCompanyName(value)}
                        style={{width: 500}}
                        allowClear
                        addonBefore="Tên công ty của thầu phụ"
                        enterButton
                        onChange={(e) => this._onSearchInputChange(e.target.value)}
                    />
                </div>
                <div>
                    {this.renderListHeader()}
                </div>
            </div>
        )
    }

    _onSearchInputChange = (value) => {
        if(!value || value.length === 0) {
            this.setState({
                searchedCompanyName: value,
                pageIndex: 0
            }, () => {
                this.props.loadAllContractsPrimeDone(
                    this.props.authReducer.profile.userId, 
                    this.state.searchedCompanyName,
                    this.state.pageIndex);
            })
        }
    }

    _onSearchSubCompanyName = (value) => {
        this.setState({
            searchedCompanyName: value,
            pageIndex: 0
        }, () => {
            console.log("STATE: ", this.state);
            this.props.loadAllContractsPrimeDone(
                this.props.authReducer.profile.userId, 
                this.state.searchedCompanyName,
                this.state.pageIndex);
        })
    }

    renderListHeader = () => {
        return (
            <div>
                <Row style={{textAlign: "center", marginBottom: 10}}>
                    <Col span={1} style={this.styCol}>
                        <Tooltip title="Mã hợp đồng. Mỗi hợp đồng có một mã duy nhất.">
                            {TABLE_LIST_CONFIG.CONTRACT_CODE.HEADER} <Icon type="info-circle"/>
                        </Tooltip>
                    </Col>
                    <Col span={3} style={this.styCol}>{TABLE_LIST_CONFIG.SUBCONTRACTOR.HEADER}</Col>
                    <Col span={4} style={this.styCol}>{TABLE_LIST_CONFIG.STARTDATE_ENDDATE.HEADER}</Col>
                    <Col span={3} style={this.styCol}>{TABLE_LIST_CONFIG.COMPLETE_DATE.HEADER}</Col>
                    <Col span={3} style={this.styCol}>{TABLE_LIST_CONFIG.PROFESSION.HEADER}</Col>
                    <Col span={4} style={this.styCol}>{TABLE_LIST_CONFIG.LOCATION.HEADER}</Col>
                    <Col span={4} style={this.styCol}>{TABLE_LIST_CONFIG.ACTION.PAYMENT.HEADER}</Col>
                    <Col span={2} style={this.styCol}>{TABLE_LIST_CONFIG.RATING.HEADER}</Col>
                </Row>
            </div>
        )
    }


    renderContractInfoAsDescrition = (contract) => {
        return (
            <div onClick={() => this._onContractClick(contract)} style={{cursor: "pointer"}}>

                <Row style={{textAlign: "center"}}>
                    <Col span={1} style={this.styCol}>
                        <div style={{
                            padding: "2px 5px",
                            background: "rgb(162, 162, 162)",
                            textAlign: "center",
                            color: "#fff",
                            borderRadius: " 5px",
                            fontWeight: "bold",
                            fontSize: "15px"
                        }}>{contract.id}</div>
                    </Col>
                    <Col span={3} style={this.styCol}>
                        <Link to={`/profile/${contract.subAccountId}`}>
                            {contract.subCompanyName}
                        </Link>
                    </Col>
                    <Col span={4} style={this.styCol}>
                        {moment(contract.startDate).format('DD/MM/YYYY')} - {moment(contract.endDate).format('DD/MM/YYYY')}
                    </Col>
                    <Col span={3} style={this.styCol}>
                        {moment(contract.completeDate).format('DD/MM/YYYY')}
                    </Col>
                    <Col span={3} style={this.styCol}>{contract.profession}</Col>
                    <Col span={4} style={this.styCol}>{contract.location} - {contract.locationDetail}</Col>
                    <Col span={4} style={this.styCol}>
                        {this.renderPaymentButton(contract)}
                    </Col>
                    <Col span={2} style={this.styCol}>
                        {this.renderRatingButton(contract)}
                    </Col>

                </Row>
            </div>
        )
    }

    _onLoadMore = () => {
        let pageIndex = this.state.pageIndex;
        pageIndex += 1;
        this.setState({
            pageIndex: pageIndex
        }, () => {
            this.props.loadAllContractsPrimeDone(
                this.props.authReducer.profile.userId,
                this.state.searchedCompanyName,
                this.state.pageIndex);
        });
    }


    //Payment
    _onClickShowPaymentModal = (contractId) => {
        this.props.loadContractPayment(contractId);
        this.setState({visiblePaymentModal:true});
    }

    renderPaymentButton = (record) => {
        return <Button 
                    type="primary" 
                    shape="circle-outline" 
                    icon="money-collect"
                    style={{marginLeft: 30}} 
                    size="large" 
                    onClick={()=>this._onClickShowPaymentModal(record.id)}
                />
    }

    renderPaymenModal = () => {
        return(
            <Modal className={'PrimeSearchModal'}
                   title="Bảng giá tiền của dự án hoan thành"
                   visible={this.state.visiblePaymentModal}
                   onOk={this._onCloseModal}
                   onCancel={this._onCloseModal}
            >
                <ViewContractPayment contractPayment={this.props.contractPayment} />
            </Modal>
        );
    }


    _onCloseModal = () => {
        this.setState({visiblePaymentModal:false});
    }


    // rating
    renderRatingButton = (contract) => {
        if ((contract)&&(contract.primeRated===true)){
            return(
                <div>
                    Bạn đã đánh giá!
                </div>
            )
        }
        return (
            <div>
                <Button
                    onClick={() => {
                        this._onClickShowRatingModal(contract);
                    }}
                >Đánh giá</Button>
            </div>
        )
    };
    renderRatingModal=()=>{
        const desc = ['Rất tệ', 'Không tốt', 'Bình thường', 'Tốt', 'Tuyệt vời'];
        const {value} = this.state;
        if (this.contract) {return(
            <Modal
                title={"Đánh giá"}
                visible={this.state.visibleRatingModal}
                onOk={() => this._onClickOKRatingModal(this.contract)}
                onCancel={this._onClickCancelRatingModal}
                okText={"Đánh giá"}
                cancelText={"Huỷ"}
            >
                <div style={{
                    color: "#000",
                }}>
                    Mời bạn đánh giá về sự hợp tác lần này với Nhà thầu
                    phụ {this.contract.subCompanyName} trong dự án {this.contract.id}:
                </div>
                <div style={{
                    textAlign: "center",
                    marginBottom: "20px",
                }}>
          <span>
              <Rate tooltips={desc} onChange={this.handleChange} value={value} style={{fontSize: "40px"}}/>
              {((value) && (value < 3)) ?
                  <div style={{margin: "20px 0",}}>Chúng tôi rất tiếc với trải nghiệm này của bạn <Icon
                      type="frown"/><br/>Nhận xét của bạn sẽ giúp những trải nghiệm lần sau được tốt hơn</div> : ''}
              {((value) && (value >= 3)) ?
                  <div style={{margin: "20px 0",}}>Cảm ơn bạn đã đánh giá<br/>Nhận xét của bạn sẽ giúp những trải nghiệm
                      lần sau được tốt hơn</div> : ''}
          </span>
                </div>
                <div style={{
                    marginBottom: "2px",
                    color: "#000",
                }}>
                    Nhận xét của bạn:
                </div>
                <div
                >
                        <TextArea
                            placeholder={"Không bắt buộc"}
                            rows={2}
                            style={{
                                borderRadius: "10px"
                            }}
                            value={this.state.review}
                            onChange={(e) => {
                                this.setState({
                                    review: e.target.value
                                })
                            }}
                        />
                </div>
            </Modal>
        )
        } else {
            return null;
        }
    };

    _onClickShowRatingModal = (contract) => {
        this.contract=contract;
        this.setState({
            visibleRatingModal: true,
            value:null,
            review:null
        });
    };
    handleChange = (value) => {
        this.setState({value});
    };
    _onClickCancelRatingModal = (e) => {// for Modal reason
        this.setState({
            visibleRatingModal: false,
        });
    };
    _onClickOKRatingModal = (contract) => {// for Modal rating
        if ((this.state.value) && ((this.state.value >= 1) && (this.state.value <= 5))) {
            const onSuccessRating = () => {
                // this.props.loadAllContractsPrimeDone(
                //     this.props.authReducer.profile.userId,
                //     this.state.pageIndex);
                this.props.loadAllContractsPrimeDone(this.props.authReducer.profile.userId,
                    this.state.searchedCompanyName,
                    this.state.pageIndex);
                this.setState({
                    visibleRatingModal: false,
                });
            };


            this.props.ratingUser({
                raterUserId: this.props.authReducer.profile.userId,
                ratedUserId: contract.subcontactorId,
                rating: this.state.value,
                review: ((this.state.review === "") ? null : this.state.review),
                roleRater: ROLESID.PRIME,
                roleRated: ROLESID.SUB,
                contractId: contract.id
            }, onSuccessRating);
        } else{
            message.warning("Bạn vẫn chưa đánh giá");
        }
    };
    //


    _onContractClick = (contract) => {
        return (
            <div>
                <DocketList/>
            </div>
        )
    }

    renderSearchNotFound = (notFoundCompanyName) => {
        const containerStyle = {
            fontSize: "20px",
            textAlign: "center",
            color: "rgb(165, 165, 165)",
            padding: 30
        }
        return (
            <div style={{...containerStyle}}>
                <Icon type={ICON_ANTD_CONSTANTS.PROJECT} style={{fontSize: "80px"}}/>
                <br/>
                <div>Không có dự án nào phù hợp với tên 
                <span style={{fontWeight: "bold", fontStyle: "italic", textDecoration: "underline"}}> {notFoundCompanyName}</span>
                </div>
            </div>
        )
    }

    renderContractList = () => {
        if(this.props.primeContractHistoryReducer.primeContractsDone) {
            if(this.props.primeContractHistoryReducer.primeContractsDone.length === 0) {
                if(this.state.searchedCompanyName === "") {
                    return <div style={{padding: 30}}>{this.renderEmptyContractHistory()}</div>
                } else {
                    return <div style={{padding: 30}}>{this.renderSearchNotFound(this.state.searchedCompanyName)}</div>
                }
            }  else {
                return (
                    <List
                        itemLayout="horizontal"
                        dataSource={this.props.primeContractHistoryReducer.primeContractsDone}
                        renderItem={contract => (
                            <List.Item>
                                <List.Item.Meta
                                    description={this.renderContractInfoAsDescrition(contract)}
                                />
                            </List.Item>
                        )}
                    />
                )
            }
        }
    }

    render() {
        
            return (
                <Spin spinning={this.props.primeContractHistoryReducer.isFetching} size="large">
                    <div style={{padding: 50}}>
                        <Row>
                            <Col span={24}>
                                <Card
                                    title={this.renderHeaderCard()}
                                >
                                    {this.renderContractList()}
                                </Card>
                            </Col>
                            <Col span={24} style={{textAlign: "center"}}>
                                <Button
                                    onClick={this._onLoadMore}
                                    style={this.buttonLoadMoreStyle}
                                    size="large"
                                    type="primary"
                                >
                                    Xem thêm
                                </Button>
                            </Col>
                        </Row>
                        {this.renderPaymenModal()}
                        {this.renderRatingModal()}
                    </div>
                </Spin>
            );
        
    }


}

function mapStateToProps(state) {
    return {
        contractPayment: state.viewContractPaymentReducer.contractPayment,
        authReducer: state.authReducer,
        primeContractHistoryReducer: state.primeContractHistoryReducer,
    };
}

const mapDispatchActionToProps = {
    loadAllContractsPrimeDone,
    loadContractPayment,
    loadContractFormData,
    ratingUser,
}

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(PrimeContractHistory);