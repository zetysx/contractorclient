import React, { Component } from 'react';
import { connect } from 'react-redux';
import { 
    Checkbox, 
    Row, 
    Tooltip, 
    Icon 
} from 'antd';
import { 
    loadAllSubcontractorProfession, 
    changeWorkerProfessions 
} from '../../actions/subWorkerManageAction';

class CheckboxProfession extends Component {


    renderCheckBox = () => {
        return this.props.subcontractorProfessions.map((profession, index) => {
            return (
                <Checkbox 
                    key = {profession.professionId}  
                    value = {profession.professionId}
                    style={{float: "left"}}
                    checked={this.props.workerForUpdate.workerProfessions.includes(profession.professionId)}
                    onChange={() => this.props.changeWorkerProfessions(index)}
                    disabled={this.props.workerProfessionsWorkingOn.includes(profession.professionId)}
                >
                    {this.props.workerProfessionsWorkingOn.includes(profession.professionId) ?
                        (
                            <span>
                                <Tooltip title="Công nhân đang làm việc trong dự án về lĩnh vực này">
                                    {profession.professionName} <Icon type="info-circle"/>
                                </Tooltip>
                            </span>
                        ) : (
                            <span>{profession.professionName}</span>
                        )
                    }
                </Checkbox>
            );
        });
    }

    render() {
        return (
            <div>
                <Row style={{marginTop: 15}}>
                    {this.renderCheckBox()}
                </Row>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        authReducer                 : state.authReducer,
        subcontractorProfessions    : state.subWorkerManageReducer.subcontractorProfessions,
        workerProfessionsStatus     : state.subWorkerManageReducer.workerProfessionsStatus,
        workerForUpdate             : state.subWorkerManageReducer.workerUpdate_Object,
        workerProfessionsWorkingOn  : state.subWorkerManageReducer.workerProfessionsWorkingOn
    };
}

const mapDispatchActionToProps = {
    loadAllSubcontractorProfession,
    changeWorkerProfessions
}

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(CheckboxProfession);


