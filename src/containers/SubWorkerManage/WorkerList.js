import React, { Component } from 'react';
import { List, Avatar, Popconfirm, Icon, Row, Col, Button, Tag, message, Spin } from 'antd';
import { connect } from 'react-redux';
import { ICON_ANTD_CONSTANTS } from '../../constants/workerConstants';
import { COLOR } from "../../constants/theme";
import {
    loadWorkerDetailForManagement,
    loadAllSubcontractorProfession,
    disableWorkerBySubcontractor,
    changeUpdateModalVisibility,
    passWorkerToModal,
    loadAllWorkerOfSubcontractor,
    getWorkerProfessionsWorkingOn

} from "../../actions/subWorkerManageAction";
import { TABLE_LIST_CONFIG } from "../../constants/requestContractConstants";
import WorkerUpdate from "./WorkerUpdate";
import { Link } from "react-router-dom";

class WorkerList extends Component {


    constructor(props) {
        super();
        this.state = {
            // drawerConfig: {
            //     visible: false,
            //     placement: "right",
            //     closable: true,
            // }
        };
        this.listConfig = {
            itemLayout: "vertical",
            size: "large",
            pagination: null,
        }
    }


    _onEditClicked = (worker) => {
        worker.accountId = this.props.authReducer.profile.accountId;
        this.props.changeUpdateModalVisibility(true);
        this.props.getWorkerProfessionsWorkingOn(worker.workerId);
        this.props.passWorkerToModal(worker);
    }

    _onDisableConfirmed = (worker) => {
        const onSuccessDisableWorker = () => {
            message.success("Thay đổi trạng thái thành công");
            this.props.loadAllWorkerOfSubcontractor(this.props.authReducer.profile.userId);
        }
    
        const onFailDisableWorker = () => {
            message.warning(`Công nhân đang tham gia ${worker.contractsWorkingOn} dự án, bạn không thể khóa`);
        }

        this.props.disableWorkerBySubcontractor(worker.workerAccountId, onSuccessDisableWorker, onFailDisableWorker);
    }

    componentDidMount() {
        this.props.loadAllSubcontractorProfession(this.props.authReducer.profile.userId);
    }

    renderEmptyWorkerList = () => {

        const container = {
            fontSize: "20px",
            textAlign: "center",
            color: "rgb(165, 165, 165)",
        }

        const iconStyle = { fontSize: "80px" }
        const instructionStyle = {
            fontSize: "15px",
            color: "rgb(154, 154, 154)",
            textAlign: "left",
            marginLeft: "31%",
        }

        const resultStyle = { fontWeight: "bold" }

        return (
            <div style={container}>
                <Icon type={ICON_ANTD_CONSTANTS.USER} style={iconStyle} />
                <br />
                <div style={resultStyle}>Hiện tại chưa có Nhân sự nào</div>
                <br />
                <div style={instructionStyle}>
                    Nhấn vào nút "Thêm nhân sự" để thêm nhân sự vào danh sách quản lý
                </div>

            </div>
        )
    }


    renderActions = (worker) => {
        return (
            <div>
                <div>
                    <Button
                        type="primary"
                        size="default"
                        style={{ width: 130, textAlign: "center" }}
                        icon="edit"
                        onClick={() => this._onEditClicked(worker)}>
                        Cập nhật
                    </Button><br />
                    {worker.enable ? (
                            <Popconfirm
                                title="Bạn có muốn khóa nhân sự này?"
                                okText="Đồng ý"
                                cancelText="Hủy"
                                onConfirm={() => this._onDisableConfirmed(worker)}
                            >
                                <Button
                                    type="danger"
                                    size="default"
                                    style={{ width: 130, marginTop: 5, textAlign: "center" }}
                                    icon="lock"
                                    >
                                        Vô hiệu hóa
                                </Button>
                            </Popconfirm>
                        ) : (
                            <Button
                                type="dashed"
                                size="default"
                                style={{ width: 130, marginTop: 5, textAlign: "center" }}
                                icon="unlock"
                                onClick={() => this._onDisableConfirmed(worker)}>
                                Kích hoạt
                            </Button>
                        )
                    }
                </div>
            </div>
        )
    }

    _onWorkerClick = (worker) => {
        let drawerConfig = this.state.drawerConfig;
        drawerConfig.visible = true;
        this.setState({
            drawerConfig: drawerConfig,
            worker: worker
        });
    }

    // renderDetailModal(worker) {
    //     return (
    //         <Modal
    //             title="Thông tin nhân công"
    //             visible={this.state.modalConfig.visible}
    //             // onOk={this.handleOk}
    //             // onCancel={this.handleCancel}
    //             >
    //             <div style={{display: "inline-block"}}>
    //                 <h4>Thông tin cá nhân</h4>
    //                 <div style={{border: "1px"}}>
    //                     <div style={{float: "left"}}>
    //                         <img src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" style={{width: 100, height: 100}}/>
    //                     </div>
    //                     <div style={{float: "left", marginLeft: 60}}>
    //                         <Row>Tên tài khoản: abc</Row>
    //                         <Row>Họ và tên: Nguyễn Văn Hùng</Row>
    //                         <Row>Số điện thoại: 0192823282</Row>
    //                         <Row>Địa chỉ: 42 Hùng Vương</Row>
    //                         <Row style={{marginTop: 10}}><Button type="primary" icon="edit">Chỉnh sửa</Button></Row>
    //                     </div>
    //                 </div>
    //             </div>
    //             <div style={{marginTop: 30}}>
    //                 <h4>Công việc</h4>
    //                 <Row>Tổng số công việc</Row>
    //                 <Row>Số lần trễ công việc</Row>
    //                 <Row>Số công việc xong/Tổng số công việc hiện tại</Row>
    //                 <Row>Tỉ số hoàn thành công việc</Row>
    //                 <Row>Statistic</Row>
    //             </div>

    //         </Modal>
    //     )
    // }

    _onDrawerClose = () => {
        let state = { ...this.state };
        state.drawerConfig.visible = false;
        state.worker = null;
        this.setState({
            ...state
        });
    }

    renderDrawer = (worker) => {
        if (worker) {
            // this.props.loadWorkerDetailForManagement(this.props.authReducer.profile.userId, worker.workerId);
        }
    }

    // renderDetailDrawer = (worker) => {

    //     const drawerStyle = {
    //         overflow: 'auto',
    //         height: 'calc(100% - 108px)',
    //         paddingBottom: '108px'
    //     }
    //     return (
    //         <Drawer
    //             title="Thông tin nhân sự"
    //             placement={this.state.drawerConfig.placement}
    //             closable={this.state.drawerConfig.closable}
    //             onClose={this._onDrawerClose}
    //             visible={this.state.drawerConfig.visible}
    //             style={drawerStyle}
    //             width={500}
    //             >
    //                 <div style={{display: "inline-block"}}>
    //                     <h4>Thông tin cá nhân</h4>
    //                     <div style={{border: "1px"}}>
    //                         <div style={{float: "left"}}>
    //                             <img 
    //                                 src={worker && worker.avatar ? worker.avatar : "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"} 
    //                                 style={{width: 100, height: 100}}
    //                             />
    //                         </div>
    //                         <div style={{float: "left", marginLeft: 60}}>
    //                             <Row style={{display: "inline"}}>Tên tài khoản: {worker && worker.username ? worker.username : ""}</Row>
    //                             <Row>Họ và tên: {worker && worker.firstName && worker.lastName ? worker.firstName + worker.lastName : ""}</Row>
    //                             <Row>Số điện thoại: {worker && worker.phone ? worker.phone : ""}</Row>
    //                             <Row>Địa chỉ: {worker && worker.address ? worker.address : ""}</Row>
    //                             <Row style={{marginTop: 10}}><Button type="primary" icon="edit">Chỉnh sửa</Button></Row>
    //                         </div>
    //                     </div>
    //                 </div>
    //                 <div style={{marginTop: 30}}>
    //                     <h4>Công việc</h4>
    //                     <Row>Tổng số công việc</Row>
    //                     <Row>Số lần trễ công việc</Row>
    //                     <Row>Số công việc xong/Tổng số công việc hiện tại</Row>
    //                     <Row>Tỉ số hoàn thành công việc</Row>
    //                     <Row>Statistic</Row>
    //                 </div>

    //         </Drawer>
    //     )
    // }


    // renderContractList = (contractList) => {
    //     let contractElement = null;
    //     if(contractList && contractList.length > 0) {
    //         contractElement = contractList.map((contract) => {
    //             return <Row key={contract.contractId}><Icon type="project"/>    
    //                         {contract.contractId} - 
    //                         {contract.primeCompanyName} - 
    //                         {contract.profession}
    //                     </Row>
    //         })

    //     } else {
    //         contractElement = <Row>Chưa có dự án</Row>
    //     } 
    //     return (
    //         <div>
    //             {contractElement}
    //         </div>
    //     )
    // }

    renderStatus = (isEnable, onContract) => {
        let component = null;
        if (isEnable) {
            if (onContract !== 0)
                component = <Tag color={COLOR.blue} style={{ height: 25 }}>Đang trong dự án</Tag>
            else
                component = <Tag color={COLOR.heavyGreen} style={{ height: 25 }}>Không có công việc</Tag>
        } else {
            component = <Tag color={COLOR.red} style={{ height: 25 }}>Đang bị khóa</Tag>
        }
        return <div>{component}</div>
    }

    renderWorkerInformation = (worker, index) => {
        const colGeneralStyle = {
            wordWrap: "break-word"
        }
        return (
            <div>
                <Row>
                    <Col span={1}>
                        <Col span={24} style={{ fontSize: "20px", fontStyle: "bold", ...colGeneralStyle }}>{index}</Col>
                    </Col>
                    <Col span={4}>
                        <Col span={24} 
                            style={{ cursor: "pointer", fontStyle: "italic", color: "#3e72c3", fontSize: "15px", display: "block" }}>
                            <Col span={6}><Avatar src={worker.avatar} size="large" /></Col>
                            <Col span={16} style={{...colGeneralStyle}}><Link to={`/profile/${worker.workerAccountId}`}>{worker.firstName} {worker.lastName}</Link></Col>
                            
                            {/*<Col span={18} onClick={() => this._onWorkerClick(worker)} style={{...colGeneralStyle}}>{worker.firstName} {worker.lastName}</Col>*/}
                        </Col>
                    </Col>
                    <Col span={3}>
                        <Col>{this.renderStatus(worker.enable, worker.contractsWorkingOn)}</Col>
                    </Col>
                    <Col span={4}>
                        <Col style={{ fontSize: "15px", ...colGeneralStyle }}>{worker.email}</Col>
                    </Col>
                    <Col span={3}>
                        <Col style={{ fontSize: "15px", ...colGeneralStyle }}>{worker.phone}</Col>
                    </Col>
                    <Col span={3}>
                        <Col style={{...colGeneralStyle}}>{this.renderProfessionList(worker.workerProfessions)}</Col>
                    </Col>
                    <Col span={3}>
                        <Col style={{ fontSize: "15px", ...colGeneralStyle }}>{worker.address}</Col>
                    </Col>
                    <Col span={3}>
                        <Col style={{textAlign: "center"}}>{this.renderActions(worker)}</Col>
                    </Col>
                </Row>
            </div>
        )
    }

    renderProfessionList = (profList) => {
        let profElements = null;
        if (profList && profList.length > 0) {
            profElements = profList.map((prof) => {
                return <Row key={prof.professionId}>{prof.professionName}</Row>
            })
        } else profElements = <div>Chưa thêm lĩnh vực</div>

        return <div>{profElements}</div>
    }

    renderListHeader = () => {
        return (
            <div>
                <Row style={{ marginBottom: 10, width: "100%" }}>
                    <Col span={1}><h3>#</h3></Col>
                    <Col span={4}><h3 style={{ marginLeft: 10, wordBreak: "break-all" }}>{TABLE_LIST_CONFIG.WORKER.HEADER}</h3></Col>
                    <Col span={3}><h3 style={{wordBreak: "break-all"}}>{TABLE_LIST_CONFIG.ACCOUNT.STATUS.HEADER}</h3></Col>
                    <Col span={4}><h3 style={{wordBreak: "break-all"}}>{TABLE_LIST_CONFIG.ACCOUNT.EMAIL.HEADER}</h3></Col>
                    <Col span={3}><h3 style={{wordBreak: "break-all"}}>{TABLE_LIST_CONFIG.ACCOUNT.PHONE.HEADER}</h3></Col>
                    <Col span={3}><h3 style={{wordBreak: "break-all"}}>{TABLE_LIST_CONFIG.PROFESSION.HEADER}</h3></Col>
                    <Col span={3}><h3 style={{wordBreak: "break-all"}}>{TABLE_LIST_CONFIG.ACCOUNT.ADDRESS.HEADER}</h3></Col>
                    <Col span={3}></Col>
                </Row>
            </div>
        )
    }

    render() {
        // console.log("PROPS WOKRE LIST: ", this.props.subWorkerManageReducer);
        if (this.props.workerListOfSub) {
            return (
                <Spin spinning={this.props.subWorkerManageReducer.isFetching} size="large">
                    <List
                        itemLayout={this.listConfig.itemLayout}
                        size={this.listConfig.size}
                        dataSource={this.props.workerListOfSub}
                        header={this.renderListHeader()}
                        renderItem={(worker, index) => (
                            <List.Item key={worker.workerId}>
                                <List.Item.Meta
                                    description={this.renderWorkerInformation(worker, index + 1)}
                                />
                            </List.Item>
                        )}
                    >
                        {/*this.renderDetailDrawer(this.state.worker)*/}
                        {/*this.renderDrawer(this.props.workerDetails)*/}
                        <WorkerUpdate />
                    </List>
                </Spin>
            )
        } else {
            return (
                <div>
                    {this.renderEmptyWorkerList()}
                </div>
            )
        }
    }
}


function mapStateToProps(state) {
    return {
        authReducer: state.authReducer,
        workerDetails: state.subWorkerManageReducer.workerDetailsForManagement,
        subWorkerManageReducer: state.subWorkerManageReducer
    };
}

const mapDispatchActionToProps = {
    loadWorkerDetailForManagement,
    loadAllSubcontractorProfession,
    disableWorkerBySubcontractor,
    changeUpdateModalVisibility,
    passWorkerToModal,
    loadAllWorkerOfSubcontractor,
    getWorkerProfessionsWorkingOn
}

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(WorkerList);



