import React, { Component } from 'react';
import { TABLE_LIST_CONFIG } from "../../constants/requestContractConstants";
import { Input, Modal, Icon, message, Spin } from "antd";
import { ICON_ANTD_CONSTANTS } from "../../constants/workerConstants";
import { connect } from 'react-redux';
import CheckboxProfession from "./CheckboxProfession";
import {
    loadAllSubcontractorProfession,
    changeUpdateModalVisibility,
    passWorkerToModal,
    updateWorkerInformation,
    loadAllWorkerOfSubcontractor,
    getWorkerProfessionsWorkingOn
} from '../../actions/subWorkerManageAction';
import ImgurUploader from "../../components/ImgurUploader";
import { validateFieldNullOfObject } from "../../helpers/validateHelper";

class WorkerUpdate extends Component {

    constructor(props) {
        super(props);
        this.state = {
            worker: undefined
        }
        this.isSettingState = false
    }

    getProfessionIdFromProfessionList = (profList) => {
        let profIdList = [];
        if(profList && profList.length > 0) {
            for(var pos = 0; pos < profList.length; pos++) {
                profIdList = profIdList.concat(profList[pos].professionId)
            }
        }
        return profIdList;
    }

    static getDerivedStateFromProps(props, state) {
        if(JSON.stringify(props.subWorkerManageReducer.workerUpdate_Object) !== JSON.stringify(state.worker)) {
            return {
                worker: props.subWorkerManageReducer.workerUpdate_Object
            }
        }
        return null;
    }

    _onHandleWorkerFieldChange = (input, value) => {
        const state = { ...this.state };
        state.worker[input] = value;

        this.setState({
            ...state
        })
    }


    

    _onUpdateClick = () => {

        const onHandleSuccessCallbackUpdateWorker = () => {
            message.success("Cập nhật nhân sự thành công");
            this._onCloseModal();
            this.props.loadAllWorkerOfSubcontractor(this.props.authReducer.profile.userId);
        }
    
        const onHandleFailCallbackUpdateWorker = () => {
            message.warning("Cập nhật nhân sự thành công");
            this._onCloseModal();
        }

        let listRequiredFields = {
            email: this.state.worker.email,
            phone: this.state.worker.phone,
            address: this.state.worker.address,
            firstName: this.state.worker.firstName,
            lastName: this.state.worker.lastName,
            workerProfessions: this.state.worker.workerProfessions
        }

        if(!validateFieldNullOfObject(listRequiredFields)) {
            message.warn("Vui lòng không để trống những giá trị có gắn *");
            return;
        }
        
        let workerUpdate = {...this.state.worker}
        workerUpdate.subcontractorId = this.props.authReducer.profile.userId;
        // console.log("workerUpdate: ", workerUpdate);
        this.props.updateWorkerInformation(workerUpdate, onHandleSuccessCallbackUpdateWorker, 
                                            onHandleFailCallbackUpdateWorker);
    }

    _onFinishedUploadingAvatar = (link) => {
        const state = {...this.state};
        link.trim() === "" ? state.worker.avatar = null : state.worker.avatar = link;
        this.setState({
            ...state
        });
    }


    renderWorkerDetail = (worker) => {
        if (!worker) {
            return null;
        }
        return (
            <div>
                <div>
                    <h3 style={{fontStyle: "italic", textAlign: "center"}}>{TABLE_LIST_CONFIG.ACCOUNT.AVATAR.HEADER}</h3>
                    <div style={{textAlign: "center"}}>
                        <div style={{display: "inline-block"}}>
                            <ImgurUploader 
                                imageUrl={worker.avatar}
                                onUploadDone={(link) => { this._onFinishedUploadingAvatar(link) }}
                            />
                        </div>
                    </div>
                </div>
                <div>
                    <h3 style={{fontStyle: "italic"}}>
                        <Icon type={ICON_ANTD_CONSTANTS.PROFILE}/> {TABLE_LIST_CONFIG.ACCOUNT.FIRST_NAME.HEADER} *
                    </h3>
                    <Input
                        name={TABLE_LIST_CONFIG.ACCOUNT.FIRST_NAME.DATAINDEX_KEY}
                        value={worker.firstName}
                        onChange={e => this._onHandleWorkerFieldChange(e.target.name, e.target.value)}
                    />
                </div>
                <div>
                    <h3 style={{fontStyle: "italic"}}>
                        <Icon type={ICON_ANTD_CONSTANTS.PROFILE}/> {TABLE_LIST_CONFIG.ACCOUNT.LAST_NAME.HEADER} *
                    </h3>
                    <Input
                        name={TABLE_LIST_CONFIG.ACCOUNT.LAST_NAME.DATAINDEX_KEY}
                        value={worker.lastName}
                        onChange={e => this._onHandleWorkerFieldChange(e.target.name, e.target.value)}
                    />
                </div>
                <div>
                    <h3 style={{fontStyle: "italic"}}>
                    <Icon type={ICON_ANTD_CONSTANTS.PHONE}/> {TABLE_LIST_CONFIG.ACCOUNT.PHONE.HEADER} *
                    </h3>
                    <Input
                        name={TABLE_LIST_CONFIG.ACCOUNT.PHONE.DATAINDEX_KEY}
                        value={worker.phone}
                        onChange={e => this._onHandleWorkerFieldChange(e.target.name, e.target.value)}
                    />
                </div>
                <div>
                    <h3 style={{fontStyle: "italic"}}>
                        <Icon type={ICON_ANTD_CONSTANTS.ENVIRONMENT}/> {TABLE_LIST_CONFIG.ACCOUNT.ADDRESS.HEADER} *
                    </h3>
                    <Input
                        name={TABLE_LIST_CONFIG.ACCOUNT.ADDRESS.DATAINDEX_KEY}
                        value={worker.address}
                        onChange={e => this._onHandleWorkerFieldChange(e.target.name, e.target.value)}
                    />
                </div>
                <div>
                    <h3 style={{fontStyle: "italic"}}>
                        <Icon type={ICON_ANTD_CONSTANTS.MAIL}/> {TABLE_LIST_CONFIG.ACCOUNT.EMAIL.HEADER} *
                    </h3>
                    <Input
                        name={TABLE_LIST_CONFIG.ACCOUNT.EMAIL.DATAINDEX_KEY}
                        value={worker.email}
                        onChange={e => this._onHandleWorkerFieldChange(e.target.name, e.target.value)}
                    />
                </div>
                <div>
                    <h3 style={{fontStyle: "italic"}}>
                        <Icon type={ICON_ANTD_CONSTANTS.PROFILE}/> {TABLE_LIST_CONFIG.WORKER_PROFESSION.HEADER} *
                    </h3>
                    <CheckboxProfession />
                </div>
            </div>
        )
    }

    _onHandleCancel = () => {
        this._onCloseModal();
    }

    _onCloseModal = () => {

        const resetState = () => {
            if(!this.isSettingState) {
                this.isSettingState = true;
                this.setState(() => {
                    return {worker: {}}
                }, () => {
                    this.isSettingState = false;
                })
            }
        }

        this.props.changeUpdateModalVisibility(false);
        resetState();
    }

    render() {
        return (
            <Spin spinning={this.props.subWorkerManageReducer.isFetching} size="large">
                <Modal
                    title="Thông tin nhân công"
                    okText="Lưu"
                    cancelText="Hủy"
                    onCancel={() => this._onHandleCancel()}
                    onOk={() => this._onUpdateClick()}
                    visible={this.props.subWorkerManageReducer.workerUpdate_modalVisible}
                >
                    {this.renderWorkerDetail(this.state.worker)}
                </Modal>
            </Spin>
            
        )
    }
}


function mapStateToProps(state) {
    return {
        subWorkerManageReducer: state.subWorkerManageReducer,
        authReducer: state.authReducer
    };
}

const mapDispatchActionToProps = {
    loadAllSubcontractorProfession,
    changeUpdateModalVisibility,
    passWorkerToModal,
    updateWorkerInformation,
    loadAllWorkerOfSubcontractor,
    getWorkerProfessionsWorkingOn
}

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(WorkerUpdate);
