import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Checkbox, Row, Spin } from 'antd';

import { 
    loadAllSubcontractorProfession,
} from '../../actions/subWorkerManageAction';

const CheckboxGroup = Checkbox.Group;



class ProfessionCheckBox extends Component {


    componentDidMount() {
        this.props.loadAllSubcontractorProfession(this.props.authReducer.profile.userId);
    }

    renderCheckBox = () => {
        if(!this.props.subWorkerManageReducer ) return;

        if(!this.props.subWorkerManageReducer.subcontractorProfessions || this.props.subWorkerManageReducer.isFetching) {
            return (
                <Spin size="large" />
            )
        } else {
            return this.props.subWorkerManageReducer.subcontractorProfessions.map((profession) => {
                return (
                    <Checkbox 
                        key = {profession.professionId}  
                        value = {profession.professionId}
                        style={{float: "left"}}
                    >
                        {profession.professionName}
                    </Checkbox>
                );
            });
        }
        
    }

    render() {
        return (
            <div>
                <CheckboxGroup onChange={(value) => this.props._onHandleFieldChange(this.props.field, value)}>
                    <Row style={{marginTop: 15}}>
                        {this.renderCheckBox()}
                    </Row>
                </CheckboxGroup>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        subWorkerManageReducer: state.subWorkerManageReducer,
        authReducer: state.authReducer
    };
}

const mapDispatchActionToProps = {
    loadAllSubcontractorProfession
}

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(ProfessionCheckBox);