import React, {Component} from 'react';
import {connect} from 'react-redux';

import {loadAllSubDoneContract, ratingUser} from '../actions/subContractHistoryAction';
import { loadContractPayment } from "../actions/viewContractPaymentAction";
import {TABLE_LIST_CONFIG} from '../constants/requestContractConstants';
import {Button, List, Row, Card, Col, Icon, Input, Spin, Modal, Rate, message, Tooltip} from 'antd';
import {ICON_ANTD_CONSTANTS} from '../constants/workerConstants';
import {Link} from "react-router-dom";
import moment from 'moment';
import {ROLESID} from "../constants/role";
import {ViewContractPayment} from "../components/ViewContractPayment";

const {TextArea} = Input;


class SubContractHistory extends Component {


    constructor(props) {
        super();
        this.state = {
            pageIndex: 0,
            searchedCompanyName: "",
            visiblePaymentModal: false,
            // for Rating of ant
            visibleRatingModal: false,
            value: null,
            review: null,
        };
        this.contract = null; // rating modal

        this.styCol = {
            marginTop: 25,
            wordWrap: "break-word",
            fontSize: "13px"
        };
        this.buttonLoadMoreStyle = {
            marginTop: 40,
            width: 200,
        }
    }

    componentDidMount() {
        this.props.loadAllSubDoneContract(
            this.props.authReducer.profile.userId,
            this.state.searchedCompanyName,
            this.state.pageIndex);
    }


    _onLoadMore = () => {
        let pageIndex = this.state.pageIndex;
        pageIndex += 1;
        this.setState({
            pageIndex: pageIndex
        }, () => {
            // this.forceUpdate();
            this.props.loadAllSubDoneContract(this.props.authReducer.profile.userId,
                this.state.searchedCompanyName,
                this.state.pageIndex);
        });
    }

    renderListHeader = () => {
        return (
            <div>
                <Row style={{textAlign: "center", marginBottom: 10}}>
                    <Col span={1} style={this.styCol}>
                        <Tooltip title="Mã hợp đồng. Mỗi hợp đồng có một mã duy nhất.">
                            {TABLE_LIST_CONFIG.CONTRACT_CODE.HEADER} <Icon type="info-circle"/>
                        </Tooltip>
                    </Col>
                    <Col span={3} style={this.styCol}>{TABLE_LIST_CONFIG.PRIMECONTRACTOR.HEADER}</Col>
                    <Col span={4} style={this.styCol}>{TABLE_LIST_CONFIG.STARTDATE_ENDDATE.HEADER}</Col>
                    <Col span={3} style={this.styCol}>{TABLE_LIST_CONFIG.COMPLETE_DATE.HEADER}</Col>
                    <Col span={3} style={this.styCol}>{TABLE_LIST_CONFIG.PROFESSION.HEADER}</Col>
                    <Col span={4} style={this.styCol}>{TABLE_LIST_CONFIG.LOCATION.HEADER}</Col>
                    <Col span={4} style={this.styCol}>{TABLE_LIST_CONFIG.ACTION.PAYMENT.HEADER}</Col>
                    <Col span={2} style={this.styCol}>{TABLE_LIST_CONFIG.RATING.HEADER}</Col>
                    {/*<Col span={3} style={this.styCol}>{TABLE_LIST_CONFIG.FORM.HEADER}</Col>*/}
                </Row>
            </div>
        )
    }

    _onSearchInputChange = (value) => {
        if(!value || value.length === 0) {
            this.setState({
                searchedCompanyName: value,
                pageIndex: 0
            }, () => {
                this.props.loadAllSubDoneContract(
                    this.props.authReducer.profile.userId, 
                    this.state.searchedCompanyName,
                    this.state.pageIndex);
            })
        }
    }

    renderHeaderCard = () => {
        const InputSearch = Input.Search;
        return (
            <div>
                <div style={{marginTop: 30, marginBottom: 30}}>
                    <InputSearch
                        // placeholder="Tên công ty của thầu chính"
                        onSearch={(value) => this._onSearchPrimeCompanyName(value)}
                        style={{width: 500}}
                        allowClear
                        addonBefore="Tên công ty của thầu chính"
                        enterButton
                        onChange={(e) => this._onSearchInputChange(e.target.value)}
                    />
                </div>
                <div>
                    {this.renderListHeader()}
                </div>
            </div>
        )
    }
    
    _onSearchPrimeCompanyName = (value) => {
        this.setState({
            searchedCompanyName: value,
            pageIndex: 0
        }, () => {
            console.log("STATE: ", this.state);
            this.props.loadAllSubDoneContract(
                this.props.authReducer.profile.userId, 
                this.state.searchedCompanyName,
                this.state.pageIndex);
        })
    }


    renderContractInfoAsDescrition = (contract) => {
        return (
            <div>
                <Row style={{textAlign: "center"}}>
                    <Col span={1} style={this.styCol}><Tooltip title="Mã dự án. Mỗi dự án sẽ có một mã số duy nhất.">
                        <div style={{
                            padding: "2px 5px",
                            background: "rgb(162, 162, 162)",
                            textAlign: "center",
                            color: "#fff",
                            borderRadius: " 5px",
                            fontWeight: "bold",
                            fontSize: "15px"
                        }}>{contract.id}</div>
                    </Tooltip></Col>
                    <Col span={3} style={this.styCol}>
                        <Link to={`/profile/${contract.primeAccountId}`}>
                            {contract.primeCompanyName}
                        </Link>
                    </Col>
                    <Col span={4} style={this.styCol}>
                        {moment(contract.startDate).format('DD/MM/YYYY')} - {moment(contract.endDate).format('DD/MM/YYYY')}
                    </Col>
                    <Col span={3} style={this.styCol}>
                        {moment(contract.completeDate).format('DD/MM/YYYY')}
                    </Col>
                    <Col span={3} style={this.styCol}>{contract.profession}</Col>
                    <Col span={4} style={this.styCol}>{contract.location} {contract.locationDetail}</Col>
                    <Col span={4} style={this.styCol}>{this.renderPaymentButton(contract)}</Col>
                    <Col span={2} style={this.styCol}>
                        {/*
                            <Button
                            style={{marginLeft: 10}}
                            type="primary"
                            icon={ICON_ANTD_CONSTANTS.PROJECT}
                            size="large"
                            onClick={() => this._onShowContractModal(contract.formId)}
                        >
                            Xem
                        </Button>
                        */}

                        {this.renderRating(contract)}
                    </Col>
                </Row>
            </div>
        )
    }

    //Payment
    _onClickShowPaymentModal = (contractId) => {
        this.props.loadContractPayment(contractId);
        this.setState({visiblePaymentModal:true});
    }

    renderPaymentButton = (record) => {
        return <Button 
                    type="primary" 
                    shape="circle-outline" 
                    icon="money-collect"
                    style={{marginLeft: 30}} 
                    size="large" 
                    onClick={()=>this._onClickShowPaymentModal(record.id)}
                />
    }

    renderPaymenModal = () => {
        return(
            <Modal className={'PrimeSearchModal'}
                   title="Bảng giá tiền của dự án hoan thành"
                   visible={this.state.visiblePaymentModal}
                   onOk={this._onCloseModal}
                   onCancel={this._onCloseModal}
            >
                <ViewContractPayment contractPayment={this.props.contractPayment} />
            </Modal>
        );
    }


    _onCloseModal = () => {
        this.setState({visiblePaymentModal:false});
    }

    // rating
    renderRating = (contract) => {
        if ((contract)&&(contract.subRated===true)){
            return(
                <div>
                    Bạn đã đánh giá!
                </div>
            )
        }

        return(
            <div>
                <Button
                    onClick={() => {
                        this._onClickShowRatingModal(contract);
                    }}
                >Đánh giá</Button>
            </div>
        )
    };

    renderRatingModal=()=>{
        const desc = ['Rất tệ', 'Không tốt', 'Bình thường', 'Tốt', 'Tuyệt vời'];
        const {value} = this.state;
        if (this.contract) {return(
          <Modal
              title={"Đánh giá"}
              visible={this.state.visibleRatingModal}
              onOk={() => this._onClickOKRatingModal(this.contract)}
              onCancel={this._onClickCancelRatingModal}
              okText={"Đánh giá"}
              cancelText={"Huỷ"}
          >
              <div style={{
                  color: "#000",
              }}>
                  Mời bạn đánh giá về sự hợp tác lần này với Nhà thầu
                  chính {this.contract.primeCompanyName} trong dự án mã {this.contract.id}:
              </div>
              <div style={{
                  textAlign: "center",
                  marginBottom: "20px",
              }}>
          <span>
              <Rate tooltips={desc} onChange={this.handleChange} value={value} style={{fontSize: "40px"}}/>
              {((value) && (value < 3)) ?
                  <div style={{margin: "20px 0",}}>Chúng tôi rất tiếc với trải nghiệm này của bạn <Icon
                      type="frown"/><br/>Nhận xét của bạn sẽ giúp những trải nghiệm lần sau được tốt hơn</div> : ''}
              {((value) && (value >= 3)) ?
                  <div style={{margin: "20px 0",}}>Cảm ơn bạn đã đánh giá<br/>Nhận xét của bạn sẽ giúp những trải nghiệm
                      lần sau được tốt hơn</div> : ''}
          </span>
              </div>
              <div style={{
                  marginBottom: "2px",
                  color: "#000",
              }}>
                  Nhận xét của bạn:
              </div>
              <div>
                    <TextArea
                        placeholder={"Không bắt buộc"}
                        rows={2}
                        style={{
                            borderRadius: "10px"
                        }}
                        value={this.state.review}
                        onChange={(e) => {
                            this.setState({
                                review: e.target.value
                            })
                        }}
                    />
              </div>
          </Modal>
      )
        } else {
            return null;
        }
    };

    _onClickShowRatingModal = (contract) => {
        this.contract=contract;
        this.setState({
            visibleRatingModal: true,
            value:null,
            review:null
        });
    };
    handleChange = (value) => {
        this.setState({value});
    };
    _onClickCancelRatingModal = (e) => {// for Modal reason
        this.setState({
            visibleRatingModal: false,
        });
    };
    _onClickOKRatingModal = (contract) => {// for Modal rating
        if ((this.state.value) && ((this.state.value >= 1) && (this.state.value <= 5))) {
            const onSuccessRating = () => {
                    // this.props.loadAllSubDoneContract(
                    //     this.props.authReducer.profile.userId,
                    //     this.state.pageIndex);
                    this.props.loadAllSubDoneContract(
                        this.props.authReducer.profile.userId,
                        this.state.searchedCompanyName,
                        this.state.pageIndex
                    );
                this.setState({
                    visibleRatingModal: false,
                });
            };

            this.props.ratingUser({
                raterUserId: this.props.authReducer.profile.userId,
                ratedUserId: contract.primecontractorId,
                rating: this.state.value,
                review: ((this.state.review === "") ? null : this.state.review),
                roleRater: ROLESID.SUB,
                roleRated: ROLESID.PRIME,
                contractId: contract.id
            }, onSuccessRating);
        } else{
            message.warning("Bạn vẫn chưa đánh giá");
        }
    };
    //


    renderEmptyContractHistory = () => {

        const container = {
            fontSize: "20px",
            textAlign: "center",
            color: "rgb(165, 165, 165)",
        }

        const iconStyle = {fontSize: "80px"}
        const instructionStyle = {
            fontSize: "15px",
            color: "rgb(154, 154, 154)",
            textAlign: "left",
            marginLeft: "31%",
        }

        const resultStyle = {fontWeight: "bold"}

        return (
            <div style={container}>
                <Icon type={ICON_ANTD_CONSTANTS.PROJECT} style={iconStyle}/>
                <br/>
                <div style={resultStyle}>Hiện tại chưa có hợp đồng nào đã hoàn thành</div>
                <br/>
                <div style={instructionStyle}>
                    Hãy tìm kiểm tra mục lời mời của bạn để xem có lời mời nào không, <br/>
                    hoặc kiểm tra các lịch trình của bạn
                </div>

            </div>
        )
    }


    renderSearchNotFound = (notFoundCompanyName) => {
        const containerStyle = {
            fontSize: "20px",
            textAlign: "center",
            color: "rgb(165, 165, 165)",
            padding: 30
        }
        return (
            <div style={{...containerStyle}}>
                <Icon type={ICON_ANTD_CONSTANTS.PROJECT} style={{fontSize: "80px"}}/>
                <br/>
                <div>Không có dự án nào phù hợp với tên 
                <span style={{fontWeight: "bold", fontStyle: "italic", textDecoration: "underline"}}> {notFoundCompanyName}</span>
                </div>
            </div>
        )
    }

    renderContractList = () => {
        if(this.props.subContractHistoryReducer.subContractsDone) {
            if(this.props.subContractHistoryReducer.subContractsDone.length === 0) {
                if(this.state.searchedCompanyName === "") {
                    return <div style={{padding: 30}}>{this.renderEmptyContractHistory()}</div>
                } else {
                    return <div style={{padding: 30}}>{this.renderSearchNotFound(this.state.searchedCompanyName)}</div>
                }
            }  else {
                return (
                    <List
                        itemLayout="horizontal"
                        dataSource={this.props.subContractHistoryReducer.subContractsDone}
                        renderItem={contract => (
                            <List.Item>
                                <List.Item.Meta
                                    description={this.renderContractInfoAsDescrition(contract)}
                                />
                            </List.Item>
                        )}
                    />
                )
            }
        }
    }

    render() {
        return (
            <Spin spinning={this.props.subContractHistoryReducer.isFetching} size="large">
                <div style={{padding: 50, borderRadius: 15}}>
                    <Row>
                        <Col span={24}>
                            <Card
                                title={this.renderHeaderCard()}
                                style={{borderRadius: 15}}
                            >
                                {/*{!this.props.subContractHistoryReducer.subContractsDone ||
                                        this.props.subContractHistoryReducer.subContractsDone.length === 0 ? 
                                    (
                                        <div style={{padding: 30}}>{this.renderEmptyContractHistory()}</div>
                                    ) : (
                                        <List
                                            itemLayout="horizontal"
                                            dataSource={this.props.subContractHistoryReducer.subContractsDone}
                                            renderItem={contract => (
                                                <List.Item>
                                                    <List.Item.Meta
                                                        description={this.renderContractInfoAsDescrition(contract)}
                                                    />
                                                </List.Item>
                                            )}
                                        />
                                    )
                                }*/}
                                {this.renderContractList()}
                            </Card>
                        </Col>

                        <Col span={24} style={{textAlign: "center"}}>
                            <Button
                                onClick={this._onLoadMore}
                                style={this.buttonLoadMoreStyle}
                                size="large"
                                type="primary"
                            >
                                Xem thêm
                            </Button>
                        </Col>

                    </Row>
                </div>
                {this.renderRatingModal()}
                {this.renderPaymenModal()}
            </Spin>
        );
    }
}

const mapDispatchActionToProps = {
    loadAllSubDoneContract,
    ratingUser,
    loadContractPayment
}

function mapStateToProps(state) {
    return {
        authReducer: state.authReducer,
        subContractHistoryReducer: state.subContractHistoryReducer,
        contractPayment: state.viewContractPaymentReducer.contractPayment
    };
}

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(SubContractHistory);