
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Row, Col, Button, message } from 'antd';


import { 
    registerNewAccount
} from "../actions/userRegisterAction";

import { 
    validateFieldNullOfObject,
    validateEmail,
    validatePhone,
    validateLength
} from "../helpers/validateHelper";

import GeneralInputField from "../containers/UserRegister/GeneralInputField";

import { CONST_INPUT_FIELDS_NAME } from "../constants/register";
import { ROLESID } from "../constants/role";
import { REGISTER_ERROR_MESSAGE } from "../constants/message";

import {
    loadAllSubcontractorProfession

} from '../actions/subWorkerManageAction';

import ProfessionCheckBox from "../containers/SubWorkerManage/ProfessionCheckBox";



class SubWorkerManageAdd extends Component {

    
    constructor(props) {
        super(props);
        this.state = {
            
            registerData: {
                username    : "",
                password    : "",
                phone       : "",
                address     : "",
                avatar      : null,
                firstName   : null,
                lastName    : null,
                email       : "",
                role        : null,
                workerProfessions   : [],
                subcontractorId     : null
            }
            
        }
        
    }

    componentDidMount() {
        this.props.loadAllSubcontractorProfession(this.props.authReducer.profile.userId);
    }

    _onHandleFieldChange = (input, value) => {
        const state = {...this.state};
        const registerDataTmp = {...state.registerData};
        registerDataTmp[input] = value;
        state.registerData = registerDataTmp;
        this.setState({
            ...state
        })
    }

    validateRequiredRegisterData = (dataObj) => {
        if(dataObj) {
            if(!validateFieldNullOfObject(dataObj)) {
                message.warn(REGISTER_ERROR_MESSAGE.BLANK_REQUIRED_FIELDS);
                return false;
            }
            if(dataObj.hasOwnProperty("email") && dataObj.email) {
                if(!validateEmail(dataObj.email)) {
                    message.warn(REGISTER_ERROR_MESSAGE.EMAIL_FORMAT);
                    return false;
                }
            }
            if(dataObj.hasOwnProperty("phone") && dataObj.phone) {
                if(!validatePhone(dataObj.phone)) {
                    message.warn(REGISTER_ERROR_MESSAGE.PHONE);
                    return false;
                }
            }
            if(dataObj.hasOwnProperty("username") && dataObj.username) {
                if(!validateLength(dataObj.username, 5, 50)) {
                    message.warn("Tên đăng nhập giới hạn từ 5 -> 50 kí tự");
                    return false;
                }
            }
            if(dataObj.hasOwnProperty("password") && dataObj.password) {
                if(!validateLength(dataObj.password, 5, 120)) {
                    message.warn("Mật khẩu giới hạn từ 5 -> 120 kí tự");
                    return false;
                }
            }
            if(dataObj.hasOwnProperty("address") && dataObj.address) {
                if(!validateLength(dataObj.address, 8, 250)) {
                    message.warn("Địa chỉ giới hạn từ 8 -> 250 kí tự");
                    return false;
                }
            }
            if(dataObj.hasOwnProperty("firstName") && dataObj.firstName) {
                if(!validateLength(dataObj.firstName, 1, 60)) {
                    message.warn("Họ giới hạn từ 1 -> 60 kí tự");
                    return false;
                }
            }
            if(dataObj.hasOwnProperty("lastName") && dataObj.lastName) {
                if(!validateLength(dataObj.lastName, 1, 60)) {
                    message.warn("Tên giới hạn từ 1 -> 60 kí tự");
                    return false;
                }
            }
            
        }
        return true;
    }

    _onRegisterClick = () => {
        const workerRegister = {
            ...this.state.registerData,
            role: ROLESID.WORKER,
            subcontractorId: this.props.authReducer.profile.userId
        };

        const onSuccessCallbackRegister = () => {
            message.success("Đăng kí nhân sự thành công");
            this.props.history.push("/worker");
        }

        const onFailDuplicatedCallbackRegister = () => {
            message.error(REGISTER_ERROR_MESSAGE.DUPLICATED_USERNAME);
            return;
        }

        let listRequiredFields = {
            username            : workerRegister.username,
            password            : workerRegister.password,
            email               : workerRegister.email,
            phone               : workerRegister.phone,
            address             : workerRegister.address,
            workerProfessions   : workerRegister.workerProfessions,
            firstName           : workerRegister.firstName,
            listName            : workerRegister.lastName,
            subcontractorId     : workerRegister.subcontractorId,
            role                : workerRegister.role,
        }
            
        if(!this.validateRequiredRegisterData(listRequiredFields)) {
            return;
        }
        this.props.registerNewAccount(workerRegister, onSuccessCallbackRegister, onFailDuplicatedCallbackRegister);
    }
    
    renderRegisterFieldsInput = () => {
        return (
            <div>

                {this.renderGeneralInfo()}
                
                {this.renderProfessionOfSubCheckBox()}

                {this.renderRegisterButton()}

            </div>
        );
    }

    renderRegisterButton = () => {

        const registerButtonStyle = {marginTop: 15}

        return (
            <Button
                type="primary"
                onClick={() => { this._onRegisterClick() }}
                style={registerButtonStyle}
            >Đăng ký</Button>
        )
    }

    renderGeneralInfo = () => {

        const generalFields = [
            CONST_INPUT_FIELDS_NAME.USERNAME,
            CONST_INPUT_FIELDS_NAME.PASSWORD,
            CONST_INPUT_FIELDS_NAME.PHONE,
            CONST_INPUT_FIELDS_NAME.ADDRESS,
            CONST_INPUT_FIELDS_NAME.EMAIL,
            CONST_INPUT_FIELDS_NAME.FIRSTNAME,
            CONST_INPUT_FIELDS_NAME.LASTNAME
        ]

        return (
            <GeneralInputField 
                _onHandleFieldChange={this._onHandleFieldChange}
                listFields={generalFields}
                fieldData={null}
            />
        )
        
    }

    renderProfessionOfSubCheckBox = () => {
        const labelStyle = {
            marginBottom: 15, 
            textAlign: "center"
        }
        return (
            <div>
                <label style={labelStyle}> - Chọn các lĩnh vực  *</label><br/>
                <ProfessionCheckBox 
                    _onHandleFieldChange={this._onHandleFieldChange}
                    field="workerProfessions"
                />    
            </div>
        )
    }


    renderRegisterForm = () => {

        const registerFormStyle = {
            colContainer: { background: "#fff", display: "inline-block", padding: "50px 50px" },
            h1Headline: {textAlign: "center"}
        }

        return (
            <div style={{padding: 30}}>
                <Row>
                    <Col span={10} offset={6} style={registerFormStyle.colContainer}>
                        <h1 style={registerFormStyle.h1Headline}>Mẫu đăng kí nhân sự</h1>
                        {this.renderRegisterFieldsInput()}
                    </Col>
                </Row>
            </div>
        );
    }

    render(){
        return this.renderRegisterForm();
    }
}

function mapStateToProps(state) {
    return {
        authReducer: state.authReducer,
        userRegisterReducer: state.userRegisterReducer,
        subWorkerManageReducer: state.subWorkerManageReducer
    };
}

const mapDispatchActionToProps = {
    registerNewAccount,
    loadAllSubcontractorProfession
}

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(SubWorkerManageAdd);
