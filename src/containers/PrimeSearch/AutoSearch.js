import React, {Component} from 'react';
import {connect} from 'react-redux';
import moment from 'moment';

import {
    Card,
    Col,
    Row,
    Button,
    Icon,
    Modal,
    Spin,
    message,
    Table,
    Drawer,
    Tooltip
} from 'antd';
import SentRequest from "./SentRequest";

import {
    getAutoSearchTitle,
    getAutoSearchContent,
    changeAutoSearchStatus,
    deleteAutoSearch
} from "../../actions/primeSearch/autoSearchAction";
import {loadAllLocations} from "../../actions/locationAction";
import {loadAllProfessions} from "../../actions/professionAction";

import {sendRequest} from "../../actions/primeSearch/sentRequestAction";

const {Meta} = Card;

class AutoSearch extends Component {
    constructor(props) {
        super(props);
        this.autoSearchTitleList = [];
        this.autosearchId = "";
        this.state = {
            expandedRowKeys: [],
            visibleModal: false,
            onSelectRecord: "",
            visible: false
        };
    }

    componentDidMount() {
        this.props.loadAllLocations();
        this.props.loadAllProfessions();
        this.props.getAutoSearchTitle(this.props.authReducer.profile.userId);
    };

    loadAutoSearchDrawer = (record) => {
        const {workerNumber, professionId, startDate, endDate, minPrice, maxPrice, locationId} = record;
        this.searchValue = {
            numOfWorker: workerNumber,
            professionId: professionId,
            fromDate: startDate,
            toDate: endDate,
            minPrice,
            maxPrice,
            locationId: locationId
        };
        this.props.getAutoSearchContent(record.autosearchId);
        this.setState({onSelectRecord: record}, () => {
            this.showDrawer();
        });
    };

    mapDataSentRequest = (price, locationDetail) => {
        this.pricePerDocket = price;
        this.locationDetail = locationDetail.replace(/^\s+|\s+$/g, '');
    };

    renderSearchedSubList() {
        return this.props.autoSearchContent.map((subcontractor, index) => {
            const priceOnCard = parseInt(subcontractor.price / 1000);
            let descriptionOnCard = "";
            if (subcontractor.description.length > 35) {
                descriptionOnCard = subcontractor.description.substring(0, 35);
                descriptionOnCard += "...";
            } else {
                descriptionOnCard = subcontractor.description
            }

            return (
                <Col
                    key={`${subcontractor.subcontractorId}${index}`}
                    xs={24} sm={24} md={12} lg={8} xl={6}
                    style={{marginBottom: '16px'}}
                >
                    <Card
                        actions={
                            [<div onClick={() => this._onClickShowModal(subcontractor)}><Icon type="file-sync"/> Mời hợp tác
                            </div>]
                        }
                        cover={
                            ((subcontractor.avatar) ? <img height={"150px"} alt="avatar" src={subcontractor.avatar}/> :
                                (<Row style={{width: "100%", height: "150px"}} type="flex" justify="space-around"
                                      align="middle">
                                    <Col><Icon style={{
                                        fontSize: "60px",
                                        color: "#d4d4d4",
                                    }} type="user"/></Col>
                                </Row>))
                        }
                        title={`Nhà thầu ${subcontractor.companyName}`} bordered={false}>
                        <Meta style={{textAlign: 'center'}}
                              title={<div>{`${priceOnCard}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}<span
                                  style={{
                                      fontWeight: "normal",
                                      margin: "0 1px",
                                  }}>,</span><span
                                  style={{
                                      fontWeight: "normal",

                                  }}>000</span><sup
                                  style={{
                                      fontWeight: "normal",
                                      fontSize: "10px",
                                      right: "-5px",
                                      color: "#ad8700",
                                  }}>đ</sup></div>}
                              description={descriptionOnCard}
                        />
                    </Card>
                </Col>
            );
        });

    }

    renderSearchResult() {
        if (this.props.autoSearchContent) {
            return (
                <Row gutter={32}>
                    {this.renderSearchedSubList()}
                </Row>
            );
        }
    }

    renderModal() {
        return (

            <Modal className={'PrimeSearchModal'}
                   title="Gửi lời mời hợp tác"
                   visible={this.state.visibleModal}
                   onOk={this.handleOk}
                   onCancel={this.handleCancel}
                   okText={"Gửi lời mời hợp tác"}
                   cancelText={"Thoát"}
                   width={"60%"}

            >
                <Spin spinning={this.props.isFetchingSentRequest} size={"large"}>
                    <SentRequest dataSentRequestFunc={this.mapDataSentRequest} searchValue={this.searchValue}
                                 subcontractorItemSelect={this.subcontractorItemSelect}/>
                </Spin>
            </Modal>
        );
    }

    render() {
        if ((this.props.autoSearchTitle)) {
            this.autoSearchTitleList = this.props.autoSearchTitle;
        }
        const columns = [
            {
                title: '',
                dataIndex: '',
                key: '',
                render: (record) => {
                    if (record.timeFound) {
                        return (
                            <Button onClick={() => {
                                this.loadAutoSearchDrawer(record);
                            }}>
                                Xem
                            </Button>
                        )
                    }
                }

            }, {
                title: <Tooltip
                    title="Mã tìm kiếm tự động. Mỗi một tìm kiếm tự động bạn đăng ký sẽ có một mã riêng để phân biệt với nhau. Mã này được dùng để thông báo với bạn tìm kiếm động mã nào đã được tìm thấy.">
                    Mã <Icon type="question-circle"/>
                </Tooltip>,
                dataIndex: '',
                key: 'autosearchId',
                render: (text, record, index) => {
                    return <Tooltip
                        title="Mã tự động tìm kiếm. Mỗi một tìm kiếm tự động bạn đăng ký sẽ có một mã riêng để phân biệt với nhau. Mã này được dùng để thông báo với bạn tìm kiếm động mã nào đã được tìm thấy.">
                        <div style={{
                            padding: "2px 5px",
                            background: "rgb(162, 162, 162)",
                            textAlign: "center",
                            color: "#fff",
                            borderRadius: " 5px",
                            fontWeight: "bold",
                            fontSize: "15px",
                        }}>{record.autosearchId}</div>
                    </Tooltip>;
                }
            },
            {
                title: 'Ngày',
                dataIndex: '',
                key: '',
                render: (text, record, index) => {
                    return `${record.startDate} - ${record.endDate}`;
                }

            }, {
                title: 'Địa điểm',
                dataIndex: 'location',
                key: 'location',
            },
            {
                title: 'Nhân công',
                dataIndex: 'workerNumber',
                key: 'workerNumber',
            },
            {
                title: 'Lĩnh vực',
                dataIndex: 'profession',
                key: 'profession',
            },
            {
                title: 'Giá',
                dataIndex: '',
                key: '',
                render: (text, record, index) => {
                    return <div>
                        <span>{`${parseInt(record.minPrice / 1000)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            <span
                                style={{
                                    fontWeight: "normal",
                                    margin: "0 1px",
                                }}>,</span><span
                                style={{
                                    fontWeight: "normal",

                                }}>000</span></span> - <span>{`${parseInt(record.maxPrice / 1000)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        <span
                            style={{
                                fontWeight: "normal",
                                margin: "0 1px",
                            }}>,</span><span
                            style={{
                                fontWeight: "normal",

                            }}>000</span><sup
                            style={{
                                fontWeight: "normal",
                                right: "-5px",
                                color: "#ad8700",
                            }}>đ</sup></span>

                    </div>;
                }
            },
            {
                title: <div style={{color: "#fafafa",}}>Tình trạng</div>,
                dataIndex: '',
                key: '',
                render: (text, record, index) => {
                    if (record.found === true) {

                        if (record.timeFound) {
                            return (
                                <Row type="flex" justify="space-around" align="middle"
                                     style={{
                                         margin: "-16px",
                                         textAlign: "center",
                                         height: "98px",
                                         minWidth: "25vw",
                                     }}>
                                    <Col span={15}>
                                        <div><Icon style={{fontSize: "20px", color: "#25af14"}} type="check-circle"
                                                   theme="filled"/>
                                            <div style={{
                                                fontSize: "13px",
                                                display: "inline-block",
                                                position: "relative",
                                                top: "-3px",
                                                right: "-5px",
                                                color: "rgb(16, 150, 0)"
                                            }}>Đã có kết quả
                                            </div>
                                            <div style={{
                                                fontSize: "10px", color: "#aaa",
                                                overflow: "hidden",
                                                whiteSpace: "pre-wrap",
                                            }}>Tìm thấy
                                                lúc:<br/>{moment(record.timeFound).format('DD/MM/YYYY, h:mm:ss A')}
                                            </div>
                                        </div>
                                    </Col>
                                    <Col span={6} style={{height: "98px", padding: "10px 10px 10px 0",}}>
                                        <Button onClick={() => {
                                            this.props.changeAutoSearchStatus(record.autosearchId, () => {
                                                this.props.getAutoSearchTitle(this.props.authReducer.profile.userId);
                                            });
                                        }} type={"primary"}
                                                style={{
                                                    width: "100%",
                                                    height: "100%"
                                                }}>Tìm<br/>lại</Button>
                                    </Col>
                                    <Col span={3} style={{height: "98px", padding: "10px 10px 10px 0",}}>
                                        <Button onClick={() => {
                                            this.props.deleteAutoSearch(record.autosearchId, () => {
                                                this.props.getAutoSearchTitle(this.props.authReducer.profile.userId);
                                            });
                                        }} type={"danger"}
                                                style={{
                                                    width: "100%",
                                                    height: "100%",
                                                    padding: "0",
                                                    fontSize: "12px",
                                                }}>Xoá</Button>
                                    </Col>
                                </Row>
                            );
                        } else {
                            return <Row type="flex" justify="space-around" align="middle"
                                        style={{
                                            margin: "-16px",
                                            textAlign: "center",
                                            height: "98px",
                                            minWidth: "25vw",
                                        }}>
                                <Col span={15}>
                                    <div><Icon style={{fontSize: "20px", color: "red"}} type="exclamation-circle"
                                               theme="filled"/>
                                        <div style={{
                                            fontSize: "13px",
                                            display: "inline-block",
                                            position: "relative",
                                            top: "-3px",
                                            right: "-5px",
                                            color: "red"
                                        }}>Chưa có kết quả
                                        </div>
                                        <div style={{
                                            fontSize: "10px",
                                            overflow: "hidden",
                                            whiteSpace: "pre-wrap",
                                        }}>Tìm kiếm đã dừng lại,<br/>bạn có muốn tiếp tục?
                                        </div>
                                    </div>
                                </Col>
                                <Col span={6} style={{height: "98px", padding: "10px 10px 10px 0",}}>
                                    <Button onClick={() => {
                                        this.props.changeAutoSearchStatus(record.autosearchId, () => {
                                            this.props.getAutoSearchTitle(this.props.authReducer.profile.userId);
                                        });
                                    }} type={"primary"}
                                            style={{
                                                width: "100%",
                                                height: "100%"
                                            }}>Tiếp<br/>tục</Button>
                                </Col>
                                <Col span={3} style={{height: "98px", padding: "10px 10px 10px 0",}}>
                                    <Button onClick={() => {
                                        this.props.deleteAutoSearch(record.autosearchId, () => {
                                            this.props.getAutoSearchTitle(this.props.authReducer.profile.userId);
                                        });
                                    }} type={"danger"}
                                            style={{
                                                width: "100%",
                                                height: "100%",
                                                padding: "0",
                                                fontSize: "12px",
                                            }}>Xoá</Button>
                                </Col>
                            </Row>

                        }
                    } else {
                        if (record.timeFound) {
                            return (
                                <Row
                                    type="flex" justify="space-around" align="middle"
                                    style={{
                                        margin: "-16px",
                                        textAlign: "center",
                                        height: "98px",
                                        minWidth: "25vw",
                                    }}>
                                    <Col span={15}>
                                        <div><Icon style={{fontSize: "20px", color: "rgb(206, 142, 0)"}} type="redo"
                                                   spin/>
                                            <div style={{
                                                fontSize: "13px",
                                                display: "inline-block",
                                                position: "relative",
                                                top: "-3px",
                                                right: "-5px",
                                                color: "rgb(206, 142, 0)"
                                            }}>Đang được tìm lại
                                            </div>
                                            <div style={{fontSize: "10px",}}>Bạn vẫn có thể xem kết quả cũ,<br/>trong
                                                lúc chờ tìm kết quả mới
                                            </div>
                                        </div>
                                    </Col>
                                    <Col span={6} style={{height: "98px", padding: "10px 10px 10px 0",}}>
                                        <Button onClick={() => {
                                            this.props.changeAutoSearchStatus(record.autosearchId, () => {
                                                this.props.getAutoSearchTitle(this.props.authReducer.profile.userId);
                                            });
                                        }} type={"danger"}
                                                style={{
                                                    width: "100%",
                                                    height: "100%"
                                                }}>Dừng<br/>lại</Button>
                                    </Col>
                                    <Col span={3} style={{height: "98px", padding: "10px 10px 10px 0",}}>
                                        <Button onClick={() => {
                                            this.props.deleteAutoSearch(record.autosearchId, () => {
                                                this.props.getAutoSearchTitle(this.props.authReducer.profile.userId);
                                            });
                                        }} type={"danger"}
                                                style={{
                                                    width: "100%",
                                                    height: "100%",
                                                    padding: "0",
                                                    fontSize: "12px",
                                                }}>Xoá</Button>
                                    </Col>
                                </Row>
                            )
                        } else {
                            return (
                                <Row
                                    type="flex" justify="space-around" align="middle"
                                    style={{
                                        margin: "-16px",
                                        textAlign: "center",
                                        height: "98px",
                                        minWidth: "25vw",
                                    }}>
                                    <Col span={15}>
                                        <div><Icon style={{fontSize: "20px", color: "#0074e0"}} type="setting" spin/>
                                            <div style={{
                                                fontSize: "13px",
                                                display: "inline-block",
                                                position: "relative",
                                                top: "-3px",
                                                right: "-5px",
                                                color: "#0074e0"
                                            }}>Đang tìm kiếm
                                            </div>
                                            <div style={{fontSize: "12px", color: "red"}}>Chưa có kết quả</div>
                                        </div>
                                    </Col>
                                    <Col span={6} style={{height: "98px", padding: "10px 10px 10px 0",}}>
                                        <Button onClick={() => {
                                            this.props.changeAutoSearchStatus(record.autosearchId, () => {
                                                this.props.getAutoSearchTitle(this.props.authReducer.profile.userId);
                                            });
                                        }} type={"danger"}
                                                style={{
                                                    width: "100%",
                                                    height: "100%"
                                                }}>Dừng<br/>tìm</Button>
                                    </Col>
                                    <Col span={3} style={{height: "98px", padding: "10px 10px 10px 0",}}>
                                        <Button onClick={() => {
                                            this.props.deleteAutoSearch(record.autosearchId, () => {
                                                this.props.getAutoSearchTitle(this.props.authReducer.profile.userId);
                                            });
                                        }} type={"danger"}
                                                style={{
                                                    width: "100%",
                                                    height: "100%",
                                                    padding: "0",
                                                    fontSize: "12px",
                                                }}>Xoá</Button>
                                    </Col>
                                </Row>
                            )
                        }
                    }
                }
            }
        ];
        const columnsAllForContent = [
            {
                title: <Tooltip
                    title="Mã tìm kiếm tự động. Mỗi một tìm kiếm tự động bạn đăng ký sẽ có một mã riêng để phân biệt với nhau. Mã này được dùng để thông báo với bạn tìm kiếm động mã nào đã được tìm thấy.">
                    Mã <Icon type="question-circle"/>
                </Tooltip>,
                dataIndex: '',
                key: 'autosearchId',
                render: (text, record, index) => {
                    return <Tooltip
                        title="Mã tự động tìm kiếm. Mỗi một tìm kiếm tự động bạn đăng ký sẽ có một mã riêng để phân biệt với nhau. Mã này được dùng để thông báo với bạn tìm kiếm động mã nào đã được tìm thấy.">
                        <div style={{
                            padding: "2px 5px",
                            background: "rgb(162, 162, 162)",
                            textAlign: "center",
                            color: "#fff",
                            borderRadius: " 5px",
                            fontWeight: "bold",
                            fontSize: "15px",
                        }}>{record.autosearchId}</div>
                    </Tooltip>;
                }
            },
            {
                title: 'Ngày',
                dataIndex: '',
                key: 'startDate',
                render: (text, record, index) => {
                    return `${record.startDate} - ${record.endDate}`;
                }

            }, {
                title: 'Địa điểm',
                dataIndex: 'location',
                key: 'location',
            },
            {
                title: 'Nhân công',
                dataIndex: 'workerNumber',
                key: 'workerNumber',
            },
            {
                title: 'Lĩnh vực',
                dataIndex: 'profession',
                key: 'profession',
            },
            {
                title: 'Giá',
                dataIndex: '',
                key: 'minPrice',
                render: (text, record, index) => {
                    return <div>
                        <span>{`${parseInt(record.minPrice / 1000)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            <span
                                style={{
                                    fontWeight: "normal",
                                    margin: "0 1px",
                                }}>,</span><span
                                style={{
                                    fontWeight: "normal",

                                }}>000</span></span> - <span>{`${parseInt(record.maxPrice / 1000)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        <span
                            style={{
                                fontWeight: "normal",
                                margin: "0 1px",
                            }}>,</span><span
                            style={{
                                fontWeight: "normal",

                            }}>000</span><sup
                            style={{
                                fontWeight: "normal",
                                right: "-5px",
                                color: "#ad8700",
                            }}>đ</sup></span>

                    </div>
                }
            },
            {
                title: <div style={{color: "#fafafa",}}>Tình trạng</div>,
                dataIndex: '',
                key: 'found',
                render: (text, record, index) => {
                    if (record.found === true) {
                        if (record.timeFound) {
                            return (
                                <Row type="flex" justify="space-around" align="middle"
                                     style={{
                                         margin: "-10px",
                                         textAlign: "center",
                                         height: "98px",
                                         width: "200px",
                                     }}>
                                    <Col span={14}>
                                        <div>
                                            <Icon
                                                style={{fontSize: "20px", color: "#25af14"}}
                                                type="check-circle"
                                                theme="filled"/>
                                            <div
                                                style={{
                                                    fontSize: "10px",
                                                    color: "rgb(16, 150, 0)",
                                                }}>
                                                Đã có kết quả
                                            </div>
                                        </div>
                                    </Col>
                                    <Col span={10} style={{height: "98px", padding: "10px 0",}}>
                                        <Button onClick={() => {
                                            this.props.changeAutoSearchStatus(record.autosearchId, () => {
                                                this.props.getAutoSearchTitle(this.props.authReducer.profile.userId,
                                                    () => {
                                                        if (this.autoSearchTitleList[0])
                                                            for (let i = 0; i < this.autoSearchTitleList.length; i++) {
                                                                if (this.autoSearchTitleList[i].autosearchId === this.state.onSelectRecord.autosearchId) {
                                                                    if (this.state.onSelectRecord !== "") {
                                                                        this.onClose();
                                                                        this.loadAutoSearchDrawer(this.autoSearchTitleList[i]);
                                                                        return;
                                                                    }
                                                                }
                                                            }
                                                    });
                                            });
                                        }}
                                                type={"primary"}
                                                style={{
                                                    width: "100%",
                                                    height: "100%",
                                                    fontSize: "12px"
                                                }}>Tìm<br/>lại</Button>
                                    </Col>
                                </Row>
                            );
                        }
                    } else {
                        if (record.timeFound) {
                            return (
                                <Row
                                    type="flex" justify="space-around" align="middle"
                                    style={{
                                        margin: "-10px",
                                        textAlign: "center",
                                        height: "98px",
                                        width: "200px",
                                    }}>
                                    <Col span={14}>
                                        <div><Icon style={{fontSize: "20px", color: "rgb(206, 142, 0)"}} type="redo"
                                                   spin/>
                                            <div style={{
                                                fontSize: "10px",
                                                color: "rgb(206, 142, 0)",
                                            }}>Đang được tìm lại
                                            </div>
                                        </div>
                                    </Col>
                                    <Col span={10} style={{height: "98px", padding: "10px 0",}}>
                                        <Button onClick={() => {
                                            this.props.changeAutoSearchStatus(record.autosearchId, () => {
                                                this.props.getAutoSearchTitle(this.props.authReducer.profile.userId,
                                                    () => {
                                                        if (this.autoSearchTitleList[0])
                                                            for (let i = 0; i < this.autoSearchTitleList.length; i++) {
                                                                if (this.autoSearchTitleList[i].autosearchId === this.state.onSelectRecord.autosearchId) {
                                                                    if (this.state.onSelectRecord !== "") {
                                                                        this.onClose();
                                                                        this.loadAutoSearchDrawer(this.autoSearchTitleList[i]);
                                                                        return;
                                                                    }
                                                                }
                                                            }
                                                    });
                                            });
                                        }} type={"danger"}
                                                style={{
                                                    width: "100%",
                                                    height: "100%",
                                                    fontSize: "12px"
                                                }}>Dừng<br/>lại</Button>
                                    </Col>
                                </Row>
                            )
                        }
                    }
                }
            }
        ];
        return (
            <div className={"autoSearch"} style={{padding: "50px"}}>
                <Spin spinning={this.props.isFetching} size={"large"}>
                    <Table
                        style={{background: "#fff"}}
                        bodyStyle={{fontSize: "13px"}}
                        pagination={false}
                        columns={columns} dataSource={this.autoSearchTitleList}
                        rowKey={record => record.autosearchId}
                    />
                </Spin>


                <Drawer
                    title="Kết quả"
                    placement="right"
                    closable={true}
                    onClose={this.onClose}
                    visible={this.state.visible}
                    width={"73%"}
                    bodyStyle={{background: "#f0f2f5"}}
                >
                    <Spin spinning={this.props.isFetching} size={"large"}>
                        <Table pagination={false}
                               style={{background: "#fff"}}
                               columns={columnsAllForContent} dataSource={[this.state.onSelectRecord]}
                               rowKey={record => `${record.autosearchId}content`}
                        />
                        <br/>
                        <br/>
                        {((this.state.onSelectRecord.found === false) ?
                            <div style={{
                                color: "#fff",
                                fontSize: "15px",
                                background: "#bf6c6c",
                                padding: "10px 10px",
                                borderRadius: "20px",
                                width: "60%",
                                textAlign: "center",
                                position: "relative",
                                left: "20%",
                            }}>Lưu ý: đây là kết quả cũ, hệ thống vẫn đang tìm kiếm kết quả mới</div> :
                            <div style={{
                                color: "#fff",
                                fontSize: "15px",
                                background: "rgb(100, 183, 90)",
                                padding: "10px 10px",
                                borderRadius: "20px",
                                width: "30%",
                                textAlign: "center",
                                position: "relative",
                                left: "35%",
                            }}>Nhà thầu phụ thoả mãn yêu cầu:</div>)}
                        <br/>
                        {this.renderSearchResult()}
                    </Spin>
                </Drawer>
                {this.renderModal()}
            </div>
        );
    }


    _onClickShowModal = (subcontractorItemSelect) => {
        this.subcontractorItemSelect = subcontractorItemSelect;

        /*dùng cho trường hợp khi cả 2 đều ko đổi onchange trong sentRequest Modal*/
        this.pricePerDocket = parseInt(subcontractorItemSelect.price / 1000) * 1000;
        this.locationDetail = "";

        this.setState({
            visibleModal: true,
        });
    };

    handleOk = (e) => {
        if (this.locationDetail === "") {
            message.warning("Địa điểm chưa rõ ràng, vui lòng điền chính xác địa chỉ");
            return;
        }
        const data = {
            "id": null,
            "primecontructorId": this.props.authReducer.profile.userId,
            "subcontructorId": this.subcontractorItemSelect.subcontractorId,
            "startDate": this.searchValue.fromDate,
            "endDate": this.searchValue.toDate,
            "locationId": this.searchValue.locationId,
            "workerNumber": this.searchValue.numOfWorker,
            "pricePerDocket": this.pricePerDocket,
            "locationDetail": this.locationDetail,
            "professionId": this.searchValue.professionId,
            "status": null,
            "confirmDate": null,
            "formId": null,
            "priceFrom": this.searchValue.minPrice,
            "priceTo": this.searchValue.maxPrice
        };
        this.props.sendRequest(data, () => {
            this.setState({
                visibleModal: false,
            });
        });
    };

    handleCancel = (e) => {
        this.setState({
            visibleModal: false,
        });
    };


    showDrawer = () => {
        this.setState({
            visible: true,
        });
    };
    onClose = () => {
        this.setState({
            visible: false,
        });
    };
}

function mapStateToProps(state) {
    return {
        isFetching: state.manualSearchReducer.isFetching,
        autoSearchContent: state.autoSearchReducer.autoSearchContent,
        autoSearchTitle: state.autoSearchReducer.autoSearchTitle,
        isFetchingSentRequest: state.sentRequestReducer.isFetching,
        authReducer: state.authReducer,
    };
}

export default connect(
    mapStateToProps,
    {
        changeAutoSearchStatus,
        getAutoSearchTitle,
        getAutoSearchContent,
        sendRequest,
        deleteAutoSearch,
        loadAllLocations,
        loadAllProfessions
    }
)(AutoSearch);