import React, {Component} from 'react';
import {connect} from 'react-redux';

import {
    Col,
    Row,
    List,
} from 'antd';

class AutoSearchInfoModal extends Component {
    componentDidMount() {

    }

    takeLocationName = (locationId) => {

        for (let i = 0; i < this.props.locationReducer.length; i++) {
            if (this.props.locationReducer[i].locationId === locationId) {
                return `${this.props.locationReducer[i].city} - ${this.props.locationReducer[i].district}`
            }
        }
    };

    takeProfessionName = (professionId) => {

        for (let i = 0; i < this.props.professionReducer.length; i++) {
            if (this.props.professionReducer[i].professionId === professionId) {
                return `${this.props.professionReducer[i].professionName}`
            }
        }
    };

    render() {
        const searchValue = this.props.searchValue;
        return (
            <div>
                <Row><p style={{fontWeight: "bold",}}>Các điều kiện tự động tìm:</p></Row>
                <Row>
                    <Col span={8}>
                        <List>
                            <List.Item>Lĩnh vực:</List.Item>
                            <List.Item> Thời gian:</List.Item>
                            <List.Item>Địa điểm:</List.Item>
                            <List.Item>Số nhân công:</List.Item>
                            <List.Item>Giá ngày công:</List.Item>
                        </List>
                    </Col>
                    <Col span={16}>
                        <List>
                            <List.Item>{this.takeProfessionName(searchValue.professionId)}</List.Item>
                            <List.Item>{searchValue.fromDate} - {searchValue.toDate}</List.Item>
                            <List.Item>{this.takeLocationName(searchValue.locationId)}</List.Item>
                            <List.Item>{searchValue.numOfWorker}</List.Item>
                            <List.Item>
                                <div>
                                    <span>
                                        {`${parseInt(searchValue.minPrice / 1000)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        <span
                                            style={{
                                                fontWeight: "normal",
                                                margin: "0 1px",
                                            }}>,</span><span
                                        style={{
                                            fontWeight: "normal",

                                        }}>000</span></span> - <span>{`${parseInt(searchValue.maxPrice / 1000)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                    <span
                                        style={{
                                            fontWeight: "normal",
                                            margin: "0 1px",
                                        }}>,</span><span
                                        style={{
                                            fontWeight: "normal",

                                        }}>000</span><sup
                                        style={{
                                            fontWeight: "normal",
                                            right: "-5px",
                                            color: "#ad8700",
                                        }}>đ</sup></span>

                                </div>
                            </List.Item>
                        </List>
                    </Col>
                </Row>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        locationReducer: state.locationReducer.locations,
        professionReducer: state.professionReducer.professions
    };
}

export default connect(
    mapStateToProps,
    {}
)(AutoSearchInfoModal);