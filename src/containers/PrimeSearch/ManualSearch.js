import React, {Component} from 'react';
import {connect} from 'react-redux';
import moment from 'moment';

import {
    Pagination,
    Card,
    Col,
    Row,
    Slider,
    Select,
    DatePicker,
    Input,
    Button,
    Icon,
    Modal,
    InputNumber,
    Spin,
    message,
    Tooltip
} from 'antd';
import ProfessionPicker from '../../components/ProfessionPicker';
import LocationPicker from '../../components/LocationPicker';
import SentRequest from "./SentRequest";
import AutoSearchInfoModal from "./AutoSearchInfoModal";

import {searchSubcontractor, autoSearchSubcontractor} from "../../actions/primeSearch/primeSearchSubcontractorAction"

import {sendRequest} from "../../actions/primeSearch/sentRequestAction"

import {Link} from "react-router-dom";

const {Meta} = Card;
const {Option, OptGroup} = Select;
const RangePicker = DatePicker.RangePicker;

class ManualSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            companyName: "",
            numOfWorker: 3,
            professionId: 1,
            fromDate: moment(new Date()).add(1, 'days').format("DD/MM/YYYY"),
            toDate: moment(new Date()).add(1, 'days').format("DD/MM/YYYY"),
            minPrice: '50000',
            maxPrice: '2000000',
            locationId: 1,

            fromDateValue: moment(new Date()).add(1, 'days'),
            toDateValue: moment(new Date()).add(1, 'days'),

            visibleModal: false,
            visibleModalAutoSearch: false,
            isSearchClick: false,
            sortBy: "price",
            sortOrder: "asc",
            pageIndex: "0"
        };

        this.widthHeightSubCoverCard = 225;
    }

    componentDidMount() {
        this.styleCss();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.styleCss();
    }


    mapDataSentRequest = (price, locationDetail) => {
        this.pricePerDocket = price;
        this.locationDetail = locationDetail.replace(/^\s+|\s+$/g, '');
    };

    renderSearchForm = () => {
        const styleBatBuoc = {
            position: "relative",
            textAlign: "right",
            width: "100%",
            height: "0",
            top: "-6px",
            left: "-2px",
            color: "rgb(255, 184, 184)",
            zIndex: "1",
            fontSize: "20px",
        };
        return (
            <Row
                style={{
                    marginBottom: "50px",
                    background: "#fff",
                    padding: 24,
                    borderRadius: "10px",
                    border: "1px solid rgba(0,0,0,.1)",
                }}
                gutter={16}
            >
                <Col span={21}>
                    <Row gutter={16}>
                        <Col span={12}>
                            <Input
                                placeholder="Tên Nhà thầu phụ (không bắt buộc)"
                                value={this.state.companyName}
                                onChange={this._onChangeCompanyName}
                            />
                        </Col>
                        <Col span={12}>
                            <Tooltip title={"Bắt buộc điền"}>
                                <div style={styleBatBuoc}>*</div>
                            </Tooltip>
                            <RangePicker
                                style={{width: "100%"}}
                                placeholder={["Từ ngày", "Đến ngày"]}
                                format={"DD/MM/YYYY"}
                                onChange={this._onChangeRangePickerValue}
                                value={[this.state.fromDateValue, this.state.toDateValue]}
                                defaultPickerValue={[this.state.fromDateValue, this.state.toDateValue]}
                                defaultValue={[this.state.fromDateValue, this.state.toDateValue]}
                                 disabledDate={disabledDate}
                                disabledTime={disabledRangeTime}
                            />
                        </Col>
                    </Row>
                    <Row type="flex" align="bottom" gutter={16}>
                        <Col span={12}>
                            <Row style={{fontSize: "10px", margin: "5px 0 0 20px"}}>
                                Giá ngày công:
                            </Row>
                            <Row gutter={8}>
                                <Col span={6}>
                                    <InputNumber
                                        min={50}
                                        step={100}
                                        max={2000}
                                        style={{width: "44px"}}
                                        placeholder="Từ"
                                        value={`${parseInt(this.state.minPrice / 1000)}`}
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '')}
                                        onChange={this._onChangeMinPrice}
                                    />
                                    <span
                                        style={{
                                            fontWeight: "normal",
                                            margin: "0 1px",
                                        }}>,</span><span
                                    style={{
                                        fontWeight: "normal",

                                    }}>000</span><sup
                                    style={{
                                        fontWeight: "normal",
                                        right: "-5px",
                                        color: "#ad8700",
                                    }}>đ</sup>
                                </Col>
                                <Col span={12}>
                                    <Slider
                                        style={{marginBottom: "0"}}
                                        tipFormatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',') + " đ"}
                                        range min={50000} max={2000000} step={50000}
                                        onChange={this._onChangeMinMaxPrice}
                                        value={[parseInt(this.state.minPrice), parseInt(this.state.maxPrice)]}
                                    />
                                </Col>
                                <Col span={6}>
                                    <div style={styleBatBuoc}>*</div>
                                    <InputNumber
                                        min={50}
                                        step={100}
                                        max={2000}
                                        style={{width: "44px"}}
                                        placeholder="Đến"
                                        value={`${parseInt(this.state.maxPrice / 1000)}`}
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '')}
                                        onChange={this._onChangeMaxPrice}
                                    />
                                    <span
                                        style={{
                                            fontWeight: "normal",
                                            margin: "0 1px",
                                        }}>,</span><span
                                    style={{
                                        fontWeight: "normal",

                                    }}>000</span><sup
                                    style={{
                                        fontWeight: "normal",
                                        right: "-5px",
                                        color: "#ad8700",
                                    }}>đ</sup>
                                </Col>
                            </Row>
                        </Col>
                        <Col span={12}>
                            <div style={styleBatBuoc}>*</div>
                            <LocationPicker
                                style={{width: '100%'}}
                                placeholder={"Địa điểm"}
                                value={this.state.locationId}
                                onChange={this._onChangeLocationId}
                            />
                        </Col>
                    </Row>
                    <Row gutter={16} type="flex" align="bottom">
                        <Col span={12}>
                            <Row style={{fontSize: "10px", margin: "5px 0 0 20px"}}>
                                Số nhân công:
                            </Row>
                            <Row gutter={8}>
                                <Col span={19}>
                                    <Slider
                                        style={{marginBottom: "0"}}
                                        min={1} max={15}
                                        tipFormatter={(value) => {
                                            return `${value} người`;
                                        }}
                                        value={this.state.numOfWorker}
                                        onChange={this._onChangeNumOfWorker}
                                    />
                                </Col>
                                <Col span={5}>
                                    <div style={styleBatBuoc}>*</div>
                                    <InputNumber
                                        style={{width: "40px",}}
                                        min={1} max={15}
                                        value={this.state.numOfWorker}
                                        onChange={this._onChangeNumOfWorker}
                                    /><span style={{fontSize: "10px", paddingLeft: "4px"}}>người</span>
                                </Col>
                            </Row>

                        </Col>
                        <Col span={12}>
                            <div style={styleBatBuoc}>*</div>
                            <ProfessionPicker
                                style={{width: '100%'}}
                                placeholder={"Lĩnh vực"}
                                value={this.state.professionId}
                                onChange={this._onChangeProfessionId}
                            />
                        </Col>
                    </Row>
                </Col>

                <Col span={3}>
                    <Button
                        style={{
                            width: '100%',
                            height: "83px",
                            marginBottom: "21px",
                            fontSize: "12px",
                        }}
                        type="primary"
                        icon="search"
                        onClick={this._onClickSearchButton}
                    >
                        <br/>Tìm thầu phụ
                    </Button>
                    <Select
                        value={this.state.order}
                        style={{
                            width: '100%',
                            height: "25px"
                        }}
                        onChange={this._onChangeOrder}
                        showSearch
                        placeholder="Sắp xếp"
                    >
                        <OptGroup label="Giá">
                            <Option value="ascPrice">Giá tăng dần</Option>
                            <Option value="descPrice">Giá giảm dần</Option>
                        </OptGroup>
                        <OptGroup label="Tên thầu phụ">
                            <Option value="ascName">A đến Z</Option>
                            <Option value="descName">Z về A</Option>
                        </OptGroup>

                    </Select>
                </Col>
            </Row>
        );
    };

    renderSearchedSubList = () => {

        return this.props.manualSearchReducer.searchedSubInfo.searchedSubcontractors.map((subcontractor) => {
            const priceOnCard = parseInt(subcontractor.price / 1000);
            let descriptionOnCard = "";
            if (subcontractor.description.length > 35) {
                descriptionOnCard = subcontractor.description.substring(0, 35);
                descriptionOnCard += "...";
            } else {
                descriptionOnCard = subcontractor.description
            }
            return (
                <Col
                    xs={24} sm={24} md={12} lg={8} xl={6} key={subcontractor.subcontractorId}
                    style={{marginBottom: '16px'}}
                >
                    <Card
                        actions={
                            [<div onClick={() => this.showModal(subcontractor)}><Icon type="file-sync"/> Mời hợp tác
                            </div>]
                        }
                        cover={
                            ((subcontractor.avatar) ? <img width={`${this.widthHeightSubCoverCard}px`}
                                                           height={`${this.widthHeightSubCoverCard * 0.7}px`}
                                                           alt="example" src={subcontractor.avatar}/> :
                                (<Row style={{
                                    width: `${this.widthHeightSubCoverCard}px`,
                                    height: `${this.widthHeightSubCoverCard * 0.7}px`
                                }} type="flex" justify="space-around" align="middle">
                                    <Col><Icon style={{
                                        fontSize: "60px",
                                        color: "#d4d4d4",
                                    }} type="user"/></Col>
                                </Row>))
                        }
                        title={<Link
                            to={`/profile/${subcontractor.subcontractorAccountId}`}>{subcontractor.companyName}</Link>}
                        bordered={false}>
                        <Meta style={{textAlign: 'center'}}
                              title={<div>{`${priceOnCard}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}<span
                                  style={{
                                      fontWeight: "normal",
                                      margin: "0 1px",
                                  }}>,</span><span
                                  style={{
                                      fontWeight: "normal",

                                  }}>000</span><sup
                                  style={{
                                      fontWeight: "normal",
                                      fontSize: "10px",
                                      right: "-5px",
                                      color: "#ad8700",
                                  }}>đ</sup></div>}
                              description={descriptionOnCard}
                        />
                    </Card>
                </Col>
            );
        });

    };

    renderSearchResult = () => {
        if (this.props.manualSearchReducer.searchedSubInfo)
            if (this.props.manualSearchReducer.searchedSubInfo.searchedSubcontractors) {
                const pageIndex = (this.props.manualSearchReducer.searchedSubInfo.pageIndex) + 1;
                return (
                    <div>
                        <Row gutter={32}>
                            {this.renderSearchedSubList()}
                        </Row>
                        <Row type="flex" justify="space-around" align="middle" style={{marginTop: "50px",}}>
                            <Col>
                                <Pagination onChange={this._onChangePageIndex} defaultCurrent={pageIndex} pageSize={12}
                                            total={this.props.manualSearchReducer.searchedSubInfo.countFound}/>
                            </Col>
                        </Row>
                    </div>
                );
            }
    };

    renderModal = () => {

        return (

            <Modal className={'PrimeSearchModal'}
                   title="Gửi lời mời hợp tác"
                   visible={this.state.visibleModal}
                   onOk={this.handleOk}
                   onCancel={this.handleCancel}
                   okText={"Gửi lời mời hợp tác"}
                   cancelText={"Thoát"}
                   width={"60%"}
            >
                <Spin spinning={this.props.sentRequestReducer.isFetching} size={"large"}>
                    <SentRequest dataSentRequestFunc={this.mapDataSentRequest} searchValue={this.searchValue}
                                 subcontractorItemSelect={this.subcontractorItemSelect}/>
                </Spin>
            </Modal>
        );
    };

    renderModalAutoSearch = () => {

        return (
            <Modal className={'AutoSearchInfoModal'}
                   title="Tự động tìm kiếm"
                   visible={this.state.visibleModalAutoSearch}
                   onOk={() => {
                       this._onClickAutoSearchButton();
                   }}
                   onCancel={() => {
                       this.setState({visibleModalAutoSearch: false})
                   }}
            >
                <Spin spinning={this.props.manualSearchReducer.isFetching} size={"large"}>
                    <AutoSearchInfoModal searchValue={this.searchValue}/>
                </Spin>
            </Modal>
        );
    };

    renderSearchNotFound = () => {
        if (this.searchValue.companyName !== null) {
            return (
                <Row>
                    <Col>
                        <div style={{
                            fontSize: "20px",
                            textAlign: "center",
                            color: "rgb(165, 165, 165)",
                        }}>
                            <Icon type="frown" style={{fontSize: "60px"}}/>
                            <br/>
                            <div style={{fontWeight: "bold"}}>Hiện tại chưa có Nhà thầu phụ thoả mãn bạn tìm</div>
                            <br/>
                        </div>
                    </Col>
                </Row>
            )
        }
        return (
            <Row>
                <Col>
                    <div style={{
                        fontSize: "20px",
                        textAlign: "center",
                        color: "rgb(165, 165, 165)",
                    }}>
                        <Icon type="frown" style={{fontSize: "60px"}}/>
                        <br/>
                        <div style={{fontWeight: "bold"}}>Hiện tại chưa có Nhà thầu phụ thoả mãn bạn tìm</div>
                        <br/>
                        <div style={{
                            fontSize: "15px",
                            color: "rgb(154, 154, 154)",
                            textAlign: "left",
                            marginLeft: "38%",
                        }}>
                            Bạn có muốn hệ thống tự động tìm kiếm<br/>và thông báo với bạn ngay khi có<br/>Nhà thầu phụ
                            thoả
                            mãn yêu cầu không?
                        </div>
                        <Button style={{width: "20%"}} onClick={() => {
                            this.setState({visibleModalAutoSearch: true})
                        }}>Tự động tìm kiếm</Button>
                    </div>
                </Col>
            </Row>
        )
    };

    render() {
        if ( (this.props.manualSearchReducer.searchedSubInfo.searchedSubcontractors) && (this.props.manualSearchReducer.searchedSubInfo) && (this.state.isSearchClick === true) && (this.props.manualSearchReducer.isFetching === false) && (this.props.manualSearchReducer.searchedSubInfo.searchedSubcontractors.length === 0)) {
            return (
                <div style={{padding: "50px"}}>
                    {this.renderSearchForm()}
                    <Spin style={{position: "relative", left: "45%"}}
                          spinning={this.props.manualSearchReducer.isFetching} size={"large"}>
                        {this.renderSearchNotFound()}
                    </Spin>
                    {this.renderModalAutoSearch()}
                </div>
            )
        } else {
            return (
                <Row style={{padding: "50px"}}>
                    <Col>
                        {this.renderSearchForm()}
                        <Spin style={{position: "relative", left: "45%"}}
                              spinning={this.props.manualSearchReducer.isFetching} size={"large"}>
                            {this.renderSearchResult()}
                        </Spin>
                        {this.renderModal()}
                    </Col>
                </Row>
            );
        }
    };


    showModal = (subcontractorItemSelect) => {
        this.subcontractorItemSelect = subcontractorItemSelect;

        /*dùng cho trường hợp khi cả 2 đều ko đổi onchange trong sentRequest Modal*/
        this.pricePerDocket = parseInt(subcontractorItemSelect.price / 1000) * 1000;
        this.locationDetail = "";

        this.setState({
            visibleModal: true,
        });
    };

    handleOk = (e) => {
        if (this.locationDetail === "") {
            message.warning("Địa điểm chưa rõ ràng, vui lòng điền chính xác địa chỉ");
            return;
        }
        const data = {
            "id": null,
            "primecontructorId": this.props.authReducer.profile.userId,
            "subcontructorId": this.subcontractorItemSelect.subcontractorId,
            "startDate": this.searchValue.fromDate,
            "endDate": this.searchValue.toDate,
            "locationId": this.searchValue.locationId,
            "workerNumber": this.searchValue.numOfWorker,
            "pricePerDocket": this.pricePerDocket,
            "locationDetail": this.locationDetail,
            "professionId": this.searchValue.professionId,
            "status": null,
            "confirmDate": null,
            "formId": null,
            "priceFrom": this.searchValue.minPrice,
            "priceTo": this.searchValue.maxPrice
        };
        this.props.sendRequest(data, () => {
            this.setState({
                visibleModal: false,
            });
            this._onClickSearchButton();

        });
    };

    handleCancel = (e) => {
        this.setState({
            visibleModal: false,
        });
    };

    _onChangeMinPrice = (value) => {
        if (value > 2000) {
            message.warning('Giá tối đa là 2.000.000 VNĐ');
        } else if (value < 50) {
            message.warning('Giá tối thiểu là 50.000 VNĐ');
        } else {
            let temp = value * 1000;

            this.setState({
                minPrice: temp,
            });
        }


    };

    _onChangeMaxPrice = (value) => {
        if (value > 2000) {
            message.warning('Giá tối đa là 2.000.000 VNĐ');
        } else if (value < 50) {
            message.warning('Giá tối thiểu là 50.000 VNĐ');
        } else {
            let temp = value * 1000;

            this.setState({
                maxPrice: temp,
            });
        }
    };

    _onChangeMinMaxPrice = (value) => {
        this.setState({
            minPrice: value[0].toString(),
            maxPrice: value[1].toString(),
        });
    };


    _onChangeCompanyName = (e) => {
        this.setState({
            companyName: e.target.value.replace(/\s\s+/g, ' ')
        });
    };

    _onChangeNumOfWorker = (value) => {
        if (Number.isInteger(value)) {
            this.setState({
                numOfWorker: value,
            });
        }
    };

    _onChangeProfessionId = (value) => {
        if (Number.isInteger(value)) {
            this.setState({
                professionId: value,
            });
        }
    };

    _onChangeLocationId = (value) => {
        if (Number.isInteger(value)) {
            this.setState({
                locationId: value,
            });
        }
    };

    _onChangeRangePickerValue = (value, dateString) => {
        this.setState({
            fromDateValue: value[0],
            toDateValue: value[1],
            fromDate: dateString[0],
            toDate: dateString[1]
        });
    };

    _onClickSearchButton = () => {
        const {companyName, numOfWorker, professionId, fromDate, toDate, minPrice, maxPrice, locationId} = this.state;
        this.searchValue = {
            companyName: companyName === "" ? null : companyName,
            numOfWorker,
            professionId,
            fromDate,
            toDate,
            minPrice,
            maxPrice,
            locationId
        };
        const criteria = {
            sortBy: this.state.sortBy,
            sortOrder: this.state.sortOrder,
            pageIndex: this.state.pageIndex,
            pageSize: 12
        };


        this.props.searchSubcontractor(this.searchValue, criteria);
        this.setState({isSearchClick: true});
    };

    _onClickAutoSearchButton = () => {
        this.setState({visibleModalAutoSearch: false})
        this.props.autoSearchSubcontractor(this.searchValue, this.props.authReducer.profile.userId);
    };

    _onChangePageIndex = (page) => {
        this.setState({pageIndex: page - 1}, () => {
            this._onClickSearchButton();
        });
    };

    _onChangeOrder = (value) => {
        let sortBy = "";
        let sortOrder = "";
        if (value === "descPrice") {
            sortBy = "price";
            sortOrder = "desc";
        } else if (value === "ascPrice") {
            sortBy = "price";
            sortOrder = "asc";
        } else if (value === "ascName") {
            sortBy = "companyName";
            sortOrder = "asc";
        } else if (value === "descName") {
            sortBy = "companyName";
            sortOrder = "desc";
        }
        this.setState({sortOrder: sortOrder, sortBy: sortBy}, () => {
            this._onClickSearchButton();
        })
    };


    styleCss = () => {
        let antDesignItem = document.getElementsByClassName("ant-input-number-handler-wrap");
        if (antDesignItem) {
            for (let i = 0; i < antDesignItem.length; i++) {
                antDesignItem[i].style.display = "none";
            }
        }

        let antDesignItem2 = document.getElementsByClassName("ant-input-number-input");
        if (antDesignItem2) {
            for (let i = 0; i < antDesignItem2.length; i++) {
                antDesignItem2[i].style.padding = "1px";
                antDesignItem2[i].style.textAlign = "center";

            }
        }

        let antDesignItem3 = document.getElementsByClassName("ant-card-cover");
        if (antDesignItem3[0]) {
            this.widthHeightSubCoverCard = antDesignItem3[0].clientWidth;
        }
    };

}

/*for datetime picker, prevent select past date*/
function range(start, end) {
    const result = [];
    for (let i = start; i < end; i++) {
        result.push(i);
    }
    return result;
}

function disabledDate(current) {
    // Can not select days before today and today
    return current && current < moment().endOf('day');
}

function disabledRangeTime(_, type) {
    if (type === 'start') {
        return {
            disabledHours: () => range(0, 60).splice(4, 20),
            disabledMinutes: () => range(30, 60),
            disabledSeconds: () => [55, 56],
        };
    }
    return {
        disabledHours: () => range(0, 60).splice(20, 4),
        disabledMinutes: () => range(0, 31),
        disabledSeconds: () => [55, 56],
    };
}


function mapStateToProps(state) {
    return {
        manualSearchReducer: state.manualSearchReducer,
        sentRequestReducer: state.sentRequestReducer,
        locationReducer: state.locationReducer.locations,
        authReducer: state.authReducer,
    };
}

export default connect(
    mapStateToProps,
    {searchSubcontractor, sendRequest, autoSearchSubcontractor}
)(ManualSearch);