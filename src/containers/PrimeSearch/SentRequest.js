import React, {Component} from 'react';
import {connect} from 'react-redux';

import {
    Col,
    Row,
    Input,
    List,
    InputNumber,
    message
} from 'antd';
import {Link} from "react-router-dom";

class SentRequest extends Component {
    constructor(props) {
        super(props);
        this.pricePerDocket = 0;
        this.locationDetail = ""
    }

    componentDidMount() {
        this.styleForAntLibrary();
    };

    takeLocationName = (locationId) => {
        for (let i = 0; i < this.props.locationReducer.length; i++) {
            if (this.props.locationReducer[i].locationId === locationId) {
                return `${this.props.locationReducer[i].district}, ${this.props.locationReducer[i].city}`
            }
        }
    };

    takeProfessionName = (professionId) => {
        for (let i = 0; i < this.props.professionReducer.length; i++) {
            if (this.props.professionReducer[i].professionId === professionId) {
                return `${this.props.professionReducer[i].professionName}`
            }
        }
    };


    render() {
        const subcontractorItemSelect = this.props.subcontractorItemSelect;

        if (!this.props.searchValue) return <div>Đang tải...</div>
        if (!this.props.subcontractorItemSelect) return <div>Đang tải...</div>
        const searchValue = this.props.searchValue;


        return (
            <div>
                <Row style={{
                    background: "rgb(245, 245, 245)",
                    margin: "-24px"
                }}>
                    <div style={{margin: "24px"}}>
                        <Col span={4}>
                            <Row type="flex" justify="center" style={{textAlign: "center"}}>
                                <img alt={"avatar"} src={subcontractorItemSelect.avatar}
                                     style={{width: "90px", height: "90px",}}/>
                            </Row>
                        </Col>
                        <Col span={20}>
                            <Row><p style={{fontWeight: "bold",}}>Thông tin nhà thầu phụ:</p></Row>
                            <Row type="flex" align="bottom"
                                 style={{
                                     paddingBottom: "10px",
                                     marginBottom: "10px",
                                     borderBottom: "solid 1px #e8e8e8",
                                 }}>
                                <Col span={6}>
                                    Tên nhà thầu phụ:
                                </Col>
                                <Col span={18}>
                                    <Link
                                        to={`/profile/${subcontractorItemSelect.subcontractorAccountId}`}>{subcontractorItemSelect.companyName}</Link>
                                </Col>
                            </Row>
                            <Row type="flex" justify="space-between" align="bottom"
                                 style={{
                                     paddingBottom: "10px",
                                     marginBottom: "10px",
                                     borderBottom: "solid 1px #e8e8e8",
                                 }}>
                                <Col span={6}>
                                    Giá ngày công<br/>Thầu phụ đề nghị:
                                </Col>
                                <Col span={18}>
                                                    <span>{`${parseInt(subcontractorItemSelect.price / 1000)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                        <span
                                                            style={{
                                                                fontWeight: "normal",
                                                                margin: "0 1px",
                                                            }}>,</span><span
                                                            style={{
                                                                fontWeight: "normal",
                                                                letterSpacing: "-2px"
                                                            }}>000</span><sup
                                                            style={{
                                                                fontWeight: "normal",
                                                                right: "-5px",
                                                                color: "#ad8700",
                                                            }}>đ</sup></span>
                                </Col>
                            </Row>
                            <Row type="flex" justify="space-between" align="top"
                                 style={{
                                     paddingBottom: "10px",
                                     marginBottom: "10px",
                                     borderBottom: "solid 1px #e8e8e8",
                                 }}>
                                <Col span={6}>
                                    Giới thiệu:
                                </Col>
                                <Col span={18}>
                                    {subcontractorItemSelect.description}
                                </Col>
                            </Row>
                        </Col>
                    </div>
                </Row>

                <Row style={{marginTop: "40px"}}><p
                    style={
                        {
                            fontWeight: "bold",
                        }
                    }>
                    Thông
                    tin
                    chi
                    tiết
                    hợp
                    đồng:</p>
                </Row>

                <Row style={{marginLeft: "40px"}}>
                    <List>
                        <List.Item>
                            <Col span={8}>
                                Lĩnh vực:
                            </Col>
                            <Col span={16}>
                                {Number.isInteger(searchValue.professionId) ? this.takeProfessionName(searchValue.professionId) : searchValue.professionId}
                            </Col>
                        </List.Item>
                        <List.Item>
                            <Col span={8}>
                                Thời gian:
                            </Col>
                            <Col span={16}>
                                {searchValue.fromDate} - {searchValue.toDate}
                            </Col>
                        </List.Item>
                        <List.Item>
                            <Col span={8}>
                                Số nhân công:
                            </Col>
                            <Col span={16}>
                                {searchValue.numOfWorker}
                            </Col>
                        </List.Item>
                        <List.Item>
                            <Col span={8}>
                                Địa điểm:
                            </Col>
                            <Col span={16}>
                                <Input
                                    size={"large"}
                                    placeholder="Địa chỉ cụ thể"
                                    addonAfter={((Number.isInteger(searchValue.locationId) ? this.takeLocationName(searchValue.locationId) : searchValue.locationId))}
                                    onChange={(event) => {
                                        event.target.value = event.target.value.replace(/^\s+|\s+$/g, '');
                                        if (event.target.value.length < 4) {
                                            this.props.dataSentRequestFunc(this.pricePerDocket, "");
                                        } else {
                                            let temp = `${event.target.value}, ${((Number.isInteger(searchValue.locationId) ? this.takeLocationName(searchValue.locationId) : searchValue.locationId))}`;
                                            this.props.dataSentRequestFunc(this.pricePerDocket, temp);
                                            this.locationDetail = temp;
                                        }
                                    }}
                                />
                            </Col>

                        </List.Item>
                        <List.Item>
                            <Col span={8}>
                                Giá ngày công bạn đề xuất:
                            </Col>
                            <Col span={16}>
                                <InputNumber
                                    size="large"
                                    formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                    parser={value => value.replace(/\$\s?|(,*)/g, '')}
                                    style={{width: "47px", fontSize: "18px"}}
                                    defaultValue={`${parseInt(subcontractorItemSelect.price / 1000)}`}
                                    min={50}
                                    step={100}
                                    max={2000}
                                    onChange={(value) => {
                                        if (value > 2000) {
                                            message.warning('Giá tối đa là 2.000.000 VNĐ');
                                        } else if (value < 50) {
                                            message.warning('Giá tối thiểu là 50.000 VNĐ');
                                        } else {
                                            let temp = value * 1000;
                                            this.props.dataSentRequestFunc(temp, this.locationDetail);
                                            this.pricePerDocket = temp;
                                        }
                                    }}
                                /><span style={{
                                fontSize: "18px",
                                position: "relative",
                            }}><span
                                style={{
                                    fontWeight: "normal",
                                    margin: "0 1px",
                                }}>,</span><span
                                style={{
                                    fontWeight: "normal",
                                    letterSpacing: "-2px"
                                }}>000</span><sup
                                style={{
                                    fontWeight: "normal",
                                    right: "-5px",
                                    color: "#ad8700",
                                }}>đ</sup></span>
                            </Col>
                        </List.Item>
                    </List>
                </Row>
            </div>
        )
            ;
    }

    styleForAntLibrary = () => {
        let antDesignItem = document.getElementsByClassName("ant-input-number-handler-wrap");
        if (antDesignItem) {
            for (let i = 0; i < antDesignItem.length; i++) {
                antDesignItem[i].style.display = "none";
            }

            if (this.props.searchValue.locationId)
                this.locationDetail = (Number.isInteger(this.props.searchValue.locationId) ? this.takeLocationName(this.props.searchValue.locationId) : this.props.searchValue.locationId);
            this.pricePerDocket = (this.props.subcontractorItemSelect.price / 1000) * 1000;
        }

        let antDesignItem2 = document.getElementsByClassName("ant-input-number-input");
        if (antDesignItem2) {
            for (let i = 0; i < antDesignItem2.length; i++) {
                antDesignItem2[i].style.padding = "1px";
                antDesignItem2[i].style.textAlign = "center";
            }
        }

    };

}

function mapStateToProps(state) {
    return {
        profile: state.authReducer.profile,
        locationReducer: state.locationReducer.locations,
        professionReducer: state.professionReducer.professions
    };
}

export default connect(
    mapStateToProps,
    {}
)(SentRequest);