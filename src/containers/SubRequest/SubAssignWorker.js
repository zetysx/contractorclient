import React, {Component} from 'react';
import {connect} from 'react-redux';
import {workerAssign} from "../../actions/subRequestAction";
import {loadAllSubRequest} from "../../actions/subRequestAction";

import {Col, Row, Button, message, Spin, Icon} from 'antd';

class SubAssignWorker extends Component {

    constructor(props) {
        super(props);

        this.workerChooseList = [];
    }

    _onClickAssignWorker = () => {
        if (this.workerChooseList.length === this.props.recordDrawer.workerNumber) {
            const data = {
                "contractId": this.props.recordDrawer.id,
                "workerIds": this.workerChooseList
            };
            this.afterActionSuccessFunction = () => {
                this.props.loadAllSubRequest(this.props.authReducer.profile.userId);
                this.props.onClose();
            };

            this.props.workerAssign(data, this.afterActionSuccessFunction);
        } else {
            message.warning(`Hợp đồng này phải có ${this.props.recordDrawer.workerNumber} nhân công`);
        }

    };

    changeChooseWorker = (event, workerId) => {
        let temp = this.workerChooseList;
        event.preventDefault();
        let i = 0;
        do {
            if ((this.workerChooseList.length !== 0) && (this.workerChooseList[i] === workerId)) {
                temp = temp.filter(obj => obj !== workerId);
                event.target.parentNode.style.background = "rgb(255, 255, 255)";
                event.target.parentNode.style.color = "rgba(0, 0, 0, 0.65)";
                break;
            }
            i++;
            if (i >= this.workerChooseList.length) {
                if (this.workerChooseList.length < this.props.recordDrawer.workerNumber) {
                    temp.push(workerId);
                    event.target.parentNode.style.background = "rgb(24, 144, 255)";
                    event.target.parentNode.style.color = "rgb(255, 255, 255)";
                } else {
                    message.warning(`Hợp đồng này phải có ${this.props.recordDrawer.workerNumber} nhân công`);
                }
                break;
            }
        } while (i < this.workerChooseList.length);

        this.workerChooseList = temp;
        this.props.workerNumberSelectFunction(this.workerChooseList.length);

    };

    renderWorkerList = () => {
        return this.props.workerReducer.workers.map((worker) => {
            return (
                <div key={worker.workerId}>
                    <Row type="flex" className={"rowWorker"}
                         align="middle" justify="start"
                         style={{marginBottom: "1px", background: "rgb(255, 255, 255)", color: "rgba(0, 0, 0, 0.65)"}}>

                        <div onClick={event => this.changeChooseWorker(event, worker.workerId)}
                             style={{
                                 position: "absolute",
                                 zIndex: " 2",
                                 width: "90%",
                                 height: "50px"
                             }}/>
                        {/*div để select tránh bị selectDefault từa lưa*/}
                        {((worker.avatar) ?
                            <img alt={"worker avatar"} src={worker.avatar} width={"50px"} height={"50px"}/> :
                            <Row type="flex" justify="space-around" align="middle"
                                 style={{height: "50px", width: "50px"}}>
                                <Icon style={{
                                    fontSize: "30px",
                                    color: "#d4d4d4",
                                }} type="user"/></Row>)}

                        <Col span={17} style={{paddingLeft: "10px"}}
                            /*onClick={event => this.changeColorChooseWorkerChildFix(event, "rgb(255, 255, 255)")}*/>{worker.firstName} {worker.lastName}</Col>
                    </Row>
                </div>
            )

        })
    };

    componentWillUpdate() {
        this.workerChooseList = [];
        this.props.workerNumberSelectFunction(this.workerChooseList.length);
        let i;
        for (i = 0; i < document.getElementsByClassName("rowWorker").length; i++) {
            document.getElementsByClassName("rowWorker")[i].style.background = "rgb(255, 255, 255)";
            document.getElementsByClassName("rowWorker")[i].style.color = "rgba(0, 0, 0, 0.65)";
        }
    }

    render() {
        return (
            <div style={{margin: "-24px"}}>
                <div>
                    <Spin spinning={this.props.isFetchingWorker} size={"large"}>
                        {this.renderWorkerList()}
                    </Spin>
                </div>
                <Button type="primary"
                        style={{
                            width: "100%",
                            height: "50px",
                            position: "absolute",
                            bottom: "0",
                            borderRadius: "0",
                            zIndex: " 9",
                        }}
                        onClick={() => this._onClickAssignWorker()}
                >Xác nhận</Button>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        isFetchingWorker: state.subRequestReducer.isFetchingWorker,
        authReducer: state.authReducer,
        workerReducer: state.subRequestReducer,
    };
}

export default connect(
    mapStateToProps,
    {workerAssign, loadAllSubRequest}
)(SubAssignWorker);