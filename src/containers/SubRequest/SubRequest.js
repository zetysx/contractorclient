import React, {Component} from 'react';
import {connect} from 'react-redux';
import {loadAllWorkerToAssign} from "../../actions/subRequestAction";
import {loadAllSubRequest, declineRequest} from "../../actions/subRequestAction";
import moment from 'moment';

// ant
import {Table, Button, Drawer, Spin} from 'antd';
import SubAssignWorker from "./SubAssignWorker";
import {Link} from "react-router-dom";

class SubRequest extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            workerNumberSelect: 0,
            workerNumberTotal: 0,
        };

    }

    componentDidMount() {
        this.props.loadAllSubRequest(this.props.authReducer.profile.userId);
    }

    render() {
        const columns = [
            {
                title: 'Tên thầu chính',
                dataIndex: '',
                key: 'primeCompanyName',
                render: (text, record, index) =>
                    <Link
                        to={`/profile/${record.primeContractAccountId}`}>{record.primeCompanyName}</Link>

            }, {
                title: 'Lĩnh vực',
                dataIndex: 'profession',
                key: 'profession',
            }, {
                title: 'Giá',
                dataIndex: '',
                key: 'pricatePerDocket',
                render: (text, record, index) =>
                    <span>{`${parseInt(record.pricatePerDocket / 1000)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        <span
                            style={{
                                fontWeight: "normal",
                                margin: "0 1px",
                            }}>,</span><span
                            style={{
                                fontWeight: "normal",

                            }}>000</span><sup
                            style={{
                                fontWeight: "normal",
                                fontSize: "10px",
                                right: "-5px",
                                color: "#ad8700",
                            }}>đ</sup></span>
            }, {
                title: 'Thời gian',
                dataIndex: '',
                key: 'time',
                render: (text, record, index) =>
                    `${moment(record.dateStart, "YYYY-MM-DD").format("DD/MM/YYYY")} - ${moment(record.dateEnd, "YYYY-MM-DD").format("DD/MM/YYYY")}`
            }, {
                title: 'Nhân công',
                dataIndex: 'workerNumber',
                key: 'workerNumber',
            }, {
                title: 'Địa điểm',
                dataIndex: 'locationDetail',
                key: 'locationDetail',
            }, {
                title: '',
                dataIndex: '',
                key: '',
                render: (text, record, index) => (
                    <div>
                        <Button type="primary" style={{width: "100%"}} onClick={(event) => this.showDrawer(record)}>Chấp
                            nhận</Button>
                        <br/>
                        <Button type="danger" style={{width: "100%", marginTop: "5px"}}
                                onClick={() => this._onClickDeclineDocket(record.id)}>Từ chối</Button>
                    </div>
                ),
            }];
        return (
            <div style={{padding: "50px"}}>
                <Spin spinning={this.props.isFetchingSubRequestListReducer} size={"large"}>
                    <Table
                        rowKey={(record) => record.id}
                        pagination={false}
                        style={{background: "#fff"}}
                        bodyStyle={{fontSize: "13px"}}
                        columns={columns} dataSource={this.props.subRequestListReducer}/>
                </Spin>
                <Drawer
                    title={`Chọn nhân công (${this.state.workerNumberSelect}/${this.state.workerNumberTotal})`}
                    placement="right"
                    closable={false}
                    onClose={this.onClose}
                    visible={this.state.visible}
                >
                    <SubAssignWorker onClose={this.onClose} workerNumberSelectFunction={this._onChangeWorkerNumberSelect}
                                     recordDrawer={this.recordDrawer}/>
                </Drawer>
            </div>
        );
    }


    _onChangeWorkerNumberSelect = (number) => {
        this.setState({workerNumberSelect: number,});
    }

    _onClickDeclineDocket = (requestId) => {
        const afterDeclineSuccess = () => {
            this.props.loadAllSubRequest(this.props.authReducer.profile.userId);
        };
        this.props.declineRequest(requestId, afterDeclineSuccess);
    };

    showDrawer = (record) => {
        this.recordDrawer = record;
        this.props.loadAllWorkerToAssign(this.props.authReducer.profile.userId, record.id);
        this.setState({
            workerNumberTotal: record.workerNumber,
            visible: true,
        });
    };

    onClose = () => {
        this.setState({
            visible: false,
        });
    };

}

function mapStateToProps(state) {
    return {
        authReducer: state.authReducer,
        subRequestListReducer: state.subRequestReducer.requests,
        isFetchingSubRequestListReducer: state.subRequestReducer.isFetching,
    }
}

export default connect(
    mapStateToProps,
    {loadAllWorkerToAssign, loadAllSubRequest, declineRequest}
)(SubRequest);

