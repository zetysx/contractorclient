import React, { Component } from 'react';
import { connect } from 'react-redux';
import { 
    loadSubcontractorProfessionsForUpdate, 
    loadSubcontractorLocationsForUpdate,
    updateSubcontractorProfessions,
    updateSubcontractorLocations,
    changePriceProfessions,
    changeCheckboxProfessions,
    changeCheckboxLocation
} from '../actions/subCapabilityAction';
import { 
    Tree, 
    Col, 
    Checkbox, 
    InputNumber, 
    Card, 
    Button, 
    Icon, 
    message, 
    Spin,
    Tooltip
} from 'antd';

const {TreeNode} = Tree;

class SubCapability extends Component {


    componentDidMount() {
        this.props.loadSubcontractorProfessionsForUpdate();
        this.props.loadSubcontractorLocationsForUpdate();
    }

    renderTreeNodes = data =>
        data.map((item, index) => {
            if (item.children && item.children.length > 0) {
                return (
                    <TreeNode 
                        title={this.renderTitle(item.title, this.props.subCapabilityReducer.subListLocationChecked.length)} 
                        key={item.key} 
                        dataRef={item}
                    >
                        {this.renderTreeNodes(item.children)}
                    </TreeNode>
                );
            }
            return <TreeNode {...item} />;
        }
    );

    renderTitle = (title, numberChecked) => {
        return (
            <span>
                <span style={{fontSize: "italic", fontWeight: "bold"}}>Thành phố {title}</span>
                <span style={{fontSize: "italic"}}>  - {numberChecked} vị trí</span>
            </span>
        )
    }

    renderLocation = (locations) => {
        return <Tree
                checkable={true}
                onCheck={(key) => this.props.changeCheckboxLocation(key)}
                checkedKeys={this.props.subCapabilityReducer.subListLocationChecked}
            >
                {this.renderTreeNodes(locations)}
            </Tree>
    }

    _onLocationUpdateClick = () => {

        const onSuccessedUpdateLocationCallback = () => {
            message.success("Cập nhật địa điểm thành công");
            this.props.loadSubcontractorLocationsForUpdate();
        }

        const onFailledUpdateLocationCallback = () => {
            message.error("Cập nhật địa điểm thất bại");
        }

        let locationsData = this.props.subCapabilityReducer.subListLocationChecked;
        if(!locationsData || locationsData.length === 0) {
            message.warn("Vui lòng chọn ít nhất một địa điểm làm việc");
            return;
        }
        
        this.props.updateSubcontractorLocations(locationsData, 
                            onSuccessedUpdateLocationCallback, 
                            onFailledUpdateLocationCallback);
    }

//// LOCATION : END


/// PROFESSIONS : START
    renderProfessionAndPrice = (professionsList) => {
        if(professionsList && professionsList.length > 0) {
            return professionsList.map((profession) => 
                <div 
                    name={profession.professionName} 
                    key={profession.professionId} 
                    style={{width: 300, marginTop: 20, backgroundColor: "red"}}
                >
                    <Col span={12} style={{marginBottom: 15}}>{this.renderCheckBox(profession)}</Col>
                    <Col span={12} style={{marginBottom: 15}}>{this.renderPriceInput(profession)}</Col>
                </div>

            );
        } else {
            return (
                <p>Không có lĩnh vực nào</p>
            )
        }
        
    }

    renderCheckBox(profession) {
        let elementTag = undefined;
        if(profession.workingOn) {
            elementTag = <Tooltip title="Bạn đang tham gia một dự án về lĩnh vực này, chỉ có thể thay đổi giá tiền của lĩnh vực">
                            <Checkbox 
                                key = {profession.professionId}  
                                value = {profession.professionId}
                                checked = {profession.subcontractorHas ? profession.subcontractorHas : false}
                                disabled = {profession.workingOn}
                                checkable = {true}
                                onChange= {(e) => this.props.changeCheckboxProfessions(e.target.value)}
                            >
                                {profession.professionName}
                            </Checkbox>
                        </Tooltip>
        } else if(!profession.workingOn) {
            if(profession.onlyOneOfWorkerProfession) {
                elementTag = <Tooltip title="Một nhân công đang chỉ có lĩnh vực này, vui lòng thêm lĩnh vực cho những nhân công chỉ có một">
                            <Checkbox 
                                key = {profession.professionId}  
                                value = {profession.professionId}
                                checked = {profession.subcontractorHas ? profession.subcontractorHas : false}
                                disabled = {profession.onlyOneOfWorkerProfession}
                                checkable = {true}
                                onChange= {(e) => this.props.changeCheckboxProfessions(e.target.value)}
                            >
                                {profession.professionName}
                            </Checkbox>
                        </Tooltip>
            } else {
                elementTag = <Checkbox 
                        key = {profession.professionId}  
                        value = {profession.professionId}
                        checked = {profession.subcontractorHas ? profession.subcontractorHas : false}
                        disabled = {profession.workingOn}
                        checkable = {true}
                        onChange= {(e) => this.props.changeCheckboxProfessions(e.target.value)}
                    >
                        {profession.professionName}
                    </Checkbox>
            }
            
        }

        return elementTag;
    }

    renderPriceInput = (profession) => {
        return (
            <div>
                <InputNumber
                    defaultValue={0}
                    formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                    parser={value => value.replace(/\$\s?|(,*)/g, '')}
                    disabled={profession.subcontractorHas ? !profession.subcontractorHas : true}
                    value={profession.price ? profession.price : 0.0}
                    min={1000}
                    max={10000000}
                    onChange={(value) => this.props.changePriceProfessions(profession.professionId, value)}
                /> VNĐ
            </div>
        );
    }

    _onProfessionUpdateClick = () => {

        const onSuccessedUpdateProfessionCallback = () => {
            message.success("Cập nhật lĩnh vực thành công");
            this.props.loadSubcontractorProfessionsForUpdate();
        }

        const onFailledUpdateProfessionCallback = () => {
            message.error("Cập nhật lĩnh vực thất bại");
        }

        let professionsData = [...this.props.subCapabilityReducer.subcontractorProfessionsForUpdate];
        console.log("BEFORE UPDATE PROFESSIONS: ", professionsData);
        if(!professionsData || professionsData.length === 0) {
            message.warn("Vui lòng chọn ít nhất một lĩnh vực và giá tiền thi công");
            return;
        }
        let isEmpty = false;
        for(var pos in professionsData) {
            if(professionsData[pos].subcontractorHas) {
                isEmpty = true;
                break;
            }
        }
        if(!isEmpty) {
            message.warn("Vui lòng chọn ít nhất một lĩnh vực và giá tiền thi công");
            return;
        }
        this.props.updateSubcontractorProfessions(professionsData, 
                            onSuccessedUpdateProfessionCallback, 
                            onFailledUpdateProfessionCallback);
    }
    
/// PROFESSIONS : END



    renderProfessionsCard = (professions) => {

        return (
            <Card
                title={<Tooltip title="Lĩnh vực mà bạn có thể làm"><span style={{fontStyle: "italic", fontWeight: "bold", fontSize: "20px"}}>
                            Lĩnh vực <Icon type="profile" />
                        </span></Tooltip>}
                style={{ width: 400, float: "left", borderRadius: 15 }}
                loading={professions ? false : true}
                type="inner"
                hoverable={true}
                headStyle={{backgroundColor: "#e6eeff", borderRadius: 15}}
                extra={<Button
                        style={{width: 100}}
                        icon="edit"
                        type="primary"
                        onClick={() => this._onProfessionUpdateClick()}>
                        Lưu
                    </Button>}
                >
                    <div>
                        {professions && professions.length > 0 ?
                            (
                                this.renderProfessionAndPrice(professions)
                            ) : (
                                <Spin size="large"/>
                            )
                        }
                    </div>
            </Card>
        )
    }


    renderLocationsCard = (locations) => {

        return (
            <Card
                title={<Tooltip title="Địa điểm mà bạn có thể thực thi"><span style={{fontStyle: "italic", fontWeight: "bold", fontSize: "20px"}}>
                        Địa điểm <Icon type="pushpin"/>
                    </span></Tooltip>}
                style={{ width: 400,  float: "left", marginLeft: 50, borderRadius: 15}}
                hoverable={true}
                type="inner"
                headStyle={{backgroundColor: "#e6eeff", borderRadius: 15}}
                extra={ <Button
                            style={{width: 100}}
                            icon="edit"
                            type="primary"
                            onClick={() => this._onLocationUpdateClick()}>
                            Lưu
                        </Button>}
                >
                    <div>
                        {locations && locations.length > 0 ? 
                            (
                                this.renderLocation(locations)
                            ) : (
                                <Spin size="large"/>
                            )
                        }
                    </div>
            </Card>
        )
    }

    render() {
        const style = {
            boxShadow: "5px 10px 15px #e0ebeb"
        }
        const style1= {
            color: "#7c795d",
            fontWeight: "normal",
            lineHeight: "48px",
            margin: 0
        }
        return (
            <Spin spinning={this.props.subCapabilityReducer.isFetching} size="large">
                <div style={{padding: 50}}>
                    <Card 
                        title={<span style={{fontSize: "20px"}}>Hồ sơ năng lực</span>} 
                        style={{borderRadius: 15, ...style1, ...style}}
                        headStyle={{backgroundColor: "#ccebff"}}
                    >
                        <div style={style}>
                            {this.renderProfessionsCard(this.props.subCapabilityReducer.subcontractorProfessionsForUpdate)}
                        </div>
                        <div>
                            {this.renderLocationsCard(this.props.subCapabilityReducer.subcontractorLocationsForUpdate)}
                        </div>
                    </Card>
                </div>
            </Spin>
        )
    }
}

function mapStateToProps(state) {
    return {
        authReducer         : state.authReducer,
        subCapabilityReducer: state.subCapabilityReducer,
    };
}

const mapDispatchActionToProps = {
    loadSubcontractorProfessionsForUpdate,
    loadSubcontractorLocationsForUpdate,
    updateSubcontractorProfessions,
    updateSubcontractorLocations,
    changePriceProfessions,
    changeCheckboxProfessions,
    changeCheckboxLocation
}

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(SubCapability);

