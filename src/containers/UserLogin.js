import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";

import {
    Col, Icon, Input, Button, Row, Spin, message
} from 'antd';

import { getAuthTokenAndLogin } from "../actions/authAction";

const LOGINIMG = require("../assets/login.jpg");

class UserLogin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            password: '',
        };
    };

    renderLoginForm() {
        const { userName, password } = this.state;

        //icon để xóa username
        const suffix = userName ? <Icon type="close-circle" onClick={this._onEmitEmptyUserName} /> : null;
        return (
            <Spin spinning={this.props.isFetching} delay={0}>
                <Row>
                    <Col span={14} offset={5}
                        style={{
                            background: "#fff", padding: '24px 24px', marginTop: "250px"
                        }}
                    >
                        <Row style={{ marginBottom: 16 }}>
                            <Input
                                placeholder="Tên đăng nhập"
                                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                suffix={suffix}
                                value={userName}
                                onChange={this._onChangeUserName}
                                ref={node => this.userNameInput = node}
                            />
                        </Row>
                        <Row style={{ marginBottom: 25 }}>
                            <Input.Password
                                placeholder="Mật khẩu"
                                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                value={password}
                                onChange={this._onChangePassword}
                                onPressEnter={this._onLoginClick}
                                ref={node => this.passwordInput = node}
                            />
                        </Row>
                        <Row style={{ marginBottom: 10 }}>
                            <Button type="primary" htmlType="submit" className="login-form-button" block
                                onClick={this._onLoginClick}
                            >
                                Đăng nhập
                        </Button>
                        </Row>
                        <Row type="flex" justify="end">
                            <Col>
                                Chưa có tài khoản, <Link to="/register">Đăng ký ngay!</Link>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Spin>
        );
    };

    render() {
        return (
            <Row style={{ height: "100vh" }}>
                <Col
                    span={14}
                    style={{
                        height: "100vh",
                        zIndex: 1,
                        boxShadow: "3px 0px 10px 0px #bbb",
                        overflow: "hidden",
                        background:`url(${LOGINIMG})`,
                        backgroundSize:"cover",
                        backgroundRepeat:"repeat"
                    }}
                >
                </Col>
                <Col span={10}
                    style={{
                        height: "100vh",
                        backgroundColor: "#fff",
                    }}
                >
                    {this.renderLoginForm()}
                </Col>
            </Row>
        );
    };

    _onEmitEmptyUserName = () => {
        this.userNameInput.focus();
        this.setState({ userName: '' });
    };

    _onChangeUserName = (e) => {
        this.setState({ userName: e.target.value });
    };

    _onChangePassword = (e) => {
        this.setState({ password: e.target.value });
    };

    _onLoginClick = (e) => {
        const { userName, password } = this.state;

        const onSuccessCallback = () => {
            this.props.history.push("dashboard");
        };

        const onFailCallback = () => {
            message.error("Sai tên đăng nhập hoặc mật khẩu")
        };

        this.props.getAuthTokenAndLogin(userName, password, onSuccessCallback, onFailCallback);
    };
}

function mapStateToProps(state) {
    return {
        isFetching: state.authReducer.isFetching,
    };
}

export default connect(
    mapStateToProps,
    { getAuthTokenAndLogin }
)(UserLogin);