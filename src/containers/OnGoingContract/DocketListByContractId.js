import React, {Component} from 'react';
import {connect} from 'react-redux';
import moment from 'moment';
import {DOCKETSTATUS} from "../../constants/docketStatus";

import {loadAllDocketByContractId} from "../../actions/docketListByContractIdAction";

import {Row, Col, Button, Icon, Timeline, Steps, Affix, Spin, Tooltip} from 'antd';
import {Link} from "react-router-dom";
import {COLOR} from "../../constants/theme";

const Step = Steps.Step;

class DocketListByContractId extends Component {

    constructor(props) {
        super(props);
        this.state = {
            top: 60,
            today: moment().format("DD/MM/YYYY"),
        };
        this.checkActionSpin = "";
        this.message = "";
    }

    renderDateInDocketListDateItem = (item, dateMoment, todayMoment) => {
        if (dateMoment.toString() === todayMoment.toString()) {
            return <span style={{fontWeight: "bold"}}>HÔM NAY</span>
        }
        if (dateMoment > todayMoment) {
            return <span>{item.date}</span>
        }
        if (dateMoment < todayMoment) {
            return <span style={{
                fontStyle: "italic",
                fontSize: "13px"
            }}>{item.date}</span>
        }

        return <span>{item.date}</span>
    };

    renderDateInDocketListStatusItem = (item, dateMoment, todayMoment) => {
        if (dateMoment > todayMoment) {
            if (item.status === DOCKETSTATUS.APPROVED) return <div
                style={{
                    color: "#fff",
                    fontStyle: "italic",
                    fontSize: "12px",
                    borderRadius: "4px",
                    padding: "1px 0",
                    width: "70%",
                    textAlign: "center",

                    background: "#48a54b",
                }}>Xong sớm</div>;
            if (item.status > DOCKETSTATUS.DECLINED) return <div
                style={{
                    color: "#fff",
                    fontStyle: "italic",
                    fontSize: "12px",
                    borderRadius: "4px",
                    padding: "1px 0",
                    width: "70%",
                    textAlign: "center",

                    background: "#00948d",
                }}>Làm sớm</div>
        }
        if (dateMoment < todayMoment) {
            if (item.status === DOCKETSTATUS.WORKING) return <div
                style={{
                    color: "#fff",
                    fontStyle: "italic",
                    fontSize: "12px",
                    borderRadius: "4px",
                    padding: "1px 0",
                    width: "70%",
                    textAlign: "center",

                    background: "#ff7600",
                }}>LÀM TRỄ</div>;
            if (item.status < DOCKETSTATUS.WORKING) return <div
                style={{
                    color: "#fff",
                    fontStyle: "italic",
                    fontSize: "12px",
                    borderRadius: "4px",
                    padding: "1px 0",
                    width: "70%",
                    textAlign: "center",

                    background: "red",
                }}>TRỄ</div>
        }
        if (dateMoment.toString() === todayMoment.toString()) {
            if (item.status < DOCKETSTATUS.WORKING) return <div
                style={{
                    color: "#fff",
                    fontStyle: "italic",
                    fontSize: "12px",
                    borderRadius: "4px",
                    padding: "1px 0",
                    width: "70%",
                    textAlign: "center",

                    background: "#ff4700",
                }}>Có thể trễ</div>
        }
        if (item.status === DOCKETSTATUS.APPROVED) return <div
            style={{
                color: "#fff",
                fontStyle: "italic",
                fontSize: "12px",
                borderRadius: "4px",
                padding: "1px 0",
                width: "70%",
                textAlign: "center",

                background: "#48a54b",
            }}>XONG</div>;
        return <div
            style={{
                color: "#fff",
                fontStyle: "italic",
                fontSize: "12px",
                borderRadius: "4px",
                padding: "1px 0",
                width: "70%",
                textAlign: "center",

                background: "rgb(0, 187, 49)",
            }}>Kịp tiến độ</div>
    };

    renderDateInDocketList = (item) => {
        const dateMoment = moment(item.date, "DD/MM/YYYY");
        const todayMoment = moment(this.state.today, "DD/MM/YYYY");
        const divColDate = (item, dateMoment, todayMoment) => {
            return (
                <div style={styles.docketListOuterRow_ColDate}>
                    <div>{this.renderDateInDocketListDateItem(item, dateMoment, todayMoment)}<br/>{this.renderDateInDocketListStatusItem(item, dateMoment, todayMoment)}
                    </div>
                </div>
            )
        };

        if (dateMoment.toString() === todayMoment.toString()) {
            return <Timeline.Item dot={<Icon type="calendar" style={{fontSize: "20px",}}/>}
                                  style={{marginLeft: "-6px", marginTop: "10px", padding: 0}}>
                {
                    divColDate(item, dateMoment, todayMoment)
                }            </Timeline.Item>
        }
        if (dateMoment < todayMoment) {
            return <Timeline.Item dot={<Icon type="clock-circle-o" style={{color: "red", fontSize: "12px",}}/>}
                                  style={{marginLeft: "-6px", marginTop: "10px", padding: 0}}>
                {
                    divColDate(item, dateMoment, todayMoment)
                }            </Timeline.Item>

        }
        return <Timeline.Item style={{marginLeft: "-6px", marginTop: "10px", padding: 0}}>
            {
                divColDate(item, dateMoment, todayMoment)
            }            </Timeline.Item>


    };

    renderPrimeDocketContentFollowStatus = (item) => {
        if (item.status === DOCKETSTATUS.DECLINED) {
            return <Row gutter={16} type="flex" justify="space-between" align="middle"
                        style={stylesPrime.docketListRowStatusDECLINED}
            >
                <Col span={18}>
                    <div style={styles.docketListRow_ColDocketDetail}>
                        <Button style={{
                            position: "absolute",
                            top: "-15px",
                            right: "-36%",
                            borderRadius: "20px",
                            width: "35%",
                            height: "calc(100% + 30px)",
                        }}
                                onClick={() => this.props._onClickFillDocketDetail(this.props.record, item)}><span
                            style={{
                                color: "red",
                                fontSize: "10px"
                            }}>Nội dung công việc không được duyệt</span><br/><span
                            style={{fontWeight: "bold"}}>Điền lại tóm tắt</span></Button>
                        <div
                            style={{
                                fontSize: "14px",
                                color: "#000",
                                fontStyle: "normal",
                            }}
                        >
                            <div
                                style={{
                                    fontSize: "13px",
                                    color: "red",
                                    textTransform: "uppercase",
                                }}
                            >
                                Công việc không được Thầu phụ duyệt với lý do:
                            </div>
                            <div
                                style={{
                                    padding: "5px 20px 0 20px",
                                }}
                            >
                                {item.declineReason}
                            </div>
                        </div>
                    </div>
                </Col>
                <Col span={6}>
                </Col>
            </Row>
        }
        if (item.status === DOCKETSTATUS.WILL_WORK) {
            return <Row gutter={16} type="flex" justify="space-between" align="middle"
                        style={stylesPrime.docketListRowStatusWILL_WORK}
            >
                <Col span={21}>
                    <div style={styles.docketListRow_ColDocketDetail}>
                        <Button type="primary" style={{
                            position: "absolute",
                            top: "-15px",
                            right: "-16.4%",
                            borderRadius: "20px",
                            width: "14%",
                            height: "calc(100% + 30px)",
                        }}
                                onClick={() => {
                                    this.props.history.push(`/docketdetail/${item.docketId}`);
                                }}
                        ><Link
                            to={`/docketdetail/${item.docketId}`}
                        >
                            Cần điền
                        </Link></Button>
                        {item.docketDetail}
                    </div>
                </Col>
                <Col span={3}/>

            </Row>
        }

        if (item.status === DOCKETSTATUS.FILLED) {
            return <Row gutter={16} type="flex" justify="space-between"
                        align="middle"
                        style={stylesPrime.docketListRowStatusFILLED}
            >
                <Col span={21}>
                    <div style={styles.docketListRow_ColDocketDetail}>
                        <Button
                            style={{
                                position: "absolute",
                                top: "-15px",
                                right: "-16.4%",
                                borderRadius: "20px",
                                width: "14%",
                                height: "calc(100% + 30px)",
                            }}
                            onClick={() => {
                                this.props.history.push(`/docketdetail/${item.docketId}`);
                            }}
                        ><Link
                            to={`/docketdetail/${item.docketId}`}
                        >
                            XEM
                        </Link></Button>
                        {item.docketDetail}
                    </div>
                </Col>
                <Col span={3}/>

            </Row>
        }
        if (item.status === DOCKETSTATUS.CONFIRMED) {
            return <Row gutter={16} type="flex" justify="space-between" align="middle"
                        style={stylesPrime.docketListRowStatusCONFIRMED}
            >
                <Col span={21}>
                    <div style={styles.docketListRow_ColDocketDetail}>
                        <Button style={{
                            position: "absolute",
                            top: "-15px",
                            right: "-16.4%",
                            borderRadius: "20px",
                            width: "14%",
                            height: "calc(100% + 30px)",
                        }}
                                onClick={() => {
                                    this.props.history.push(`/docketdetail/${item.docketId}`);
                                }}
                        ><Link
                            to={`/docketdetail/${item.docketId}`}
                        >
                            XEM
                        </Link></Button>
                        {item.docketDetail}
                    </div>
                </Col>
                <Col span={3}/>

            </Row>
        }

        if (item.status === DOCKETSTATUS.WORKING) {

            if (item.rejectReason){ // trường hợp sub reject docket
                return (
                 <Row gutter={16} type="flex" justify="space-between" align="middle"
                            style={stylesPrime.docketListRowStatusREJECTED}
                >
                    <Col span={2} style={{textAlign: "center"}}>
                        <Tooltip
                            title={"Công nhân đã thông báo công việc này hoàn thành, nhưng Thầu phụ đã từ chối xác nhận điều đó. Do đó nó bị trả ngược về Công nhân."}>
                            <Icon
                                type="warning"
                                style={{
                                    fontSize: "30px",
                                    margin: "-15px 0",
                                    top: "5px",
                                    position: "relative",
                                    color: "red",
                                }}
                            />
                        </Tooltip>
                    </Col>
                    <Col span={19}>
                        <div style={styles.docketListRow_ColDocketDetail}>
                            <Button
                                style={{
                                    position: "absolute",
                                    top: "-15px",
                                    right: "-18%",
                                    borderRadius: "20px",
                                    width: "15.5%",
                                    height: "calc(100% + 30px)",
                                }}
                                onClick={() => {
                                    this.props.history.push(`/docketdetail/${item.docketId}`);
                                }}
                            ><Link
                                to={`/docketdetail/${item.docketId}`}
                            >
                                XEM
                            </Link></Button>
                            {item.docketDetail}
                        </div>
                    </Col>
                    <Col span={3}/>

                </Row>
                )
            }

            return (
                <Row gutter={16} type="flex" justify="space-between" align="middle"
                        style={stylesPrime.docketListRowStatusWORKING}
            >
                <Col span={2} style={{textAlign: "center"}}>
                    <Tooltip
                        title={"ĐANG LÀM"}>
                        <Icon
                            type="thunderbolt"
                            style={{
                                color: "#1890ff",
                                fontSize: "30px",
                                margin: "-15px 0",
                                top: "5px",
                                position: "relative",
                            }}
                        />
                    </Tooltip>
                </Col>
                <Col span={19}>
                    <div style={styles.docketListRow_ColDocketDetail}>
                        <Button style={{
                            position: "absolute",
                            top: "-15px",
                            right: "-18%",
                            borderRadius: "20px",
                            width: "15.5%",
                            height: "calc(100% + 30px)",
                        }}
                                onClick={() => {
                                    this.props.history.push(`/docketdetail/${item.docketId}`);
                                }}
                        ><Link
                            to={`/docketdetail/${item.docketId}`}
                        >
                            XEM
                        </Link></Button>
                        {item.docketDetail}
                    </div>
                </Col>
                <Col span={3}/>

            </Row>
            )
        }
        if (item.status === DOCKETSTATUS.REJECTED) {
            return <Row gutter={16} type="flex" justify="space-between" align="middle"
                        style={stylesPrime.docketListRowStatusREJECTED}
            >
                <Col span={2} style={{textAlign: "center"}}>
                    <Tooltip
                        title={"Phía Thầu phụ đã thông báo công việc này hoàn thành, nhưng bạn đã từ chối xác nhận điều đó. Do đó nó bị trả ngược về Công nhân."}>
                        <Icon
                            type="warning"
                            style={{
                                fontSize: "30px",
                                margin: "-15px 0",
                                top: "5px",
                                position: "relative",
                                color: "red",
                            }}
                        />
                    </Tooltip>
                </Col>
                <Col span={19}>
                    <div style={styles.docketListRow_ColDocketDetail}>
                        <Button
                            style={{
                                position: "absolute",
                                top: "-15px",
                                right: "-18%",
                                borderRadius: "20px",
                                width: "15.5%",
                                height: "calc(100% + 30px)",
                            }}
                            onClick={() => {
                                this.props.history.push(`/docketdetail/${item.docketId}`);
                            }}
                        ><Link
                            to={`/docketdetail/${item.docketId}`}
                        >
                            XEM
                        </Link></Button>
                        {item.docketDetail}
                    </div>
                </Col>
                <Col span={3}/>

            </Row>
        }
        if (item.status === DOCKETSTATUS.SUB_CHECK) {
            return <Row gutter={16} type="flex" justify="space-between" align="middle"
                        style={stylesPrime.docketListRowStatusSUB_CHECK}
            >
                <Col span={21}>
                    <div style={styles.docketListRow_ColDocketDetail}>
                        <Button
                            style={{
                                position: "absolute",
                                top: "-15px",
                                right: "-16.4%",
                                borderRadius: "20px",
                                width: "14%",
                                height: "calc(100% + 30px)",
                            }}
                            onClick={() => {
                                this.props.history.push(`/docketdetail/${item.docketId}`);
                            }}
                        ><Link
                            to={`/docketdetail/${item.docketId}`}
                        >
                            XEM
                        </Link></Button>
                        {item.docketDetail}
                    </div>
                </Col>
                <Col span={3}/>

            </Row>
        }
        if (item.status === DOCKETSTATUS.NEED_APPROVE) {
            return <Row gutter={16} type="flex" justify="space-between" align="middle"
                        style={stylesPrime.docketListRowStatusNEEDAPPROVE}
            >
                <Col span={21}>
                    <div style={styles.docketListRow_ColDocketDetail}>
                        <Button type="primary" style={{
                            position: "absolute",
                            top: "-15px",
                            right: "-16.4%",
                            borderRadius: "20px",
                            width: "14%",
                            height: "calc(100% + 30px)",
                            fontSize: "13px"
                        }}
                                onClick={() => {
                                    this.props.history.push(`/docketdetail/${item.docketId}`);
                                }}
                        ><Link
                            to={`/docketdetail/${item.docketId}`}
                        >
                            Cần xác nhận
                        </Link></Button>
                        {item.docketDetail}
                    </div>
                </Col>
                <Col span={3}/>

            </Row>
        }
        if (item.status === DOCKETSTATUS.APPROVED) {
            return <Row gutter={16} type="flex" justify="space-between" align="middle"
                        style={stylesPrime.docketListRowStatusAPPROVED}
            >
                <Col span={21}>
                    <div style={styles.docketListRow_ColDocketDetail}>
                        <Button style={{
                            position: "absolute",
                            top: "-15px",
                            right: "-16.4%",
                            borderRadius: "20px",
                            width: "14%",
                            height: "calc(100% + 30px)",
                            color: "green",
                        }}
                                onClick={() => {
                                    this.props.history.push(`/docketdetail/${item.docketId}`);
                                }}
                        ><Link
                            to={`/docketdetail/${item.docketId}`}
                        >
                            XEM
                        </Link></Button>
                        {item.docketDetail}
                    </div>
                </Col>
                <Col span={3}/>

            </Row>
        }

    };
    renderSubDocketContentFollowStatus = (item) => {
        if (item.status === DOCKETSTATUS.DECLINED) {
            return <Row gutter={16} type="flex" justify="space-between" align="middle"
                        style={stylesSub.docketListRowStatusDECLINED}
            >
                <Col span={21}>
                    <div style={styles.docketListRow_ColDocketDetail}>
                        <Button
                            style={{
                                position: "absolute",
                                top: "-15px",
                                right: "-16.4%",
                                borderRadius: "20px",
                                width: "14%",
                                height: "calc(100% + 30px)",
                            }}
                            onClick={() => {
                                this.props.history.push(`/docketdetail/${item.docketId}`);
                            }}
                        ><Link
                            to={`/docketdetail/${item.docketId}`}
                        >
                            XEM
                        </Link></Button>
                        <span
                            style={{
                                color: "rgb(255, 64, 64)",
                                fontStyle: "italic",
                                fontSize: "16px"
                            }}
                        >Nội dung công việc không được bạn duyệt. Đã trả về cho Thầu chính điền lại.</span>
                    </div>
                </Col>
                <Col span={3}/>
            </Row>
        }

        if (item.status === DOCKETSTATUS.WILL_WORK) {
            return <Row gutter={16} type="flex" justify="space-between" align="middle"
                        style={stylesSub.docketListRowStatusWILL_WORK}
            >
                <Col span={21}>
                    <div style={styles.docketListRow_ColDocketDetail}>
                        <Button
                            style={{
                                position: "absolute",
                                top: "-15px",
                                right: "-16.4%",
                                borderRadius: "20px",
                                width: "14%",
                                height: "calc(100% + 30px)",
                            }}
                            onClick={() => {
                                this.props.history.push(`/docketdetail/${item.docketId}`);
                            }}
                        ><Link
                            to={`/docketdetail/${item.docketId}`}
                        >
                            XEM
                        </Link></Button>
                        {item.docketDetail}
                    </div>
                </Col>
                <Col span={3}/>

            </Row>
        }

        if (item.status === DOCKETSTATUS.FILLED) {
            return <Row gutter={16} type="flex" justify="space-between" align="middle"
                        style={stylesSub.docketListRowStatusFILLED}
            >
                <Col span={21}>
                    <div style={styles.docketListRow_ColDocketDetail}>
                        <Button type="primary" style={{
                            position: "absolute",
                            top: "-15px",
                            right: "-16.4%",
                            borderRadius: "20px",
                            width: "14%",
                            height: "calc(100% + 30px)",
                        }}
                                onClick={() => {
                                    this.props.history.push(`/docketdetail/${item.docketId}`);
                                }}
                        ><Link
                            to={`/docketdetail/${item.docketId}`}
                        >
                            Cần duyệt
                        </Link></Button>
                        {item.docketDetail}
                    </div>
                </Col>
                <Col span={3}/>

            </Row>
        }
        if (item.status === DOCKETSTATUS.CONFIRMED) {
            return <Row gutter={16} type="flex" justify="space-between" align="middle"
                        style={stylesSub.docketListRowStatusCONFIRMED}
            >
                <Col span={21}>
                    <div style={styles.docketListRow_ColDocketDetail}>
                        <Button
                            style={{
                                position: "absolute",
                                top: "-15px",
                                right: "-16.4%",
                                borderRadius: "20px",
                                width: "14%",
                                height: "calc(100% + 30px)",
                            }}
                            onClick={() => {
                                this.props.history.push(`/docketdetail/${item.docketId}`);
                            }}
                        ><Link
                            to={`/docketdetail/${item.docketId}`}
                        >
                            XEM
                        </Link></Button>
                        {item.docketDetail}
                    </div>
                </Col>
                <Col span={3}/>

            </Row>
        }
        if (item.status === DOCKETSTATUS.WORKING) {

            if (item.rejectReason) { // trường hợp sub reject docket
                return (
                    <Row gutter={16} type="flex" justify="space-between" align="middle"
                         style={stylesSub.docketListRowStatusREJECTED}
                    >
                        <Col span={2} style={{textAlign: "center"}}>
                            <Tooltip
                                title={"Công nhân đã xác nhận công việc này hoàn thành, nhưng bạn đã từ chối xác nhận điều đó. Do đó nó bị trả ngược về Công nhân."}>
                                <Icon
                                    type="warning"
                                    style={{
                                        fontSize: "30px",
                                        margin: "-15px 0",
                                        top: "5px",
                                        position: "relative",
                                        color: "red",
                                    }}
                                />
                            </Tooltip>
                        </Col>
                        <Col span={19}>
                            <div style={styles.docketListRow_ColDocketDetail}>
                                <Button
                                    style={{
                                        position: "absolute",
                                        top: "-15px",
                                        right: "-18%",
                                        borderRadius: "20px",
                                        width: "15.5%",
                                        height: "calc(100% + 30px)",
                                    }}
                                    onClick={() => {
                                        this.props.history.push(`/docketdetail/${item.docketId}`);
                                    }}
                                ><Link
                                    to={`/docketdetail/${item.docketId}`}
                                >
                                    XEM
                                </Link></Button>
                                {item.docketDetail}
                            </div>
                        </Col>
                        <Col span={3}/>

                    </Row>
                );
            }
                return (
                    <Row gutter={16} type="flex" justify="space-between" align="middle"
                        style={stylesSub.docketListRowStatusWORKING}
            >
                <Col span={2} style={{textAlign: "center"}}>
                    <Tooltip
                        title={"ĐANG LÀM"}>
                        <Icon
                            type="thunderbolt"
                            style={{
                                fontSize: "30px",
                                margin: "-15px 0",
                                top: "5px",
                                position: "relative",
                                color: "#1890ff",
                            }}
                        />
                    </Tooltip>
                </Col>
                <Col span={19}>
                    <div style={styles.docketListRow_ColDocketDetail}>
                        <Button
                            style={{
                                position: "absolute",
                                top: "-15px",
                                right: "-18%",
                                borderRadius: "20px",
                                width: "15.5%",
                                height: "calc(100% + 30px)",
                            }}
                            onClick={() => {
                                this.props.history.push(`/docketdetail/${item.docketId}`);
                            }}
                        ><Link
                            to={`/docketdetail/${item.docketId}`}
                        >
                            XEM
                        </Link></Button>
                        {item.docketDetail}
                    </div>
                </Col>
                <Col span={3}/>

            </Row>
                )
        }
        if (item.status === DOCKETSTATUS.REJECTED) {
            return <Row gutter={16} type="flex" justify="space-between" align="middle"
                        style={stylesSub.docketListRowStatusREJECTED}
            >
                <Col span={2} style={{textAlign: "center"}}>
                    <Tooltip
                        title={"Bạn đã xác nhận công việc này hoàn thành, nhưng Thầu chính đã từ chối xác nhận điều đó. Do đó nó bị trả ngược về Công nhân."}>
                        <Icon
                            type="warning"
                            style={{
                                fontSize: "30px",
                                margin: "-15px 0",
                                top: "5px",
                                position: "relative",
                                color: "red",
                            }}
                        />
                    </Tooltip>
                </Col>
                <Col span={19}>
                    <div style={styles.docketListRow_ColDocketDetail}>
                        <Button
                            style={{
                                position: "absolute",
                                top: "-15px",
                                right: "-18%",
                                borderRadius: "20px",
                                width: "15.5%",
                                height: "calc(100% + 30px)",
                            }}
                            onClick={() => {
                                this.props.history.push(`/docketdetail/${item.docketId}`);
                            }}
                        ><Link
                            to={`/docketdetail/${item.docketId}`}
                        >
                            XEM
                        </Link></Button>
                        {item.docketDetail}
                    </div>
                </Col>
                <Col span={3}/>

            </Row>
        }
        if (item.status === DOCKETSTATUS.SUB_CHECK) {
            return <Row gutter={16} type="flex" justify="space-between" align="middle"
                        style={stylesSub.docketListRowStatusSUB_CHECK}
            >
                <Col span={21}>
                    <div style={styles.docketListRow_ColDocketDetail}>
                        <Button type="primary" style={{
                            position: "absolute",
                            top: "-15px",
                            right: "-16.4%",
                            borderRadius: "20px",
                            width: "14%",
                            height: "calc(100% + 30px)",
                            fontSize: "13px"
                        }}
                                onClick={() => {
                                    this.props.history.push(`/docketdetail/${item.docketId}`);
                                }}
                        ><Link
                            to={`/docketdetail/${item.docketId}`}
                        >
                            Cần xác nhận
                        </Link></Button>
                        {item.docketDetail}
                    </div>
                </Col>
                <Col span={3}/>

            </Row>
        }
        if (item.status === DOCKETSTATUS.NEED_APPROVE) {
            return <Row gutter={16} type="flex" justify="space-between" align="middle"
                        style={stylesSub.docketListRowStatusNEEDAPPROVE}
            >
                <Col span={21}>
                    <div style={styles.docketListRow_ColDocketDetail}>
                        <Button
                            style={{
                                position: "absolute",
                                top: "-15px",
                                right: "-16.4%",
                                borderRadius: "20px",
                                width: "14%",
                                height: "calc(100% + 30px)",
                            }}
                            onClick={() => {
                                this.props.history.push(`/docketdetail/${item.docketId}`);
                            }}
                        ><Link
                            to={`/docketdetail/${item.docketId}`}
                        >
                            XEM
                        </Link></Button>
                        {item.docketDetail}
                    </div>
                </Col>
                <Col span={3}/>

            </Row>
        }
        if (item.status === DOCKETSTATUS.APPROVED) {
            return <Row gutter={16} type="flex" justify="space-between" align="middle"
                        style={stylesSub.docketListRowStatusAPPROVED}
            >
                <Col span={21}>
                    <div style={styles.docketListRow_ColDocketDetail}>
                        <Button
                            style={{
                                position: "absolute",
                                top: "-15px",
                                right: "-16.4%",
                                borderRadius: "20px",
                                width: "14%",
                                height: "calc(100% + 30px)",
                                color: "green",
                            }}
                            onClick={() => {
                                this.props.history.push(`/docketdetail/${item.docketId}`);
                            }}
                        ><Link
                            to={`/docketdetail/${item.docketId}`}
                        >
                            XEM
                        </Link></Button>
                        {item.docketDetail}
                    </div>
                </Col>
                <Col span={3}/>

            </Row>
        }

    };

    renderDocketList = (docketList) => {
        if (docketList) {
            let docketListTemp = docketList;
            return docketListTemp.map((item) => {
                if (item.docketDetail === null) {
                    item.docketDetail = "";
                } else {
                    item.docketDetail = item.docketDetail.replace(/^\s+|\s+$/g, '');
                }
                if (item.status === DOCKETSTATUS.NOT_WORK) return null;
                if (item.status === DOCKETSTATUS.INIT) return null;
                if (item.date === this.state.today) {
                    return (
                        <Row
                            key={`${item.docketId}today`}
                            gutter={16} type="flex" justify="space-between" align="middle"
                            style={styles.docketListOuterRowToday}>
                            <Col span={3} style={styles.docketListRow_dateToday}>
                                {this.renderDateInDocketList(item)}
                            </Col>
                            <Col span={21}>
                                <Spin
                                    spinning={((this.props.isFetching) && (this.checkActionSpin === `${item.docketId}${item.status}`)) ? (this.props.isFetching) : false}
                                    size="large">
                                    {(this.props.userrole === "prime") ? this.renderPrimeDocketContentFollowStatus(item) : this.renderSubDocketContentFollowStatus(item)}
                                </Spin>
                            </Col>
                        </Row>
                    )
                }
                return (
                    <Row
                        key={`${item.docketId}${item.date}`}
                        gutter={16} type="flex" justify="space-between" align="middle"
                        style={styles.docketListOuterRow}>
                        <Col span={3} style={styles.docketListRow_date}>
                            {this.renderDateInDocketList(item)}
                        </Col>
                        <Col span={21}>
                            <Spin
                                spinning={((this.props.isFetching) && (this.checkActionSpin === `${item.docketId}${item.status}`)) ? (this.props.isFetching) : false}
                                size="large">
                                {(this.props.userrole === "prime") ? this.renderPrimeDocketContentFollowStatus(item) : this.renderSubDocketContentFollowStatus(item)}
                            </Spin>
                        </Col>
                    </Row>
                )

            })
        }
    };

    componentWillMount() {
        this.props.loadAllDocketByContractId(this.props.record.contractId);
        this.contractIdForPreventReload = this.props.record.contractId;
        this.docketList = [];
        this.isFetchingThisDocketList = false;
    };

    componentDidUpdate() {
        this.styleForAnt();
    };

    shouldComponentUpdate() {
        if (this.props.allDocketFromContractId) {
            if (this.isFetchingThisDocketList === true) {
                return false;
            } else if ((this.props.allDocketFromContractId[0]) && (this.props.allDocketFromContractId[0].contractId === this.contractIdForPreventReload)) {
                this.docketList = this.props.allDocketFromContractId;
                this.isFetchingThisDocketList = true;
                return true;
            } else return true;
        } else return false;
    };

    render() {

        // style for dot in Steps of ant
        const iconDotStyle = {
            fontSize: "25px",
            position: "relative",
            top: "-16px",
            left: "-12px",
            background: "rgb(251, 251, 251)",
            padding: "5px",
            color: COLOR.primaryBlue
        };
        const dotStyleByStatus = (statusIndex, statusStep) => {
            if (statusIndex === 0) {
                return (
                    <Icon type="calendar"
                          style={{
                              fontSize: "25px",
                              position: "relative",
                              top: "-16px",
                              left: "-12px",
                              background: "rgb(251, 251, 251)",
                              padding: "5px",
                              color: "#aaa"
                          }}
                    />
                )
            } else if (statusIndex === 1) {
                return (
                    <Icon type="edit" style={iconDotStyle}/>
                )
            } else if (statusIndex === 2) {
                return (
                    <Icon type="file-done"
                          style={{
                              fontSize: "25px",
                              position: "relative",
                              top: "-16px",
                              left: "-12px",
                              background: "rgb(251, 251, 251)",
                              padding: "5px",
                              color: "green"
                          }}
                    />
                )
            } else if (statusIndex === 3) {
                return (
                    <Icon type="setting" spin
                          style={{
                              fontSize: "25px",
                              position: "relative",
                              top: "-16px",
                              left: "-12px",
                              background: "rgb(251, 251, 251)",
                              padding: "5px",
                              color: "#d48900"
                          }}
                    />
                )
            } else if (statusIndex === 4) {
                return (
                    <Icon type="eye" style={iconDotStyle}/>
                )
            } else if (statusIndex === 5) {
                return (
                    <Icon type="eye" style={iconDotStyle}/>
                )
            } else if (statusIndex === 6) {
                return (
                    <Icon type="check-circle"
                          style={{
                              fontSize: "25px",
                              position: "relative",
                              top: "-16px",
                              left: "-12px",
                              background: "rgb(251, 251, 251)",
                              padding: "5px",
                              color: "green"
                          }}
                    />
                )
            }
        };
        const customDot = (dot, {status, index}) => (
            <div>
                {dotStyleByStatus(index, status)}
            </div>
        );


        return (
            <Spin
                spinning={(this.isFetchingThisDocketList === false) ? (this.props.isFetchingAllDocketFromContractId) : false}
                size="large">
                <div style={{
                    margin: "0 0 0 -50px",
                    padding: "30px 0",
                    background:"repeating-linear-gradient(to right, #fbfbfb 0%, #fbfbfb 14.2%, rgb(220, 220, 220) 14.3%, #fbfbfb 14.4%) -2.25vw",
                }}
                >
                    <Row type="flex" justify="space-between" align="bottom" style={{marginBottom:"30px", marginTop:"-10px"}}>
                        <Col offset={1} span={23} style={{width: "100%",}}>
                            <Affix offsetTop={this.state.top}>
                            <Steps size="small" progressDot={customDot} current={-1}
                                       style={{
                                           width: "100%",
                                           background: "linear-gradient(to right, rgba(251, 251, 251, 0) 0%, rgba(251, 251, 251, 1) 1%,rgba(251, 251, 251, 1) 97%, rgba(251, 251, 251, 0) 97%)",
                                           paddingTop: "20px",
                                       }}>
                                    <Step description={<span style={{fontSize:"12px"}}>Thầu chính đã chọn ngày làm việc</span>}/>
                                    <Step description={<span style={{fontSize:"12px"}}>Thầu chính đã điền nội dung công việc</span>}/>
                                    <Step description={<span style={{fontSize:"12px"}}>Thầu phụ đã duyệt công việc</span>}/>
                                    <Step description={<span style={{fontSize:"12px"}}>Công nhân đang làm</span>}/>
                                    <Step description={<span style={{fontSize:"12px"}}>Chờ Thầu phụ kiểm tra</span>}/>
                                    <Step description={<span style={{fontSize:"12px"}}>Chờ thầu chính xác nhận</span>}/>
                                    <Step description={<span style={{fontSize:"12px"}}>Xong!</span>}/>
                                </Steps>
                            </Affix>
                        </Col>
                    </Row>

                <Timeline style={{borderLeft: "2px solid #e8e8e8"}}>
                        {(this.isFetchingThisDocketList === false) ? this.renderDocketList(this.props.allDocketFromContractId) : this.renderDocketList(this.docketList)}
                    </Timeline>
                </div>
            </Spin>
        );
    }

    styleForAnt = () => {
        let antDesignItem = document.getElementsByClassName("ant-timeline-item-head-blue");
        if (antDesignItem) {
            for (let i = 0; i < antDesignItem.length; i++) {
                antDesignItem[i].style.marginTop = "10px";
            }
        }
    };

}

const styles = {
    docketListRow_ColDocketDetail: {
        overflow: "hidden",
        whiteSpace: "pre-wrap",
        fontSize:"12px"
    },
    docketListOuterRow: {},
    docketListOuterRowToday: {
        margin: "15px -8px",
    },

    docketListRow_date: {
        background: "linear-gradient(to right, rgb(251, 251, 251) 0%, rgb(226, 226, 226) 20%, rgb(251, 251, 251) 100%)"
    }
    , docketListRow_dateToday: {
        background: "linear-gradient(to right, rgb(251, 251, 251) 0%, rgb(255, 246, 0) 20%, rgb(251, 251, 251) 100%)"
    },
    docketListOuterRow_ColDate: {},
};

const stylesPrime = {
    docketListRowStatusDECLINED: {
        background: "linear-gradient(to right, rgb(174, 255, 202) 0%, rgb(239, 239, 239) 0%)",
        borderRadius: "20px",
        margin: "5px -8px 5px -20px",
        padding: "15px 15px",
        color: "rgb(156, 156, 156)",
        fontStyle: "italic",
    },
    docketListRowStatusFILLED: {
        background: "linear-gradient(to right, rgb(168, 213, 255) 15.5%, rgb(239, 239, 239) 15.3%)",
        borderRadius: "20px",
        margin: "5px -8px",
        padding: "15px 15px",
        color: "#000"
    },
    docketListRowStatusCONFIRMED: {
        background: "linear-gradient(to right, rgb(142, 255, 145) 31.6%, rgb(239, 239, 239) 31.6%)",
        borderRadius: "20px",
        margin: "5px -8px",
        padding: "15px 15px",
        color: "#000"
    },
    docketListRowStatusWORKING: {
        background: "linear-gradient(to right, rgb(255, 251, 135) 47.8%, rgb(239, 239, 239) 47.8%)",
        borderRadius: "20px",
        margin: "5px -8px",
        padding: "15px 15px",
        color: "#000"
    },
    docketListRowStatusNEEDAPPROVE: {
        background: "linear-gradient(to right, rgb(168, 213, 255) 80.4%, rgb(239, 239, 239) 80.4%)",
        transition: "2s",
        borderRadius: "20px",
        margin: "5px -8px",
        padding: "15px 15px",
        color: "#000"
    },
    docketListRowStatusAPPROVED: {
        background: "rgb(72, 165, 75)",
        borderRadius: "20px",
        margin: "5px -8px",
        padding: "15px 15px",
        color: "#fff"
    },
    docketListRowStatusWILL_WORK: {
        background: "linear-gradient(to right, rgb(174, 255, 202) 0%, rgb(239, 239, 239) 0%)",
        borderRadius: "20px",
        margin: "5px -8px 5px -20px",
        padding: "15px 15px",
        color: "#000"
    },
    docketListRowStatusREJECTED: {
        background: "linear-gradient(to right, rgb(255, 188, 188) 47.8%, rgb(239, 239, 239) 47.8%)",
        borderRadius: "20px",
        margin: "5px -8px",
        padding: "15px 15px",
        color: "#000"
    },
    docketListRowStatusSUB_CHECK: {
        background: "linear-gradient(to right, rgb(168, 213, 255) 64.1%, rgb(239, 239, 239) 64.1%)",
        borderRadius: "20px",
        margin: "5px -8px",
        padding: "15px 15px",
        color: "#000"
    },

};
const stylesSub = {
    docketListRowStatusDECLINED: {
        background: "linear-gradient(to right, rgb(174, 255, 202) 0%, rgb(239, 239, 239) 0%)",
        borderRadius: "20px",
        margin: "5px -8px 5px -20px",
        padding: "15px 15px",
        color: "#575757",
        fontSize: "13px"
    },

    docketListRowStatusFILLED: {
        background: "linear-gradient(to right, rgb(168, 213, 255) 15.3%, rgb(239, 239, 239) 15.3%)",
        borderRadius: "20px",
        margin: "5px -8px",
        padding: "15px 15px",
        color: "#000"
    },
    docketListRowStatusCONFIRMED: {
        background: "linear-gradient(to right,  rgb(142, 255, 145) 31.6%, rgb(239, 239, 239) 31.6%)",
        borderRadius: "20px",
        margin: "5px -8px",
        padding: "15px 15px",
        color: "#000"
    },
    docketListRowStatusWORKING: {
        background: "linear-gradient(to right, rgb(255, 251, 135) 47.8%, rgb(239, 239, 239) 47.8%)",
        borderRadius: "20px",
        margin: "5px -8px",
        padding: "15px 15px",
        color: "#000"
    },
    docketListRowStatusNEEDAPPROVE: {
        background: "linear-gradient(to right,  rgb(168, 213, 255) 80.4%, rgb(239, 239, 239) 80.4%)",
        borderRadius: "20px",
        margin: "5px -8px",
        padding: "15px 15px",
        color: "#000"
    },
    docketListRowStatusAPPROVED: {
        background: "rgb(72, 165, 75)",
        borderRadius: "20px",
        margin: "5px -8px",
        padding: "15px 15px",
        color: "#fff"
    },
    docketListRowStatusWILL_WORK: {
        background: "linear-gradient(to right, rgb(174, 255, 202) 0%, rgb(239, 239, 239) 0%)",
        borderRadius: "20px",
        margin: "5px -8px 5px -20px",
        padding: "15px 15px",
        color: "#000"
    },
    docketListRowStatusREJECTED: {
        background: "linear-gradient(to right, rgb(255, 188, 188) 47.8%, rgb(239, 239, 239) 47.8%)",
        borderRadius: "20px",
        margin: "5px -8px",
        padding: "15px 15px",
        color: "#000"
    },
    docketListRowStatusSUB_CHECK: {
        background: "linear-gradient(to right, rgb(168, 213, 255) 64.1%, rgb(239, 239, 239) 64.1%)",
        borderRadius: "20px",
        margin: "5px -8px",
        padding: "15px 15px",
        color: "#000"
    },
};

function mapStateToProps(state) {

    return {
        isFetchingAllDocketFromContractId: state.docketListByContractIdReducer.isFetchingAllDocketFromContractId,
        isFetching: state.docketListByContractIdReducer.isFetching,
        allDocketFromContractId: state.docketListByContractIdReducer.allDocketWaitFill,
    };
}

export default connect(
    mapStateToProps,
    {loadAllDocketByContractId}
)(DocketListByContractId);