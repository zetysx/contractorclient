import React, { Component } from 'react';
import { connect } from 'react-redux';

import { loadContractPayment } from "../../../actions/viewContractPaymentAction";
import { loadAllContractByPrimeId } from "../../../actions/primeOnGoingContractAction";
import { loadAllDocketByContractId } from "../../../actions/docketListByContractIdAction";
import { getAllFormByPrimeId } from '../../../actions/primeFormManageAction';
import { cancelContract } from "../../../actions/primeCancelContractAction";

import PrimeFillDocketTab from "./PrimeFillDocket";
import { ViewContractPayment } from "../../../components/ViewContractPayment";
import DocketListFromContractId from "../DocketListByContractId";
import ViewWorkerList from "../../../components/ViewWorkerList";
import PrimeAddFormToContract from "./PrimeAddFormToContract";

import {
    Input,
    Table,
    Button,
    Modal,
    Tabs,
    Spin,
    Tooltip,
    Icon,
    Drawer,
    Row,
    Col,
    message
} from 'antd';
import { Link } from "react-router-dom";
import { COLOR } from "../../../constants/theme";

const TabPane = Tabs.TabPane;

class PrimeOnGoingContract extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentAddContract: [],
            currentDocketItem: null,
            expandedRowKeys: [],
            expandRowContractId: "",
            activeKey: "1",
            visibleCancelModal : false,
            visible: false,
            cancellable: false,
        };
    }

    componentDidMount() {
        this.props.loadAllContractByPrimeId(this.props.profile.userId);
    };

    renderPaymenModal = () => {
        return (
            <Modal className={'PrimeSearchModal'}
                title="Hoá đơn dự án"
                visible={this.state.visibleModal}
                onOk={this._onModalOk}
                onCancel={this._onModalOk}
                centered={true}
                closable={true}
                destroyOnClose={true}
                width={"90%"}
            >
                <Spin spinning={this.props.isFetchingContractReducer} size={"large"}>
                    <ViewContractPayment contractPayment={this.props.contractPayment} />
                </Spin>
            </Modal>
        );
    }

    renderCancelContractModal = () => {

        return (
            <Modal 
                title="Dừng dự án đang làm"
                visible={this.state.visibleCancelModal}
                onOk={this._onCancelModalOk}
                onCancel={this._onClickCloseCancelContractModal}
                onClose={this._onClickCloseCancelContractModal}
                centered={true}
                closable={true}
                destroyOnClose={true}
            >
                <Spin spinning={this.props.isFetchingContractReducer} size={"large"}>
                    <Row style={{
                        padding:"10px 30px",
                        fontSize: "16px",
                        color: "red",
                        fontWeight: "bold",
                    }}>
                        Bạn sẽ dừng vĩnh viễn dự án này. Dự án bị dừng sẽ không thể khôi phục lại.
                    </Row>
                    <Row style={{padding:"10px 30px", color:"#000"}}>
                        Khi bạn dừng dự án này, toàn bộ các công việc chưa hoàn thành sẽ bị dừng.
                        <br/>
                        Các bên liên quan sẽ được thông báo về việc dừng này.
                    </Row>
                    <Row style={{
                        padding:"20px 30px",
                        fontStyle: "italic",
                    }}>
                    Để đảm bảo hành động này không phải tai nạn, vui lòng nhập lại các kí tự dưới đây:
                        <br/>
                        <img
                            alt={"xác nhận huỷ dự án"}
                             src={require("../../../assets/xacnhanhuyduan.PNG")}
                            style={{
                            display: "block",
                            width: "200px",
                        }}/>
                    </Row>
                    <Input
                        onChange={(event) => {
                            const mustMatchText = "XACNHANHUYDUAN";
                            if(event.target.value === mustMatchText) {
                                this.setState({cancellable:true});
                            }                            
                        }}
                    />
                </Spin>
            </Modal>
        );
    }

    render() {
        const columns = [
            {
                title: <Tooltip title="Mã dự án. Mỗi dự án sẽ có một mã số duy nhất.">
                    Mã <Icon type="question-circle" />
                </Tooltip>,
                dataIndex: '',
                key: 'contractId',
                render: (text, record, index) => {
                    return <Tooltip title="Mã dự án. Mỗi dự án sẽ có một mã số duy nhất.">
                        <div style={{
                            padding: "2px 5px",
                            background: "rgb(162, 162, 162)",
                            textAlign: "center",
                            color: "#fff",
                            borderRadius: " 5px",
                            fontWeight: "bold",
                            fontSize: "12px",
                        }}>{record.contractId}</div>
                    </Tooltip>;
                }
            },
            {
                title: 'Thầu phụ',
                dataIndex: '',
                key: 'subCompanyName',
                render: (text, record, index) =>
                    <Link
                        to={`/profile/${record.subAccId}`}>{record.subCompanyName}</Link>
            },
            {
                title: 'Lĩnh vực',
                dataIndex: 'profession',
                key: 'profession',
            },
            {
                title: 'Giá/ngày/người',
                dataIndex: '',
                key: 'pricePerDocket',
                render: (record) => {
                    return (
                        <span>{`${parseInt(record.pricePerDocket / 1000)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            <span
                                style={{
                                    fontWeight: "normal",
                                    margin: "0 1px",
                                }}>,</span><span
                                    style={{
                                        fontWeight: "normal",

                                    }}>000</span><sup
                                        style={{
                                            fontWeight: "normal",
                                            fontSize: "10px",
                                            right: "-5px",
                                            color: "#ad8700",
                                        }}>đ</sup></span>
                    )
                }
            },
            {
                title: 'Thời gian',
                dataIndex: '',
                key: 'Time',
                render: (text, record, index) => `${record.dateStart} - ${record.dateEnd}`
            },
            {
                title: 'Nhân công',
                dataIndex: 'workerNumber',
                key: 'workerNumber',
                render: (text, record, index) => {
                    return (
                        <Tooltip title={"Xem chi tiết"}>
                            <Button onClick={(event) => {
                                event.stopPropagation();
                                this.workerForDrawer = record.workerDTOS;
                                this.setState({ visible: true });
                            }}>
                                {record.workerDTOS.length}
                            </Button>
                        </Tooltip>
                    )
                }
            },
            {
                title: 'Địa điểm',
                dataIndex: 'locationDetail',
                key: 'locationDetail',
            },
            {
                title: <Tooltip
                    title={"Tổng số tiền mà Thầu chính phải trả cho Thầu phụ, tính theo số công việc đã hoàn thành"}>Tổng
                    hoá đơn cả dự án <Icon type="question-circle" /></Tooltip>,
                dataIndex: "",
                key: "",
                render: (text, record, index) => {
                    const contractId = record.contractId;
                    if (record.totalPrice) {
                        return (
                            <Tooltip title={"Xem chi tiết"}>
                                <Button type="default"
                                    onClick={(e) => this._onClickShowPaymentModal(e, contractId)}>
                                    <span>{`${parseInt(record.totalPrice / 1000)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        <span
                                            style={{
                                                fontWeight: "normal",
                                                margin: "0 1px",
                                            }}>,</span><span
                                                style={{
                                                    fontWeight: "normal",

                                                }}>000</span><sup
                                                    style={{
                                                        fontWeight: "normal",
                                                        fontSize: "10px",
                                                        right: "-5px",
                                                        color: "#ad8700",
                                                    }}>đ</sup></span>
                                </Button>
                            </Tooltip>
                        )
                    }
                    return (
                        <span
                            style={{
                                fontSize: "11px",
                                color: "red"
                            }}
                        >Chưa có công việc nào hoàn thành</span>
                    )
                        ;
                }
            }
        ];

        return (

            <div style={{ padding: "50px" }}>
                <Tabs defaultActiveKey="1"
                    activeKey={this.state.activeKey}
                    onChange={(activeKey) => this._onChangeTab(activeKey)}

                >

                    <TabPane tab="Danh sách dự án" key="1">
                        <Drawer
                            title={"Danh sách nhân công"}
                            placement="right"
                            closable={false}
                            onClose={this._onClickCloseModal}
                            visible={this.state.visible}
                        >
                            <ViewWorkerList workerDTOS={this.workerForDrawer} />
                        </Drawer>

                        {this.renderPaymenModal()}
                        {this.renderCancelContractModal()}
                        <Spin spinning={this.props.isFetchingContractReducer} size={"large"}>
                            <Table pagination={false}
                                style={{ background: "#fff" }}
                                bodyStyle={{ fontSize: "12px" }}
                                expandRowByClick={true}
                                columns={columns} dataSource={this.props.contractReducer}
                                rowKey={record => record.contractId}
                                expandedRowRender={(record) => {
                                    if (record.formId === null) {
                                        return (
                                            <div>
                                                <Row
                                                    style={{
                                                        padding: "10px 0",
                                                        fontWeight: "bold",
                                                    }}
                                                >Trước khi bắt đầu công việc, bạn cần thiết lập sơ khởi cho toàn dự
                                                       án.<br />Mời bạn lần lượt thực hiện các bước:</Row>
                                                <Row type="flex" justify="space-between" align="middle"
                                                    style={{
                                                        padding: "5px 20px",
                                                        color: COLOR.primaryBlue,
                                                        fontWeight: "bold"
                                                    }}
                                                >
                                                    <Col span={10}>
                                                        1. Chọn mẫu chấm công:
                                                       </Col>
                                                    <Col span={14}>
                                                        <Button
                                                            key={`${record.contractId}form`}
                                                            style={{ width: "200px" }}
                                                            onClick={() => {
                                                                this.setState({ currentAddContract: record }, () => {
                                                                    this.props.getAllFormByPrimeId(this.props.profile.userId);
                                                                    this._onChangeTab("3");
                                                                });
                                                            }}
                                                        >
                                                            Chọn mẫu chấm công
                                                           </Button>
                                                    </Col>
                                                </Row>
                                                <Row
                                                    type="flex"
                                                    justify="space-between"
                                                    align="middle"
                                                    style={{ padding: "5px 20px" }}
                                                >
                                                    <Col span={10}>
                                                        2. Chọn ngày làm việc và tóm tắt sơ lược từng ngày:
                                                       </Col>
                                                    <Col span={14}>
                                                        <Button
                                                            type={"primary"}
                                                            disabled
                                                            style={{ width: "200px" }}
                                                        >
                                                            Chọn ngày làm việc
                                                           </Button>
                                                    </Col>
                                                </Row>
                                                <Row
                                                    style={{
                                                        textAlign:"right",
                                                        margin:"20px"
                                                    }}>
                                                    <Button
                                                        type="danger"
                                                        onClick={(e) => this._onClickShowCancelContractModal(e, record.contractId)}
                                                    >
                                                        Dừng dự án
                                                    </Button>
                                                </Row>
                                            </div>
                                        )
                                    } else if (record.status !== 3) {
                                        return (
                                            <div>
                                                <Row
                                                    style={{
                                                        padding: "10px 0",
                                                        fontWeight: "bold",
                                                    }}
                                                >Trước khi bắt đầu công việc, bạn cần thiết lập sơ khởi cho toàn dự
                                                       án.<br />Mời bạn lần lượt thực hiện các bước:</Row>
                                                <Row type="flex" justify="space-between" align="middle"
                                                    style={{
                                                        padding: "5px 20px",
                                                    }}
                                                >
                                                    <Col span={10}>
                                                        1. Chọn mẫu chấm công:
                                                       </Col>
                                                    <Col span={14}>
                                                        <Button
                                                            key={`${record.contractId}form`}
                                                            style={{ width: "200px" }}
                                                            disabled
                                                        >
                                                            Chọn mẫu chấm công
                                                           </Button>
                                                        <Icon
                                                            theme="filled"
                                                            type="check-circle"
                                                            style={{
                                                                color: "green",
                                                                fontSize: "18px",
                                                                marginLeft: "10px",
                                                            }} />
                                                    </Col>
                                                </Row>
                                                <Row
                                                    type="flex"
                                                    justify="space-between"
                                                    align="middle"
                                                    style={{
                                                        padding: "5px 20px",
                                                        color: COLOR.primaryBlue,
                                                        fontWeight: "bold"
                                                    }}
                                                >
                                                    <Col span={10}>
                                                        2. Chọn ngày làm việc và tóm tắt sơ lược từng ngày:
                                                       </Col>
                                                    <Col span={14}>
                                                        <Button
                                                            type={"primary"}
                                                            style={{ width: "200px" }}
                                                            onClick={() => {
                                                                this._onClickFillDocketDetail(record);
                                                            }}
                                                        >
                                                            Chọn ngày làm việc
                                                           </Button>
                                                    </Col>
                                                </Row>
                                                <Row
                                                    style={{
                                                        textAlign:"right",
                                                        margin:"20px"
                                                    }}>
                                                    <Button
                                                        type="danger"
                                                        onClick={(e) => this._onClickShowCancelContractModal(e, record.contractId)}
                                                    >
                                                        Dừng dự án
                                                    </Button>
                                                </Row>

                                            </div>
                                        )
                                    } else {
                                        return <div>
                                        <DocketListFromContractId
                                            history={this.props.history} userrole={"prime"}
                                            record={record}
                                            _onClickFillDocketDetail={this._onClickFillDocketDetail}
                                        />
                                            <Row
                                                style={{
                                                textAlign:"right",
                                                margin:"20px"
                                            }}>
                                                <Button
                                                    type="danger"
                                                    onClick={(e) => this._onClickShowCancelContractModal(e, record.contractId)}
                                                >
                                                    Dừng dự án
                                                </Button>
                                            </Row>
                                        </div>
                                        ;
                                    }
                                }} />
                        </Spin>

                    </TabPane>
                    <TabPane disabled tab="Chọn mẫu chấm công cho dự án" key="3">
                        <PrimeAddFormToContract history={this.props.history}
                            currentAddContract={this.state.currentAddContract}
                            _onChangeTab={this._onChangeTab} />
                    </TabPane>
                    <TabPane disabled tab="Chọn ngày làm việc" key="2">
                        <PrimeFillDocketTab
                            history={this.props.history}
                            allDocketFromContractId={this.props.allDocketFromContractId}
                            currentAddContract={this.state.currentAddContract}
                            currentDocketItem={this.state.currentDocketItem}
                            _onChangeTab={this._onChangeTab} />
                    </TabPane>
                </Tabs>

            </div>
        );

    }

    _onClickFillDocketDetail = (record, docketItem) => {
        this.setState({ currentAddContract: record, currentDocketItem: docketItem }, () => {
            this.props.loadAllDocketByContractId(this.state.currentAddContract.contractId);
            this._onChangeTab("2");
        });
    };

    /* tabs */
    _onChangeTab = (activeKey) => {
        if (activeKey === "1") {
            this.props.loadAllContractByPrimeId(this.props.profile.userId);
        }
        this.setState(
            {
                activeKey: activeKey
            }
        )
    };

    _onClickShowPaymentModal = (e, contractId) => {
        e.stopPropagation();
        this.props.loadContractPayment(contractId);
        this.setState({ visibleModal: true });
    };

    _onClickShowCancelContractModal = (e, contractId) => {
        e.stopPropagation();
        this.setState({ visibleCancelModal: true });
        this.setState({cancelContractId: contractId});
    };

    _onClickCloseCancelContractModal = (e) => {
        e.stopPropagation();
        this.setState({ visibleCancelModal: false });
    };

    _onModalOk = () => {
        this.setState({ visibleModal: false });
    };

    _onCancelModalOk = () => {
        if(this.state.cancellable && this.state.cancelContractId) {
            this.props.cancelContract(this.state.cancelContractId);
            this.setState({ visibleCancelModal: false });
        } else {
            message.warning("Mã xác nhận không chính xác!");
        }
    };

    _onClickCloseModal = () => {
        this.setState({
            visible: false,
        });
    };
}

const mapStateToProps = (state) => {
    return {
        contractPayment: state.viewContractPaymentReducer.contractPayment,
        profile: state.authReducer.profile,
        contractReducer: state.primeOnGoingContractReducer.contracts,
        isFetchingContractReducer: state.primeOnGoingContractReducer.isFetching,
        allDocketFromContractId: state.docketListByContractIdReducer.allDocketWaitFill,
    }
};

const mapDispatchActionToProps = {
    loadAllContractByPrimeId,
    loadAllDocketByContractId,
    getAllFormByPrimeId,
    loadContractPayment,
    cancelContract
};

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(PrimeOnGoingContract);