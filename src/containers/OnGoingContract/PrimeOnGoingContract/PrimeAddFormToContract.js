import React, {Component} from 'react';
import {connect} from 'react-redux';

import {getAllFormByPrimeId} from '../../../actions/primeFormManageAction';
import {addFormToContract} from '../../../actions/primeOnGoingContractAction';


import StaticForm from '../../PrimeFormManage/StaticForm';
import {
    Row,
    Col,
    Card,
    Spin,
    Table, Tooltip, Icon, Collapse
} from 'antd';
import {Link} from "react-router-dom";

const Panel = Collapse.Panel;


class PrimeAddFormToContract extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }


    componentDidMount() {
        this.props.getAllFormByPrimeId(this.props.profile.userId);
    };

    renderFormCardAddButton(formId, index) {
        return (
            <div style={{
                color: "#1890ff",
                fontSize: "16px",
                fontWeight: "bold",
                letterSpacing: "1px",
            }} onClick={() => {
                this._onClickAddFormButton(formId, index)
            }}>
                Chọn
            </div>
        );
    };


    renderCardStaticForm(form) {
        const formControl = JSON.parse(form.formControls);
        return (
            <StaticForm formControl={formControl}/>
        );
    }

    renderFormListCard() {
        const styles = {
            formContainer: {
                paddingBottom: 16,
                paddingRight: 8,
                paddingLeft: 8
            }
        };

        const {formList} = this.props;

        if (!formList) {
            return null;
        }
        ;

        return formList.map((form, index) => {
            return (
                <Col span={6} style={styles.formContainer} key={form.formId}>
                    <Card key={form.formId}
                          cover={this.renderCardStaticForm(form)}
                          actions={[this.renderFormCardAddButton(form.formId, index)]}
                    >
                        <Card.Meta
                            title={form.formName}
                            description={form.createdTime}
                        />
                    </Card>
                </Col>
            );
        });
    };

    render() {
        const contractColumns = [
            {
                title: <Tooltip title="Mã dự án. Mỗi dự án sẽ có một mã số duy nhất.">
                    Mã <Icon type="question-circle"/>
                </Tooltip>,
                dataIndex: '',
                key: 'contractId',
                render: (text, record, index) => {
                    return <Tooltip title="Mã dự án. Mỗi dự án sẽ có một mã số duy nhất.">
                        <div style={{
                            padding: "2px 5px",
                            background: "rgb(162, 162, 162)",
                            textAlign: "center",
                            color: "#fff",
                            borderRadius: " 5px",
                            fontWeight: "bold",
                            fontSize: "15px",
                        }}>{record.contractId}</div>
                    </Tooltip>;
                }
            },
            {
                title: 'Thầu phụ',
                dataIndex: '',
                key: 'subCompanyName',
                render: (text, record, index) =>
                    <Link
                        to={`/profile/${record.subAccId}`}>{record.subCompanyName}</Link>
            },
            {
                title: 'Lĩnh vực',
                dataIndex: 'profession',
                key: 'profession',
            },
            {
                title: 'Giá/ngày/người',
                dataIndex: '',
                key: 'pricePerDocket',
                render: (record) => {
                    return (
                        <span>{`${parseInt(record.pricePerDocket / 1000)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            <span
                                style={{
                                    fontWeight: "normal",
                                    margin: "0 1px",
                                }}>,</span><span
                                style={{
                                    fontWeight: "normal",

                                }}>000</span><sup
                                style={{
                                    fontWeight: "normal",
                                    fontSize: "10px",
                                    right: "-5px",
                                    color: "#ad8700",
                                }}>đ</sup></span>
                    )
                }
            },
            {
                title: 'Thời gian',
                dataIndex: '',
                key: 'Time',
                render: (text, record, index) => `${record.dateStart} - ${record.dateEnd}`
            },
            {
                title: 'Nhân công',
                dataIndex: 'workerNumber',
                key: 'workerNumber',
                render: (text, record, index) => record.workerDTOS.length
            },
            {
                title: 'Địa điểm',
                dataIndex: 'locationDetail',
                key: 'locationDetail',
            },
            {
                title: <Tooltip
                    title={"Tổng số tiền mà Thầu chính phải trả cho Thầu phụ, tính theo số công việc đã hoàn thành"}>Tổng
                    hoá đơn cả dự án <Icon type="question-circle"/></Tooltip>,
                dataIndex: "",
                key: "",
                render: (text, record, index) => {
                    if (record.totalPrice) {
                        return (
                            <span>{`${parseInt(record.totalPrice / 1000)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                <span
                                    style={{
                                        fontWeight: "normal",
                                        margin: "0 1px",
                                    }}>,</span><span
                                    style={{
                                        fontWeight: "normal",

                                    }}>000</span><sup
                                    style={{
                                        fontWeight: "normal",
                                        fontSize: "10px",
                                        right: "-5px",
                                        color: "#ad8700",
                                    }}>đ</sup></span>
                        )
                    }
                    return (
                        <span
                            style={{
                                fontSize: "11px",
                                color: "red"
                            }}
                        >Chưa có công việc nào hoàn thành</span>
                    )
                        ;
                }
            }
        ];


        return (
            <div>
                <Collapse
                    style={{background: "#f9f9f9", borderRadius: "10px", marginBottom: "30px"}}
                    bordered={false}
                    defaultActiveKey={['1']}
                >
                    <Panel header={`Thông tin dự án ${this.props.currentAddContract.contractId}`} key="2"
                           style={{
                               paddingLeft: "24", border: 0,
                               overflow: 'hidden'
                           }}>
                        <Table pagination={false}
                               style={{background: "#fff"}}
                               rowKey={record => record.contractId}
                               columns={contractColumns} dataSource={[this.props.currentAddContract]}/>
                    </Panel>
                </Collapse>

                <Row>
                    <Spin spinning={this.props.isFetchingPrimeFormManageReducer} size={"large"}>
                        {this.renderFormListCard()}
                    </Spin>
                </Row>
            </div>
        );
    };

    _onClickAddFormButton(formId, index) {
        this.props.addFormToContract(formId, this.props.currentAddContract.contractId, () => {
            this.props._onChangeTab("1");
            /*
                        this.props.history.push('/formManage');
            */

        });

    };
}

const mapStateToProps = (state) => {
    return {
        profile: state.authReducer.profile,
        formList: state.primeFormManageReducer.formList,
        isFetchingPrimeFormManageReducer: state.primeFormManageReducer.isFetching,
        addFormToContractStatus: state.primeAddFormToContractReducer.addFormToContractStatus
    };
};

const mapDispatchActionToProps = {
    getAllFormByPrimeId,
    addFormToContract
};

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(PrimeAddFormToContract);