import React, {Component} from 'react';
import {connect} from 'react-redux';

import {fillAllDocket, loadAllDocketByContractId} from "../../../actions/docketListByContractIdAction";

import {
    Table,
    Icon,
    Button,
    Row,
    Input,
    Col,
    message,
    Tooltip,
    Collapse,
} from 'antd';
import {DOCKETSTATUS} from "../../../constants/docketStatus";
import {Link} from "react-router-dom";

const {TextArea} = Input;
const Panel = Collapse.Panel;

class PrimeFillDocket extends Component {
    constructor(props) {
        super(props);
        this.state = {
            docketsStateForEditDocketdetail: [],
            selectedRowKeys: null, /*chức năng select trên column đầu từng row*/
            noiDungChung: ""
        };
        this.docketTemplateItem = [];
        this.selectedRowKeys = [];
    }

    getAllDocketStatus_INIT_DECLINED = (currentAddContractDocket) => {
        let currentAddContractDocketTemp = currentAddContractDocket; // biến tạm để trả toàn bộ docketDetail về rỗng để save mới
        let docketListTemp = [];
        for (let i = 0; i < currentAddContractDocketTemp.length; i++) {
            if ((currentAddContractDocketTemp[i].status === DOCKETSTATUS.INIT) || (currentAddContractDocketTemp[i].status === DOCKETSTATUS.DECLINED)) {
                currentAddContractDocketTemp[i].docketDetail = ""; // trả toàn bộ docketDetail về rỗng để save mới
                docketListTemp.push(currentAddContractDocketTemp[i]);
            }
        }
        return docketListTemp;
    };

    takeAllSelectedRowKeys = (docketTemplateItem) => {
        let docketListTemp = [];
        for (let i = 0; i < docketTemplateItem.length; i++) {
            docketListTemp.push(docketTemplateItem[i].docketId);
        }
        return docketListTemp;
    };

    renderButtonRowDocketFill = (docketId, docketDate) => {

        if (this.docketTemplateItem[0].status !== DOCKETSTATUS.INIT) {
            return null;
        }

        if ((this.props.currentAddContract.dateStart === docketDate) || (this.props.currentAddContract.dateEnd === docketDate)) {
            return (
                <Tooltip
                    title="Không thể bỏ ngày bắt đầu và ngày kết thúc dự án.">
                    <Button
                        style={{
                            width: "40%",
                            height: "70px",
                            borderRadius: "0 10px 10px 0",
                            borderLeft: "0",
                            cursor: "not-allowed",
                        }}
                        type={"danger"}
                    >
                        Bỏ
                    </Button>
                </Tooltip>
            )
        }

        let tempArr = [];
        if (this.state.selectedRowKeys !== null) {
            tempArr = this.state.selectedRowKeys;
        } else {
            tempArr = this.selectedRowKeys;
        }
        const indexOfIdInTempArr = tempArr.indexOf(docketId);

        if (tempArr.includes(docketId)) {
            return <Tooltip
                title="Bỏ ngày làm này trong dự án. Bạn có thể bỏ vài ngày bạn cảm thấy không cần thiết và không được bỏ quá 50% tổng số ngày dự án. Tuy nhiên, nhằm để dự án được liên tục và hiệu quả, bạn hãy nhắc kĩ trước khi bỏ ngày làm.">
                <Button
                    style={styles.styleDocketOnButton}
                    type={"danger"}
                    onClick={() => {
                        if (this.state.selectedRowKeys) { /*chặn trường hợp riêng status DECLINED sẽ dc quyền bỏ hết ngày*/
                            if ((this.state.selectedRowKeys.length < (this.docketTemplateItem.length / 2))) {
                                message.warning("Số ngày làm không  được nhỏ hơn 50% tổng số ngày dự án");
                            } else if ((this.state.selectedRowKeys.length < (this.docketTemplateItem.length * (2 / 3)))) {
                                message.warning("Để đảm bảo dự án được liên tục, liền mạch và hiệu quả, chúng tôi không khuyến khích bỏ quá nhiều ngày");
                            }
                        }
                        if (indexOfIdInTempArr > -1) {
                            tempArr.splice(indexOfIdInTempArr, 1);
                        }
                        this.setState({selectedRowKeys: tempArr});
                    }}>
                    Bỏ
                </Button>
            </Tooltip>
        } else {
            return <Button
                style={styles.styleDocketOffButton}
                type={"primary"}
                onClick={() => {
                    tempArr.push(docketId);
                    this.setState({selectedRowKeys: tempArr});
                }}>
                Chọn
            </Button>
        }
    };

    renderRowDocketFill_INIT = (docketTemplateItem) => {
        if (docketTemplateItem) {
            let tempArr = [];
            if (this.state.selectedRowKeys !== null) {
                tempArr = this.state.selectedRowKeys;
            } else {
                tempArr = this.selectedRowKeys;
            }

            return docketTemplateItem.map((docket, index) => {
                if ((this.props.currentAddContract.dateStart === docket.date) || (this.props.currentAddContract.dateEnd === docket.date)) {
                    return (
                        <Row
                            key={`${docket.docketId}${index}`}
                            style={styles.styleDocketOn}
                            type="flex" justify="center" align="middle"
                        >
                            <Col span={4}>
                                <div
                                    style={styles.styleDocketOnDivDate}
                                >
                                    {docket.date}
                                </div>
                            </Col>
                            <Col span={16}>
                            <TextArea
                                placeholder="Tóm tắt công việc trong ngày"
                                style={styles.styleDocketOnTextArea}

                                onChange={(event) => {
                                    let temp = this.docketTemplateItem;
                                    temp[index].docketDetail = event.target.value;
                                    this.docketTemplateItem = temp;
                                }}
                            />
                            </Col>
                            <Col span={4}
                                 style={{textAlign: "left"}}>{this.renderButtonRowDocketFill(docket.docketId, docket.date)}</Col>
                        </Row>
                    )
                }
                return (
                    <Row
                        key={`${docket.docketId}${index}`}
                        style={((tempArr.includes(docket.docketId)) ? styles.styleDocketOn : styles.styleDocketOff)}
                        type="flex" justify="center" align="middle"
                    >
                        <Col span={4}>
                            <div
                                style={((tempArr.includes(docket.docketId)) ? styles.styleDocketOnDivDate : styles.styleDocketOffDivDate)}
                            >
                                {docket.date}
                            </div>
                        </Col>
                        <Col span={16}>
                            <TextArea
                                placeholder="Tóm tắt công việc trong ngày"
                                style={((tempArr.includes(docket.docketId)) ? styles.styleDocketOnTextArea : styles.styleDocketOffTextArea)}

                                onChange={(event) => {
                                    let temp = this.docketTemplateItem;
                                    temp[index].docketDetail = event.target.value;
                                    this.docketTemplateItem = temp;
                                }}
                            />
                        </Col>
                        <Col span={4}
                             style={{textAlign: "left"}}>{this.renderButtonRowDocketFill(docket.docketId, docket.date)}</Col>
                    </Row>
                )
            })
        }
    };

    renderRowDocketFill_DECLINE = (docket) => {
        if (docket) {
            return (
                <Row
                    key={`${docket.docketId}`}
                    style={styles.styleDocketOn}
                    type="flex" justify="center" align="middle"
                >
                    <Col span={4}>
                        <div
                            style={styles.styleDocketOnDivDate}
                        >
                            {docket.date}
                        </div>
                    </Col>
                    <Col span={16}>
                            <TextArea
                                placeholder="Điền lại tóm tắt công việc"
                                style={{
                                    height: "70px",
                                    opacity: "1",
                                    transition: "0.5s",
                                    resize: "none",
                                    borderRadius: "0 10px 10px 0"
                                }}
                                onChange={(event) => {
                                    let temp = this.docketTemplateItem;
                                    temp.docketDetail = event.target.value;
                                    this.docketTemplateItem = temp;
                                }}
                            />
                    </Col>
                </Row>
            )
        }
    };

    render() {
        const contractColumns = [
            {
                title: <Tooltip title="Mã dự án. Mỗi dự án sẽ có một mã số duy nhất.">
                    Mã <Icon type="question-circle"/>
                </Tooltip>,
                dataIndex: '',
                key: 'contractId',
                render: (text, record, index) => {
                    return <Tooltip title="Mã dự án. Mỗi dự án sẽ có một mã số duy nhất.">
                        <div style={{
                            padding: "2px 5px",
                            background: "rgb(162, 162, 162)",
                            textAlign: "center",
                            color: "#fff",
                            borderRadius: " 5px",
                            fontWeight: "bold",
                            fontSize: "15px",
                        }}>{record.contractId}</div>
                    </Tooltip>;
                }
            },
            {
                title: 'Thầu phụ',
                dataIndex: '',
                key: 'subCompanyName',
                render: (text, record, index) =>
                    <Link
                        to={`/profile/${record.subAccId}`}>{record.subCompanyName}</Link>
            },
            {
                title: 'Lĩnh vực',
                dataIndex: 'profession',
                key: 'profession',
            },
            {
                title: 'Giá/ngày/người',
                dataIndex: '',
                key: 'pricePerDocket',
                render: (record) => {
                    return (
                        <span>{`${parseInt(record.pricePerDocket / 1000)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            <span
                                style={{
                                    fontWeight: "normal",
                                    margin: "0 1px",
                                }}>,</span><span
                                style={{
                                    fontWeight: "normal",

                                }}>000</span><sup
                                style={{
                                    fontWeight: "normal",
                                    fontSize: "10px",
                                    right: "-5px",
                                    color: "#ad8700",
                                }}>đ</sup></span>
                    )
                }
            },
            {
                title: 'Thời gian',
                dataIndex: '',
                key: 'Time',
                render: (text, record, index) => `${record.dateStart} - ${record.dateEnd}`
            },
            {
                title: 'Nhân công',
                dataIndex: 'workerNumber',
                key: 'workerNumber',
                render: (text, record, index) => record.workerDTOS.length
            },
            {
                title: 'Địa điểm',
                dataIndex: 'locationDetail',
                key: 'locationDetail',
            },
            {
                title: <Tooltip
                    title={"Tổng số tiền mà Thầu chính phải trả cho Thầu phụ, tính theo số công việc đã hoàn thành"}>Tổng
                    hoá đơn cả dự án <Icon type="question-circle"/></Tooltip>,
                dataIndex: "",
                key: "",
                render: (text, record, index) => {
                    if (record.totalPrice) {
                        return (
                            <span>{`${parseInt(record.totalPrice / 1000)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                <span
                                    style={{
                                        fontWeight: "normal",
                                        margin: "0 1px",
                                    }}>,</span><span
                                    style={{
                                        fontWeight: "normal",

                                    }}>000</span><sup
                                    style={{
                                        fontWeight: "normal",
                                        fontSize: "10px",
                                        right: "-5px",
                                        color: "#ad8700",
                                    }}>đ</sup></span>
                        )
                    }
                    return (
                        <span
                            style={{
                                fontSize: "11px",
                                color: "red"
                            }}
                        >Chưa có công việc nào hoàn thành</span>
                    )
                        ;
                }
            }
        ];

        if (this.props.currentDocketItem) {
            this.docketTemplateItem = {...this.props.currentDocketItem};
            this.docketTemplateItem.docketDetail = ""; // trả toàn bộ docketDetail về rỗng để save mới
            this.selectedRowKeys = this.takeAllSelectedRowKeys([this.docketTemplateItem]);
        } else {
            this.docketTemplateItem = this.getAllDocketStatus_INIT_DECLINED(this.props.allDocketFromContractId);
            this.selectedRowKeys = this.takeAllSelectedRowKeys(this.docketTemplateItem);
        }
        if (this.props.currentDocketItem) {
            return (
                <div>
                    <Collapse
                        style={{background: "#f9f9f9", borderRadius: "10px", marginBottom: "30px"}}
                        bordered={false}
                        defaultActiveKey={['1']}
                    >
                        <Panel header={`Thông tin dự án ${this.props.currentAddContract.contractId}`} key="2"
                               style={{
                                   paddingLeft: "24", border: 0,
                                   overflow: 'hidden'
                               }}>
                            <Table pagination={false}
                                   style={{background: "#fff"}}
                                   rowKey={record => record.contractId}
                                   columns={contractColumns} dataSource={[this.props.currentAddContract]}/>
                        </Panel>
                    </Collapse>
                    <div style={{
                        padding: "20px",
                        background: "#ffebba",
                        marginBottom: "20px",
                        borderLeft: "4px red solid",
                        fontSize: "15px",
                    }}>
                        <div
                            style={{
                                fontSize: "13px",
                                color: "red",
                                textTransform: "uppercase",
                            }}
                        >
                            Lý do nội dung công việc này bị Thầu phụ từ chối duyệt:
                        </div>
                        <div
                            style={{
                                padding: "5px 20px 0 20px",
                            }}
                        >
                            {this.props.currentDocketItem.declineReason}
                        </div>

                        <Collapse
                            style={{background: "none", borderRadius: "10px", marginTop: "20px"}}
                            bordered={false}
                            defaultActiveKey={['1']}
                        >
                            <Panel header={"Xem lại tóm tắt cũ"} key="2"
                                   style={{
                                       paddingLeft: "24", border: 0,
                                       overflow: 'hidden'
                                   }}>
                                {this.props.currentDocketItem.docketDetail}
                            </Panel>
                        </Collapse>
                    </div>

                    <Row>
                        <div style={{
                            width: "100%",
                            textAlign: "center",
                        }}>
                            {this.renderRowDocketFill_DECLINE(this.docketTemplateItem)}
                        </div>
                    </Row>
                    <Row
                        style={{
                            textAlign: "center"
                        }}
                    >
                        <Button
                            style={{
                                width: "25%",
                                height: "40px",
                            }}
                            type={"primary"} onClick={() => this._onClickSubmit([this.docketTemplateItem])}>Lưu</Button>

                    </Row>

                </div>
            );
        }
        return (
            <div>
                <Collapse
                    style={{background: "#f9f9f9", borderRadius: "10px", marginBottom: "30px"}}
                    bordered={false}
                    defaultActiveKey={['1']}
                >
                    <Panel header={`Thông tin dự án ${this.props.currentAddContract.contractId}`} key="2"
                           style={{
                               paddingLeft: "24", border: 0,
                               overflow: 'hidden'
                           }}>
                        <Table pagination={false}
                               style={{background: "#fff"}}
                               rowKey={record => record.contractId}
                               columns={contractColumns} dataSource={[this.props.currentAddContract]}/>
                    </Panel>
                </Collapse>

                <Row type="flex" justify="space-around" align="middle" style={{
                    marginTop: "20px",
                    left: "-35%",
                    position: "relative",
                    fontSize: "12px",
                    fontStyle: "italic",
                    bottom: "-12px",
                    zIndex: "100",
                }}><span>Nội dung chung cho dự án này</span>
                </Row>
                <Row type="flex" justify="center" align="middle">
                    <Tooltip
                        title="Nội dung này sẽ được thêm vào nội dung từng công việc trong dự án này. Nhà thầu phụ và công nhân trong dự án này đều sẽ thấy được nội dung này."><Icon
                        style={{
                            fontSize: "20px",
                            position: "relative",
                            bottom: "-30px",
                            zIndex: "100",
                            right: "-48%",
                            color: "#aaa",
                        }}
                        type="question-circle"/></Tooltip>
                    <Col span={24} style={{textAlign: "center",}}>
                        <TextArea
                            style={{
                                resize: "none",
                                width: "86%",
                                left: "-3%",
                                height: "80px",
                                marginBottom: "20px",
                                borderRadius: "10px",
                            }}
                            value={this.state.noiDungChung}
                            onChange={(event) => {
                                this.setState({noiDungChung: event.target.value})
                            }}
                        />
                    </Col>
                </Row>

                <Row style={{textAlign: "right"}}>
                    <Tooltip
                        title="Nội dung công việc theo từng ngày mà tất cả công nhân trong dự án này sẽ cùng làm. Công nhân sẽ nhìn theo nội dung này và nội dung dự án phía trên để làm việc trong ngày đó. Bạn sẽ không được chỉnh sửa sau khi điền xong. Nội dung này sau đó cần được Thầu phụ duyệt theo từng ngày trước khi tiến hành làm, bạn sẽ phải điền lại nếu nội dung ngày nào đó không được duyệt."><Icon
                        style={{
                            fontSize: "20px",
                            position: "relative",
                            bottom: "-45px",
                            zIndex: "100",
                            left: "-1%",
                            color: "#aaa",
                        }}
                        type="question-circle"/></Tooltip>
                </Row>
                <Row>
                    <div style={{
                        width: "100%",
                        textAlign: "center",
                    }}>
                        {this.renderRowDocketFill_INIT(this.docketTemplateItem)}
                    </div>
                </Row>
                <Row
                    style={{
                        textAlign: "center"
                    }}
                >
                    <Button
                        style={{
                            width: "25%",
                            height: "40px",
                        }}
                        type={"primary"} onClick={() => this._onClickSubmit(this.docketTemplateItem)}>Lưu</Button>

                </Row>

            </div>
        );

    }

    _onClickSubmit = (currentAddContractDocket) => {
        /*format data cho giống api, xoá property dư và thêm property thíu*/
        let tempArr = [];
        if (this.state.selectedRowKeys !== null) {
            tempArr = this.state.selectedRowKeys;
        } else {
            tempArr = this.selectedRowKeys;
        }

        let currentAddContractDocketTemp = currentAddContractDocket;

        if ((currentAddContractDocket[0].status === DOCKETSTATUS.INIT) && (tempArr.length < (currentAddContractDocket.length / 2))) { /*chặn trường hợp riêng status DECLINED sẽ dc quyền bỏ hết ngày*/
            message.warning("Số ngày làm không được nhỏ hơn 50% tổng số ngày dự án");
            return;
        }

        for (let i = 0; i < currentAddContractDocketTemp.length; i++) {
            currentAddContractDocketTemp[i].formData = "[]";
            if (tempArr.includes(currentAddContractDocket[i].docketId)) {
                currentAddContractDocketTemp[i].notWork = false;
            } else {
                currentAddContractDocketTemp[i].notWork = true;
            }


            if (currentAddContractDocketTemp[i].docketDetail === null) {
                currentAddContractDocketTemp[i].docketDetail = this.state.noiDungChung.replace(/^\s+|\s+$/g, '');
            } else {
                currentAddContractDocketTemp[i].docketDetail = currentAddContractDocketTemp[i].docketDetail.replace(/^\s+|\s+$/g, '');
                currentAddContractDocketTemp[i].docketDetail += ". " + this.state.noiDungChung.replace(/^\s+|\s+$/g, '');
            }
            if ((currentAddContractDocketTemp[i].notWork === false)) {
                if (currentAddContractDocketTemp[i].docketDetail.match(/[A-Za-z0-9]{2,}/i)) {
                    delete currentAddContractDocketTemp[i].date;
                    delete currentAddContractDocketTemp[i].contractId;
                    delete currentAddContractDocketTemp[i].status;
                } else {
                    message.warning("Vui lòng nhập nội dung tóm tắt có ý nghĩa");
                    return;
                }
            }
        }
        this.props.fillAllDocket(currentAddContractDocketTemp, () => {
            this.props._onChangeTab("1");
            if (this.props.currentDocketItem){
                window.location.reload();
            }
            this.props.loadAllDocketByContractId(this.props.currentAddContract.contractId);
        });
    };
}

const styles = {
    styleDocketOn: {
        height: "100px",
        transition: "0.5s",
        borderBottom: "0",
    },
    styleDocketOnTextArea: {
        height: "70px",
        opacity: "1",
        transition: "0.5s",
        resize: "none",
    },

    styleDocketOnButton: {
        width: "40%",
        height: "70px",
        borderRadius: "0 10px 10px 0",
        borderLeft: "0",
        transition: "0.5s",
    },

    styleDocketOnDivDate: {
        width: "80%",
        right: "-20%",
        position: "relative",
        background: "#f5f5f5",
        borderRadius: "10px 0 0 10px",
        border: "#d9d9d9 solid 1px",
        borderRight: "0",
        height: "70px",
        lineHeight: "65px",
        transition: "0.5s",

    },

    styleDocketOffButton: {
        width: "80%",
        left: "0%", transition: "0.5s",

    },
    styleDocketOffDivDate: {
        width: "80%",
        right: "calc(-70vw + 200px)",
        position: "relative",
        borderRadius: "0",
        border: "#ecd8d8 solid 1px",
        background: "#ecd8d8",
        height: "21px",
        lineHeight: "1.5",
        transition: "0.5s",

    },

    styleDocketOffTextArea: {
        height: "0",
        opacity: "0",
        transition: "0.5s",
        resize: "none",
    },


    styleDocketOff: {
        height: "50px",
        borderBottom: "solid 1px #fff",
        background: "#ecd8d8",
        transition: "0.5s",
    },
};


function mapStateToProps(state) {
    return {};

}

export default connect(
    mapStateToProps,
    {fillAllDocket, loadAllDocketByContractId}
)(PrimeFillDocket);