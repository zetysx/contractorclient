import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { Row, Col, Button, Card } from "antd";
import { Scrollbars } from 'react-custom-scrollbars';
import { Link } from "react-router-dom";

import { loadUserNotificationPaging, seenNotification, setLocalUnseenNoti } from "../actions/notificationAction"



export class NotificationPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            initLoading: false,
            notifications: [],
            pageIndex: 0
        };
    }

    componentDidMount() {
        this.props.loadUserNotificationPaging(this.props.profile.accountId, this.state.pageIndex);

    }

    _onClickSeenNotification = (notificationId, index) => {
        const newNotficationList = this.props.notificationReducer.notificationsPage;
        if (newNotficationList[index].seen) {
            return;
        }
        const oldLocalUnseen = this.props.notificationReducer.localUnseen;
        this.props.setLocalUnseenNoti(oldLocalUnseen - 1);
        this.props.seenNotification(notificationId);
    }

    onLoadMore = async () => {
        const pageIndex = ++this.state.pageIndex;
        await this.setState({ pageIndex: pageIndex });
        await this.setState({ notiPage: this.props.notificationReducer });
        this.props.loadUserNotificationPaging(this.props.profile.accountId, this.state.pageIndex)
    }

    render() {
        if (!this.props.profile.accountId || !this.props.notificationReducer.notificationsPage) {
            return null;
        }
        const { initLoading, loading, list, } = this.state;
        const loadMore = !initLoading && !loading ? (
            <div style={{
                textAlign: 'center', marginTop: 12, height: 32, lineHeight: '32px',
            }}
            >
                <Button onClick={this.onLoadMore}>loading more</Button>
            </div>
        ) : null;
        return (
            <Scrollbars
                autohide
            >
                <Card style={{ padding: '50px' }}>
                    {
                        this.props.notificationReducer.notificationsPage.map((notification, key) => {
                            const seen = notification.seen;
                            var timePassed;
                            // const timePassssssss = (moment().subtract('ms', notification.createdTime).format("HH") - 7) + ":" + moment().subtract('ms', notification.createdTime).format("mm") + " trước";
                            // const timePassedLessThanADay = moment().subtract('ms', notification.createdTime).format("HH[:]mm") + " trước";
                            // const timePassedInDay = moment().subtract('ms', notification.createdTime).format("DD") - 1;
                            // if (timePassedInDay > 0) {
                            //     timePassed = timePassedInDay + ' ngày ' + timePassedLessThanADay;
                            // } else {
                            //     timePassed = timePassedLessThanADay;
                            // }
                            const now = moment().format('DD/MM/YYYY HH:mm');
                            //here i try the difference
                            var ms = moment(now, "DD/MM/YYYY HH:mm").diff(moment(notification.createdTime, "DD/MM/YYYY HH:mm"));
                            var d = moment.duration(ms);
                            var timePassed = null;
                            if (Math.floor(d.asDays()) > 0) timePassed = Math.floor(d.asDays()) + " ngày trước";
                            else timePassed = Math.floor(d.asHours()) + moment.utc(ms).format(":mm") + " giờ trước";
                            const notificationId = notification.notificationId;
                            var link = null;

                            switch (notification.type) {
                                case "view autosearch":
                                    link = "/autosearch";
                                    break;
                                case "view request":
                                    link = "/request";
                                    break;
                                case "view docket":
                                    link = "/docket";
                                    break;
                                case "view contract":
                                    link = "/docket";
                                    break;
                                default:
                                    break;
                            }

                            return (
                                <Row
                                    key={key}
                                    style={{
                                        boxSizing: "border-box",
                                        padding: "0 !important",
                                        background: seen ? "#fff" : "#edf2fa" ,
                                        paddingTop: "10px",
                                        overflow: "hidden",
                                        borderBottom: '2px solid #dddfe2',
                                        //width: '350px',
                                        height: '80px'
                                    }}
                                    onClick={() => this._onClickSeenNotification(notificationId, key)}
                                >
                                    <Link to={link} key={key} style={{ padding: '0' }}>
                                        <Col
                                            span={20}
                                            style={{ fontSize: '20px', color: 'black', height: '45px', overflow: 'hidden', paddingLeft: '15px' }}
                                        >{notification.messsage}</Col>
                                        <Col span={4}
                                            style={{ fontStyle: 'italic', fontSize: '15px', alignSelf: 'flex-end', color: '#90949c' }}
                                        >
                                            {timePassed}
                                        </Col>

                                    </Link>

                                </Row>

                            );
                        })}
                    <Row style={{
                        height: '25px',
                        textAlign: 'center',
                        fontWeight: 'bolder',
                        fontSize: '20px',
                        position: 'static'
                    }}>
                        <div style={{
                            textAlign: 'center', marginTop: 12, height: 32, lineHeight: '32px',
                        }}
                        >
                            <Button onClick={this.onLoadMore}>loading more</Button>
                        </div>
                    </Row>
                </Card>


            </Scrollbars>
        )
    }
}

const mapStateToProps = (state) => ({
    profile: state.authReducer.profile,
    notificationReducer: state.notificationReducer
})

const mapDispatchToProps = {
    loadUserNotificationPaging,
    seenNotification,
    setLocalUnseenNoti
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationPage)
