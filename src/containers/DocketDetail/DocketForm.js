import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
    FORM_CONTROL_TYPE,
    FORM_STATIC_CONTROL_TYPE,
    FORM_INPUT_CONTROL_TYPE,
    A4WIDTH,
    A4HEIGHT
} from '../../constants/docketForm';
import {
    DOCKETSTATUS
} from "../../constants/docketStatus";

import { COLOR } from "../../constants/theme";

import {
    hasValue
} from "../../helpers/dataHelper";

import {
    getStyleFromFormat,
    checkIsFormEditable,
    checkIsControlEditable,
} from "../../helpers/formHelper";


import {
    changeInputDrawerVisable,
    changeInputDrawerFormControl,
    updateFormDataByDocketId
} from '../../actions/docketDetailAction';

import ControlInputDrawer from "./ControlInputDrawer";

import DocketPDF from "./DocketPDF";
import { Rnd } from 'react-rnd';
import {
    Row,
    Checkbox,
    Card,
    Button,
    Spin,
    Icon
} from 'antd';

const INPUTCONTROLBACKGROUND = '#eee';

export class DocketForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            a4PageFormScale: 1, //sử dụng để tính toán scale page A4 sao cho vừa giao diện
            isRenderPDF: false,
        };
    };

    componentDidMount() {
        //tính toán scale khi bắt được witdth của parrent sau đó render sao chưa vừa width
        this.setState({
            a4PageFormScale: this.a4PageFormCol.getBoundingClientRect().width / A4WIDTH,
            A4PageFormContainerWidth: this.a4PageFormCol.getBoundingClientRect().width,
            A4PageFormContainerHeight: A4HEIGHT * this.a4PageFormCol.getBoundingClientRect().width / A4WIDTH,
            ////styles to scale A4 Page
        }, () => {
            this.forceUpdate();
        });
    };

    componentWillUnmount() {
        this.setState({
            isRenderPDF: false
        });
    };

    renderStaticControlContent(control, index) {
        const { statictype } = control.config;
        const value = this.props.formData[index];
        const style = getStyleFromFormat(control.format);

        switch (statictype) {
            case FORM_STATIC_CONTROL_TYPE.TEXT: {
                return (
                    <div
                        key={`staticControlContent${index}`}
                        style={style}
                    >
                        {value}
                    </div>
                );
            }
            case FORM_STATIC_CONTROL_TYPE.IMAGE: {
                return (
                    <div
                        key={`staticControlContent${index}`}
                        style={style}
                    >
                        <img
                            src={value}
                            alt={value}
                            style={{ width: '100%', height: '100%' }}
                        />
                    </div>
                );
            }
            default: {
                return null;
            }
        }
    };

    //render FormControl có type là predefined
    renderPredefinedControlContent(control, index) {
        // const { predefinedtype } = control.config;
        const value = this.props.formData[index];
        const style = getStyleFromFormat(control.format);
        return (
            <div
                key={`predefinedControlContent${index}`}
                style={style}
            >
                {value}
            </div>
        );
    };

    //render FormControl có type là input
    renderInputControlContent(control, index) {
        const { inputtype, label } = control.config;
        const value = this.props.formData[index];

        const renderInputFormValue = () => {
            const style = getStyleFromFormat(control.format);

            if (!hasValue(value)) {
                return (
                    <div
                        style={style}
                    >
                        {label}
                    </div>
                );
            } else {
                switch (inputtype) {
                    case FORM_INPUT_CONTROL_TYPE.DATE:
                    case FORM_INPUT_CONTROL_TYPE.NUMBER:
                    case FORM_INPUT_CONTROL_TYPE.TEXT: {
                        return (
                            <div
                                style={style}
                            >
                                {value}
                            </div>
                        );
                    }
                    case FORM_INPUT_CONTROL_TYPE.SIGNATURE:
                    case FORM_INPUT_CONTROL_TYPE.IMAGE: {
                        return (
                            <div
                                style={style}
                            >
                                <img
                                    style={{ width: '100%', height: '100%' }}
                                    src={value}
                                    alt={value}
                                />
                            </div>
                        );
                    }
                    case FORM_INPUT_CONTROL_TYPE.CHECKBOX: {
                        return (
                            <div
                                style={style}
                            >
                                <Checkbox checked={value} />
                            </div>
                        );
                    }
                    default:
                        return null;
                }
            }
        };

        const inputFormControlStyle = {
            width: '100%',
            height: '100%',
            backgroundColor: INPUTCONTROLBACKGROUND,
        };

        const editContainer = {
            boxSizing: "border-box",
            position: "absolute",
            backgroundColor: COLOR.primaryBlue,
            right: 0,
            bottom: 0,
            width: 22,
            height: 22,
            borderTopLeftRadius: "90%",
        };

        const renderEditableIcon = () => {
            if (!checkIsControlEditable(this.props.docketStatus, this.props.profile.role, control)) {
                return null;
            }
            return (
                <div style={editContainer}>
                    <Icon
                        type="edit"
                        theme="filled"
                        style={{
                            fontSize: 14, color: "#fff",
                            position: "absolute",
                            right: 2, bottom: 3,
                        }} />
                </div>
            );
        };

        return (
            <div
                style={inputFormControlStyle}
                onClick={() => this._onClickInputControl(control, index)}
            >
                {renderInputFormValue()}
                {renderEditableIcon()}
            </div>
        );
    };

    //render nội dung của cái form control
    renderControlContent(control, index) {
        const { type } = control.config;
        switch (type) {
            case FORM_CONTROL_TYPE.STATIC: {
                return this.renderStaticControlContent(control, index);
            }
            case FORM_CONTROL_TYPE.INPUT: {
                return this.renderInputControlContent(control, index);
            }
            case FORM_CONTROL_TYPE.PREDEFINED: {
                return this.renderPredefinedControlContent(control, index);
            }
            default:
                return null;
        }
    };

    renderControlRnd() {
        const { formControl } = this.props;

        const styles = {
            formControlContainer: {
                overflow: "hidden",
                width: '100%',
                height: '100%',
                MozUserSelect: "none",
                WebkitUserSelect: "none",
                msUserSelect: "none",
                userSelect: "none",
            },
            formControlContentContainer: {
                width: '100%',
                height: '100%',
            }
        };

        return formControl.map((control, index) => {
            const { pos } = control;

            return (
                <Rnd
                    key={index}
                    size={{ width: pos.width, height: pos.height }}
                    position={{ x: pos.ox, y: pos.oy }}
                    scale={this.state.a4PageFormScale}//scale for every control in A4Paper
                    bounds="parent"
                    enableResizing={false}
                    disableDragging={true}
                >
                    <div style={styles.formControlContainer}>
                        <Row
                            type="flex" justify="center" align="middle"
                            style={styles.formControlContentContainer}
                        >
                            {this.renderControlContent(control, index)}
                        </Row>
                    </div>
                </Rnd>
            );
        });
    };

    renderA4Page() {
        const styles = {
            a4PageForm: {
                boxSizing: "border-box",
                position: "relative", //must relative for draggable work
                width: A4WIDTH,
                height: A4HEIGHT,
                backgroundColor: "#fff",
                transform: `scale(${this.state.a4PageFormScale})`,//styles to scale A4 Paper
                transformOrigin: "left top",
            },
        };

        return (
            <div className={"fontForPDF"} style={styles.a4PageForm}>
                {this.renderControlRnd()}
            </div>
        );
    };

    renderExportPDFButton() {
        if (!this.state.isRenderPDF) {
            return (
                <Button
                    block
                    onClick={() => this._onClickExportPDF()}
                    icon="file-pdf"
                    type="primary"
                >
                    Xuất tệp PDF
                </Button>
            )
        } else {
            return (
                <DocketPDF />
            );
        }
    };

    renderFormActionButton() {
        if (this.props.docketStatus !== DOCKETSTATUS.APPROVED) {
            if (checkIsFormEditable(this.props.docketStatus, this.props.profile.role)) {
                return (
                    <Button
                        onClick={() => this._onClickSaveFormDataButton()}
                        icon="save"
                        type="primary"
                    >
                        Lưu hợp đồng chấm công
                    </Button>
                );
            } else {
                return (
                    <Button
                        icon="save"
                        type="primary"
                        disabled
                    >
                        Lưu hợp đồng chấm công
                    </Button>
                );
            }
        } else {
            return (
                <Row>
                    {this.renderExportPDFButton()}
                </Row>
            );
        }
    };

    render() {
        const styles = {
            a4PageFormContainer: {
                boxSizing: "border-box",
                overflowY: "hidden",
                overflowX: "hidden",
                //for scale A4Page
                width: this.state.A4PageFormContainerWidth ? this.state.A4PageFormContainerWidth : "100%",
                height: this.state.A4PageFormContainerHeight ? this.state.A4PageFormContainerHeight : "100%",
            },
        };
        return (
            <Spin spinning={this.props.isUpdateFormFetching}>
                <Card
                    title="Hợp đồng chấm công"
                    extra={this.renderFormActionButton()}
                >
                    <div
                        style={styles.a4PageFormContainer}
                        ref={(ref) => { this.a4PageFormCol = ref }}
                    >
                        {this.renderA4Page()}
                        <ControlInputDrawer />
                    </div>
                </Card>
            </Spin>
        );
    };

    _onClickInputControl = (control, index) => {
        if (checkIsControlEditable(this.props.docketStatus, this.props.profile.role, control)) {
            const currentData = this.props.formData[index];

            this.props.changeInputDrawerFormControl(currentData, index, true);
        }
    };

    _onClickSaveFormDataButton = () => {
        const { docketId, formData } = this.props;

        const onSuccessCallback = () => {
            console.log("DADENDAY");
        }
        this.props.updateFormDataByDocketId(docketId, formData, onSuccessCallback);
    };

    _onClickExportPDF = () => {
        this.setState({
            isRenderPDF: true,
        });
    };
}

const mapStateToProps = (state) => {
    return {
        profile: state.authReducer.profile,
        formControl: state.docketDetailReducer.formControl,
        formData: state.docketDetailReducer.formData,
        docketId: state.docketDetailReducer.docketDetail.docketId,
        docketStatus: state.docketDetailReducer.docketDetail.status,
        isUpdateFormFetching: state.docketDetailReducer.isUpdateFormFetching
    };
};

const mapDispatchActionToProps = {
    changeInputDrawerVisable,
    changeInputDrawerFormControl,
    updateFormDataByDocketId
};

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(DocketForm)