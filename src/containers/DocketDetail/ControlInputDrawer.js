import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import {
    FORM_INPUT_CONTROL_TYPE,
} from '../../constants/docketForm';

import {
    DATE_FORMAT
} from '../../constants/common';

import {
    changeInputDrawerVisable,
    changeFormData,
} from '../../actions/docketDetailAction';


import ImgurUploader from '../../components/ImgurUploader';
import SignatureUploader from "../../components/SignatureUploader";
import {
    Row,
    Col,
    Drawer,
    Button,
    Form,
    Input,
    DatePicker,
    Checkbox,
} from 'antd';

export class ControlInputDrawer extends Component {
    //khi index của control thay đổi thì đổi set lại state theo props
    //onChange input không cần phải thông qua reducer gây tốn perfomances
    static getDerivedStateFromProps(props, state) {
        if (props.inputControlIndex !== state.inputControlIndex) {
            return {
                inputControlData: props.inputControlData,
                inputControlIndex: props.inputControlIndex,
            }
        }
        return null;
    };

    constructor(props) {
        super(props);
        this.state = {
            inputControlData: undefined,
            inputControlIndex: undefined,
        }
    };

    renderControlInput() {
        const { inputControlIndex } = this.props;

        if (inputControlIndex === undefined) {
            return null;
        }

        const { inputtype, role, isrequire, label } = this.props.formControl[inputControlIndex].config;
        const { inputControlData } = this.state;

        switch (inputtype) {
            case FORM_INPUT_CONTROL_TYPE.TEXT: {
                return (
                    <div>
                        <Row>
                            <Col span={24}>
                                <Form.Item label={label}>
                                    <Input.TextArea
                                        placeholder={label}
                                        value={inputControlData}
                                        autosize={{ minRows: 3, maxRows: 15 }}
                                        onChange={(event) => this._onChangeTextInput(event)}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </div>
                );
            }
            case FORM_INPUT_CONTROL_TYPE.IMAGE: {
                return (
                    <div>
                        <Row>
                            <Col span={24}>
                                <Form.Item label={label}>
                                    <ImgurUploader
                                        imageUrl={inputControlData}
                                        onUploadDone={(link) => { this._onUploadDoneImgurImage(link) }}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </div>
                );
            }
            case FORM_INPUT_CONTROL_TYPE.SIGNATURE: {
                return (
                    <div>
                        <Row>
                            <Col span={24}>
                                <Form.Item label={label}>
                                    <SignatureUploader
                                        imageUrl={inputControlData}
                                        onUploadDone={(link) => { this._onUploadDoneSignatureImage(link) }}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </div>
                );
            }
            case FORM_INPUT_CONTROL_TYPE.DATE: {
                return (
                    <div>
                        <Row>
                            <Col span={24}>
                                <Form.Item label={label}>
                                    <DatePicker
                                        defaultPickerValue={moment()}
                                        format={DATE_FORMAT}
                                        onChange={(date, dateString) => { this._onChangeDateInput(date, dateString) }}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </div>
                );
            }
            case FORM_INPUT_CONTROL_TYPE.NUMBER: {
                return (
                    <div>
                        <Row>
                            <Col span={24}>
                                <Form.Item label={label}>
                                    <Input
                                        placeholder={label}
                                        value={inputControlData}
                                        onChange={(event) => this._onChangeNumberInput(event)}
                                    />,
                                </Form.Item>
                            </Col>
                        </Row>
                    </div>
                );
            }
            case FORM_INPUT_CONTROL_TYPE.CHECKBOX: {
                return (
                    <div>
                        <Row>
                            <Col span={24}>
                                <Form.Item label={label}>
                                    <Checkbox
                                        value={inputControlIndex}
                                        onChange={(event) => this._onChangeCheckboxInput(event)}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </div>
                );
            }
            default:
                return null;
        }
    };

    //render button trong drawer để edit form
    renderControlButton() {
        return (
            <div
                style={{
                    position: 'absolute',
                    left: 0,
                    bottom: 0,
                    width: '100%',
                    borderTop: '1px solid #e9e9e9',
                    padding: '10px 16px',
                    background: '#fff',
                    textAlign: 'right',
                }}
            >
                <Row gutter={16}>
                    <Col span={24}>
                        <Button block onClick={() => this._onClickDoneButton()} type="primary">
                            Hoàn tất
                        </Button>
                    </Col>
                </Row>
            </div>
        )
    };

    //render cái Edit Drawer -> khi click vào form control nó sẽ hiện ra để edit form control
    render() {
        const { isControlInputDrawerVisable } = this.props;
        return (
            <Drawer
                title="Nhập dữ liệu"
                placement="right"
                closable={false}
                onClose={this._onCloseFormControlEditDrawer}
                visible={isControlInputDrawerVisable}
            >
                {this.renderControlInput()}
                {this.renderControlButton()}
            </Drawer>
        );
    };

    //hàm của input trong drawer
    _onChangeTextInput = (event) => {
        // this.props.changeInputDrawerFormControl(event.target.value, this.props.inputControlIndex);
        const value = event.target.value;
        this.setState({
            inputControlData: value.trim() === "" ? null : value
        });
    };

    _onChangeNumberInput = (event) => {
        try {
            const value = parseInt(event.target.value);

            if (isNaN(value)) {
                this.setState({
                    inputControlData: null
                });
                return;
            }

            if (value < 0 || value > 99999999) {
                return;
            }
            this.setState({
                inputControlData: value
            });
        } catch (error) {
            return;
        }
    };

    _onChangeDateInput = (date, dateString) => {
        this.setState({
            inputControlData: dateString.trim() === "" ? null : dateString
        });
    };

    _onChangeCheckboxInput = (event) => {
        this.setState({
            inputControlData: event.target.checked
        });
    };

    _onUploadDoneImgurImage = (link) => {
        this.setState({
            inputControlData: link.trim() === "" ? null : link
        });
    };

    _onUploadDoneSignatureImage = (link) => {
        this.setState({
            inputControlData: link.trim() === "" ? null : link
        });
    };

    //hàm của drawer
    //tắt drawer edit
    _onCloseFormControlEditDrawer = () => {
        this.props.changeInputDrawerVisable(false);
    };

    //click done button để lưu xuống formControl
    _onClickDoneButton = () => {
        const { formData, inputControlIndex } = this.props;

        const newFormData = [
            ...formData
        ];

        newFormData[inputControlIndex] = this.state.inputControlData;
        this.props.changeFormData(newFormData, false);
    };
}

const mapStateToProps = (state) => {
    return {
        formControl: state.docketDetailReducer.formControl,
        formData: state.docketDetailReducer.formData,

        isControlInputDrawerVisable: state.docketDetailReducer.isControlInputDrawerVisable,
        inputControlIndex: state.docketDetailReducer.inputControlIndex,
        inputControlData: state.docketDetailReducer.inputControlData
    };
};

const mapDispatchActionToProps = {
    changeInputDrawerVisable,
    changeFormData,
};

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(ControlInputDrawer)
