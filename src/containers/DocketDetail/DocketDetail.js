import React, {Component} from 'react';
import {connect} from 'react-redux';

import {
    getDocketDetailById,
    declineDocket,
    confirmDocket,
    approveDocket,
    rejectDocket,
    changeToNeedApproveDocket,
    changeToFormFilledDocket,
    returnDocketBySub
} from "../../actions/docketDetailAction";
import {loadAllDocketByContractId} from "../../actions/docketListByContractIdAction";
import {loadAllContractByPrimeId} from "../../actions/primeOnGoingContractAction";
import {loadAllSubContract} from "../../actions/subOnGoingContractAction";

import DocketForm from "./DocketForm";

import {DOCKETSTATUS} from "../../constants/docketStatus";
import {COLOR} from "../../constants/theme";
import {
    Skeleton,
    Collapse,
    Row,
    Table,
    Tooltip,
    Col,
    Icon,
    Button,
    Steps,
    Modal,
    Input,
    message,
} from 'antd';
import {Link} from "react-router-dom";
import { GoogleMapComponent } from '../../components/GoogleMap';

const {TextArea} = Input;
const Step = Steps.Step;
const Panel = Collapse.Panel;

export class DocketDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            reason: ""
        };
    }

    componentDidMount() {
        const {docketId} = this.props.match.params;
        this.props.getDocketDetailById(docketId);
        window.scrollTo(0, 0);
    }

    renderContractInfo() {
        const {docketDetail} = this.props;
        const contractColumns = [
            {
                title: 'Thầu phụ',
                dataIndex: 'subCompanyName',
                key: 'subCompanyName',
                render: (text, record, index) => <span>{record.predefinedField.subContcCompanyname}</span>
            }, {
                title: 'Thầu chính',
                dataIndex: 'primeContcCompanyname',
                key: 'primeContcCompanyname',
                render: (text, record, index) => <span>{record.predefinedField.primeContcCompanyname}</span>
            },
            {
                title: 'Lĩnh vực',
                dataIndex: 'profession',
                key: 'profession',
                render: (text, record, index) => <span>{record.predefinedField.contractProfessionname}</span>
            },
            {
                title: 'Giá/ngày/người',
                dataIndex: '',
                key: 'pricePerDocket',
                render: (record) => {
                    return (
                        <span>{`${parseInt(record.predefinedField.contractPricePerDocket / 1000)}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            <span
                                style={{
                                    fontWeight: "normal",
                                    margin: "0 1px",
                                }}>,</span><span
                                style={{
                                    fontWeight: "normal",

                                }}>000</span><sup
                                style={{
                                    fontWeight: "normal",
                                    fontSize: "10px",
                                    right: "-5px",
                                    color: "#ad8700",
                                }}>đ</sup></span>
                    )
                }

            },
            {
                title: 'Thời gian',
                dataIndex: '',
                key: 'Time',
                render: (text, record, index) => `${record.dateStart} - ${record.dateEnd}`
            },
            {
                title: 'Nhân công',
                dataIndex: 'workerNumber',
                key: 'workerNumber',
                render: (text, record, index) => record.predefinedField.contractWorkerNumber //workerNumber
            },
            {
                title: 'Địa điểm',
                dataIndex: 'locationDetail',
                key: 'locationDetail',
                render: (text, record, index) => record.predefinedField.contractLocationDetail //locationdetail

            }
        ];
        return (
            <Panel header={`Thông tin dự án ${docketDetail.contractId}`} key="2"
                   style={{
                       paddingLeft: "24", border: 0,
                       overflow: 'hidden'
                   }}>

                <Table pagination={false}
                       style={{background: "none"}}
                       rowKey={record => record.contractId}
                       columns={contractColumns} dataSource={[docketDetail]}/>
            </Panel>
        );
    };

    renderMap=()=>{
        return(
            <Panel header={`Bản đồ`} key="3"
            style={{
                paddingLeft: "24", border: 0,
                overflow: 'hidden'
            }}>
                <GoogleMapComponent locationDetail = {this.props.docketDetail.predefinedField.contractLocationDetail}/>
            </Panel>
        );
    };

    renderPrimeSubInfo() {
        const {docketDetail} = this.props;
        if (this.props.profile.role === 1) {
            return (
                <Panel header={`Thông tin Nhà thầu phụ ${docketDetail.predefinedField.subContcCompanyname}`} key="1"
                       style={{
                           paddingLeft: "24", border: 0,
                           overflow: 'hidden'
                       }}>
                    <Row style={{padding: "0px 50px",}}>
                        <Row style={{padding: "5px 0"}}>
                            <Col span={4} style={{fontWeight: "bold",}}>
                                Địa chỉ
                            </Col>
                            <Col span={1}>
                                :
                            </Col>
                            <Col span={18}>
                                {docketDetail.predefinedField.subContcAddress}
                            </Col>
                        </Row>
                        <Row style={{padding: "5px 0"}}>
                            <Col span={4} style={{fontWeight: "bold",}}>
                                Tên người đại diện
                            </Col>
                            <Col span={1}>
                                :
                            </Col>
                            <Col span={18}>
                                {docketDetail.predefinedField.subContcFullname}
                            </Col>
                        </Row>
                        <Row style={{padding: "5px 0"}}>
                            <Col span={4} style={{fontWeight: "bold",}}>
                                Số điện thoại:
                            </Col>
                            <Col span={1}>
                                :
                            </Col>
                            <Col span={18}>
                                {docketDetail.predefinedField.subContcPhone}
                            </Col>
                        </Row>

                    </Row>
                </Panel>
            );
        } else {
            return (
                <Panel header={`Thông tin Nhà thầu chính ${docketDetail.predefinedField.primeContcCompanyname}`} key="1"
                       style={{
                           paddingLeft: "24", border: 0,
                           overflow: 'hidden'
                       }}>
                    <Row style={{padding: "0px 50px",}}>
                        <Row style={{padding: "5px 0"}}>
                            <Col span={4} style={{fontWeight: "bold",}}>
                                Mã số kinh doanh:
                            </Col>
                            <Col span={1}>
                                :
                            </Col>
                            <Col span={18}>
                                {docketDetail.predefinedField.primeContcBusinessLicense}
                            </Col>
                        </Row>
                        <Row style={{padding: "5px 0"}}>
                            <Col span={4} style={{fontWeight: "bold",}}>
                                Địa chỉ
                            </Col>
                            <Col span={1}>
                                :
                            </Col>
                            <Col span={18}>
                                {docketDetail.predefinedField.primeContcAddress}
                            </Col>
                        </Row>
                        <Row style={{padding: "5px 0"}}>
                            <Col span={4} style={{fontWeight: "bold",}}>
                                Tên người đại diện
                            </Col>
                            <Col span={1}>
                                :
                            </Col>
                            <Col span={18}>
                                {docketDetail.predefinedField.primeContcFullname}
                            </Col>
                        </Row>
                        <Row style={{padding: "5px 0"}}>
                            <Col span={4} style={{fontWeight: "bold",}}>
                                Số điện thoại:
                            </Col>
                            <Col span={1}>
                                :
                            </Col>
                            <Col span={18}>
                                {docketDetail.predefinedField.primeContcPhone}
                            </Col>
                        </Row>

                    </Row>
                </Panel>
            );
        }
    };


    renderGuidelineByStatus = (status, role) => {
        if ((status === DOCKETSTATUS.INIT) && (role === 1)) {
            return (
                <span>Hiện chưa có công việc cho dự án này<br/>Mời bạn chọn ngày làm việc ở 'Danh sách dự án'</span>
            )
        } else if ((status === DOCKETSTATUS.DECLINED) && (role === 1)) {
            return (
                <span>Nội dung công việc này không được Thầu phụ duyệt<br/>Mời bạn chọn lại ở 'Danh sách dự án'</span>
            )
        } else if ((status === DOCKETSTATUS.INIT) && (role === 2)) {
            return (
                <span>Hiện chưa có công việc cho dự án này<br/>Đang chờ Thầu chính chọn công việc</span>
            )
        } else if ((status === DOCKETSTATUS.DECLINED) && (role === 2)) {
            return (
                <span>Bạn đã từ chối duyệt nội dung công việc này<br/>Đang chờ Thầu chính chỉnh sửa lại công việc này</span>
            )
        } else if ((status === DOCKETSTATUS.WILL_WORK) && (role === 1)) {
            return (
                <span>Đang chờ bạn điền nội dung công việc<br/> ở Hợp đồng chấm công phía dưới <br/>Sau đó, bạn sẽ gửi qua cho Thầu phụ duyệt</span>
            )
        } else if ((status === DOCKETSTATUS.WILL_WORK) && (role === 2)) {
            return (
                <span>Đang chờ Thầu chính điền nội dung công việc<br/> ở Hợp đồng chấm công phía dưới<br/>Sau đó, Thầu chính sẽ gửi qua cho bạn duyệt.</span>
            )
        } else if ((status === DOCKETSTATUS.FILLED) && (role === 1)) {
            return (
                <span>Đang chờ Thầu phụ duyệt nội dung công việc</span>
            )
        } else if ((status === DOCKETSTATUS.FILLED) && (role === 2)) {
            return (
                <span>Mời bạn duyệt nội dung công việc phía dưới</span>
            )
        } else if (status === DOCKETSTATUS.CONFIRMED) {
            return (
                <span>Công việc đã được duyệt và chờ đến lúc làm</span>
            )
        } else if (status === DOCKETSTATUS.WORKING) {
            return (
                <span>CÔNG VIỆC ĐANG ĐƯỢC CÔNG NHÂN THỰC HIỆN</span>
            )
        } else if ((status === DOCKETSTATUS.REJECTED) && (role === 1)) {
            return (
                <span>Thầu phụ đã xác nhận công việc này hoàn thành<br/> nhưng bạn từ chối xác nhận<br/>Công việc đã trả về cho Công nhân.</span>
            )
        } else if ((status === DOCKETSTATUS.REJECTED) && (role === 2)) {
            return (
                <span>Bạn đã xác nhận công việc này hoàn thành<br/>  nhưng Thầu chính từ chối xác nhận<br/>Công việc đã trả về cho Công nhân.</span>
            )
        } else if ((status === DOCKETSTATUS.SUB_CHECK) && (role === 1)) {
            return (
                <span>Công việc đã được Công nhân làm xong và đang chờ Thầu phụ kiểm tra và xác nhận</span>
            )
        } else if ((status === DOCKETSTATUS.SUB_CHECK) && (role === 2)) {
            return (
                <span>Công việc đã được Công nhân làm xong<br/>Mời bạn kiểm tra và xác nhận</span>
            )
        } else if ((status === DOCKETSTATUS.NEED_APPROVE) && (role === 1)) {
            return (
                <span>Bạn đã xác nhận công việc này hoàn thành<br/>Đang chờ Thầu chính kiểm tra và xác nhận</span>
            )
        } else if ((status === DOCKETSTATUS.NEED_APPROVE) && (role === 2)) {
            return (
                <span>Thầu phụ đã xác nhận công việc này hoàn thành<br/>Mời bạn kiểm tra lại và xác nhận</span>
            )
        } else if (status === DOCKETSTATUS.APPROVED) {
            return (
                <span>Công việc đã hoàn thành!</span>
            )
        }
    };

    renderButtonByStatus = (status, docketId, role) => {
        const styleDiv = {
            margin: "-20px -20px -20px 0",
        };
        const styleDoubleButton1 = {
            width: "45%",
            height: "50px",
            borderRadius: "12px",
        };
        const styleDoubleButton2 = {
            width: "45%",
            height: "50px",
            borderRadius: "12px",
            margin: "0 5%",
            fontSize: "12px",
        };

        const styleSingleButton = {
            width: "80%",
            height: "50px",
            borderRadius: "12px",
        };

        if ((status === DOCKETSTATUS.WILL_WORK) && (role === 1)) {
            return (
                <Button
                    style={styleSingleButton}
                    onClick={() => {
                        this._onClickChangeToFilledDocket(docketId);
                    }}>Lưu & gửi Thầu phụ</Button>
            )
        } else if ((status === DOCKETSTATUS.FILLED) && (role === 2)) {
            return (
                <div style={styleDiv}>
                    <Button
                        style={styleDoubleButton1}
                        onClick={() => {
                            this._onClickConfirmDocket(docketId);
                        }}
                    >Duyệt</Button>
                    <Button
                        style={styleDoubleButton2}
                        type={"danger"}
                        onClick={() => {
                            this._onClickDeclineDocket(docketId);
                        }}
                    >Từ chối duyệt</Button>

                    <Modal
                        title="Lý do từ chối duyệt?"
                        visible={this.state.visible}
                        onOk={() => this._onClickCancelReasonModal(docketId)}
                        onCancel={this._onClickCancelModal}
                        okText={"Từ chối duyệt"}
                        cancelText={"Huỷ"}
                    >
                        <div style={{
                            marginBottom: "10px",
                            color: "#000",
                        }}>
                            Bạn vui lòng điền lý do mình từ chối duyệt nội dung công việc này:
                        </div>
                        <div
                        >
                            <TextArea rows={4}
                                      style={{
                                          borderRadius: "10px"
                                      }}
                                      value={this.state.reason}
                                      onChange={(e) => {
                                          this.setState({
                                              reason: e.target.value
                                          })
                                      }}
                            />
                        </div>
                    </Modal>
                </div>
            )
        } else if ((status === DOCKETSTATUS.SUB_CHECK) && (role === 2)) {
            return (
                <div style={styleDiv}>
                    <Button
                        style={styleDoubleButton1}
                        onClick={() => {
                            this._onClickChangeToNeedApproveDocket(docketId);
                        }}
                    >Xác nhận</Button>
                    <Button
                style={styleDoubleButton2}
                type={"danger"}
                onClick={() => {
                    this._onClickReturnDocket(docketId);
                }}
            >Từ chối</Button>
            <Modal
            title="Lý do từ chối xác nhận?"
            visible={this.state.visible}
            onOk={() => this._onClickOKReasonSubModal(docketId)}
            onCancel={this._onClickCancelModal}
            okText={"Từ chối xác nhận"}
            cancelText={"Huỷ"}
                >
                <div style={{
                marginBottom: "10px",
                    color: "#000",
            }}>
            Bạn vui lòng điền lý do mình từ chối xác nhận công việc này:
                </div>
        <div
            >
            <TextArea rows={4}
            style={{
                borderRadius: "10px"
            }}
            value={this.state.reason}
            onChange={(e) => {
                this.setState({
                    reason: e.target.value
                })
            }}
            />
        </div>
        </Modal>
        </div>
            )
        } else if ((status === DOCKETSTATUS.NEED_APPROVE) && (role === 1)) {
            return (
                <div style={styleDiv}>
                    <Button
                        style={styleDoubleButton1}
                        onClick={() => {
                            this._onClickApproveDocket(docketId);
                        }}
                    >Xác nhận</Button>
                    <Button
                        style={styleDoubleButton2}
                        type={"danger"}
                        onClick={() => {
                            this._onClickRejectDocket(docketId);
                        }}
                    >Từ chối</Button>
                    <Modal
                        title="Lý do từ chối xác nhận?"
                        visible={this.state.visible}
                        onOk={() => this._onClickOKReasonModal(docketId)}
                        onCancel={this._onClickCancelModal}
                        okText={"Từ chối xác nhận"}
                        cancelText={"Huỷ"}
                    >
                        <div style={{
                            marginBottom: "10px",
                            color: "#000",
                        }}>
                            Bạn vui lòng điền lý do mình từ chối xác nhận công việc này:
                        </div>
                        <div
                        >
                            <TextArea rows={4}
                                      style={{
                                          borderRadius: "10px"
                                      }}
                                      value={this.state.reason}
                                      onChange={(e) => {
                                          this.setState({
                                              reason: e.target.value
                                          })
                                      }}
                            />
                        </div>
                    </Modal>
                </div>
            )
        }
    };

    renderDocketForm() {
        //const pdfFileName = "Docket ngày "+this.props.predefinedField.docketDate + " của contract #" +this.props.docketDetail.contractId;
        return (
            <DocketForm/>
        )
    };

    renderDocketInfo = () => {
        const {docketDetail} = this.props;
        let currentStep = 0;
        let mainColor = COLOR.primaryBlue;
        if (docketDetail.status === DOCKETSTATUS.FILLED) {
            currentStep = 1;
        } else if (docketDetail.status === DOCKETSTATUS.CONFIRMED) {
            currentStep = 2;
            mainColor = "rgb(0, 179, 105)";
        } else if ((docketDetail.status === DOCKETSTATUS.WORKING) || (docketDetail.status === DOCKETSTATUS.REJECTED)) {
            currentStep = 3;
            mainColor = "rgb(230, 164, 44)";
        } else if (docketDetail.status === DOCKETSTATUS.SUB_CHECK) {
            currentStep = 4;
        } else if (docketDetail.status === DOCKETSTATUS.NEED_APPROVE) {
            currentStep = 5;
        } else if (docketDetail.status === DOCKETSTATUS.APPROVED) {
            currentStep = 6;
            mainColor = "rgb(0, 179, 105)";
        }

        // style for dot in Steps of ant
        const iconDotStyle = {
            fontSize: "25px",
            position: "relative",
            top: "-16px",
            left: "-12px",
            background: "#fff",
            padding: "5px",
            color: COLOR.primaryBlue
        };
        const dotStyleByStatus = (statusIndex) => {
            if (statusIndex === 0) {
                return (
                    <Icon type="calendar"
                          style={iconDotStyle}
                    />
                )
            } else if (statusIndex === 1) {
                return (
                    <Icon type="edit" style={iconDotStyle}/>
                )
            } else if (statusIndex === 2) {
                return (
                    <Icon type="file-done"
                          style={{
                              fontSize: "25px",
                              position: "relative",
                              top: "-16px",
                              left: "-12px",
                              background: "#fff",
                              padding: "5px",
                              color: "green"
                          }}
                    />
                )
            } else if (statusIndex === 3) {
                return (
                    <Icon type="setting" spin
                          style={{
                              fontSize: "25px",
                              position: "relative",
                              top: "-16px",
                              left: "-12px",
                              background: "#fff",
                              padding: "5px",
                              color: "#d48900"
                          }}
                    />
                )
            } else if (statusIndex === 4) {
                return (
                    <Icon type="eye" style={iconDotStyle}/>
                )
            } else if (statusIndex === 5) {
                return (
                    <Icon type="eye" style={iconDotStyle}/>
                )
            } else if (statusIndex === 6) {
                return (
                    <Icon type="check-circle"
                          style={{
                              fontSize: "25px",
                              position: "relative",
                              top: "-16px",
                              left: "-12px",
                              background: "#fff",
                              padding: "5px",
                              color: "green"
                          }}
                    />
                )
            }
        };
        const customDot = (dot, {status, index}) => (
            <div>
                {dotStyleByStatus(index)}
            </div>
        );
        return (
            <div
                style={{
                    background: mainColor,
                    padding: "20px",
                    color: "#fff",
                    borderRadius: "10px",
                    fontSize: "15px"
                }}
            >
                <Row type="flex" justify="space-between" align="middle"
                >
                    <Col span={4}>
                        <Icon type="bars"/> <span
                        onClick={() => {
                            if (this.props.profile.role === 1) {
                                this.props.loadAllContractByPrimeId(this.props.profile.userId);
                            } else if (this.props.profile.role === 2) {
                                this.props.loadAllSubContract(this.props.profile.userId);
                            }
                            this.props.loadAllDocketByContractId(this.props.docketDetail.contractId);
                        }}>
                        <Link to={"/docket"}
                              style={{
                                  color: "#fff",
                                  textDecoration: "underline",
                                  cursor: "pointer",
                              }}>Danh sách dự án</Link>
                    </span>
                        <span style={{paddingLeft: "10px"}}><Icon type="caret-right"/></span>
                    </Col>
                    <Col span={3}>
                        <Tooltip
                            title="Mã dự án. Mỗi dự án sẽ có một mã số duy nhất.">
                            <span>Dự án {this.props.docketDetail.contractId}</span></Tooltip>
                        <span style={{paddingLeft: "10px"}}><Icon type="caret-right"/></span>
                    </Col>
                    <Col span={7}>
                            <span style={{
                                background: "rgba(173, 173, 173,0.5)",
                                padding: "5px 10px",
                                borderRadius: "10px",
                            }}>Công việc ngày {this.props.docketDetail.predefinedField.docketDate}</span>
                    </Col>
                    <Col span={10}/>
                </Row>
                <Row type="flex" justify="space-between" align="middle" style={{marginTop: "15px"}}>
                    <Col span={7}/>
                    <Col span={12} style={{
                        paddingLeft: "10px",
                    }}>
                        <Col span={4} style={{
                            fontSize: "14px",
                            color: "rgb(255, 255, 255)",
                        }}>Trạng thái:</Col>
                        <Col span={20} style={{
                            fontSize: "14px",
                            paddingLeft: "10px",
                            display: "inline",
                            fontStyle: "italic"
                        }}>{this.renderGuidelineByStatus(this.props.docketDetail.status, this.props.profile.role)}</Col>
                    </Col>
                    <Col span={5} style={{textAlign: "right"}}>
                        {this.renderButtonByStatus(this.props.docketDetail.status, this.props.docketDetail.docketId, this.props.profile.role)}
                    </Col>
                </Row>
                <Row type="flex" justify="space-between" align="middle" style={{
                    background: "#fff",
                    margin: "25px -20px -21px -20px",
                    borderRadius: "0px 0 10px 10px",
                    padding: "25px 0 30px 0",
                    color: mainColor,
                }}>
                    <Row
                        style={{
                            margin: "0 0 20px 70px",
                            fontStyle: "italic",
                            width: "100%",
                        }}>Tiến độ:</Row>
                    <Steps size="small" progressDot={customDot} current={currentStep}
                           style={{width: "100%", color: "#fff"}}>
                        <Step description={"Thầu chính đã chọn ngày làm việc"}/>
                        <Step description={"Thầu chính đã điền nội dung công việc"}/>
                        <Step description={"Thầu phụ đã duyệt công việc"}/>
                        <Step description={"Công nhân đang làm"}/>
                        <Step description={"Chờ Thầu phụ kiểm tra"}/>
                        <Step description={"Chờ thầu chính xác nhận"}/>
                        <Step description={"Xong!"}/>
                    </Steps>
                    <Row
                        style={{
                            width: "100%",
                            marginTop: "30px",
                            padding: "0 70px 0 70px",
                        }}
                    >
                        <div
                            style={{
                                fontStyle: "italic",
                            }}>
                            Tóm tắt:
                        </div>
                        <div
                            span={19}
                            style={{
                                color: "rgba(0, 0, 0, 0.65)",
                                padding: "5px 0 0 15px",
                                fontSize: "14px"
                            }}>{this.props.docketDetail.docketDetail}</div>
                    </Row>
                </Row>
            </div>)
    };

    renderWarning = () => {
        if (this.props.docketDetail.status === 9) {
            return (
                <div style={{
                    padding: "20px",
                    background: "#ffebba",
                    marginBottom: "20px",
                    borderLeft: "4px red solid",
                    fontSize: "15px",
                }}>
                    <div
                        style={{
                            fontSize: "13px",
                            color: "red",
                            textTransform: "uppercase",
                        }}
                    >
                        Công việc không được Thầu chính xác nhận với lý do:
                    </div>
                    <div
                        style={{
                            padding: "5px 20px 0 20px",
                        }}
                    >
                        {this.props.docketDetail.rejectReason}
                    </div>
                </div>
            )
        }
        if (this.props.docketDetail.status === 4) {
            return (
                <div style={{
                    padding: "20px",
                    background: "#ffebba",
                    marginBottom: "20px",
                    borderLeft: "4px red solid",
                    fontSize: "15px",
                }}>
                    <div
                        style={{
                            fontSize: "13px",
                            color: "red",
                            textTransform: "uppercase",
                        }}
                    >
                        Nội dung công việc không được Thầu phụ duyệt với lý do:
                    </div>
                    <div
                        style={{
                            padding: "5px 20px 0 20px",
                        }}
                    >
                        {this.props.docketDetail.declineReason}
                    </div>
                </div>
            )
        }
        if ((this.props.docketDetail.status === 5)&&(this.props.docketDetail.rejectReason)) {
            return (
                <div style={{
                    padding: "20px",
                    background: "#ffebba",
                    marginBottom: "20px",
                    borderLeft: "4px red solid",
                    fontSize: "15px",
                }}>
                    <div
                        style={{
                            fontSize: "13px",
                            color: "red",
                            textTransform: "uppercase",
                        }}
                    >
                        Nội dung công việc không được Thầu phụ duyệt với lý do:
                    </div>
                    <div
                        style={{
                            padding: "5px 20px 0 20px",
                        }}
                    >
                        {this.props.docketDetail.rejectReason}
                    </div>
                </div>
            )
        }

        return null;
    };

    render() {
        const {docketDetail} = this.props;
        if (!docketDetail) {
            return (
                <Skeleton/>
            );
        }
        if ((this.props.docketDetail.status) && (this.props.docketDetail.status === DOCKETSTATUS.NOT_WORK)) {
            return (
                <div>Công việc này không tồn tại</div>
            )
        }
        return (
            <div style={{padding: "50px 50px"}}>
                <div>
                    {this.renderWarning()}
                    {this.renderDocketInfo()}

                    <div style={{margin: "20px"}}>
                        <Collapse
                            style={{background: "#f9f9f9", borderRadius: "10px"}}
                            bordered={false}
                            defaultActiveKey={['1']}
                        >
                            {this.renderContractInfo()}
                            {this.renderPrimeSubInfo()}
                            {this.renderMap()}
                        </Collapse>

                    </div>

                    <div style={{margin: "20px"}}>
                        {this.renderDocketForm()}
                    </div>

                </div>
                <div style={{marginTop: 20}}>
                </div>

            </div>
        );
    };

    _onChangeMapCollapse = async () => {
        console.log(this.props.docketDetail.predefinedField.contractLocationDetail);
        try {
            
        } catch (error) {
            
        };
    }

    _onClickShowModal = () => { // for Modal reason
        this.setState({
            visible: true,
        });
    };

    _onClickOKReasonModal = (docketId) => {// for Modal reason
        if (this.state.reason.length < 5) {
            message.warning("Vui lòng nhập lý do rõ ràng hơn!");
            return null;
        }

        const onSuccessConfirm = () => {
            this.props.getDocketDetailById(docketId);
            this.setState({
                visible: false,
            });
        };
        this.props.rejectDocket(docketId, this.state.reason, onSuccessConfirm);
    };

    _onClickOKReasonSubModal = (docketId) => {// for Modal reason
        if (this.state.reason.length < 5) {
            message.warning("Vui lòng nhập lý do rõ ràng hơn!");
            return null;
        }

        const onSuccessConfirm = () => {
            this.props.getDocketDetailById(docketId);
            this.setState({
                visible: false,
            });
        };
        this.props.returnDocketBySub(docketId, this.state.reason, onSuccessConfirm);
    };

    _onClickCancelReasonModal = (docketId) => {// for Modal reason
        if (this.state.reason.length < 5) {
            message.warning("Vui lòng nhập lý do rõ ràng hơn!");
            return null;
        }

        const onSuccessConfirm = () => {
            this.props.getDocketDetailById(docketId);
        };
        this.props.declineDocket(docketId, this.state.reason, onSuccessConfirm);
    };

    _onClickCancelModal = (e) => {// for Modal reason
        this.setState({
            visible: false,
        });
    };

    _onClickConfirmDocket = (docketId) => {
        const onSuccessConfirm = () => {
            this.props.getDocketDetailById(docketId);
        };
        this.props.confirmDocket(docketId, onSuccessConfirm);
    };

    _onClickReturnDocket= (docketId) => {
        this._onClickShowModal(); // modal điền reason
    };

    _onClickDeclineDocket = (docketId) => {
        this._onClickShowModal(); // modal điền reason
    };

    _onClickApproveDocket = (docketId) => {
        const onSuccessConfirm = () => {
            this.props.getDocketDetailById(docketId);
            this.props.getDocketDetailById(docketId);
        };
        this.props.approveDocket(docketId, onSuccessConfirm);
    };

    _onClickRejectDocket = (docketId) => {
        this._onClickShowModal(); // modal điền reason
    };

    _onClickChangeToNeedApproveDocket = (docketId) => {
        const onSuccessConfirm = () => {
            this.props.getDocketDetailById(docketId);
        };
        this.props.changeToNeedApproveDocket(docketId, onSuccessConfirm);
    };

    _onClickChangeToFilledDocket = (docketId) => {
        const onSuccessConfirm = () => {
            this.props.getDocketDetailById(docketId);
        };
        this.props.changeToFormFilledDocket(docketId, onSuccessConfirm);
    };

}

const mapStateToProps = (state) => {
    return {
        profile: state.authReducer.profile,
        docketDetail: state.docketDetailReducer.docketDetail,
    };
};

const mapDispatchActionToProps = {
    getDocketDetailById,
    declineDocket,
    confirmDocket,
    approveDocket,
    rejectDocket,
    changeToNeedApproveDocket,
    changeToFormFilledDocket,
    loadAllContractByPrimeId,
    loadAllSubContract,
    loadAllDocketByContractId,
    returnDocketBySub
};

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(DocketDetail)