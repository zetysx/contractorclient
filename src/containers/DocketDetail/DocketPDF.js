import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
    FORM_CONTROL_TYPE,
    FORM_STATIC_CONTROL_TYPE,
    FORM_INPUT_CONTROL_TYPE,
    A4WIDTH,
    A4HEIGHT,
    DEFAULT_SCALE_PDF,
} from '../../constants/docketForm';

import { COLOR } from "../../constants/theme";

import {
    getViewStyleFromFormatForPDF,
    getTextStyleFromFormatForPDF,
} from "../../helpers/formHelper";

import {
    hasValue
} from "../../helpers/dataHelper";

import {
    PDFViewer,
    Page,
    Text,
    View,
    Document,
    StyleSheet,
    Font,
    Image,
    PDFDownloadLink
} from '@react-pdf/renderer';
import {
    Button
} from 'antd';

Font.register(
    `https://kendo.cdn.telerik.com/2017.2.621/styles/fonts/DejaVu/DejaVuSans.ttf`,
    { family: 'pdfFont' },
);

Font.register(
    `https://kendo.cdn.telerik.com/2017.2.621/styles/fonts/DejaVu/DejaVuSans-Bold.ttf`,
    { family: 'pdfFont-bold', fontWeight: 'bold' },
);
Font.register(
    `https://kendo.cdn.telerik.com/2017.2.621/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf`,
    { family: 'pdfFont-italic', fontStyle: 'italic' },
);
Font.register(
    `https://kendo.cdn.telerik.com/2017.2.621/styles/fonts/DejaVu/DejaVuSans-Oblique.ttf`,
    { family: 'pdfFont-bold-italic', fontWeight: 'bold', fontStyle: 'italic' },
);

export class DocketPDF extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ready: false
        };
    };

    componentDidMount() {
        //tạm thời khắc phục thư viện PDFViewer, khi state thay đổi, nó render tiếp dẫn đến bị lỗi
        //chỗ này là bug của libary, rất dễ có bug xảy ra
        this.setState({ ready: false });
        setTimeout(() => {
            this.setState({ ready: true });
        }, 0.5);
    };

    renderStaticControlContent(control, index) {
        const { statictype } = control.config;
        const controlValue = this.props.formData[index];

        const controlStyle = StyleSheet.create({
            viewStyle: getViewStyleFromFormatForPDF(control.format),
            textStyle: getTextStyleFromFormatForPDF(control.format)
        });

        switch (statictype) {
            case FORM_STATIC_CONTROL_TYPE.TEXT: {
                return (
                    <View
                        wrap={false}
                        style={controlStyle.viewStyle}
                        key={`staticControlContent${index}`}
                    >
                        <Text
                            style={controlStyle.textStyle}
                        >
                            {controlValue}
                        </Text>
                    </View>
                );
            }
            case FORM_STATIC_CONTROL_TYPE.IMAGE: {
                return (
                    <View
                        wrap={false}
                        style={controlStyle.viewStyle}
                        key={`staticControlContent${index}`}
                    >
                        <Image
                            style={{ width: '100%', height: '100%' }}
                            source={{ uri: controlValue }}
                        />
                    </View>
                );
            }
            default: {
                return null;
            }
        };
    };

    renderPredefinedControlContent(control, index) {
        const controlValue = this.props.formData[index];
        const controlStyle = StyleSheet.create({
            viewStyle: getViewStyleFromFormatForPDF(control.format),
            textStyle: getTextStyleFromFormatForPDF(control.format)
        });
        return (
            <View
                wrap={false}
                style={controlStyle.viewStyle}
                key={`predefinedControlContent${index}`}
            >
                <Text
                    style={controlStyle.textStyle}
                >
                    {controlValue}
                </Text>
            </View>
        );
    };

    renderInputControlContent(control, index) {
        const controlValue = this.props.formData[index];

        const { inputtype } = control.config;

        const controlStyle = StyleSheet.create({
            viewStyle: getViewStyleFromFormatForPDF(control.format),
            textStyle: getTextStyleFromFormatForPDF(control.format),
        });

        //không có value thì không cần render
        if (!hasValue(controlValue)) {
            return null;
        };

        //có value thì mới render value
        switch (inputtype) {
            case FORM_INPUT_CONTROL_TYPE.DATE:
            case FORM_INPUT_CONTROL_TYPE.NUMBER:
            case FORM_INPUT_CONTROL_TYPE.TEXT: {
                return (
                    <View
                        wrap={false}
                        key={`inputControlContent${index}`}
                        style={controlStyle.viewStyle}
                    >
                        <Text style={controlStyle.textStyle}>
                            {controlValue}
                        </Text>
                    </View>
                );
            }
            case FORM_INPUT_CONTROL_TYPE.SIGNATURE:
            case FORM_INPUT_CONTROL_TYPE.IMAGE: {
                return (
                    <View
                        wrap={false}
                        key={`inputControlContent${index}`}
                        style={controlStyle.viewStyle}
                    >
                        <Image
                            style={{ width: '100%', height: '100%' }}
                            source={{ uri: controlValue }}
                        />
                    </View>
                );
            }
            case FORM_INPUT_CONTROL_TYPE.CHECKBOX: {
                return null;
                // return (
                //     <View
                //         key={`inputControlContent${index}`}
                //         style={controlStyle.viewStyle}
                //     >
                //         {/* <ListItem> */}
                //         <CheckBox checked={controlValue} />
                //         {/* </ListItem> */}
                //     </View>
                // );
            }
        };
    };

    renderControlContent(control, index) {
        const { type } = control.config;
        switch (type) {
            case FORM_CONTROL_TYPE.STATIC: {
                return this.renderStaticControlContent(control, index);
            }
            case FORM_CONTROL_TYPE.INPUT: {
                return this.renderInputControlContent(control, index);
            }
            case FORM_CONTROL_TYPE.PREDEFINED: {
                return this.renderPredefinedControlContent(control, index);
            }
            default: {
                return null;
            }
        };
    };

    renderControl() {
        const { formControl } = this.props;

        return formControl.map((control, index) => {

            const { pos } = control;

            const styles = StyleSheet.create({
                controlContainer: {
                    left: pos.ox,
                    top: pos.oy,
                    width: pos.width,
                    height: pos.height,
                    position: 'absolute',
                    // backgroundColor: 'red',
                },
            });

            return (
                <View wrap={false} style={styles.controlContainer} key={`ccontain${index}`}>
                    {this.renderControlContent(control, index)}
                </View>
            );
        });
    };

    renderPDFDocument() {
        const { docketDetail } = this.props;
        return (
            <Document
                title={`docket-${docketDetail.docketId}-${docketDetail.predefinedField.docketDate}`}
                author={`${docketDetail.predefinedField.primeContcFullname}`}
                subject={`Docket Form Information`}
                keywords={"Docket, Contractor Portal, Payment"}
                creator={`${docketDetail.predefinedField.primeContcFullname}`}
                producer={"Contractor Portal"}
            >
                <Page size="A4" wrap={false}>
                    <View wrap={false}
                        style={{
                            width: A4WIDTH,
                            height: A4HEIGHT,
                            transform: `scaleX(${DEFAULT_SCALE_PDF}) scaleY(${DEFAULT_SCALE_PDF})`,
                            transformOrigin: "0 0",
                            position: 'relative',
                            overflow: 'hidden',
                        }}
                    >
                        {this.renderControl()}
                    </View>
                </Page>
            </Document >
        );
    };

    renderPDFViewer() {
        if (!this.state.ready) {
            return null;
        }
        const { docketDetail } = this.props;

        return (
            <div>
                <PDFDownloadLink
                    document={this.renderPDFDocument()}
                    fileName={`docket-${docketDetail.docketId}-${docketDetail.predefinedField.docketDate}`}
                >
                    {({ blob, url, loading, error }) => {
                        if (loading) {
                            return (
                                <Button block loading type="primary">
                                    Đang xử lý
                                </Button>
                            );
                        } else {
                            return (
                                <Button block icon="download" type="primary">
                                    Lưu tệp PDF
                                </Button>
                            );
                        }
                    }}
                </PDFDownloadLink>
            </div>
        );

        //test thử render khung PDF
        // return (
        //     <PDFViewer
        //         style={{ width: "100%", height: 1000 }}
        //     >
        //         {this.renderPDFDocument()}
        //     </PDFViewer>
        // );
    };

    render() {
        return (
            <div>
                {this.renderPDFViewer()}
            </div>
        );
    };
}

const mapStateToProps = (state) => {
    return {
        formControl: state.docketDetailReducer.formControl,
        formData: state.docketDetailReducer.formData,
        docketDetail: state.docketDetailReducer.docketDetail,
    };
};

const mapDispatchActionToProps = {
};

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(DocketPDF)