import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Table, message, Col, Row, Popconfirm } from 'antd';
import { TABLE_LIST_CONFIG } from '../constants/requestContractConstants';
import {
    loadAllRequestsPrimeSentWaitting,
    deleteRequestNotConfirmPrime
} from '../actions/primeRequestAction';
import {Link} from "react-router-dom";
import moment from 'moment';

class PrimeRequest extends Component {

    constructor(props) {
        super(props);
        this.tableContractHeaders = [
            {
                title: TABLE_LIST_CONFIG.CONTRACT.HEADER,
                dataIndex: TABLE_LIST_CONFIG.CONTRACT.DATAINDEX_KEY,
                key: TABLE_LIST_CONFIG.CONTRACT.DATAINDEX_KEY
            }, {
                title: TABLE_LIST_CONFIG.SUBCONTRACTOR.HEADER,
                dataIndex: '',
                key: '',
                render: (text, record, index) =>
                            <Link
                                to={`/profile/${record.subAccountId}`}>{record.subCompanyName}</Link>
            }, {
                title: TABLE_LIST_CONFIG.REQUESTED_DATE.HEADER,
                dataIndex: '',
                key: '',
                render: (text, record, index) => {
                    return `${moment(record.requestDate).format('DD/MM/YYYY')}`;
                }
            }, {
                title: TABLE_LIST_CONFIG.STARTDATE_ENDDATE.HEADER,
                dataIndex: '',
                key: '',
                render: (text, record, index) => {
                    return <div style={{width: 160}}>
                        <Col span={24} style={{marginTop: 10, textAlign: "center"}}>{moment(record.startDate).format('DD/MM/YYYY')} - {moment(record.endDate).format('DD/MM/YYYY')}</Col>
                        <Col span={24} style={{textAlign: "center", height: 20}}>
                            {this.renderLeftDate(moment(), moment(record.startDate))} 
                        </Col>
                    </div>;
                }
            }, {
                title: TABLE_LIST_CONFIG.LOCATION.HEADER,
                dataIndex: TABLE_LIST_CONFIG.LOCATION.DATAINDEX_KEY,
                key: TABLE_LIST_CONFIG.LOCATION.DATAINDEX_KEY,
            }, {
                title: TABLE_LIST_CONFIG.WORKER_NUMBER.HEADER,
                dataIndex: TABLE_LIST_CONFIG.WORKER_NUMBER.DATAINDEX_KEY,
                key: TABLE_LIST_CONFIG.WORKER_NUMBER.DATAINDEX_KEY,
            }, {
                title: TABLE_LIST_CONFIG.PRICE_PER_DOCKET.HEADER,
                dataIndex: TABLE_LIST_CONFIG.PRICE_PER_DOCKET.DATAINDEX_KEY,
                key: TABLE_LIST_CONFIG.PRICE_PER_DOCKET.DATAINDEX_KEY,
                render: (text, record, index) => { return <div>
                        <span>{`${record.pricePerDocket}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</span>
                        <sup
                            style={{
                                fontWeight: "normal",
                                right: "-5px",
                                color: "#ad8700",
                            }}>đ</sup>
                    </div>}
            }, {
                title: TABLE_LIST_CONFIG.PROFESSION.HEADER,
                dataIndex: TABLE_LIST_CONFIG.PROFESSION.DATAINDEX_KEY,
                key: TABLE_LIST_CONFIG.PROFESSION.DATAINDEX_KEY,
            }, {
                title: '',
                dataIndex: '',
                key: '',
                render: (text, record, index) => {
                    const onSuccessDeleteRequestCallback = () => {
                        message.success("Xóa lời mời thành công");
                        this.props.loadAllRequestsPrimeSentWaitting(this.props.authReducer.profile.userId);
                    };

                    const onFailDeleteRequestCallback = () => {
                        message.success("Xóa lời mời thành công");
                        this.props.loadAllRequestsPrimeSentWaitting(this.props.authReducer.profile.userId);
                    };
                    return (
                        <Popconfirm title="Bạn có muốn xóa lời mời này?" 
                            onConfirm={() => {
                                this.props.deleteRequestNotConfirmPrime(this.props.authReducer.profile.userId, record.id, 
                                                                        onSuccessDeleteRequestCallback, 
                                                                        onFailDeleteRequestCallback)}} 
                            onCancel={null}
                            okText="Đồng ý" 
                            cancelText="Hủy"
                        >
                            <Button type="danger">Xóa lời mời</Button>
                        </Popconfirm>    
                    )
                }
            }
        
        ];
    }

    renderLeftDate = (startDate, endDate) => {
    
        let color = null;
        let leftDate = (moment(endDate)).diff(startDate, 'days');
        if(leftDate > 10) {
            color = "blue";
        } else if (leftDate < 10 && leftDate > 3) {
            color = "orange";
        } else if (leftDate < 3 && leftDate > 0) {
            color = "red"
        } 

        return (
            <div style={{display: "inline-block"}}>
                <Row>
                    <Col span={24}>
                        {color != null ? (
                                <p style={{color: `${color}`}}>Còn lại {leftDate} ngày</p>
                            ) : (
                                <p style={{color: "grey"}}>Quá hạn đăng kí lời mời</p>
                            )
                        }
                        </Col>
                </Row>
            </div>
        )
    }

    
    

    componentDidMount() {
        this.props.loadAllRequestsPrimeSentWaitting(this.props.authReducer.profile.userId);
    }


    render() {
        return (
            <div style={{padding: 50, border: 1}}>
                <Table
                    pagination={true}
                    columns={this.tableContractHeaders}
                    dataSource={this.props.notConfirmRequests}
                    rowKey={record => record.id}
                />
            </div>
        );
    }
}



function mapStateToProps(state) {
    return {
        authReducer: state.authReducer,
        notConfirmRequests: state.primeRequestReducer.requestsPrimeSentWaitting,
        requestDeletedStatus: state.primeRequestReducer.requestDeletedStatus
    };
}


const mapDispatchActionToProps = {
    loadAllRequestsPrimeSentWaitting,
    deleteRequestNotConfirmPrime
}

export default connect(
    mapStateToProps,
    mapDispatchActionToProps
)(PrimeRequest);